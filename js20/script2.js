$(document).ready(function() {

    var num = 0;
    var num2 = 0;
    var arr = [];
  
    $("img.getter").on('click', function(event) {
        // alert($(this).attr('data-val'));
        var count = 1;
        var count2 = 1;
        var item_id = $(this).attr('id');
        var src = $(this).attr('src');
        var url = ($(this).attr('data-val') == 'paid') ? 'expandData.php' : 'expandData-unpaid.php';
        if (src == 'img/icon-add1-small.png') {
            return;
        } 
        $.each(arr, function(x, y) {
            if (y == item_id) {
                count2++;
            }
        }); 


        if (count2 % 2 == 0) {
            $(this).attr('src', 'img/icon-add-small.png' );
        } else {
            $(this).attr('src', 'img/icon-minus-small.png' );
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: {'id' : item_id},
            beforeSend: function() {
            },
            success: function(data) {
                $.each(arr, function(x, y) {
                    if (y == item_id) {
                        count++;
                    }
                }); 

                if (count % 2 == 0) {
                    var contentToRemove = document.querySelectorAll("#item_" + item_id);
                    $(contentToRemove).remove();
                    arr.push(item_id);
                } else {
                    $(data).insertAfter('#'+item_id);
                    arr.push(item_id);
                }
                
              $('[data-toggle="tooltip"]').tooltip(); 
	      $('.circle a').click(function(){
                $(this).parent().find('i').removeClass('active');
                $(this).children().addClass('active');
                return false;
              });
            }

        });
    });


    // datatables

    $('#tbl1').dataTable( {
        "pageLength": 25,
        "ordering": false,
        "bFilter": false ,
        "bLengthChange": false, 
    });

   $('#asd').dataTable({
        "order":[],
        "oLanguage": {
   	 "sSearch": "Search for Third Party: "
 	}
    });

    // $('#asd').dataTable( {
    //     "pageLength": 25,
    //     "ordering": false,
    //     "bFilter": false ,
    //     "bLengthChange": false, 
    //     "ajax": {
    //          "url": "tableData.php?category=country",
    //          "dataSrc": "items"
    //      },
    //      "columns": [
    //          { "data": "company" },
    //          { "data": "email" },
    //          { "data": "country" },
    //          { "data": "risk_review" },
    //          { "data": "date_created" }
    //      ] ,
    //      "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
    //         $('td:eq(0)', nRow).html('<a href="#">' +
    //             aData['company'] + '</a>');
    //         return nRow;
    //     },
    // });

    $('#tb-atp.tableData').click(function(){
        $('#asd').dataTable().fnDestroy();
          $('#asd').dataTable( {
        "pageLength": 25,
        "ordering": false,
        "bFilter": false ,
        "bLengthChange": false,
        "ajax": {
             "url": "tableData.php?category=company",
             "dataSrc": "items"
         },
         "columns": [
             { "data": "company" },
             { "data": "email" },
             { "data": "country" },
             { "data": "risk_review" },
             { "data": "date_created" }
         ],
         "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            $('td:eq(0)', nRow).html('<a href="#">' +
                aData['company'] + '</a>');
            return nRow;
        },
       });   
     });

    $('#tb-ttp.tableData').click(function(){
         $('#asd').dataTable().fnDestroy();
          $('#asd').dataTable( {
            "pageLength": 25,
            "ordering": false,
            "bFilter": false ,
            "bLengthChange": false,
         "ajax": {
             "url": "tableData.php?category=company",
             "dataSrc": "items"
         },
         "columns": [
             { "data": "company" },
             { "data": "email" },
             { "data": "country" },
             { "data": "risk_review" },
             { "data": "date_created" }
         ],
         "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            $('td:eq(0)', nRow).html('<a href="#">' +
                aData['company'] + '</a>');
            return nRow;
        },
        });
    });

    $('#tb-bc.tableData').click(function(){
         $('#asd').dataTable().fnDestroy();
          $('#asd').dataTable( {
            "pageLength": 25,
            "ordering": false,
            "bFilter": false ,
            "bLengthChange": false,
         "ajax": {
             "url": "tableData.php?category=country",
             "dataSrc": "items"
         },
         "columns": [
             { "data": "company" },
             { "data": "email" },
             { "data": "country" },
             { "data": "risk_review" },
             { "data": "date_created" }
         ],
         "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            $('td:eq(0)', nRow).html('<a href="#">' +
                aData['company'] + '</a>');
            return nRow;
        },
        });
    });

    $('#tb-bg.tableData').click(function(){
         $('#asd').dataTable().fnDestroy();
          $('#asd').dataTable( {
            "pageLength": 25,
            "ordering": false,
            "bFilter": false ,
            "bLengthChange": false,
         "ajax": {
             "url": "tableData.php?category=company",
             "dataSrc": "items"
         },
         "columns": [
             { "data": "company" },
             { "data": "email" },
             { "data": "country" },
             { "data": "risk_review" },
             { "data": "date_created" }
         ],
         "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            $('td:eq(0)', nRow).html('<a href="#">' +
                aData['company'] + '</a>');
            return nRow;
        },
        });
    });

    $('#tb-brl.tableData').click(function(){
         $('#asd').dataTable().fnDestroy();
          $('#asd').dataTable( {
            "pageLength": 25,
            "ordering": false,
            "bFilter": false ,
            "bLengthChange": false,
         "ajax": {
             "url": "tableData.php?category=status",
             "dataSrc": "items"
         },
         "columns": [
             { "data": "company" },
             { "data": "email" },
             { "data": "country" },
             { "data": "risk_review" },
             { "data": "date_created" }
         ],
         "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            $('td:eq(0)', nRow).html('<a href="#">' +
                aData['company'] + '</a>');
            return nRow;
        },
        });
    });
    
// chart

$("#pie-chart").chart({

 data : new Array(),
 labels : new Array("A", "B", "C", "D", "E",'s'),
 width : 400,
 height : 200

}); 

    

});