function inputFocus(i){
    if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
}
function inputBlur(i){
    if(i.value==""){ i.value=i.defaultValue; i.style.color="#888"; }
}

$(".toggler").click(function(e){
    // you handle the event here
});

// Create the tooltips only when document ready
$(document).ready(function () {
    // Change the first select to the document if you want to 
    // detect addition of elements accross the whole page!
    $('.csv-uploader').on('mouseenter', 'a[title]', function (event) {
        $(this).qtip({
            overwrite: false, // Don't overwrite tooltips already bound
            show: {
                event: event.type, // Use the same event type as above
                ready: true // Show immediately - important!
            }
        });
    });
	  
		//TP SIDE MENU
		//All Third Parties
		$('#tb-atp').click(function(){
				$('#tb-atp').attr('src', 'img/sidemenu-red-allthirdparties.png');
				$('#tb-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#tb-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#tb-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#tb-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//All Third Parties
		$('#tb-ttp').click(function(){
				$('#tb-ttp').attr('src', 'img/sidemenu-red-totalthirdparties.png');
				$('#tb-atp').attr('src', 'img/sidemenu-grn-allthirdparties.png');
				$('#tb-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#tb-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#tb-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//By Country
		$('#tb-bc').click(function(){
				$('#tb-bc').attr('src', 'img/sidemenu-red-bycountry.png');
				$('#tb-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#tb-atp').attr('src', 'img/sidemenu-grn-allthirdparties.png');
				$('#tb-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#tb-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//By Group
		$('#tb-bg').click(function(){
				$('#tb-bg').attr('src', 'img/sidemenu-red-bygroup.png');
				$('#tb-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#tb-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#tb-atp').attr('src', 'img/sidemenu-grn-allthirdparties.png');
				$('#tb-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//By Risk Level
		$('#tb-brl').click(function(){
				$('#tb-brl').attr('src', 'img/sidemenu-red-byrisklevel.png');
				$('#tb-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#tb-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#tb-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#tb-atp').attr('src', 'img/sidemenu-grn-allthirdparties.png');
    });
		
		//PIE SIDE MENU
		//Total Third Parties
		$('#pie-ttp').click(function(){
				$('#pie-ttp').attr('src', 'img/sidemenu-red-totalthirdparties.png');
				$('#pie-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#pie-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#pie-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//By Country
		$('#pie-bc').click(function(){
				$('#pie-bc').attr('src', 'img/sidemenu-red-bycountry.png');
				$('#pie-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#pie-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#pie-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//By Group
		$('#pie-bg').click(function(){
				$('#pie-bg').attr('src', 'img/sidemenu-red-bygroup.png');
				$('#pie-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#pie-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
				$('#pie-brl').attr('src', 'img/sidemenu-grn-byrisklevel.png');
    });
		//Total Third Parties
		$('#pie-brl').click(function(){
				$('#pie-brl').attr('src', 'img/sidemenu-red-byrisklevel.png');
				$('#pie-bc').attr('src', 'img/sidemenu-grn-bycountry.png');
				$('#pie-bg').attr('src', 'img/sidemenu-grn-bygroup.png');
				$('#pie-ttp').attr('src', 'img/sidemenu-grn-totalthirdparties.png');
    });
    
    //tooltip
    $('[data-toggle="tooltip"]').tooltip(); 
		
});