<?php
  include('header20.php');
  @include_once('include20/iddLiteFunc/countryName.php');
  @include_once('include20/iddLiteFunc/notes.php');
  @include_once('include20/iddLiteFunc/num2words.php');
  require_once 'include20/ocHelper20.php';
  $och = new ocHelper();
  $och->mode = 'strict';
  
  $nsrch_id = $_REQUEST['srch_id'];
  $cnt_sanc = $cnt_enf = $cnt_sanc_asso = $cnt_enf_asso = 0;
  $country_arr = array();
  $sql_sel = "SELECT * FROM srch_tpi st, search20_tpis sts WHERE sts.id=st.srch20_id AND st.id=$nsrch_id";
  echo $sql_sel;
  $res_sel = mysql_query($sql_sel,$con1);
  $row_sel = mysql_fetch_assoc($res_sel);
  $comp_id = $row_sel['comp_id'];
  $tpi_id = $row_sel['tpi_id'];
  $srch_id = $row_sel['srch20_id'];
  $entity_type = $row_sel['entity_type'];
  $countryos = $row_sel['country'];
  $country = "";
  echo $row_sel['srch_txt'];
  $srch_txts = htmlspecialchars(trim($row_sel['srch_txt']), ENT_QUOTES);
  $srch_txt = trim($row_sel['srch_txt']);
  
  /*$sql_sel = "SELECT * FROM search20_assos WHERE srch_id=$srch_id";
  //echo $sql_ins;
  $res_sel = mysql_query($sql_sel,$con1);
  while($row_sel = mysql_fetch_assoc($res_sel))
  { 
	$fullname[] = trim($row_sel['asso_name']);
  }*/
  
  if($entity_type=='company')
  {
  	$sql_getOCInfo = "SELECT `name`
					, company_number
					, current_status
					, jurisdiction_code
					, incorporation_date
					, dissolution_date
					, company_type
					, created_at
					, updated_at
					, alternative_names
					, previous_names
					, registered_address
					, registry_url
					, source_publisher
					, source_url
					, source_date
					, agent_name
					, agent_address
					, officers
					, filings
					FROM oc_orginfo
					WHERE srch_id = '{$srch_id}'";
  }
  elseif($entity_type=='individual')
  {
	  $sql_getOCInfo = "SELECT fullname, `position`, current_status, occupation, address, nationality 
	  FROM oc_offinfo WHERE srch_id='{$srch_id}'";
  }
	$res_getOCInfo = mysql_query($sql_getOCInfo,$con1);
	$row_getOCInfo = mysql_fetch_assoc($res_getOCInfo);
	  
  if($entity_type == 'company') $och->category = 'organization';
  if($entity_type == 'individual') $och->category = 'individual';
  
  $och->subject = urlencode(trim($_REQUEST['search']));
  $och->jcode = trim($_REQUEST['country']);
  $ocResult = $och->srchSubject(30,1,false);
	
	if($srch_txt != '')
	{
	 	$types = 1;
	 	$res_chk_all = srap_call($srch_txt,$types);
		$cnt_arr = count($res_chk_all);
		if(is_array($res_chk_all))
		{ 
			for($aa=1;$aa<=$cnt_arr;$aa++)
			{
				foreach($res_chk_all[$aa] as $row_chk_all)
				{
					$tp=1;
					$rec_id = (int)$row_chk_all['id'];
					
					if(check_rec($tpi_id,$comp_id,$aa,$rec_id,$nsrch_id,$tp))
					{
						$is_alrted = 1;
						$final_risk = $risk_cate = "";
						$risk_cate = $row_chk_all['risk_category'];
						$country_arr[] = trim($row_chk_all['country_name']);
						$final_risk = explode(",",$risk_cate);
						if(in_array("Sanctions",$final_risk))
						{
							$cnt_sanc++; 
						}
						if(in_array("Enforcements",$final_risk))
						{
							$cnt_enf++; 
						}					
					}
				}
			}
		}
		
		$country_arr = array_unique($country_arr);
		$tcountries = count($country_arr);
	}
?>
<style>
	.table-inner {
		border-collapse: collapse;
		border-spacing: 0px;
		width: 100%;
		height: 100%;
		margin: 0px;
		padding: 0px;
	}
	.table-inner tr:first-child td {
		background:#FFF !important;
		color:#000 !important;
		font-size: 14px !important;
		font-weight: normal !important;
		vertical-align: middle !important;
		text-align:left !important;
	}
	.table-inner tr:first-child:hover td {
		background:#FFF !important;
		color:#000 !important;
		font-size: 14px !important;
		font-weight: normal !important;
		vertical-align: middle !important;
		text-align:left !important;
	}
	.td-word-wraps {
		  overflow-wrap: break-word;
		  word-wrap: break-word;
		  -ms-word-break: break-all;
		  word-break: break-all;
		  -ms-hyphens: auto;
		  -moz-hyphens: auto;
		  -webkit-hyphens: auto;
		  hyphens: auto;
		}
</style>
<div class="int-row-1 edd-row-1">
  <div class="container">
    <div class="text-center">
      <p>&nbsp;</p>
    </div>
    
    <div class="col-xs-12">
      <p class="dd4-label td-word-wraps"><strong>Review Results & Download Report</strong></p>
      <p class="dd4-label td-word-wraps" style="width:1000px;">Name Searched: <b><?php echo ucwords($srch_txt); ?></b><?php
	  if(($row_getOCInfo['fullname']=="" && $row_getOCInfo['name']!="") || $countryos!='')
	  {
	  ?>
      <br />
	  Jurisdiction: <?php if($row_getOCInfo['jurisdiction_code']!=""){ echo getCountryName($row_getOCInfo['jurisdiction_code']); }elseif($countryos!=''){ echo $countryos; } ?>
      <?php
	  }
	  ?>
      </p>
      <div class="form-03">
        <table class="table-02">
          <thead>
           <tr>
            <?php
			if($entity_type=='company')
			{
			?>
            	<td style="width:250px;">Subject Name</td>
            	<td style="width:150px;">Country</td>
            	<td style="width:150px;"><a href="sanctions_enforcement" target="_blank" style="text-decoration:none; color:#FFF;">Instant Report Results</a></td>
                <td style="width:150px;">Download & Save Instant Due Diligence Report</td>
                <td style="width:60px;">Order<br />IDD+</td>
                <td style="width:60px;">Order<br />EDD</td>
			<?php
			}
			elseif($entity_type=='individual')
			{
			?>
            	<td style="width:350px;">Subject Name</td>
            	<td style="width:200px;"><a href="sanctions_enforcement" target="_blank" style="text-decoration:none; color:#FFF;">Instant Report Results</a></td>
                <td style="width:150px;">Download & Save Instant Due Diligence Report</td>
                <td style="width:60px;">Order<br />IDD+</td>
                <td style="width:60px;">Order<br />EDD</td>
            <?php	
			}
			?>
            </tr>
          </thead>
          <tr>
            <td class="td-word-wraps">
            	<?php if($is_alrted==1 || $row_getOCInfo['name']!="" || $row_getOCInfo['fullname']!=""){ ?><img src="img20/icon-add-small.png" class="img-plus" id="plus1" onClick="get_alerts('include1','plus1');"/><?php } echo ucwords($srch_txt); ?>
            </td>
            <?php
			if($entity_type=='company')
			{
			?>
            	<td style="text-align:center !important;"><?php if($tcountries>1){ echo "Multiple"; }elseif($row_getOCInfo['jurisdiction_code']!=""){  echo getCountryName($row_getOCInfo['jurisdiction_code']); }elseif($country_arr[0]!=''){ echo $country_arr[0]; }elseif($countryos!=''){ echo $countryos; } ?></td>
            <?php
			}
			?>
            <td style="text-align:center !important;">
            	<div style="float:left;"><a href="sanctions_enforcement" target="_blank"><img src="img20/SE.png" width="30" style="margin-left:30px;" /></a></div>
				<div style="float:left; margin-left:20px; margin-top:5px;"><?php if($cnt_enf>0 || $cnt_sanc>0){ echo "<span style='color:#FF0000;'>Risk Alert </span>"; }else{ echo "No Risk Alert"; } ?></div>
            </td>
            <td style="text-align:center !important;">
            	<div style="float:left; margin-left:20px;">
            		<a data-toggle="tooltip" title="Save and Download" href="#" onClick='printRpt(<?php echo $nsrch_id; ?>,<?php echo $_SESSION['company_id']; ?>);' data-placement="bottom"><img src="img20/Download_icon.png" width="30" /></a>
            	</div>
                <!--<div style="float:right; margin-right:20px;">
            		<a data-toggle="tooltip" title="Save" href="#" onClick='save_alerts(<?php echo $srch_id; ?>);' data-placement="bottom"><img src="img20/Floppy_icon.png" width="30" /></a>
            	</div>-->
            </td>
            <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;">
            	<img src="img20/IDD_icon.png" width="30" />
            </td>
            <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;">
            	<a data-toggle="tooltip" title="Order EDD Report" href="<?php echo APP_URL."enhanced_without"; ?>" data-placement="bottom"><img src="img20/EDD_icon.png" width="30" /></a>
            </td>
          </tr>
          <?php
		  	if($entity_type=='company')
			{
		  		if($res_getOCInfo!='' && $row_getOCInfo['name']!="")
				{
					?>    
                    <tr id="include1" style="display:none;">
                        <td colspan="6" class="indent">
                            <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['name']); ?></span><br />
                            <?php if($row_getOCInfo['company_number']!=""){ ?><b>Company Number: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['company_number']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['current_status']!=""){ ?><b>Status: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['current_status']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['company_type']!=""){ ?><b>Company Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['company_type']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['registered_address']!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['registered_address']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['jurisdiction_code']!=""){ ?><b>Jurisdiction Code: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo getCountryName($row_getOCInfo['jurisdiction_code']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['incorporation_date']!=""){ ?><b>Incorporation Date: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['incorporation_date']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['dissolution_date']!=""){ ?><b>Dissolution Date: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['dissolution_date']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['registry_url']!=""){ ?><b>Registry URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_getOCInfo['registry_url']; ?>" target="_blank"><?php echo $row_getOCInfo['registry_url']; ?></a></span><br /><?php } ?>
                            <?php if($row_getOCInfo['previous_names']!=""){ ?><b>Previous Names: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['previous_names']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['filings']!=""){ ?><b>Filings: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['filings']); ?></span><br /><?php } ?>
                        </td>
                    </tr>
					<?php
				}
			}
			elseif($entity_type=='individual')
			{
				if($res_getOCInfo!='' && $row_getOCInfo['fullname']!="")
				{
					?>    
                    <tr id="include1" style="display:none;">
                        <td colspan="5" class="indent">
                            <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['fullname']); ?></span><br />
                            <?php if($row_getOCInfo['position']!=""){ ?><b>Position: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo ucwords($row_getOCInfo['position']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['current_status']!=""){ ?><b>Status: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['current_status']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['occupation']!=""){ ?><b>Occupation: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo ucwords($row_getOCInfo['occupation']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['address']!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['address']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['nationality']!=""){ ?><b>Nationality: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['nationality']; ?></span><br /><?php } ?>
                        </td>
                    </tr>
					<?php
				}
			}
			if($is_alrted==1)
			{
				for($aa=1;$aa<=$cnt_arr;$aa++)
				{
					foreach($res_chk_all[$aa] as $row_chk_all)
					{
						$tp=1;
						$rec_id = $row_chk_all['id'];
						if(check_rec($tpi_id,$comp_id,$aa,$rec_id,$nsrch_id,$tp))
						{
							if($entity_type=='company')
							{
							?>    
								<tr id="include1" style="display:none;">
									<td colspan="6" class="indent">
										<?php if(trim($row_chk_all['source_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['source_name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['url_site'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_chk_all['url_site']; ?>" target="_blank"><?php echo $row_chk_all['url_site']; ?></a></span><br /><?php } ?>
										<b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Entity_Name']; ?></span><br />
										<?php if(trim($row_chk_all['Entity_Type'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Entity_Type']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Alt_Name'])!=""){ ?><b>Alt Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Alt_Name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Alert'])!=""){ ?><b>Alert: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Alert']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Event'])!=""){ ?><b>Event: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Event']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Description'])!=""){ ?><b>Description: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Description']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Address'])!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Address']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['city_state_zip'])!=""){ ?><b>City/State/Zip: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['city_state_zip']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['country_name'])!=""){ ?><b>Country: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['country_name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['risk_category'])!=""){ ?><b>Risk Category: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['risk_category']; ?></span><br /><?php } ?>
									</td>
									<!--<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
											<select disabled>
											<option>Possible match</option>
											<option>Not a match</option>
											<option>Positive match</option>
										  </select>
									</td>
									<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
									  <div class="circle" style="opacity: 0.6; float:left;">
										<a data-toggle="tooltip" title="No findings / Risk has been dealt with." data-placement="top"><i class="green"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding that is of interest, however is not related directly to a corruption risk." data-placement="top"><i class="amber"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding on the name related to curruption." data-placement="top"><i class="red"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="This is a color only assigned by the client which indicates that they have made a decision they are not going to continue business with the third party." data-placement="top"><i class="black"><span class="glyphicon glyphicon-record"></span></i></a>
									  </div>
										<div style="float:left; margin-left:20px;">
											<input type="text" value="Free Text" disabled></input>
										</div>
									</td>-->
								</tr>
							<?php
							}
							elseif($entity_type=='individual')
							{
							?>
								<tr id="include1" style="display:none;">
									<td colspan="5" class="indent">
										<?php if(trim($row_chk_all['source_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['source_name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['url_site'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_chk_all['url_site']; ?>" target="_blank"><?php echo $row_chk_all['url_site']; ?></a></span><br /><?php } ?>
										<b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Entity_Name']; ?></span><br />
										<?php if(trim($row_chk_all['Entity_Type'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Entity_Type']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Alt_Name'])!=""){ ?><b>Alt Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Alt_Name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Alert'])!=""){ ?><b>Alert: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Alert']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Event'])!=""){ ?><b>Event: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Event']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Description'])!=""){ ?><b>Description: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Description']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['Address'])!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['Address']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['city_state_zip'])!=""){ ?><b>City/State/Zip: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['city_state_zip']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['country_name'])!=""){ ?><b>Country: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['country_name']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all['risk_category'])!=""){ ?><b>Risk Category: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all['risk_category']; ?></span><br /><?php } ?>
									</td>
									<!--<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
											<select disabled>
											<option>Possible match</option>
											<option>Not a match</option>
											<option>Positive match</option>
										  </select>
									</td>
									<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
									  <div class="circle" style="opacity: 0.6; float:left;">
										<a data-toggle="tooltip" title="No findings / Risk has been dealt with." data-placement="top"><i class="green"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding that is of interest, however is not related directly to a corruption risk." data-placement="top"><i class="amber"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding on the name related to curruption." data-placement="top"><i class="red"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="This is a color only assigned by the client which indicates that they have made a decision they are not going to continue business with the third party." data-placement="top"><i class="black"><span class="glyphicon glyphicon-record"></span></i></a>
									  </div>
										<div style="float:left; margin-left:20px;">
											<input type="text" value="Free Text" disabled></input>
										</div>
									</td>-->
								</tr>
							<?php
							}
						}
					}
				}
			}
		  	$jk = $ij = 0;
			$sql_sel1 = "SELECT * FROM srch_tpi_asso WHERE srch_id=$nsrch_id AND tpi_id=$tpi_id AND comp_id=$comp_id";
			$res_sel1 = mysql_query($sql_sel1);
		  	while($row_sel1 = mysql_fetch_assoc($res_sel1))
		  	{
				//echo $row_sel1['asso_name'];
			    $fullassoname = trim($row_sel1['asso_name']);
				$is_alrted1 = $cnt_enf_asso = $cnt_sanc_asso = 0;
				$res_chk_all_asso = srap_call($fullassoname,$types);
		
				$cnt_arr_asso = count($res_chk_all_asso);
				if(is_array($res_chk_all_asso))
				{ 
					for($aa=1;$aa<=$cnt_arr_asso;$aa++)
					{
						foreach($res_chk_all_asso[$aa] as $row_chk_all_asso)
						{
							$tp=2;
							$rec_ida = $row_chk_all_asso['id'];
							if(check_rec($tpi_id,$comp_id,$aa,$rec_ida,$nsrch_id,$tp))
							{
								$is_alrted1 = 1;
								$final_risk = $risk_cate = "";
								$risk_cate = $row_chk_all_asso['risk_category'];
								$final_risk = explode(",",$risk_cate);
								if(in_array("Sanctions",$final_risk))
								{
									$cnt_sanc_asso++; 
								}
								if(in_array("Enforcements",$final_risk))
								{
									$cnt_enf_asso++; 
								}
							}
						}
					}
				}
				?>
				<tr>
                    <td class="td-word-wraps">
                    	<?php if($is_alrted1==1 || $row_sel1['asso_posi']!=''){ ?><img src="img20/icon-add-small.png" class="img-plus" id="plusa<?php echo $ij.$jk; ?>" onClick="get_alerts_asso('includea<?php echo $ij; ?>','plusa<?php echo $ij.$jk; ?>');"/><?php } echo ucwords($fullassoname); ?>
                    </td>
                    <?php
					if($entity_type=='company')
					{
					?>
                    	<td style="text-align:center !important;"></td>
                    <?php
					}
					?>
                    <td style="text-align:center !important;">
                        <div style="float:left;"><a href="sanctions_enforcement" target="_blank"><img src="img20/SE.png" width="30" style="margin-left:30px;" /></a></div>
						<div style="float:left; margin-left:20px; margin-top:5px;"><?php if($cnt_enf_asso>0 || $cnt_sanc_asso>0){ echo "<span style='color:#FF0000;'>Risk Alert</span>"; }else{ echo "No Risk Alert"; } ?></div>
                    </td>
                    <td style="text-align:center !important;"></td>
                    <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;"></td>
                    <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;"></td>
				</tr>
                <?php if($row_sel1['asso_posi']!=''){ ?><tr id="includea<?php echo $ij; ?>" style="display:none;">
					<td colspan="5" class="indent">
                    	Subject Type: <?php echo ucwords($row_sel1['asso_posi']); ?>
                    </td>
                </tr><?php } ?>
				<?php
				if($is_alrted1==1)
				{
					for($aa=1;$aa<=$cnt_arr_asso;$aa++)
					{
						foreach($res_chk_all_asso[$aa] as $row_chk_all_asso)
						{ 
							$tp=2;
							$rec_ida = $row_chk_all_asso['id'];
							if(check_rec($tpi_id,$comp_id,$aa,$rec_ida,$nsrch_id,$tp))
							{  
								if($entity_type=='company')
								{
								?>    
									<tr id="includea<?php echo $ij; ?>" style="display:none;">
										<td colspan="6" class="indent">
											<?php if(trim($row_chk_all_asso['source_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['source_name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['url_site'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_chk_all_asso['url_site']; ?>" target="_blank"><?php echo $row_chk_all['url_site']; ?></a></span><br /><?php } ?>
											<b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Entity_Name']; ?></span><br />
											<?php if(trim($row_chk_all_asso['Entity_Type'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Entity_Type']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Alt_Name'])!=""){ ?><b>Alt Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Alt_Name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Alert'])!=""){ ?><b>Alert: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Alert']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Event'])!=""){ ?><b>Event: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Event']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Description'])!=""){ ?><b>Description: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Description']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Address'])!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Address']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all_asso['city_state_zip'])!=""){ ?><b>City/State/Zip: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['city_state_zip']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['country_name'])!=""){ ?><b>Country: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['country_name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['risk_category'])!=""){ ?><b>Risk Category: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['risk_category']; ?></span><br /><?php } ?>
										</td>
										<!--<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
												<select disabled>
												<option>Possible match</option>
												<option>Not a match</option>
												<option>Positive match</option>
											  </select>
										</td>
										<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
									  <div class="circle" style="opacity: 0.6; float:left;">
										<a data-toggle="tooltip" title="No findings / Risk has been dealt with." data-placement="top"><i class="green"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding that is of interest, however is not related directly to a corruption risk." data-placement="top"><i class="amber"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding on the name related to curruption." data-placement="top"><i class="red"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="This is a color only assigned by the client which indicates that they have made a decision they are not going to continue business with the third party." data-placement="top"><i class="black"><span class="glyphicon glyphicon-record"></span></i></a>
									  </div>
										<div style="float:left; margin-left:20px;">
											<input type="text" value="Free Text" disabled></input>
										</div>
									</td>-->
									</tr>
								<?php
								}
								elseif($entity_type=='individual')
								{
								?>    
									<tr id="includea<?php echo $ij; ?>" style="display:none;">
										<td colspan="5" class="indent">
											<?php if(trim($row_chk_all_asso['source_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['source_name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['url_site'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_chk_all_asso['url_site']; ?>" target="_blank"><?php echo $row_chk_all['url_site']; ?></a></span><br /><?php } ?>
											<b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Entity_Name']; ?></span><br />
											<?php if(trim($row_chk_all_asso['Entity_Type'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Entity_Type']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Alt_Name'])!=""){ ?><b>Alt Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Alt_Name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Alert'])!=""){ ?><b>Alert: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Alert']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Event'])!=""){ ?><b>Event: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Event']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Description'])!=""){ ?><b>Description: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Description']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['Address'])!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['Address']; ?></span><br /><?php } ?>
										<?php if(trim($row_chk_all_asso['city_state_zip'])!=""){ ?><b>City/State/Zip: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['city_state_zip']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['country_name'])!=""){ ?><b>Country: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['country_name']; ?></span><br /><?php } ?>
											<?php if(trim($row_chk_all_asso['risk_category'])!=""){ ?><b>Risk Category: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_chk_all_asso['risk_category']; ?></span><br /><?php } ?>
										</td>
										<!--<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
												<select disabled>
												<option>Possible match</option>
												<option>Not a match</option>
												<option>Positive match</option>
											  </select>
										</td>
										<td class="greyed " data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;background-color:#E3E3E3;border-color:#FFFFFF;">
									  <div class="circle" style="opacity: 0.6; float:left;">
										<a data-toggle="tooltip" title="No findings / Risk has been dealt with." data-placement="top"><i class="green"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding that is of interest, however is not related directly to a corruption risk." data-placement="top"><i class="amber"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="There has been a finding on the name related to curruption." data-placement="top"><i class="red"><span class="glyphicon glyphicon-record"></span></i></a>
										<a data-toggle="tooltip" title="This is a color only assigned by the client which indicates that they have made a decision they are not going to continue business with the third party." data-placement="top"><i class="black"><span class="glyphicon glyphicon-record"></span></i></a>
									  </div>
										<div style="float:left; margin-left:20px;">
											<input type="text" value="Free Text" disabled></input>
										</div>
									</td>-->
									</tr>
								<?php
								}
							}
						}
					}
				}
				?> 
				<?php
				$jk++;
				$ij++;
		  	}		  
			?>
        </table>
        <ul class="bottom-menu">
          <li><a data-toggle="tooltip" title="MIS" href="mis_graphs" data-placement="bottom"><img src="img20/Pie_icon.png" style="width:30px; height:30px !important; margin-left:5px;" /></a></li>
          <li><a data-toggle="tooltip" title="Save and Download" href="#" onClick='printRpt(<?php echo $nsrch_id; ?>,<?php echo $_SESSION['company_id']; ?>);' data-placement="bottom"><img src="img20/icon-header2-analytics.png" /></a></li>
          <li><a data-toggle="tooltip" title="Manage Third Parties" href="#" data-placement="bottom" class="save"><img src="img20/icon-header2-bars.png" /></a></li>
          <li><a data-toggle="tooltip" title="Order EDD Report" href="<?php echo APP_URL."order_edd?vid=".$tpi_id; ?>" data-placement="bottom"><img src="img20/icon-header2-edd.png" /></a></li>
       </ul>
      </div>
    </div>
  </div>
</div>
<div class="overlay"></div>
<div class="modal-confirm">
  <p class="message">Do you want to save your changes?</p>
  <div class="action">
     <a href="#" class="btn btn-default btn-yes" role="button">Yes</a>
     <a href="#" class="btn btn-default btn-no" role="button">No</a>
  </div>
</div>

<script language="javascript">
	function get_alerts(divname,imageid)
	{ 
		if(document.getElementById(divname).style.display=='none')
		{
			divs=document.getElementsByTagName('tr'); 
			for (var i=0;i<divs.length;i++){ 
				if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
					divs[i].style.display=""; 
				} 
			} 
			document.getElementById(imageid).src="img20/icon-minus-small.png";
			document.getElementById(divname).style.display='';
		}
		else
		{
			divs=document.getElementsByTagName('tr'); 
			for (var i=0;i<divs.length;i++){ 
				if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
					divs[i].style.display="none"; 
				} 
			} 
			document.getElementById(imageid).src="img20/icon-add-small.png";
			document.getElementById(divname).style.display='none';
		}
	}
	
	function get_alerts_asso(divname,imageid)
	{ 
		if(document.getElementById(divname).style.display=='none')
		{
			divs=document.getElementsByTagName('tr'); 
			for (var i=0;i<divs.length;i++){ 
				if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
					divs[i].style.display=""; 
				} 
			} 
			document.getElementById(imageid).src="img20/icon-minus-small.png";
			document.getElementById(divname).style.display='';
		}
		else
		{
			divs=document.getElementsByTagName('tr'); 
			for (var i=0;i<divs.length;i++){ 
				if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
					divs[i].style.display="none"; 
				} 
			} 
			document.getElementById(imageid).src="img20/icon-add-small.png";
			document.getElementById(divname).style.display='none';
		}
	}
	
	function save_alerts(srch_id)
	{ 
		var comp_id = <?php echo $_SESSION['company_id']; ?>;
		if(window.XMLHttpRequest){
			xmlHttp=new XMLHttpRequest();
		}
		else{
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url="save_tpis";
		url=url+"?srch_id="+srch_id+"&comp_id="+comp_id;
		xmlHttp.onreadystatechange=function()
		{ 
			if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
			{
				alert('Saved!');
				window.location = "<?php echo APP_URL."third-parties220"; ?>";
				/*var comp_id = '<?php //echo $_SESSION['company_id']; ?>';
				var names = '<?php //echo $srch_txt; ?>';
				printRpt(srch_id,names,comp_id);*/
				/*document.getElementById(divname).innerHTML=xmlHttp.responseText
				document.getElementById(divname).style.display='';*/
			}
		}
		xmlHttp.open("GET",url,true);
		xmlHttp.send(null);
	}
	
	function printRpt(srch_id,comp_id)
	{
		var URL = "free20_rpt_gen?srch_id="+srch_id+"&comp_id="+comp_id+"&tp=2";
		var W = window.open(URL);
		//window.location = "<?php echo APP_URL."third-parties220"; ?>";
	}
	
	function printRpt1(comp_id)
	{
		var srch_id = '<?php echo $srch_id; ?>';
		var names = '<?php echo $srch_txts; ?>';
		printRpt(srch_id,comp_id);
	}
</script>
<?php
include('footer20.php');
?>