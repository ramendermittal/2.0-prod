<?php include('header20.php'); ?>

<div class="int-row-1 edd-row-1">

	<div class="form-section">
		<div class="container">
			<div class="row">
				
				<?php
				
				//preVar($_SESSION);
				
				if ($_REQUEST['formsubmit'])
				{
					require_once 'include20/MysqliDb.php';
					$fdb = new MysqliDb;
					
					$ins = array
					(
						'name'			=> $_REQUEST['sbfullname'],
						'email'			=> $_REQUEST['sbemail'],
						'description'	=> $_REQUEST['sbdescription'],
						'created'		=> $fdb->now(),
					);
					$id = $fdb->insert('email_support', $ins);
					
					if ($id)
					{
						//send email
						$headers = 'MIME-Version: 1.0'."\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
						$headers .= "From: support@ethixbase.com\r\nReply-To: support@ethixbase.com"."\r\n";
						
						$message = '
							<html>
							<h2>Email Support #'.$id.'</h2>
							<table border="0">
							<tr><td><strong>Full Name:</strong></td></tr>
							<tr><td>'.$_REQUEST['sbfullname'].'</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td><strong>Email:</strong></td></tr>
							<tr><td>'.$_REQUEST['sbemail'].'</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td><strong>Query Description:</strong></td></tr>
							<tr><td>'.nl2br($_REQUEST['sbdescription']).'</td></tr>
							</table>
							</html>
						';
						
						$mail = mail('mfsaad@ethixbase.com,rmittal@ethixbase.com,etalisic@ethixbase.com', "Email Support #$id", $message, $headers);
						//preVar($mail);
					}
				}
				
				?>
				
				<?php if ($id): ?>
				<div class="form-area-2 form-01">
					<h3>Thank you, your information has been successfully saved.</h3>
				</div>
				<?php else: ?>
				<br><br>
				<?php endif; ?>
				
				<form class="form-area-1 form-01" action="" method="post" onSubmit="return fsubmit();">
				<input type="hidden" name="formsubmit" value="formsubmit">
					<div class="col-xs-12">
						<table class="tb-form">
							<tr>
								<td style="text-align:left;">
									<label><strong>Full Name:</strong></label><br>&nbsp;<?php echo $_SESSION['fname']; ?>
									<input id="sbfullname" type="text" readonly="readonly" name="sbfullname" value="<?php echo $_SESSION['fname']; ?>" placeholder="Enter your full name here..." style="width:100%;border-radius:0;display:none;">
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:left;">
									<label><strong>Email:</strong></label><br>&nbsp;<?php echo $_SESSION['username']; ?>
									<input id="sbemail" type="text" readonly="readonly" name="sbemail" value="<?php echo $_SESSION['username']; ?>" placeholder="Enter your email here..." style="width:100%;border-radius:0;display:none;">
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:left;"><br>
									<label><strong>Please enter your query here:</strong></label><br>
									<textarea id="sbdescription" name="sbdescription" placeholder="Enter query here..." style="width:100%;resize:none;padding:5px 10px;" rows="7"></textarea>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:left;">
									<div class="btn-next">
										<button class="btn-submit">Submit <span class="glyphicon glyphicon-menu-right"></span></button>
									</div>
								</td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

function fsubmit()
{
	var sbfullname = $("#sbfullname").val();
	var sbemail = $("#sbemail").val();
	var sbdescription = $("#sbdescription").val();
	var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	if (sbfullname == '' || sbemail == '' || sbdescription == '')
	{
		alert('All informations are required');
		return false;
	}
	
	if (!re.test(sbemail))
	{
		alert('Invalid e-mail address');
		return false;
	}
	
	return true;
}

</script>

<?php include('footer20.php'); ?>
