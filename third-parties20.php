<?php
include('header20.php');
mysql_set_charset('utf8');
  $vid = $_REQUEST['vid'];
  
  $sql_get = "SELECT * FROM shield_vendor_info WHERE comp_id=$_SESSION[company_id] AND id=$vid";
  $res_get = mysql_query($sql_get);
  $row_get = mysql_fetch_assoc($res_get);
  $row_chk = decryptRow($row_get);
?>
<div class="map-bg int-row-1 tp-row-1">
  <div class="container">
    <div class="text-center">
      <h1>Third Parties</h1>
      
      <div class="col-xs-12 tp-company-name text-left">
        <h2><!--<img width="100px" height="100px" src="img20/sample-fireball-securities-logo.png" />--><?php echo $row_chk['comp_name_eng']; ?></h2>
      </div>
      
      <div class="col-sm-12">
        <table class="text-left">
          <tr>
            <td class="text-right text-bold"><nobr>Company Name:</nobr></td>
            <td><?php echo $row_chk['comp_name_eng']; ?></td>
            <td rowspan="8" valign="middle">
            	<!--form action="third-parties2.php"-->
                <a href="third-parties420?vid=<?php echo $vid; ?>"><button class="btn-tp-edit">Edit 3rd Party Details</button><br /></a>
                <button class="btn-tp-review">Review Risk Color</button>
              <!--/form-->
          	</td>
          </tr>
          <tr>
            <td class="text-right text-bold"><nobr>Address:</nobr></td>
            <td><?php echo $row_chk['address']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold"><nobr>Country:</nobr></td>
            <td><?php echo $row_chk['country']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold"><nobr>Telephone Number:</nobr></td>
            <td><?php echo $row_chk['tel_no']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold"><nobr>Contact Name:</nobr></td>
            <td><?php echo $row_chk['compl_con_nam']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold"><nobr>Contact Title:</nobr></td>
            <td><?php echo $row_chk['compl_con_tit']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold">Contact Email:</td>
            <td><?php echo $row_chk['lg_email']; ?></td>
          </tr>
          <tr>
            <td class="text-right text-bold">Joined Date:</td>
            <td><?php echo date("M/d/Y", strtotime($row_chk['created_date'])); ?></td>
          </tr>
        </table>
      </div>
    </div>  
  </div>
</div>

<?php
  include('footer20.php');
?>