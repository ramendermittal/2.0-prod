<?php

include('header20.php');

$entity_type = $_REQUEST['entity_type'];
$searchTemp= $_REQUEST['searchTemp'];
$country = $_REQUEST['country'];

foreach ($_REQUEST['selections'] as $k => $v)
{
	$searchkey = $k;
	$search = $v;
}

/*echo "<pre>";
print_r($_REQUEST);
echo "</pre>";

echo "<pre>";
print_r($_SESSION);
echo "</pre>";*/

$is_idd = $_REQUEST['is_idd'];

?>

<script type="text/javascript">
function check_names()
{
	var passed = true;
	$("input[name='assofname[]']").each(function(idx) {
		var v1 = $(this).val().trim();
		var v2 = $("input[name='assolname[]']").eq(idx).val().trim();
		if (v1 == '' && v2 == '')
		{
			//skip if both are empty
		}
		else if (v1 == '' || v2 == '')
		{
			alert('Please fill out both First and Last Name for each individual');
			passed = false;
			return false;
		}
	});
	if (!passed) return false;
	return true;
}

</script>

<div class="int-row-1 edd-row-1">

	<div class="text-center">
	<p>&nbsp;</p>
	<div class="steps-container hidden-xs hidden-sm">
	<div class="step-dot text-center">
	<a href="javascript:history.go(-2)"><img class="step-dot-0" src="img20/icsteps1g.png" /></a>
	<p>Search</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<a href="javascript:history.back()"><img class="step-dot-0" src="img20/icsteps2g.png" /></a>
	<p>Review Company Results</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<img class="step-dot-1" src="img20/icsteps3r.png" />
	<p>Associated Individuals</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<img class="step-dot-0" src="img20/icsteps4g.png" />
	<p>Review Results &amp; Download Report</p>
	</div>
	</div>
	</div>
	
	<div class="form-section">
	
		<form class="form-01 step-2" action="<?php if($is_idd==1){ echo "due-diligenceidd.php"; }else{ echo "due-diligence420.php"; } ?>" method="post" name="assoc_individual" onSubmit="return check_names();">
		
			<input type="hidden" name="search" value="<?php echo $search; ?>">
			<input type="hidden" name="searchkey" value="<?php echo $searchkey; ?>">
			<input type="hidden" name="country" value="<?php echo $country; ?>">
			<input type="hidden" name="entity_type" value="<?php echo $entity_type; ?>">
			<input type="hidden" name="searchTemp" value="<?php echo $searchTemp; ?>">
            <input type="hidden" name="is_idd" id="is_idd" value="<?php echo $is_idd; ?>">
			<input type="hidden" name="formsubmit" value="formsubmit">
		
			<div class="container">
		
				<?php if ($searchkey): ?>
				
					<?php
						require_once 'include20/OcFunctions.php';
						$off = OcGetCompany($searchkey, $country, 'officers');
					?>
			
					<?php if ($off): ?>
					<p><strong>Associated Individuals</strong></p>
					<p>
						The following individuals are listed as associated individuals within <strong><?php echo base64_decode($search); ?>'s</strong> online registry information in <strong><?php echo countryName($country); ?></strong>.
						<table width="100%" style="border-collapse:collapse;">
							<tr>
								<td style="border:1px solid #999;padding:8px;background-color:#9a9c9e;color:#fff;font-weight:bold;">Individual Name</td>
								<td style="border:1px solid #999;padding:8px;background-color:#9a9c9e;color:#fff;font-weight:bold;">Role</td>
							</tr>
							<?php foreach ($off as $o): ?>
							<tr>
								<td style="border:1px solid #999;padding:8px;"><?php echo $o['name']; ?></td>
								<td style="border:1px solid #999;padding:8px;"><?php echo $o['position']; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</p><br>
					<?php endif; ?>
		
				<?php endif; ?>
		
				<!--
				<p>Should you wish to search any individuals associated with this entity please enter these here. You can enter up to 5 associated individuals. Should you not wish to enter associated individuals please simply skip this step.</p>
				-->
				
				<p><strong>Add Additional Associated Individuals (Optional)</strong><p>
				
				<?php if ($searchkey && !$off): ?>
				<p>There are currently no associated individuals listed in conjunction with <strong><?php echo base64_decode($search); ?></strong> in <strong><?php echo countryName($country); ?>&apos;s</strong> online registration records.</p>
				<?php endif; ?>
				
				<p>You may add up five (5) additional associated individuals below to be searched against <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a> in conjunction with <strong><?php echo base64_decode($search); ?></strong>. </p>
                <p>Should you not wish to add any associated names simply click <strong>Next</strong>.</p><br />

				<div class="row">
					<div class="col-xs-10">
						<div class="name pull-left assorow">
							<input name="assofname[]" type="text" placeholder="First Name">
							<input name="assolname[]" type="text" placeholder="Last Name">
							&nbsp;&nbsp;
							<span class="btn-remove" style="font-size:12px;cursor:pointer;">remove</span>
						</div>
						<span id="assodummy"></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<img src="img20/icon_add.png" class="btn-add" id="addasso" />
					</div>
					
					<div class="col-xs-2">
						<div class="btn-next">
							<button name="submitbtn" value="submitnext" type="submit" class="btn-submit">Next <span class="glyphicon glyphicon-menu-right"></span></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

/*** START updated by @ronnieChu 20151106 ***/
window.onhashchange = function() {
	window.history.back();
}
/**** END updated by @ronnieChu 20151106 ***/

$('#addasso').click(function()
{
	if ($('div.assorow').length == 5) return;
	var i = $('div.assorow:first').clone(true,true).insertBefore($('#assodummy'));
	$(i).find('input').val('');
	
	$('.btn-remove').click(function()
	{
		if ($('.assorow').length == 1) return false;
		$(this).parents('.assorow').remove();
	});
});

$('.btn-remove').click(function()
{
	if ($('.assorow').length == 1) return false;
	$(this).parents('.assorow').remove();
});

</script>

<?php include('footer20.php'); ?>
