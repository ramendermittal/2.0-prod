<?php
session_start();
error_reporting(0);
  /**** START updated by @ronnieChu 20151106 ***/
header('Cache-Control: max-age=900');
  /**** END updated by @ronnieChu 20151106 ***/
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
@include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');
if(!isset($_SESSION['member_id']))
{
	@header("Location: ".APP_URL."index20");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <title>ethiXbase</title>
<link href="favicon.ico" rel="icon" type="image/x-icon" />
<link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
<link href="css20/style.css" media="all" rel="stylesheet" />
<link href="css20/style-2.css" media="all" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="datatables/css/jquery.dataTables.min.css">
  <!-- <link rel="stylesheet" href="css20/jquery.qtip.min"> -->
  <script src="js20/jquery-1.11.3.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<header>
<div class="header-lower">
  <nav class="container" role="navigation">
    <h1 class="logo">
      <a href="due-diligence20" class="navbar-brand"><img src="img20/ethixbase-logo.png" /></a>
    </h1>
    <div class="main-nav">
      <ul class="nav navbar-nav navbar-right table-cell v-mid">
      <?php
	  //echo $_SERVER['PHP_SELF'];
	  //if($_SERVER['PHP_SELF']!='/ethixbase2/due-diligence20.php' || !$_REQUEST['formsubmit'])
	  //echo $_REQUEST['formsubmit'];
	  if($_REQUEST['formsubmit']=='formsubmit' && $_SERVER['PHP_SELF']=='/ethixbase2/due-diligence20.php')
	  {
	  ?>
        <li><a data-toggle="tooltip" title="Search Again" href="due-diligence20" data-placement="bottom"><img src="img20/Search_icon.png" style="width:30px; height:30px !important; margin-right:5px; margin-top:5px;" /></a></li>
       <?php
	  }
	  elseif($_SERVER['PHP_SELF']=='/ethixbase2/due-diligence20.php'){}
	  else
	  {
	   ?>
        <li><a data-toggle="tooltip" title="Search Again" href="due-diligence20" data-placement="bottom"><img src="img20/Search_icon.png" style="width:30px; height:30px !important; margin-right:5px; margin-top:5px;" /></a></li>
       <?php 
	  }
	  if($_SERVER['PHP_SELF']=='/ethixbase2/due-diligence420.php' || $_SERVER['PHP_SELF']=='/ethixbase2/due-diligence420-sec.php')
	  {
		?>
        <li><a data-toggle="tooltip" title="Management Reporting" href="mis_graphs" data-placement="bottom" class="animated pulse"><img src="img20/Pie_maron.png" style="width:30px; height:30px !important; margin-right:3px; margin-top:5px;" /></a></li>
        <?php
	  }
	  else
	  {
	  ?>
      	<li><a data-toggle="tooltip" title="Management Reporting" href="mis_graphs" data-placement="bottom"><img src="img20/Pie_icon.png" style="width:30px; height:30px !important; margin-right:3px; margin-top:5px;" /></a></li>
      <?php
	  }
	  if($_SERVER['PHP_SELF']=='/ethixbase2/due-diligence420.php' || $_SERVER['PHP_SELF']=='/ethixbase2/due-diligence420-sec.php')
	  {
		?>
        <li><a data-toggle="tooltip" title="My Third Parties" href="third-parties220.php" data-placement="bottom" class="animated pulse save"><img src="img20/icon-header2-bars-pulse.png" /></a></li>
        <li><a data-toggle="tooltip" title="Order Enhanced Due Diligence Report(s)" href="<?php echo APP_URL."order_edd_form?src=menu"; ?>" data-placement="bottom" class="animated pulse"><img src="img20/icon-header2-edd-pulse.png" /></a></li>
        <?php
	  }
	  else
	  {
	  ?>
        <li><a data-toggle="tooltip" title="My Third Parties" href="third-parties220.php" data-placement="bottom"><img src="img20/icon-header2-bars.png" /></a></li>
        <li><a data-toggle="tooltip" title="Order Enhanced Due Diligence Report(s)" href="<?php echo APP_URL."order_edd_form?src=menu"; ?>" data-placement="bottom"><img src="img20/icon-header2-edd.png" /></a></li>
       <?php
	  }
	  ?>
        <li class="dropdown drop_menu drop1" data-toggle="tooltip" data-original-title='My Account' title="My Account" data-placement="bottom">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="img20/icon-header2-user.png" /></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo APP_URL."change_password"; ?>">Change Password</a></li>
            <li class="divider"></li>
            <!--li><a href="#">Manage Account</a></li-->
            <li class="divider"></li>
            <li><a href="<?php echo APP_TURL."logout20"; ?>" onclick="remove_local_storage()">Log Out</a></li>
          </ul>
        </li>
		
        <li class="dropdown drop_menu drop2" data-toggle="tooltip" data-original-title='Help' title="Help" data-placement="bottom">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="img20/icon-header2-question.png" /></a>
          <ul class="dropdown-menu">
            <!--li><a href="<?php echo APP_URL."faq20"; ?>">FAQ</a></li-->
            <li class="divider"></li>
            <li><a href="<?php echo APP_URL."email-support"; ?>">Email Support</a></li>
          </ul>
        </li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </nav>
</div>
</header>