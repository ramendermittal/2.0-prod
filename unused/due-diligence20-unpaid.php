<?php
  include('header20.php');
?>

<div class="map-bg int-row-1 edd-row-1">
  <div class="container">
    <div class="text-center">
      <h1>Submit Due Diligence Request</h1>
      <div class="steps-container hidden-xs hidden-sm">
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Search</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-0" src="img20/icon-steps-0.png" />
          <p>Add Individuals</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-0" src="img20/icon-steps-0.png" />
          <p>Review Results</p>
        </div>
      </div>
    </div>
    
    <form class="form-01" action="due-diligence220-unpaid.php">
      <div class="col-md-10">
        <div class="col-xs-4 text-right">
          <label for="search">Search:</label>
        </div>
        <div class="col-xs-8">
           <input type="text" id="search"></input>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-4 text-right">
          &nbsp;
        </div>
        <div class="col-xs-8 radio-section">
					<input type="radio" id="company" name="entity-type"><label for="company"><span><span></span></span>Company</label>
          <input type="radio" id="individual" name="entity-type"><label for="individual"><span><span></span></span>Individual</label>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-4 text-right">
          <label for="country">Country/Territory of Registration:</label>
        </div>
        <div class="col-xs-8">
          <select id="country">
            
			<?php
			
			//using optgroup rather than two selects
			
			$sql2 = "SELECT state_name, oc_ccode, oc_scode FROM state_list WHERE oc_scode IS NOT NULL AND oc_ccode IS NOT NULL ORDER BY state_name";
			$res2 = mysql_query($sql2) or die(mysql_error());
			while ($row2 = mysql_fetch_assoc($res2))
			{
				$states[$row2['oc_ccode']][$row2['oc_scode']] = $row2['state_name'];
			}
			
			$sql1 = "SELECT id, country_name, oc_ccode FROM country_list WHERE id not in (233,234,235) and oc_ccode is not null ORDER BY country_name";
			$res1 = mysql_query($sql1) or die(mysql_error());
			while ($row1 = mysql_fetch_assoc($res1))
			{
				if (isset($states[$row1['oc_ccode']]))
				{
					echo '<optgroup label="'.$row1['country_name'].'">';
					foreach($states[$row1['oc_ccode']] as $skey => $sname)
					{
						echo '<option value="'.$row1['oc_ccode'].'_'.$skey.'">'.$sname.'</option>';
					}
					echo '</optgroup>';
				}
				else
				{
					echo '<option value="'.$row1['oc_ccode'].'">'.$row1['country_name'].'</option>';
				}
			}
			
			?>
			
          </select>
        </div>
      </div>
      <div class="col-md-2 text-center">
        <button class="btn-submit">Next</button>
      </div>
    </form>
  </div>

</div>

<?php
  include('footer20.php');
?>