<?php
  include('header20.php');
?>


<div class="map-bg int-row-1 edd-row-1">
  <div class="container">
    <div class="text-center">
      <h1>Submit Due Diligence Request</h1>
      <div class="steps-container hidden-xs hidden-sm">
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Search</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Add Individuals</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-0" src="img20/icon-steps-0.png" />
          <p>Review Results</p>
        </div>
      </div>
    </div>
    
    <form class="form-01" action="due-diligence320-unpaid.php">
      <div class="col-md-10">
        <div class="col-xs-12">
        	<p>Enter names of Associated Individuals below:</p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-5">
        	<input type="text" onfocus="if (this.value=='First Name 1') this.value = ''" onblur="if (this.value=='') this.value = 'First Name 1'" value="First Name 1">
        </div>
        <div class="col-sm-5">
        	<input type="text" onfocus="if (this.value=='Last Name 1') this.value = ''" onblur="if (this.value=='') this.value = 'Last Name 1'" value="Last Name 1">
        </div>
        <div class="col-sm-2">
        	<button class="btn-remove">remove</button>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-5">
        	<input type="text" onfocus="if (this.value=='First Name 2') this.value = ''" onblur="if (this.value=='') this.value = 'First Name 2'" value="First Name 2">
        </div>
        <div class="col-sm-5">
        	<input type="text" onfocus="if (this.value=='Last Name 2') this.value = ''" onblur="if (this.value=='') this.value = 'Last Name 2'" value="Last Name 2">
        </div>
        <div class="col-sm-2">
        	<button class="btn-remove">remove</button>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12">
        	<a href="#"><img src="img20/icon_add.png" class="btn-add" /></a>
        </div>
      </div>
      <div class="col-md-2 text-center">
        <button class="btn-submit">Next</button>
      </div>
    </form>
  </div>
  
  <div class="clearfix"></div>
  
</div>

<?php
  include('footer20.php');
?>