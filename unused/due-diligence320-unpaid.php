<?php
  include('header20.php');
?>


<div class="map-bg int-row-1 edd-row-1">
  <div class="container">
    <div class="text-center">
      <h1>Submit Due Diligence Request</h1>
      <div class="steps-container hidden-xs hidden-sm">
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Search</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Add Individuals</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Review Results</p>
        </div>
      </div>
    </div>
    
    <div class="col-xs-12">
      <p class="dd4-label">Names associated with: <b>Marc Armstrong</b></p>
      <div class="form-03">
        <table class="table-01">
          <tr>
            <td>Company/Individual</td>
            <td>Company/Individual</td>
            <td>Country</td>
            <td>EthiXbase<br/>Rating</td>
            <td class="greyed">Include in<br/>Report</td>
            <td class="greyed">My Remediation</td>
          </tr>
          <tr>
            <td><a href="due-diligence4-unpaid.php"><img src="img20/icon-add-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc H. Armstrong</td>
            <td>Individual</td>
            <td>Singapore</td>
            <td style="text-align:center !important;"><img src="img20/icon-pass.png" /></td>
            <td class="greyed"><input id="checkbox3" type="checkbox" name="checkbox" value="3" disabled><label for="checkbox3">exclude</label></td>
            <td>
              <select disabled>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" disabled></input>
            </td>
          </tr>
          <tr>
            <td><a href="#"><img src="img20/icon-add-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc D. Armstrong</td>
            <td>Individual</td>
            <td>India</td>
            <td style="text-align:center !important;"><img src="img20/icon-pass.png" /></td>
            <td class="greyed"><input id="checkbox3" type="checkbox" name="checkbox" value="3" disabled><label for="checkbox3">exclude</label></td>
            <td>
              <select disabled>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" disabled></input>
            </td>
          </tr>
          <tr>
            <td><a href="#"><img src="img20/icon-add-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc Armstrong</td>
            <td>Individual</td>
            <td>New Zealand&nbsp;</td>
            <td style="text-align:center !important;"><img src="img20/icon-fail.png" /></td>
            <td class="greyed"><input id="checkbox3" type="checkbox" name="checkbox" value="3" disabled><label for="checkbox3">exclude</label></td>
            <td>
              <select disabled>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" disabled></input>
            </td>
          </tr>
        </table>
        <div class="button-area text-center">
          <button>Save</button><button>Print</button><button>Order EDD</button>
        </div>
      </div>
    </div>
    
  </div>
  
</div>

<?php
  include('footer20.php');
?>