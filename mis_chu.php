<?php 
    include('header20.php'); 
	error_reporting(0);
	$stored_tpi_qry = "SELECT COUNT(*) as stored_tpi FROM shield_vendor_info WHERE comp_id='{$_SESSION['company_id']}'";	
	$stored_tpi_rslt = mysql_query($stored_tpi_qry);
	$stored_tpi_rslt = mysql_fetch_assoc($stored_tpi_rslt);
?>
<style type="text/css" media="print">
   .no-print { 
        display: none !important;
        height: 0;
	}
   .print-top {
		padding-top:30px !important;
	}
</style>
<div style="padding-top:100px;" class="no-print"></div> 
<div style="padding-top:20px;" class="no-print"></div> 
	<link href="css20/style-dashboard.css" media="all" rel="stylesheet" />
	<link href="css20/style-dashboard-2.css" media="all" rel="stylesheet" />
	<script src="js20/highchart/highcharts.js"></script>
	<script src="js20/printElement.js"></script>
<div class="container">	
	<div class="row">
		<div class="system-data" style="padding-bottom:10px;margin-bottom:10px;">
		<?php
			require_once('include20/simple_html_dom.php');
			$url ="https://opencorporates.com/";
			$table = file_get_contents($url);

			$len = strlen($table);
			if ( !($len < 18 || strcmp(substr($table,0,2),"\x1f\x8b")) ) {
				$table = gzdecode($table);
			}
			$html = str_get_html($table);
			foreach($html->find('input[class="input--large"]') as $a){
				$input_count = $a->attr;
			}
			$company_records = explode(' ', $input_count['placeholder']);
			
			$stat_qry = "SELECT * FROM risk_category_statistics ORDER BY last_updated DESC LIMIT 1";
			$stat_risks = scrap_exec($stat_qry);
			$stat_risks = $stat_risks[0];	
		?>
			<table width="100%">
				<tbody>
				<tr>
					<td style="height:50px;border:0px;">
						<span style="font-size:24px;font-weight:400;color:#407199;margin-top:55px;margin-left:15px;">
						System Data
						</span>
					</td>
				</tr>
				<tr>
					<td style="width:25%;height:100px;border-right:1px solid #8CC514;text-align:center;vertical-align:middle;" >
						<span style="font-size:40px;color:#515254;">
						<?php echo array_key_exists(1, $company_records) ? $company_records[1] : ""; ?>
						</span>
						<br>
						<span style="font-size:20px;color:#409995;"> Company Registry Records </span>
					</td>
					<td style="width:25%;height:100px;border-right:1px solid #8CC514;text-align:center;vertical-align:middle;" >
						<span style="font-size:40px;color:#515254;">
						<?php echo array_key_exists('sanctions', $stat_risks) ? number_format($stat_risks['sanctions'],0) : ""; ?>
						</span>
						<br>
						<span style="font-size:20px;color:#409995;"> Sanctions </span>
					</td>
					<td style="width:25%;height:100px;text-align:center;vertical-align:middle;" >
						<span style="font-size:40px;color:#515254;">
						<?php echo array_key_exists('enforcements', $stat_risks) ? number_format($stat_risks['enforcements'],0) : ""; ?>
						</span>
						<br>
						<span style="font-size:20px;color:#409995;"> Enforcements </span>
					</td>
					<td style="width:25%;height:100px;text-align:center;vertical-align:middle;display:none" >
						<span style="font-size:40px;color:#515254;">
						<?php echo array_key_exists('sanctions', $stat_risks) ? number_format($stat_risks['sanctions'],0) : ""; ?>
						</span>
						<br>
						<span style="font-size:20px;color:#409995;"> Sanctions </span>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<!--div style="page-break-before:always" class="print-top"></div-->
	<div style="padding-top:10px;"></div> 
	
	<div class="row">
		<div class="portlet-body tabbable-custom " style="width:900px;float:left;">
			<ul class="nav nav-tabs no-print">
				<li class="active">
					<a data-toggle="tab" href="#my_third_parties">My Third Parties</a>
				</li>
			</ul>

			<div class="tab-content" style="overflow: hidden;padding-bottom:10px;" id="div_print">
				<!-- My Third Parties -->
				<div id="my_third_parties" class="tab-pane active">
			
					<div id="printDiv" style="float:right" class="no-print">
						<a href="javascript:window.print();">
							<img src="img20/print_icon.png" alt="Print" style="width:25px;height:25px;" />
						</a>
						<a href="javascript:window.print();">
							Print My Third Parties
						</a>
					</div>
					<div class="chart_2" >				
						<h2 style="border-bottom:3px solid #ddd !important;padding-bottom:10px">Stored TPI</h2>                            
						<div align="center" style="margin-bottom:10px;padding-bottom:0px;">					
						<strong style="font-size:60px;margin-top:30px;color:#666"><?php echo $stored_tpi_rslt['stored_tpi']; ?>
						</strong>
						</div>
					</div>
					
					<?php
					function get_vendor_info($data_dates)
					{
						$data_dates = array_reverse($data_dates);
						$cnts = count($data_dates);
						for($hj=0;$hj<$cnts;$hj++)
						{
							$sql_cnt = "SELECT COUNT(id) as tcnt FROM shield_vendor_info WHERE DATE_FORMAT(created_date,'%Y-%m-%d')<='".$data_dates[$hj]."' AND comp_id=$_SESSION[company_id]";
							$res_cnt = mysql_query($sql_cnt);
							$row_cnt = mysql_fetch_assoc($res_cnt);
							$datas[] = $row_cnt['tcnt'];
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
/*
					for ($i=0; $i<6; $i++) { 
						/*$dates = date("M d Y", strtotime('Sunday -'.$i.' week'));
						$datos[] = "'".$dates."'";
						$data_date[] = date("Y-m-d", strtotime($dates));
						*//*
						$dates = date('M', strtotime("-$i month"));        
						$datos[] = "'".$dates."'";
						$data_date[] = date("Y-m-t", strtotime($dates));
					// echo '<option value="'.date('Y-m-d', strtotime("-$i month")).'">'.date('Y-m', strtotime("-$i month")).'</option>';
					 }*/
					for ($i = 0; $i < 6; $i++) {
						$data_date[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates = date('M', strtotime($data_date[$i]));        
						$datos[] = "'".$dates."'";
					}
					$datos = array_reverse($datos);
					$dates = implode(",",$datos);
					?>
					<div class="lineChart">
						<div id="lineContainer" style="width: 640px; height: 260px; margin: 0 auto">
						</div>
					</div>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'lineContainer',
								type: 'line',
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								//lineWidth: 1,
								tickInterval: 2,
								title: {
									text: ' No. of TPIs ',
									align: 'middle'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										'No. of Third Parties' +': '+ this.y +'';
								}
							},
							plotOptions: {
								bar: {
									size:'100%',
									dataLabels: {
										enabled: true
									}
								}
							},				   
							credits: {
								enabled: false
							},
							series: [ {							
								name: 'Year <?php echo date("Y"); ?>',							
								color:'#E7B422',						
								data: [<?php echo get_vendor_info($data_date); ?>]
							}]
						});
					</script>
					<div class="chart_1" >	
						<div id="containerpie" style="width:350px; height:360px;">
						</div>
					</div>
					<?php
					function random_color_part() {
						return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
					}

					function random_color() {
						return random_color_part() . random_color_part() . random_color_part();
					}
					
					$sql_cate = "SELECT COUNT(*) AS nums, country FROM shield_vendor_info WHERE comp_id = '$_SESSION[company_id]' GROUP BY country;";
					$res_cate1 = mysql_query($sql_cate);
					$count1 = mysql_num_rows($res_cate1);
					$totalCount1 = $stored_tpi_rslt['stored_tpi'];
					
					if($count1>0)
					{
						$sum1 = 0;
						while($row_cate1 = mysql_fetch_assoc($res_cate1))
						{
							$row_cate1['nums'] = ($row_cate1['nums']==0)?1:$row_cate1['nums'];
							$sql_cates = "SELECT COUNT(*) AS nums FROM shield_vendor_info WHERE comp_id = '$_SESSION[company_id]' and country = '$row_cate1[country]'";
							$res_cate1s = mysql_query($sql_cates);
							$sumgg = mysql_fetch_assoc($res_cate1s);
													
								$countries[$sum1]['name'] = trim($row_cate1['country']) == "" ? "N/A" : $row_cate1['country'];
								$countries[$sum1]['y'] = ($sumgg['nums']/$totalCount1)*100;
								//$countries[$sum1]['color'] = '#'.random_color();
								//$countries[$sum1]['selected'] = 'true';
								$sum1++;
							
						}

						$max_count = 10;
						$max_percent = 21;
						if( $count1 > $max_count ){
							$others_count = 0;
							$others_array = array();
							function sortByOrder($a, $b) {
								return $a['y'] - $b['y'];
							}
							usort($countries, 'sortByOrder');
							
							$others_count = $count1 - $max_count;
							$others_y = 0;
							for( $others_index = 0; $others_y < $max_percent; $others_index++ ){
								$others_array[$others_index] = $countries[$others_index];
								$others_y += $countries[$others_index]['y'];
								unset($countries[$others_index]);
							}
							$new_count = count( $countries );
							$countries[$new_count]['name'] = "Others";
							$countries[$new_count]['y'] = $others_y;
							$countries[$new_count]['sliced'] = true;
							
							$countries = array_values($countries);							

							function sortByName ($x, $y) {
								return strcasecmp($x['name'], $y['name']);
							}

							usort($countries, 'sortByName');					
						}
						
						$country_data = json_encode($countries);
					}			
					?>					
					<script type="text/javascript">
					Number.prototype.toFixedDown = function(digits) {
						var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
							m = this.toString().match(re);
						return m ? parseFloat(m[1]) : this.valueOf();
					};
						var country_pie_chart = new Highcharts.Chart({
							chart: {
								renderTo: 'containerpie'
							},
							title: {
								text: 'TPI\'s by Country',
								verticalAlign: 'bottom',
								style: {
									fontSize: '15px',
									color: '#C2168E',
									fontWeight: 'bold',
									fontFamily: 'Verdana'
								}
							},
							tooltip: {
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
								}
							},
							plotOptions: {
								pie: {
									size:'100%',
									cursor: 'pointer',
									data: <?php echo $country_data ?>
								}
							},
							series: [{
								type: 'pie',
								name: 'TPI\'s by Country',
								dataLabels: {
									verticalAlign: 'top',
									enabled: true,
									color: '#000000',
									connectorWidth: 1,
									distance: -30,
									connectorColor: '#000000',
									formatter: function () {
										return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
									}
								}
							}, {
								type: 'pie',
								name: 'TPI\'s by Country',
								dataLabels: {
									enabled: false,
									color: '#000000',
									connectorWidth: 1,
									distance: 30,
									connectorColor: '#000000',
									formatter: function() {
										return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
									}
								}
							}],
							credits: {
								enabled: false
							}
						});
						</script>
					
					<div class="recents recent_search">
						<h3>Recently Searched</h3><br>
						<?php
						
						$search_qry = "SELECT *, gettimeline(created_date) as timeline FROM shield_vendor_info WHERE comp_id = '$_SESSION[company_id]' ORDER BY created_date DESC LIMIT 5";
						$search_res = mysql_query($search_qry);
						$search_cnt = mysql_num_rows($search_res);
						if($search_cnt > 0){
							while($search_row = mysql_fetch_assoc($search_res))
							{
								echo "<div class='recents_div'>";								
								echo "<div class='searched_txt'>".$ese->decrypt(trim($search_row['comp_name_eng']))."</div>";
								echo "<br>";
								echo "<div class='searched_on'>".$search_row['timeline']."</div>";
								
								echo "<div class='clearfix'></div>";
								echo "</div>";
								//echo "<br>";
							}
						}
						
												
						?>
					</div>
					<div class="recents recent_alert">
						<h3>Recent Alerts</h3><br>
					</div>
					<div class="barContainer" style="clear: both;padding-top:40px;">
						<div id="barContainer1" style="width: 640px; margin: 0 auto">
						</div>
					</div>
					
					<?php
					
					function get_vendor_info_bar($data_dates_bar)
					{
						//$data_dates_bar = array_reverse($data_dates_bar);
						$cnts = count($data_dates_bar);
						for($hj=0;$hj<$cnts;$hj++)
						{
							$sql_cnt = "SELECT COUNT(id) as tcnt FROM shield_vendor_info WHERE DATE_FORMAT(created_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_bar[$hj]) )."' 
							 AND comp_id=$_SESSION[company_id]";
							$res_cnt = mysql_query($sql_cnt);
							$row_cnt = mysql_fetch_assoc($res_cnt);
							$datas[] = $row_cnt['tcnt'];
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
					
					for ($i = 0; $i < 6; $i++) {
						$data_date_bar[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates_bar = date('M', strtotime($data_date_bar[$i]));        
						$datos_bar[] = "'".$dates_bar."'";
					}
					//$datos_bar = array_reverse($datos_bar);
					$dates_bar = implode(",",$datos_bar);
					
					?>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'barContainer1',
								type: 'bar'
							},
							title: {
								text: 'Number of Checks Run per Month'
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates_bar; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								title: {
									text: 'No. of checks',
									align: 'high'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										' '+this.y+' run checks';
								}
							},
							plotOptions: {
								bar: {
									dataLabels: {
										enabled: true
									}
								}
							},
							credits: {
								enabled: false
							},
							series: [ {								
								name: 'Year <?php echo date("Y"); ?>',
								color:'#409995',								
								data: [<?php echo get_vendor_info_bar($data_date_bar); ?>]
							}]
						});
					</script>
					
					<div style="page-break-before:always" class="print-top"></div>
					<div class="barContainer" style="padding-top:8px;float:left">
						<div id="companies_with_hits" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					
					<?php
					
					function get_vendor_info_hits($data_dates_hits)
					{
						//$data_dates_hits = array_reverse($data_dates_hits);
						$cnts = count($data_dates_hits);
						for($hj=0;$hj<$cnts;$hj++)
						{
							//select count(id) from shield_vendor_info where risk_level=3 and comp_id=$_SESSION['company_id']
							$sql_cnt = "SELECT *, DATE_FORMAT(created_date,'%d %M %Y') AS created_date, DATE_FORMAT(created_date,'%d %M %Y') AS date_modified FROM shield_vendor_info WHERE DATE_FORMAT(created_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_hits[$hj]) )."' AND comp_id=$_SESSION[company_id]";
							//echo $sql_cnt;
							$res_cnt = mysql_query($sql_cnt);
							//$row_cnt = mysql_fetch_assoc($res_cnt);
							//$datas[] = $row_cnt['tcnt'];
							$alerted_count = 0;
							$types = 1;
							while($row=mysql_fetch_assoc($res_cnt))
							{
								$cnt_sanc = $cnt_enf = $is_alrted = $cnt_arr = 0;
								$final_risk = $risk_cate = "";
								$res_chk_all = array();
								$row = decryptRow($row);
								$srch_id = $row['srch_id'];
								$sql_sel = "SELECT * FROM search20_tpis WHERE id=$srch_id";
								$res_sel = mysql_query($sql_sel,$con1);
								$row_sel = mysql_fetch_assoc($res_sel);
								$entity_type = $row_sel['entity_type'];
								$srch_txt = htmlspecialchars(trim($row['comp_name_eng']), ENT_QUOTES);
								if($srch_txt != '')
								{
									$res_chk_all = srap_call($srch_txt,$types);
									$cnt_arr = count($res_chk_all);
									if(is_array($res_chk_all))
									{ 
										for($aa=1;$aa<=$cnt_arr;$aa++)
										{
											foreach($res_chk_all[$aa] as $row_chk_all)
											{
												$is_alrted = 1;
												$final_risk = $risk_cate = "";
												$risk_cate = $row_chk_all['risk_category'];
												$final_risk = explode(",",$risk_cate);
												if(in_array("Sanctions",$final_risk))
												{
													$cnt_sanc++; 
												}
												if(in_array("Enforcements",$final_risk))
												{
													$cnt_enf++; 
												}					
											}
										}
									}
								}
								if($is_alrted==1){
									$alerted_count++;
								}
							}
								
							$datas[] = $alerted_count;
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
					
					for ($i = 0; $i < 6; $i++) {
						$data_date_hits[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates_hits = date('M', strtotime($data_date_hits[$i]));        
						$datos_hits[] = "'".$dates_hits."'";
					}
					//$datos_hits = array_reverse($datos_hits);
					$dates_hits = implode(",",$datos_hits);
										
					?>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'companies_with_hits',
								type: 'bar'
							},
							title: {
								text: "Number of Companies with 'Hits'"
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates_hits; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								title: {
									text: 'No. of hits',
									align: 'high'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										' '+this.y+' companies';
								}
							},
							plotOptions: {
								bar: {
									dataLabels: {
										enabled: true
									}
								}
							},
							credits: {
								enabled: false
							},
							series: [ {								
								name: 'Year <?php echo date("Y"); ?>',
								color:'#F05151',						
								data: [<?php echo get_vendor_info_hits($data_date_hits); ?>]
							}]
						});
					</script>
					<div class="barContainer" style="margin-left:60px;padding-top:8px;float:left">
						<div id="companies_with_prcnt" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					
					<?php
					
					function get_vendor_info_prcnt($data_dates_prcnt)
					{
						//$data_dates_prcnt = array_reverse($data_dates_prcnt);
						$cnts = count($data_dates_prcnt);
						for($hj=0;$hj<$cnts;$hj++)
						{
							$sql_cnt = "SELECT *, DATE_FORMAT(created_date,'%d %M %Y') AS created_date, DATE_FORMAT(created_date,'%d %M %Y') AS date_modified FROM shield_vendor_info WHERE DATE_FORMAT(created_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_prcnt[$hj]) )."' AND comp_id=$_SESSION[company_id]";
							$res_cnt = mysql_query($sql_cnt);
							//$row_cnt = mysql_fetch_assoc($res_cnt);							
							//$with_hits= $row_cnt['cnt'];
							
							$alerted_count = 0;
							$types = 1;
							while($row=mysql_fetch_assoc($res_cnt))
							{
								$cnt_sanc = $cnt_enf = $is_alrted = $cnt_arr = 0;
								$final_risk = $risk_cate = "";
								$res_chk_all = array();
								$row = decryptRow($row);
								$srch_id = $row['srch_id'];
								$sql_sel = "SELECT * FROM search20_tpis WHERE id=$srch_id";
								$res_sel = mysql_query($sql_sel,$con1);
								$row_sel = mysql_fetch_assoc($res_sel);
								$entity_type = $row_sel['entity_type'];
								$srch_txt = htmlspecialchars(trim($row['comp_name_eng']), ENT_QUOTES);
								if($srch_txt != '')
								{
									$res_chk_all = srap_call($srch_txt,$types);
									$cnt_arr = count($res_chk_all);
									if(is_array($res_chk_all))
									{ 
										for($aa=1;$aa<=$cnt_arr;$aa++)
										{
											foreach($res_chk_all[$aa] as $row_chk_all)
											{
												$is_alrted = 1;
												$final_risk = $risk_cate = "";
												$risk_cate = $row_chk_all['risk_category'];
												$final_risk = explode(",",$risk_cate);
												if(in_array("Sanctions",$final_risk))
												{
													$cnt_sanc++; 
												}
												if(in_array("Enforcements",$final_risk))
												{
													$cnt_enf++; 
												}					
											}
										}
									}
								}
								if($is_alrted==1){
									$alerted_count++;
								}
							}						
							$with_hits= $alerted_count;
							
							$total_cnt = 0;
							$sql_tcnt = "SELECT COUNT(id) as tcnt FROM shield_vendor_info WHERE DATE_FORMAT(created_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_prcnt[$hj]) )."' AND comp_id={$_SESSION['company_id']}";
							//echo "<pre>"; echo $sql_tcnt;
							$res_tcnt = mysql_query($sql_tcnt);
							$row_tcnt = mysql_fetch_assoc($res_tcnt);							
							$total_cnt= $row_tcnt['tcnt'];
							
							if($total_cnt > 0){
								$datas[] = round( (($with_hits/$total_cnt)*100), 2 );
							}else{
								$datas[] = 0;
							}
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
					
					for ($i = 0; $i < 6; $i++) {
						$data_date_prcnt[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates_prcnt = date('M', strtotime($data_date_prcnt[$i]));        
						$datos_prcnt[] = "'".$dates_prcnt."'";
					}
					//$datos_prcnt = array_reverse($datos_prcnt);
					$dates_prcnt = implode(",",$datos_prcnt);
										
					?>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'companies_with_prcnt',
								type: 'bar'
							},
							title: {
								text: "% of Companies with 'Hits'"
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates_prcnt; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								title: {
									text: '% with hits',
									align: 'high'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										' '+this.y+'%';
								}
							},
							plotOptions: {
								bar: {
									dataLabels: {
										enabled: true
									}
								}
							},
							credits: {
								enabled: false
							},
							series: [ {								
								name: 'Year <?php echo date("Y"); ?>',
								color:'#F15F22',						
								data: [<?php echo get_vendor_info_prcnt($data_date_prcnt); ?>]
							}]
						});
					</script>
					<div class="barContainer" style="padding-top:8px;float:left">
						<div id="matchpie" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					
					<?php
						$sql_cnt = "SELECT COUNT(id) as tcnt FROM shield_vendor_info WHERE comp_id=$_SESSION[company_id]";
						//echo $sql_cnt;
						$res_cnt = mysql_query($sql_cnt);
						$row_cnt = mysql_fetch_assoc($res_cnt);
						$total_count = $row_cnt['tcnt'];						
						
						$sql_mcnt = "SELECT *, DATE_FORMAT(created_date,'%d %M %Y') AS created_date, DATE_FORMAT(created_date,'%d %M %Y') AS date_modified FROM shield_vendor_info WHERE comp_id=$_SESSION[company_id]";
						//echo $sql_cnt;
						$res_mcnt = mysql_query($sql_mcnt);
						//$row_mcnt = mysql_fetch_assoc($res_mcnt);
						//$match = $row_mcnt['mtch_cnt'];
						
						$alerted_count = 0;
						$types = 1;
						while($row=mysql_fetch_assoc($res_mcnt))
						{
							$cnt_sanc = $cnt_enf = $is_alrted = $cnt_arr = 0;
							$final_risk = $risk_cate = "";
							$res_chk_all = array();
							$row = decryptRow($row);
							$srch_id = $row['srch_id'];
							$sql_sel = "SELECT * FROM search20_tpis WHERE id=$srch_id";
							$res_sel = mysql_query($sql_sel,$con1);
							$row_sel = mysql_fetch_assoc($res_sel);
							$entity_type = $row_sel['entity_type'];
							$srch_txt = htmlspecialchars(trim($row['comp_name_eng']), ENT_QUOTES);
							if($srch_txt != '')
							{
								$res_chk_all = srap_call($srch_txt,$types);
								$cnt_arr = count($res_chk_all);
								if(is_array($res_chk_all))
								{ 
									for($aa=1;$aa<=$cnt_arr;$aa++)
									{
										foreach($res_chk_all[$aa] as $row_chk_all)
										{
											$is_alrted = 1;
											$final_risk = $risk_cate = "";
											$risk_cate = $row_chk_all['risk_category'];
											$final_risk = explode(",",$risk_cate);
											if(in_array("Sanctions",$final_risk))
											{
												$cnt_sanc++; 
											}
											if(in_array("Enforcements",$final_risk))
											{
												$cnt_enf++; 
											}					
										}
									}
								}
							}
							if($is_alrted==1){
								$alerted_count++;
							}
						}						
						$match= $alerted_count;
						
						$match_prnt = 0;
						$no_match_prcnt = 100;
						
						if($match>0){
							$match_prnt = round( ($match/$total_count)*100, 2 );
							$no_match_prcnt = round( 100 - $match_prnt, 2 );
						}
										
					?>
					<script type="text/javascript">
						var country_pie_chart = new Highcharts.Chart({
							chart: {
								renderTo: 'matchpie'
							},
							title: {
								text: 'Company Match Percentage',
								verticalAlign: 'top',
								style: {
									fontSize: '15px',
									color: '#C2168E',
									fontWeight: 'bold',
									fontFamily: 'Verdana'
								}
							},
							tooltip: {
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
								}
							},
							plotOptions: {
								pie: {
									size:'80%',
									cursor: 'pointer',
									data: <?php echo $country_data ?>
								}
							},							
							series: [{
										type: 'pie',
										name: 'Match Percentage',
										data: [
											{
												name: 'Match: ',
												y: <?php echo $match_prnt; ?>,
												sliced: true,
												color:'#AB3E5B',
												selected: true
											},
											{
												name: 'No Match: ',
												y: <?php echo $no_match_prcnt; ?>,
												sliced: true,
												color:'#8CC514'
											}
										]
									}] ,
							credits: {
								enabled: false
							}
						});
						</script>
					<div class="barContainer" style="margin-left:60px;padding-top:8px;float:left">
						<div id="companies_with_chcks" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					
					<?php
					
					function get_vendor_info_chcks($data_dates_chcks)
					{
						//$data_dates_chcks = array_reverse($data_dates_chcks);
						$cnts = count($data_dates_chcks);
						for($hj=0;$hj<$cnts;$hj++)
						{
							//select count(id) from shield_vendor_info where risk_level=3 and comp_id=$_SESSION['company_id']
							$sql_cnt = "SELECT COUNT(id) as cnt FROM search20_tpis WHERE DATE_FORMAT(created_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_chcks[$hj]) )."' AND FIND_IN_SET({$_SESSION['member_id']}, user_id) > 0 ";
							
							$res_cnt = mysql_query($sql_cnt);
							$row_cnt = mysql_fetch_assoc($res_cnt);							
							$with_hits= $row_cnt['cnt'];
							
							$datas[] = $with_hits;
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
					
					for ($i = 0; $i < 6; $i++) {
						$data_date_chcks[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates_chcks = date('M', strtotime($data_date_chcks[$i]));        
						$datos_chcks[] = "'".$dates_chcks."'";
					}
					//$datos_chcks = array_reverse($datos_chcks);
					$dates_chcks = implode(",",$datos_chcks);
										
					?>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'companies_with_chcks',
								type: 'bar'
							},
							title: {
								text: "Number of Searches per Month"
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates_chcks; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								title: {
									text: 'No. of searches',
									align: 'high'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										' '+this.y+' searches';
								}
							},
							plotOptions: {
								bar: {
									dataLabels: {
										enabled: true
									}
								}
							},
							credits: {
								enabled: false
							},
							series: [ {								
								name: 'Year <?php echo date("Y"); ?>',
								color:'#407199',						
								data: [<?php echo get_vendor_info_chcks($data_date_chcks); ?>]
							}]
						});
					</script>
					<div class="barContainer" style="padding-top:8px;float:left">
						<div id="companies_with_logins" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					
					<?php
					
					function get_vendor_info_logins($data_dates_logins)
					{
						//$data_dates_logins = array_reverse($data_dates_logins);
						$cnts = count($data_dates_logins);
						for($hj=0;$hj<$cnts;$hj++)
						{
							//select count(id) from shield_vendor_info where risk_level=3 and comp_id=$_SESSION['company_id']
							$sql_cnt = "SELECT COUNT(id) as cnt FROM login_stats WHERE DATE_FORMAT(inserted_date,'%m-%Y')='".date( "m-Y", strtotime($data_dates_logins[$hj]) )."' AND comp_id={$_SESSION['company_id']} ";
							$res_cnt = mysql_query($sql_cnt);
							$row_cnt = mysql_fetch_assoc($res_cnt);							
							$with_hits= $row_cnt['cnt'];
							
							$datas[] = $with_hits;
						}
						$grh_data = implode(",",$datas);
						return $grh_data;
					}
					
					for ($i = 0; $i < 6; $i++) {
						$data_date_logins[] = date("Y-m-t", strtotime( date( 'Y-m-01' )." -$i months"));						
						$dates_logins = date('M', strtotime($data_date_logins[$i]));        
						$datos_logins[] = "'".$dates_logins."'";
					}
					//$datos_logins = array_reverse($datos_logins);
					$dates_logins = implode(",",$datos_logins);
										
					?>
					<script type="text/javascript">
						var chart;
						chart = new Highcharts.Chart({
							chart: {
								renderTo: 'companies_with_logins',
								type: 'bar'
							},
							title: {
								text: "Number of Logins per Month"
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: [<?php echo $dates_logins; ?>],
								title: {
									text: null
								}
							},
							yAxis: {
								min: 0,	
								title: {
									text: 'No. of logins',
									align: 'high'
								}
							},
							tooltip: {
								formatter: function() {
									return ''+
										' '+this.y+' logins';
								}
							},
							plotOptions: {
								bar: {
									dataLabels: {
										enabled: true
									}
								}
							},
							credits: {
								enabled: false
							},
							series: [ {								
								name: 'Year <?php echo date("Y"); ?>',
								color:'#684099',						
								data: [<?php echo get_vendor_info_logins($data_date_logins); ?>]
							}]
						});
					</script>
					
					<div class="barContainer" style="margin-left:60px;padding-top:8px;float:left">
						<div id="country_hits" style="width: 400px; margin: 0 auto">
						</div>
					</div>
					<?php					
					$sql_chits = "SELECT *, IF(country='','N/A',country) as country FROM shield_vendor_info WHERE comp_id = '$_SESSION[company_id]' ";
					$res_chits1 = mysql_query($sql_chits);
					$count1_hits = mysql_num_rows($res_chits1);
					$countries = array();
					
					$alerted_countries = array();
					$total_alerts = 0;
					$types = 1;
					while($row=mysql_fetch_assoc($res_chits1))
					{
						$cnt_sanc = $cnt_enf = $is_alrted = $cnt_arr = 0;
						$final_risk = $risk_cate = "";
						$res_chk_all = array();
						$row = decryptRow($row);
						$srch_id = $row['srch_id'];
						$sql_sel = "SELECT * FROM search20_tpis WHERE id=$srch_id";
						$res_sel = mysql_query($sql_sel,$con1);
						$row_sel = mysql_fetch_assoc($res_sel);
						$entity_type = $row_sel['entity_type'];
						$srch_txt = htmlspecialchars(trim($row['comp_name_eng']), ENT_QUOTES);
						if($srch_txt != '')
						{
							$res_chk_all = srap_call($srch_txt,$types);
							$cnt_arr = count($res_chk_all);
							if(is_array($res_chk_all))
							{ 
								for($aa=1;$aa<=$cnt_arr;$aa++)
								{
									foreach($res_chk_all[$aa] as $row_chk_all)
									{
										$is_alrted = 1;
										$final_risk = $risk_cate = "";
										$risk_cate = $row_chk_all['risk_category'];
										$final_risk = explode(",",$risk_cate);
										if(in_array("Sanctions",$final_risk))
										{
											$cnt_sanc++; 
										}
										if(in_array("Enforcements",$final_risk))
										{
											$cnt_enf++; 
										}					
									}
								}
							}
						}
						if($is_alrted==1){
							$total_alerts++;
							$alerted_countries[$row['country']] +=1;
						}
					}	
					/*
echo "<pre>";
print_r($alerted_countries);	
echo "<br>total_alerts - $total_alerts";
	*/
					if($total_alerts>0)
					{
						$sum1 = 0;
						foreach($alerted_countries as $countryName => $alertCount)
						{													
							$countries[$sum1]['name'] = $countryName;
							$countries[$sum1]['y'] = ($alertCount/$total_alerts)*100;
							//$countries[$sum1]['color'] = '#'.random_color();
							//$countries[$sum1]['selected'] = 'true';
							$sum1++;
							
						}
						$max_count = 12;
						$max_percent = 21;
						if( count($alerted_countries) > $max_count ){
							$others_count = 0;
							$others_array = array();
							usort($countries, 'sortByOrder');
							
							$others_count = $total_alerts - $max_count;
							$others_y = 0;
							for( $others_index = 0; $others_y < $max_percent; $others_index++ ){
								$others_array[$others_index] = $countries[$others_index];
								$others_y += $countries[$others_index]['y'];
								unset($countries[$others_index]);
							}
							$new_count = count( $countries );
							$countries[$new_count]['name'] = "Others";
							$countries[$new_count]['y'] = $others_y;
							$countries[$new_count]['sliced'] = true;
							
							$countries = array_values($countries);							

							usort($countries, 'sortByName');					
						}						
						$country_data = json_encode($countries);
					}			
					?>					
					<script type="text/javascript">
						var country_pie_chart = new Highcharts.Chart({
							chart: {
								renderTo: 'country_hits'
							},
							title: {
								text: '% Hits by Country',
								verticalAlign: 'top',
								style: {
									fontSize: '15px',
									color: '#C2168E',
									fontWeight: 'bold',
									fontFamily: 'Verdana'
								}
							},
							tooltip: {
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
								}
							},
							plotOptions: {
								pie: {
									size:'85%',
									cursor: 'pointer',
									data: <?php echo $country_data ?>
								}
							},
							series: [{
								type: 'pie',
								name: '% Hits by Country',
								dataLabels: {
									verticalAlign: 'top',
									enabled: true,
									color: '#000000',
									connectorWidth: 1,
									distance: -30,
									connectorColor: '#000000',
									formatter: function () {
										return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
									}
								}
							}, {
								type: 'pie',
								name: '% Hits by Country',
								dataLabels: {
									enabled: false,
									color: '#000000',
									connectorWidth: 1,
									distance: 30,
									connectorColor: '#000000',
									formatter: function() {
										return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixedDown(2) +' %';
									}
								}
							}],
							credits: {
								enabled: false
							}
						});
						</script>
				
				</div>				
					
				
			</div>
		</div>
		<div id="news_alerts" style="width:270px;float:left;text-align:center;" class="no-print">
			<span style="color:#555;font-weight:400;font-size:23px;">Corruption Alerts</span>
			<div id="news_content" style="margin-left:20px;margin-top:18px;text-align:left;">
			<?php
			  	$rss = new DOMDocument();
				$rss->load('http://ethixbase.com/feed/');
				$feed = array();
				foreach ($rss->getElementsByTagName('item') as $node) {
					$item = array ( 
						'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
						'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
						'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
						'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
						);
					array_push($feed, $item);
				}
				$limit = 9;
				for($x=0;$x<$limit;$x++) {
					$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
					$link = $feed[$x]['link'];
					$description = $feed[$x]['desc'];
					$date = date('l F d, Y', strtotime($feed[$x]['date']));
					echo '<p style="margin-top:15px"><strong><a href="'.$link.'" target="_blank" title="'.$title.'">'.$title.'</a></strong><br />';
					echo '<small><em>Posted on '.$date.'</em></small></p>';
					
					$pos = strpos($description, '['); 
					$descs = substr($description, 0, $pos); 					
					$desc = substr(strstr($descs, ') '), strlen(') '));					
					echo '<p style="text-align:justify">'.trim($desc).'...</p>';
				}
			?>
				<div style="margin-top:0px;font-size:15px;color:#F15F22;text-align:justify;">
					<ul class="news_menu">
						<li><a href="http://ethixbase.com/ethical-alliance-daily-news/" target="_blank">News</a></li>
						<li><a href="http://ethixbase.com/ethical-alliance-forum-login/" target="_blank">Forum</a></li>
						<li class="nobullet"><a href="http://ethixbase.com/ethical-alliance-resource-centre/" target="_blank">Resources</a></li>
					</ul>
				</div>
			</div>			
		</div>
	</div>
	
</div>
<script src="js20/jquery-1.11.3.min.js"></script>

<script type="text/javascript">
	$('.header-lower').addClass('no-print');
</script>


<?php include('footer20.php'); ?>