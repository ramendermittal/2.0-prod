<?php
session_start();
error_reporting(0);

require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
@include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');

    $sql_pdfs = "SELECT st.*, st.inserted_date AS inserted_dates 
                  FROM risk_documents st
                  WHERE st.comp_id={$_SESSION['company_id']} AND id = {$_REQUEST['doc_id']}
                  ORDER BY inserted_date DESC";

    $res_pdfs = mysql_query($sql_pdfs);
    $row_pdfs=mysql_fetch_assoc($res_pdfs);

    header('Content-Description: File Transfer');
    header('Content-Type: application/force-download');
    header("Content-Disposition: attachment; filename=\"" . basename($row_pdfs['risk_doc']) . "\";");
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize(basename($row_pdfs['risk_doc'])));
    readfile($row_pdfs['risk_doc']); //showing the path to the server where the file is to be download
    exit;
?>