<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>ethiXbase</title>

<link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
<link href="css20/style.css" media="all" rel="stylesheet" />
<link href="css20/style-2.css" media="all" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="datatables/css/jquery.dataTables.min.css">
  <!-- <link rel="stylesheet" href="css20/jquery.qtip.min"> -->
  <script src="js20/jquery-1.11.3.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

    <header>
    <div class="header-lower">
      <nav class="container" role="navigation">
        <h1 class="logo">
          <a href="" class="navbar-brand"><img src="img20/ethixbase-logo.png" /></a>
        </h1>
        <div class="main-nav" style="display:none;">
          <ul class="nav navbar-nav navbar-right table-cell v-mid">
            
          <?php
          if($_SERVER['PHP_SELF']!='/due-diligence20.php' && $_SERVER['PHP_SELF']!='/due-diligence220.php' && $_SERVER['PHP_SELF']!='/ethixbase2/due-diligence320.php')
          {
          ?>
            <li><a data-toggle="tooltip" title="Start Again" href="due-diligence20" data-placement="bottom"><img src="img20/icon-header2-startover.png" /></a></li>
           <?php
          }
          if($_SERVER['PHP_SELF']=='/due-diligence420.php' || $_SERVER['PHP_SELF']=='/due-diligence420-sec.php')
          {
            ?>
            <li><a data-toggle="tooltip" title="Manage Third Parties" href="third-parties220.php" data-placement="bottom" class="animated pulse"><img src="img20/icon-header2-bars-pulse.png" /></a></li>
            <li><a data-toggle="tooltip" title="Order EDD Report" href="#" data-placement="bottom" class="animated pulse"><img src="img20/icon-header2-edd-pulse.png" /></a></li>
            <?php
          }
          else
          {
          ?>
            <li><a data-toggle="tooltip" title="Manage Third Parties" href="third-parties220.php" data-placement="bottom"><img src="img20/icon-header2-bars.png" /></a></li>
            <li><a data-toggle="tooltip" title="Order EDD Report" href="<?php echo APP_URL."enhanced_without.php"; ?>" data-placement="bottom"><img src="img20/icon-header2-edd.png" /></a></li>
           <?php
          }
          ?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="img20/icon-header2-user.png" /></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo APP_URL."change_password"; ?>">Change Password</a></li>
                <li class="divider"></li>
                <li><a href="#">Manage Account</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo APP_TURL."logout20"; ?>">Log Out</a></li>
              </ul>
            </li>
            <li><a data-toggle="tooltip" title="Help" href="faq20" data-placement="bottom"><img src="img20/icon-header2-question.png" /></a></li>
          </ul>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </nav>
    </div>
    </header>
    <div style="padding-top:80px;"></div> 
    	<div style="width:980px; margin-top:34px; margin-left:auto; margin-right:auto;">
            <div class="wrapper">
                <div style="width:260px; text-align:left; margin-right:20px; float:left; padding-top:20px;display:none">
                    <div style="padding:10px;">
                        <a href="img20/edd/EthixBase_EDD_Flyer_US.PDF" target="_blank">
                            <img src="img20/edd/ethixbase-360-Brochure.jpg" width="200" style="border:1px solid #999999;" />
                        </a>
                    </div>
                    <div style="padding:10px;">
                        <a href="img20/edd/ethiXbase360-Sample-Premium-Report.pdf" target="_blank">
                            <img src="img20/edd/ethiXbase360-Sample-Premium-Report.jpg" width="200" style="border:1px solid #999999;" />
                        </a>
                    </div>
                    <div style="padding:10px;">
                        <a href="img20/edd/ethiXbase360-Sample-Standard-Report.pdf" target="_blank">
                            <img src="img20/edd/ethiXbase360-Sample-Standard-Report.jpg" width="200" style="border:1px solid #999999;" />
                        </a>
                    </div>
                </div>
                <div style=" float:left; text-align:left; width:700px;margin-left:130px; ">
    				<script type="text/javascript" src="https://ethixbase.formstack.com/forms/js.php/enhanced_due_diligence_order_form_2_0"></script><noscript><a href="https://ethixbase.formstack.com/forms/enhanced_due_diligence_order_form_2_0" title="Online Form">Online Form - Enhanced Due Diligence order form - 2.0</a></noscript>
    				<input type="hidden" id="refreshed" value="no"> 
    			</div>
                <div class="clearfix"></div>
                <div class="clear"></div>
                <style>
                .section_footer {
                    text-align:left !important;
                }
                </style>
            </div>
    	</div>

        <script type="text/javascript">
        onload=function(){
        	var e=document.getElementById("refreshed");
        	if(e.value=="no"){
        		e.value="yes";
        	}else{
        		var form = $('form'); 
        		form[0].reset();
        		e.value="no";
        		location.reload();
        	}
        }
        </script>
    </body>
</html>