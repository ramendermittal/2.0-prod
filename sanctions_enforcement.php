<?php
error_reporting(0);
require_once 'include20/config20.php';
 include('header20.php');
?>
<style>
.td-word-wraps {
	overflow-wrap: break-word;
	word-wrap: break-word;
	-ms-word-break: break-all;
	word-break: break-all;
	-ms-hyphens: auto;
	-moz-hyphens: auto;
	-webkit-hyphens: auto;
	hyphens: auto;
}

#asd_filter
{
	display:none !important;
}
</style>
<div class="int-row-1 tp-row-1">
    <div class="container">
        <div class="text-left">
        	<h1>ethixbase Sanctions and Enforcements</h1>
        </div>
        <div class="text-left">
        	<p>
            	ethiXbase Basic Due Diligence reports check the company name and any ‘associated individual’ names provided by the client, along with those identified by ethiXbase as key individuals during the Company Registration search against the below list of sanctions and enforcements.
            </p>
            <p>&nbsp;</p>
        </div>
        <div class="row">
            <div class="col-md-12 pie-graph">
                <table class="table-02 table table-responsive" id="asd" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<td>Sanctions and Enforcement Lists</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$sql_chk = "SELECT * FROM sources_list WHERE is_active=1 AND is_type=1 AND is_display=1 ORDER BY is_order ASC";
					$con2 = mysql_connect(MYSQL_HOST2, MYSQL_USER2, MYSQL_PASSWORD2,true);
						if (!$con2) die('unable to connect to database2: '.mysql_error());
					$link2 = mysql_select_db(MYSQL_DB2,$con2) or die('unable to select database2: '.mysql_error());	
					$res_chk = mysql_query($sql_chk,$con2);
					if(mysql_num_rows($res_chk)>0)
					{
					while($row_chk=mysql_fetch_assoc($res_chk))
					{
						?>
                            <tr>
                                <td class="td-word-wraps">
                                    <?php echo $row_chk['source_name']; ?>
                                </td>
                            </tr>
                        <?php
						}
					}
					?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-left">
        	<p>&nbsp;</p>
        	<p>
                <strong>Note:</strong> Please note that an ethiXbase Basic Due Diligence Report does not screen a company or associated individual name against negative media, politically exposed persons lists or additional lists/data categories listed outside the above. Should you require a greater level of detail or in-depth investigation into this third party, please upgrade to one of our professional, enterprise or enhanced due diligence products by visiting the ethiXbase 2.0 platform.
            </p>
        </div>
    </div>  
</div>
<?php
  include('footer20.php');
?>