<?php include('header20.php');?>

<?php

	require_once 'include20/MysqliDb.php';
	$fdb = new MysqliDb;
	
	$errList = array(
		0 => false,
		1 => 'Invalid file content (mismatch columns count)',
		2 => 'Invalid file content (missing required information)',
	);
	$errCode = (int) $_REQUEST['errcode'];
	$errMsg = $errList[$errCode];
	
	$array_IDcount = 0;
	$json_IDs = 0;

?>

<div class="int-row-1 edd-row-1">

	<div class="text-center">
	<p>&nbsp;</p>
	<div class="steps-container hidden-xs hidden-sm">
	<div class="step-dot text-center">
	<?php if ($_REQUEST['formsubmit']): ?>
	<a href="javascript:history.back()"><img class="step-dot-0" src="img20/icsteps1g.png" /></a>
	<?php else: ?>
	<img class="step-dot-1" src="img20/icsteps1r.png" />
	<?php endif; ?>
	<p>Search</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center comp-only">
	<?php if ($_REQUEST['formsubmit']): ?>
	<img class="step-dot-1" src="img20/icsteps2r.png" />
	<?php else: ?>
	<img class="step-dot-0" src="img20/icsteps2g.png" />
	<?php endif; ?>
	<p>Review Company Results</p>
	</div>
	
	<!-- individual icon -->
	<div class="step-dot text-center ind-only" style="display:none;">
	<img class="step-dot-0" src="img20/icsteps3g.png" />
	<p>Review Individual Results</p>
	</div>
	<!-- individual icon -->
	
	<div class="step-line line-hide">
	&nbsp;
	</div>
	<div class="step-dot text-center comp-only">
	<img class="step-dot-0" src="img20/icsteps3g.png" />
	<p>Associated Individuals</p>
	</div>
	
	<!-- individual icon -->
	<div class="step-dot text-center ind-only" style="display:none;">
	<img class="step-dot-0" src="img20/icsteps2g.png" />
	<p>Review Associated Companies</p>
	</div>
	<!-- individual icon -->
	
	<div class="step-line line-hide">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<img class="step-dot-0" src="img20/icsteps4g.png" />
	<p>Review Results &amp; Download Report</p>
	</div>
	</div>
	</div>
	<?php 
	if ($_REQUEST['formsubmit']): ?>
		<div class="form-section">
			<form class="form-area-1 form-01-1" action="" method="post" name="form3">
				
				<input type="hidden" name="entity_type" value="<?php echo $_REQUEST['entity_type']; ?>">
				<input type="hidden" id="thecountry" name="country" value="<?php echo $_REQUEST['country']; ?>">
				<input type="hidden" id="search" name="search" value="<?php echo $_REQUEST['search']; ?>">
				<input type="hidden" id="searchTemp" name="searchTemp" value="<?php echo $_REQUEST['search']; ?>">
                <input type="hidden" name="is_idd" id="is_idd" value="<?php echo $_REQUEST['is_idd']; ?>">
				<input type="hidden" name="sch" value="1">
				<input type="hidden" name="alt" id="alt" value="0">
				<input type="hidden" name="formsubmit" value="formsubmit">
				
				<div class="container">
					<div class="row">
						<div class="col-xs-12">

							<?php

							//preVar($_REQUEST);
							require_once('include20/OcFunctions.php');
							$search = trim($_REQUEST['search']);
							$jurisdiction = trim($_REQUEST['country']);
							$country_searched = $jurisdiction ? countryName($jurisdiction, true) : 'all countries';
							$page = $_REQUEST['pageno'] ? (int) $_REQUEST['pageno'] : 1;
							$page_alt = $_REQUEST['pageno_alt'] ? (int) $_REQUEST['pageno_alt'] : 1;
							
							if ($_REQUEST['entity_type'] == 'company')
							{
								$selectionkey = 'company_number';
								$label1 = 'Registered Company</br>Name';
								$label2 = 'Company';
								$label3 = 'company';
								$page_all = $jurisdiction ? $page_alt : $page;

								$res1 = OcSearchCompanies($search, '', false, $pgdetails_all, $page_all);
								if ($jurisdiction)
								{
									$records = OcSearchCompanies($search, $jurisdiction, false, $pgdetails, $page);
									$records_alt = $res1;
									$pgdetails_alt = $pgdetails_all;
								}
								else
								{
									$records = $res1;
									$pgdetails = $pgdetails_all;
									$records_alt = array();
									$pgdetails_alt = array();
								}
								$cnt=count($records);
								
							}

							if ($_REQUEST['entity_type'] == 'individual')
							{
								$selectionkey = 'id';
								$label1 = 'Individual Name';
								$label2 = 'Individual';
								$label3 = 'individual';
								$page_all = $jurisdiction ? $page_alt : $page;

								$res1 = OcSearchOfficers($search, '', false, $pgdetails_all, $page_all);
								if ($jurisdiction)
								{
									$records = OcSearchOfficers($search, $jurisdiction, false, $pgdetails, $page);
									$records_alt = $res1;
									$pgdetails_alt = $pgdetails_all;
								}
								else
								{
									$records = $res1;
									$pgdetails = $pgdetails_all;
									$records_alt = array();
									$pgdetails_alt = array();
								}
								
							}
							
							$page = (int) $pgdetails['page'];
							$per_page = (int) $pgdetails['per_page'];
							$total_count = (int) $pgdetails['total_count'];
							$total_pages = (int) $pgdetails['total_pages'];

							$page_alt = (int) $pgdetails_alt['page'];
							$per_page_alt = (int) $pgdetails_alt['per_page'];
							$total_count_alt = (int) $pgdetails_alt['total_count'];
							$total_pages_alt = (int) $pgdetails_alt['total_pages'];
							
							$fdb->where('jcode', $jurisdiction);
							$ct = $fdb->getOne('idd_oc_collections');
							$jrcount = (int) $ct['total_record'];

							?>

							<strong>Your search:</strong><br><br>
							
							<table width="100%">
								<tr>
									<td>
										<p>
										<?php echo $label2; ?> name searched: <strong><?php echo $search; ?></strong>
										<br>
										Country/Territory of Registration selected: <strong><?php echo strtoupper($country_searched); ?></strong>
										</p>
									</td>
									
								</tr>
							</table>
							
							<table>
								<tr>
									<td style="padding-right:25px;">
										<p style="width: 540px;">Please click "<strong>Proceed with my original search</strong>" to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a>) on the entity above <span style="text-decoration: underline;">without</span> <?php echo $label3; ?> registry information.</p>
									</td>
									<td valign="top">
										<div class="btn-next">
											<button type="button" class="btn-submit wsubmit" style="white-space:nowrap;" value="b1">Proceed with my<br>original search<span class="glyphicon glyphicon-menu-right"></span></button>
										</div>
									</td>
								</tr>
							</table>

							<?php if (count($records) || count($records_alt)): ?>
							
								<br>
								<table width="100%">
									<tr>
										<td style="padding-right:25px;">
											<p><strong>Online Registry Matches in <?php echo strtoupper($country_searched); ?></strong></p>
											
											<?php if ($records): ?>
											
												<?php if ($jurisdiction && $jrcount): ?>
												<p style="width: 540px;">A search of over <?php echo $jrcount; ?> records in <?php echo $country_searched; ?>’s online company records has identified the below potential registry matches. Please select the most relevant record before clicking "<strong>Proceed with listed location</strong>" to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a> and company registry information).</p>
												<?php else: ?>
												<p style="width: 540px;">A search of over 90 million online company records sourced direct from more than 300 official company registration offices, in 54 countries has identified the below potential registry matches. Please select the most relevant company record before clicking "<strong>Proceed with listed location</strong>" to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a> and company registry information).</p>
												<?php endif; ?>
												
											<?php else: ?>
											
												<?php if ($jrcount): ?>
												<p style="width: 540px;">ethiXbase has been unable to locate company registry information for "<?php echo $search; ?>" amongst <?php echo $jrcount; ?> company registry records in <?php echo $country_searched; ?>. "<?php echo $search; ?>" may still be a registered company in <?php echo $country_searched; ?>, however not listed amongst that country's online registry records.</p>
												<?php else: ?>
												<p style="width: 540px;">Company registry information for <?php echo $country_searched; ?> is currently not available online. An Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a>) can still be obtained from the system based on the company name and any associated individuals provided. Please select "<strong>Proceed with my original search</strong>" to continue.</p>
												<?php endif; ?>
											
											<?php endif; ?>
											
										</td>
										
									</tr>
								</table>
								
								<?php if ($records): ?>
									<table class="table table-responsive" id="tb1" style="border-bottom:none;border-collapse:collapse;">
										
										<thead>
											<tr class="t-head">
												<td style="border-bottom:none;"><?php echo $label1; ?></td>

												<?php if ($_REQUEST['entity_type'] == 'individual'): ?>
												<td style="border-bottom:none;">Registered Company</br>Name</td>
												<?php endif; ?>

												<td style="border-bottom:none;">Registered Country</td>

												<?php if ($_REQUEST['entity_type'] == 'company'): ?>
												<td style="border-bottom:none;">Registration Status</td>
												<?php endif; ?>

												<td style="border-bottom:none;width:50px;">Select</td>
												<td valign="top">
													<div class="btn-next">
														<button type="button" class="btn-submit wsubmit" style="white-space:nowrap;" value="b2">Proceed with listed<br>company (select)<span class="glyphicon glyphicon-menu-right"></span></button>
													</div>
												</td>
											</tr>
										</thead>

										<tbody id="tbody1">
											<?php foreach($records as $r): ?>
											<tr>
												<td style="border:1px solid #9a9c9e;width:800px;"><?php echo $r['name']; ?></td>

												<?php if ($_REQUEST['entity_type'] == 'individual'): ?>
												<td style="border:1px solid #9a9c9e;"><?php echo $r['company']['name']; ?></td>
												<?php endif; ?>

												<td style="text-align:center;border:1px solid #9a9c9e;"><?php echo strtoupper(countryName($r['jurisdiction_code'],true)); ?></td>

												<?php if ($_REQUEST['entity_type'] == 'company'): ?>
												<td style="text-align:center;border:1px solid #9a9c9e;"><?php echo strtoupper($r['current_status']); ?></td>
												<?php endif; ?>

												<td style="text-align:center;border:1px solid #9a9c9e;">
												<input type="checkbox" class="cbsel cbsel1" ctr="<?php echo $r['jurisdiction_code']; ?>" name="selections[<?php echo $r[$selectionkey]; ?>]" value="<?php echo base64_encode($r['name']); ?>">
												</td>
												<td></td>
											</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
									
									<?php if ($total_pages > 1): ?>
										<div id="plist1" style="margin-top:5px;">
										<?php echo pgHtml($page,$total_pages,false); ?>
										</div>
										<input type="hidden" name="tpage" value="<?php echo $total_pages; ?>">
									<?php endif; ?>
								<?php endif; ?>
								<br>
							<?php endif; ?>
							<?php if (count($records_alt)): ?>
								<table>
									<tr>
										<td style="padding-right:25px;">
											<p><strong>Potential Online Registry Matches in other locations</strong></p>
											<?php if ($records): ?>
											<p style="width: 540px;">A search of over 90 million online company records sourced direct from more than 300 official company registration offices, in 54 countries has identified the below potential registry matches outside of <?php echo $country_searched; ?>. Please select the most relevant company record before clicking "<strong>Proceed with alternate location</strong>" to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a> and company registry information).</p>
											<?php else: ?>
											<p style="width: 540px;">Potential company registry details in alternate jurisdictions which may be of interest appear below. Should you wish to run an Instant Due Diligence report on one of these entities please do just select this before clicking "<strong>Proceed with alternate location</strong>". Alternatively please click "<strong>Proceed with my original search</strong>" without making a selection to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a>) on the entity searched <span style="text-decoration: underline;">without</span> company registry information.</p>
											<?php endif; ?>
										</td>
										<td>
										<td valign="top">
											<div class="btn-next">
												<button type="button" class="btn-submit wsubmit" style="white-space:nowrap;" value="b3">Proceed with alternate<br>location (select)<span class="glyphicon glyphicon-menu-right"></span></button>
											</div>
										</td>
										</td>
									</tr>
								</table>
								<table class="table table-responsive" id="tb2" style="border-bottom:none;border-collapse:collapse;">
									<thead>
										<tr class="t-head">
											<td style="border-bottom:none;"><?php echo $label1; ?></td>
											<?php if ($_REQUEST['entity_type'] == 'individual'): ?>
											<td style="border-bottom:none;">Registered Company</br>Name</td>
											<?php endif; ?>
											<td style="border-bottom:none;">Registered Country</td>
											<?php if ($_REQUEST['entity_type'] == 'company'): ?>
											<td style="border-bottom:none;">Registration Status</td>
											<?php endif; ?>
											<td style="border-bottom:none;width:50px;">Select</td>
											<td style="border-bottom:none;">
												<div class="btn-next" style="visibility:hidden;">
													<button class="btn-submit" style="white-space:nowrap;">Next <span class="glyphicon glyphicon-menu-right"></span></button>
												</div>
											</td>
										</tr>
									</thead>
									<tbody id="tbody2">
										<?php $flag=0;foreach($records_alt as $r): ?>
										<?php if ($r['jurisdiction_code'] == $jurisdiction) continue; ?>
										<tr>
											<td style="width:600px;border:1px solid #9a9c9e;"><?php echo $r['name']; ?></td>
											<?php $flag=1; if ($_REQUEST['entity_type'] == 'individual'): ?>
											<td style="border:1px solid #9a9c9e;"><?php echo $r['company']['name']; ?></td>
											<?php endif; ?>
											<td style="text-align:center;border:1px solid #9a9c9e;"><?php echo strtoupper(countryName($r['jurisdiction_code'],true)); ?></td>
											<?php if ($_REQUEST['entity_type'] == 'company'): ?>
											<td style="text-align:center;border:1px solid #9a9c9e;"><?php echo strtoupper($r['current_status']); ?></td>
											<?php endif; ?>
											<td style="text-align:center;border:1px solid #9a9c9e;">
												<input type="checkbox" class="cbsel cbsel2" ctr="<?php echo $r['jurisdiction_code']; ?>" name="selections[<?php echo $r[$selectionkey]; ?>]" value="<?php echo base64_encode($r['name']); ?>">
											</td>
											<td></td>
										</tr>
										<?php endforeach; ?>
										<?php if ( $flag==0): ?>
											  <tr>
								
													<td colspan="3" style="text-align:center !important;"><?php echo "No Records Found"; ?></td></tr>
											  <?php endif; ?>
									</tbody>
								</table>
								<?php if ($total_pages_alt > 1): ?>
									<div id="plist2" style="margin-top:5px;">
									<?php echo pgHtml($page_alt,$total_pages_alt,true); ?>
									</div>
									<input type="hidden" name="tpagealt" value="<?php echo $total_pages_alt; ?>">
								<?php endif; ?>
							<?php endif; ?>
							<?php if (!count($records) && !count($records_alt)): ?>
								<br>
								<table width="100%">
									<tr>
										<td style="padding-right:25px;">
											<p><strong>Potential Online Registry Matches in <?php echo strtoupper($country_searched); ?></strong></p>
											<p style="width: 540px;">Following a search of over 90 million online registry records from over 300 offices in 54 countries we have been unable to locate any potential matches for "<strong><?php echo $search; ?></strong>".</p>
											<p>Please click "<strong>Proceed with my original search</strong>" to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a>).</p>
										</td>
										<td valign="top">
											<div class="btn-next" style="visibility:hidden;">
												<button class="btn-submit" style="white-space:nowrap;">Next <span class="glyphicon glyphicon-menu-right"></span></button>
											</div>
										</td>
									</tr>
								</table>
								<input type="hidden" name="selections[0]" value="<?php echo base64_encode($search); ?>">
								<input type="checkbox" class="cbsel cbselskip" style="display:none;" checked>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php else: ?>
    	<style>
		.switch {
			position: relative;
			display: block;
			vertical-align: top;
			width: 95px;
			height: 23px;
			padding: 3px;
			margin: 0 10px 10px 0;
			background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
			background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
			border-radius: 18px;
			box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
			cursor: pointer;
		}
		.switch-input {
			position: absolute;
			top: 0;
			left: 0;
			opacity: 0;
		}
		.switch-label {
			position: relative;
			display: block;
			height: inherit;
			font-size: 16px;
			text-transform: uppercase;
			background: #eceeef;
			border-radius: inherit;
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
			border:1px solid #999999;
		}
		.switch-label:before, .switch-label:after {
			position: absolute;
			top: 50%;
			margin-top: -.5em;
			line-height: 1;
			-webkit-transition: inherit;
			-moz-transition: inherit;
			-o-transition: inherit;
			transition: inherit;
		}
		.switch-label:before {
			content: attr(data-off);
			right: 25px;
			color: #333333;
			text-shadow: 0 1px rgba(255, 255, 255, 0.5);
		}
		.switch-label:after {
			content: attr(data-on);
			left: 25px;
			color: #FFFFFF;
			text-shadow: 0 1px rgba(0, 0, 0, 0.2);
			opacity: 0;
		}
		.switch-input:checked ~ .switch-label {
			background: #409995;
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
		}
		.switch-input:checked ~ .switch-label:before {
			opacity: 0;
		}
		.switch-input:checked ~ .switch-label:after {
			opacity: 1;
		}
		.switch-handle {
			position: absolute;
			top: 5px;
			left: 3px;
			width: 20px;
			height: 20px;
			background: linear-gradient(to bottom, #ac3958 40%, #ac3958);
			background-image: -webkit-linear-gradient(top, #ac3958 40%, #ac3958);
			border-radius: 100%;
			box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
		}
		.switch-handle:before {
			content: "";
			position: absolute;
			top: 50%;
			left: 50%;
			margin: -6px 0 0 -6px;
			width: 12px;
			height: 12px;
			background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
			background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
			border-radius: 6px;
			box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
		}
		.switch-input:checked ~ .switch-handle {
			left: 71px;
			box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
		}
		 
		/* Transition
		========================== */
		.switch-label, .switch-handle {
			transition: All 0.3s ease;
			-webkit-transition: All 0.3s ease;
			-moz-transition: All 0.3s ease;
			-o-transition: All 0.3s ease;
		}


		.optionGroup
		{
		    font-weight:bold;
		    font-style:italic;
		}

		/*.optionChild
		{
		    padding-left:15px;
		}*/
		</style>
		<div class="form-section">
			<div class="container">
				<div class="row">
					<form class="form-area-1 form-01" action="" method="post" name="form1" onSubmit="return fsubmit();">
					<input type="hidden" name="formsubmit" value="formsubmit">
						<div class="col-xs-12">
							<table class="tb-form">
								<tr>
									<td></td>
									<td class="text-a-left">
										<label for="radio" class="radio-btn">
											<input type="radio" id="company" name="entity_type" value="company" checked class="search-radio ent_rd"><label for="company"><span><span></span></span>Company</label>
											<input type="radio" id="individual" name="entity_type" value="individual" class="search-radio ent_rd"><label for="individual"><span><span></span></span>Individual</label>
										</label>
									</td>
									<td></td>
								</tr>
								<tr>
									<td><label for="search">Search:</label></td>
									<td><input type="text" id="search" name="search" value="" data-toggle="collapse" title="Please Enter Full company or individual name for your search" ></td>
									<td>
										<div class="btn-next">
											<button class="btn-submit">Next <span class="glyphicon glyphicon-menu-right"></span></button>
										</div>
									</td>
								</tr>
								<tr id="country_row">
									<td><label for="country">Country/Territory of Registration: </label></td>
									<td>
										<select name="country" id="country" data-toggle="modal" title="Please select the country or territory of registration of this company. If you are not sure of this, you can search 'All'">
										<option value="">-- All --</option>
										<?php
										$fdb->orderBy('country', 'asc');
										$oc_countries = $fdb->get('oc_countries', null, 'code,country');
										$oc_states = $fdb->get('oc_states', null, 'code,state,country_code');

										$tmp = array();
										foreach ($oc_states as $s) $tmp[$s['country_code']][] = $s;
										$oc_states = $tmp;

										foreach ($oc_countries as $c)
										{
											if (isset($oc_states[$c['code']]))
											{
												// echo '<optgroup label="'.$c['country'].' - Select state:">';
												echo '<option value="'.$c['code'].'" class="optionGroup">'.$c['country'].'</option>';
												foreach ($oc_states[$c['code']] as $s)
												{
													echo '<option value="'.$s['code'].'" class="optionChild">&nbsp;&nbsp;&nbsp;'.$s['state'].'</option>';
												}
												// echo '</optgroup>';
											}
											else
											{
												//filters out weird countries
												if (strlen($c['country']) > 30) continue;

												$countrylbl = strlen($c['country']) > 30 ? substr($c['country'],0,30).'..' : $c['country'];
												echo '<option value="'.$c['code'].'">'.$countrylbl.'</option>';
											}
										}
										?>
										</select>
									</td>
									<td></td>
								</tr>
								<tr id="search_asso_row" style="display:none;">
									<td>
										<label for="search_asso">Search Associated Companies
										<sup>
											<img data-container="body" src="img20/icon-info.png" data-toggle="tooltip" title="If you would like to search this individual in association with listed companies please select Yes. If you would like to search this individual without associated companies, please select No" />
										</sup>: </label>
									</td>
									<td class="text-a-left">
										<label for="radio" class="radio-btn" >
											<input type="radio" id="search_asso1" name="search_asso" value="1" checked class="search-radio search_asso_rd"><label for="search_asso1" ><span><span></span></span>Yes</label>
											<input type="radio" id="search_asso0" name="search_asso" value="0" class="search-radio search_asso_rd"><label for="search_asso0" ><span><span></span></span>No</label>
										</label>
									</td>
									<td></td>
								</tr>
                                <tr>
									<td colspan="3" style="text-align:left; padding-left:117px;">
										<div style="text-align:left;">
                                        	<div style="float:left; margin-top:8px; margin-right:10px;margin-left: 1px;">
                                            	Run an <a href="<?php echo APP_URL."order_idd_basic_info"; ?>">IDD Report</a> (FREE)
                                            </div>
                                        	<div style="float:left;">
                                                <label class="switch">
                                                    <input class="switch-input" id="switch-input" type="checkbox" onclick="get_switch();" />
                                                    <span class="switch-label" data-on="" data-off=""></span> 
                                                    <span class="switch-handle"></span> 
                                                </label>
                                            </div>
                                            <div style="float:left; margin-top:8px; margin-right:3px;">
											<?php
												$vendor_info = array("id" => 0, "total_credits" => 0, "used_credits" => 0);
												$vendors_info = mysql_query("SELECT * FROM vendor_edu WHERE FIND_IN_SET({$_SESSION['member_id']}, access_user_id) > 0");
												if(mysql_num_rows($vendors_info) > 0){
													$vendor_info = mysql_fetch_array($vendors_info);
												}
												$remaining_credits = $vendor_info['total_credits'] - $vendor_info['used_credits'];
													?>
                                            	Order a <a href="<?php echo APP_URL."order_idd_info"; ?>">IDD<sup>+</sup> Report</a> US$59<br>(<?php echo $remaining_credits ;?> Remaining Credits) 
												
                                            </div>
                                            <!--div style="float:left;">
                                            	<a data-toggle="tooltip" title="IDD+ Report or IDD Report" href="#"  data-placement="bottom"><span class="glyphicon glyphicon-info-sign"></span></a>
                                            </div-->
                                        </div>
                                        <input type="hidden" value="" name="is_idd" id="is_idd" />
                                    </td>
									<td></td>
								</tr>
							</table>
						</div>
					</form>
				</div>

<div class="gray_header_overlay"></div> 
<div class="gray_overlay"></div>

<div class="modal-confirm-left">
	<p style="font-weight: bold;">Welcome to the ethiXbase 2.0 Platform</p>
	<p style="">
		Conducting instant due diligence and managing third parties with <br>
		ethiXbase is simple. Either search a <b>single third party name</b> or <br>
		upload a <b>batch file</b> of your suppliers, vendors, distributors, <br>
		partners and agents using our <a href="ethiXbase_Data_Template.xls">third party upload template.</a>
	</p>
	<div class="action" style="margin-top:25px;text-align: center;">
		<a href="#" class="btn btn-default btn-yes" role="button" style="font-weight: bold;">Get Started ></a>
	</div>
	<p style="margin-top:10px;text-align: center;">
	<input type="checkbox" id="removed_pop_up" name="removed_pop_up" value="0" class="cbsel"> <label for="removed_pop_up"> Do not show me this message again</label>
</div>

<div id="batch_upload_modal" class="modal-batch-upload" style="">
	<p style="text-align: center;font-size: 25px;font-weight: 550;">Batch Upload In Progress</p>
	<p style="margin-top:20px;line-height: 160%;">
		Thank you for uploading your third parties! Your batch file is now being processed. <br>
		Depending on the size of your file this may take some time. Once the report is <br> 
		ready for review, we will send you an email.
	</p>
	<p style="margin-top:20px;padding-bottom: 13px;">
		You can also view the progress of your upload below.
	</p>
	
	  <div class="progress progress-striped active" style="border:1px solid #8cc540;height:20px;border-radius:10px;">
            <div class="progress-bar" style="width:0%;background-color:#8cc540;height:20px;border-radius:10px;">&nbsp;
            </div>
	  </div>
	<!-- Progress bar holder -->
	<!-- <div id="progress" style="border:1px solid #8cc540;height:20px;border-radius:10px;"></div> -->
	<!-- Progress information -->
	<!-- <div id="information" style="padding-bottom: 20px;padding-top: 8px;padding-left: 8px;"></div> -->

	<div style="display:none;float:left;cursor: pointer; cursor: hand; border:1px solid #AB3E5B; background-color:#AB3E5B; height:38px; width:210px;border-radius:15px;padding-top:7px;margin-left:50px;" id="view_third_parties">
		<a title="View My Third Parties" style="text-decoration:none;" href="javascript:redirectWUpload('mthirdp');" >
			<p style="vertical-align:central; text-align:center;color:#FFFFFF;font-size:14px;">View My Third Parties</p>
		</a>
	</div>
	<div style="display:none;float:left;cursor: pointer; cursor: hand; border:1px solid #AB3E5B; background-color:#AB3E5B; height:38px; width:210px;border-radius:15px;padding-top:7px;margin-left:35px;" id="return_to_search">
		<a title="Return to Search" style="text-decoration:none;" href="javascript:redirectWUpload('returns');" >
			<p style="vertical-align:central; text-align:center;color:#FFFFFF;font-size:14px;">Return to Search</p>
		</a>
	</div>
</div>

<input type="hidden" name="id_count" id="id_count" value="0">
<input type="hidden" name="orgId" id="orgId" value="0">
<input type="hidden" name="srch_id" id="srch_id" value="0">
<input type="hidden" name="srch_tpi" id="srch_tpi" value="0">
				<?php 

					if (isset($_REQUEST['csvsubmit']))
					{
						ini_set('memory_limit','2048M');
						set_time_limit(0);
						echo '<input type="hidden" name="process_xls" id="process_xls" >';
						require_once('include20/readExcel.php');
						$srch_tpi=0;
						$srch_asso_id=0;
						$entity_type = 'company';
						$file = $_FILES['csvfile']['tmp_name'];
						$filetype = pathinfo($_FILES['csvfile']['name'], PATHINFO_EXTENSION);
						
						$rows = false;
						if ($filetype == 'xls' || $filetype == 'xlsx') $rows = readExcel($file);
						if ($filetype == 'csv') $rows = readCsv($file);

						$array_IDcount = count($rows);
						$json_IDs = json_encode($rows);

						// echo "<br>here1:";
						// echo "<pre>";
						// print_r($rows);
						// echo "</pre>";
						// echo "<br>here2:";
						// echo "<pre>";
						// print_r($json_IDs);
						// echo "</pre>";
						// exit();
					}

				?>
				<form name="form2" id="form2" action="" method="post" enctype="multipart/form-data" onSubmit="return fsubmit2();">
					<div class="form-area-2 form-01" action="#" id="bulk_frm">
						<h3>Would you like to add more than one third party at a time, or add all of your third parties in one upload? Please simply complete our <a href="ethiXbase_Data_Template.xls">Third Party Upload Template</a> for batch processing.</h3>
						<h3>You can upload excel files up to 50 MB in size. <a href="ethiXbase_Data_Template.xls">Download Template</a></h3>
						<h3>Should you need any assistance please just <a href="email-support">contact support</a>.</h3><br>

						<input type="file" name="csvfile">
						<input type="hidden" name="csvsubmit" value="csvsubmit">
						<div><button class="submit">Upload</button></div>
					</div>
				</form>

				<?php if ($errMsg): ?>
				<div class="warning">
					<i class="icon"></i>
					An error has occurred while trying to upload your file. <br> 
					Please use the ethiXbase template and try again, or contact customer support XXXXXXXX
				</div>
				<?php endif; ?>
				
			</div>
		</div>
	<?php endif; ?>
	
</div>



<script type="text/javascript">

var tp = '<?php echo $_REQUEST['entity_type']; ?>';
var selected_country = '<?php echo $_REQUEST['country']; ?>';
var account_type = '<?php echo $_SESSION['v2_account']; ?>';
if (account_type == '') account_type = 'free';

function cbClick()
{
	$(".cbsel").click(function(e)
	{
		chk = $(this).prop('checked');
		$(".cbsel").prop('checked', false);
		$(this).prop('checked', chk);
		thecountry = $(this).attr('ctr');
		//alert(thecountry);
		$("#thecountry").val(thecountry);
	});
}

function pgClick()
{
	$('.pagebox').click(function()
	{
		var pgn = $(this).attr('pg');
		$("#pageno").val(pgn);
		$("#alt").val('0');
		pageChange($('#tbody1'),$('#plist1'),$('#tb1'));
	});
	$('.pageboxalt').click(function()
	{
		var pgn = $(this).attr('pg');
		$("#pageno_alt").val(pgn);
		$("#alt").val('1');
		pageChange($('#tbody2'),$('#plist2'),$('#tb2'));
	});
}

$(document).ready(function()
{
	$('.ent_rd').click(function()
	{
		if ($('input[name="entity_type"]:checked').val() == 'individual')
		{
			$("#country_row").hide();
			$("#search_asso_row").show();
			$("#form2").hide();
		}
		else
		{
			$("#country_row").show();
			$("#search_asso_row").hide();
			$("#form2").show();
		}
		navIcons();
	});
	
	$('.search_asso_rd').click(function()
	{
		navIcons();
	});
	
	$('.wsubmit').click(function()
	{
		fsubmit3('<?php echo $_REQUEST['entity_type']; ?>', $(this).val());
	});
	
	if(localStorage.getItem('popState') != 'shown' && localStorage.getItem('dont_show') != 'no')
	{
		//show popup here	
		start_pop_out();
		$('.modal-confirm-left .action a.btn-yes').click(function()
		{
			if($("#removed_pop_up").is(':checked'))
			{
				localStorage.setItem('dont_show','no'); 
				//console.log("yes it is");
			}
			else
			{
				localStorage.setItem('dont_show','yes'); 
				//console.log("NO it isn't");
			}
			$('.gray_overlay, .gray_header_overlay, .modal-confirm-left').fadeOut();
			localStorage.setItem('popState','shown'); 
		});
	}
	
	pgClick();
	cbClick();
});

function navIcons()
{
	$('.steps-container').css('padding-left', '');
	var f1 = $('input[name="entity_type"]:checked').val();
	var f2 = document.forms['form1'].elements['search_asso'].value;
	
	if (f1 == 'company')
	{
		$(".comp-only").show();
		$(".ind-only").hide();
		$(".line-hide").show();
	}
	if (f1 == 'individual')
	{
		$(".comp-only").hide();
		if (f2 == '0')
		{
			$('.steps-container').css('padding-left', '90px');
			$(".ind-only").hide();
			$(".line-hide").hide();
		}
		if (f2 == '1')
		{
			$(".ind-only").show();
			$(".line-hide").show();
		}
	}
}

function fsubmit()
{
	if (document.forms['form1'].elements['search'].value.trim() == '')
	{
		alert('Please enter search text');
		return false;
	}
	
	if (document.forms['form1'].elements['entity_type'].value == 'company') document.forms['form1'].action = '';
	
	if (document.forms['form1'].elements['entity_type'].value == 'individual')
	{
		if (document.forms['form1'].elements['search_asso'].value == '0')
		{
			ind1 = '<input type="hidden" name="selections[]" value="' + document.forms['form1'].elements['search'].value + '"><input type="checkbox" class="cbsel" style="display:none;" checked><input type="hidden" name="noencode" value="1">';
			$(ind1).insertAfter("#search_asso_row");
			if (document.forms['form1'].elements['is_idd'].value == '1')
			{
				document.forms['form1'].action = 'due-diligenceidd.php?type=1&sac=0';
			}
			else
			{
				document.forms['form1'].action = 'due-diligence420.php?type=1&sac=0';
			}
		}
		if (document.forms['form1'].elements['search_asso'].value == '1')
		{
			document.forms['form1'].action = 'due-diligence620.php?type=1&sac=1';
		}
	}
	
	return true;
}

function fsubmit2()
{
	var upf = document.forms['form2'].elements['csvfile'].value;
	if (upf == '')
	{
		alert('Please select a file');
		return false;
	}
	else if (!endsWith(upf,'.xls') && !endsWith(upf,'.xlsx'))
	//else if (!endsWith(upf,'.csv'))
	{
		alert('Invalid file type. Only Excel file is accepted');
		//alert('Invalid file type. Only CSV file is accepted');
		return false;
	}
	// start_progress_bar();
	return true;
}

function fsubmit3(entity_type, wbtn)
{
	if (wbtn == 'b1')
	{
		if ($(".cbselskip").length == 1) {}
		else
		{
			if (!confirm('Are you sure want to proceed without selecting any '+ entity_type +' from the search result?'))
			{
				return false;
			}
			$('.cbsel').removeAttr('checked');
			$("#thecountry").val(selected_country);
			nocbselect = '<input type="hidden" name="selections[0]" value="<?php echo base64_encode($search); ?>"><input type="checkbox" class="cbsel" style="display:none;" checked>';
			$(nocbselect).insertAfter("#searchTemp");
		}
	}
	
	if (wbtn == 'b2')
	{
		if ($(".cbsel1:checked").length != 1)
		{
			alert('Please select one of the search results');
			return false;
		}
	}
	
	if (wbtn == 'b3')
	{
		if ($(".cbsel2:checked").length != 1)
		{
			alert('Please select one of the search results');
			return false;
		}
	}
	
	if (entity_type == 'company') actionphp = "due-diligence220.php";
	if (entity_type == 'individual') actionphp = "due-diligence420.php";
	
	document.forms['form3'].action = actionphp;
	document.forms['form3'].submit();
	return true;
}

function pageChange(e,p,t)
{
	$(".cbsel").unbind("click");
	$('.pagebox').unbind("click");
	$('.pageboxalt').unbind("click");
	
	$("#thecountry").val(selected_country);
	$(e).fadeTo(0,0.1);
	$(p).fadeTo(0,0.1);
	
	$.ajax({
		method: "POST",
		url: "include20/OcFunctions.php",
		data: $(document.forms['form3']).serialize()+'&pageChange=1'
	})
	.done(function(msg) {
		//alert(msg);
		$(e).html(msg);
		$(e).fadeTo(0,1);
		cbClick();
		
		$('html, body').animate({
			scrollTop: $(t).offset().top-100
		}, 200);
	});
	
	$.ajax({
		method: "POST",
		url: "include20/OcFunctions.php",
		data: $(document.forms['form3']).serialize()+'&pageNumChange=1'
	})
	.done(function(msg) {
		//alert(msg);
		$(p).html(msg);
		$(p).fadeTo(0,1);
		pgClick();
	});
	
	return false;
	
	document.forms['form3'].action = '';
	document.forms['form3'].submit();
}

function endsWith(str, suffix)
{
	return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function start_pop_out(){	
	$('.gray_overlay, .gray_header_overlay, .modal-confirm-left').fadeIn();
	return false;
}

function start_progress_bar(){	
	$('.gray_overlay, .gray_header_overlay, .modal-batch-upload').fadeIn();
	return false;
}

function get_switch()
{
	if((document.getElementById('is_idd').value=='') || (document.getElementById('is_idd').value==0))
	{
		document.getElementById('is_idd').value = "1";
		document.forms['form1'].action = 'due-diligenceidd.php';
		document.getElementById('bulk_frm').style.display="none";
	}
	else
	{
		document.getElementById('is_idd').value = "0";
		document.getElementById('bulk_frm').style.display="block";
	}
}

if($('#process_xls').length){

	function doAjax(i, orgId, srch_id, srch_tpi) {
	start_progress_bar();
		// var dataToSend = {'id':i, 'orgId':orgId, 'srch_id':srch_id, 'srch_tpi':srch_tpi, 'json_IDs' : <?php echo $json_IDs; ?> };		
		var dataToSend = {'id':i, 'orgId':orgId, 'srch_id':srch_id, 'srch_tpi':srch_tpi, 'json_IDs' : "<?php echo addslashes(serialize($json_IDs)); ?>" };
	    $.ajax({  
	        'url': "dataHandler.php",
			'type': 'POST',
			'data': dataToSend
	    }).done(function(returnData){
	    	returnData = JSON.parse(returnData);
	    	$("#id_count").val(i);
	    	$("#orgId").val( returnData.orgId );
	    	$("#srch_id").val( returnData.srch_id );
	    	$("#srch_tpi").val( returnData.srch_tpi );
	        if ( i <= <?php echo $array_IDcount; ?> ) {
				div_width = ((i)/(<?php echo $array_IDcount; ?>) * 100);
				// $(".progress-bar").animate({width: div_width + '%'}, 500);
				$('.progress-bar').css('width', div_width + '%').show('slow');
				setTimeout(
				  function() 
				  {
					doAjax(i+1, returnData.orgId, returnData.srch_id, returnData.srch_tpi);
				  }, 500);
		        }else{
					start_progress_bar();
					var dataToSend = {'batchUpload' : 1 };
				    $.ajax({  
				        'url': "dataHandler.php",
						'type': 'POST',
						'data': dataToSend
				    }).done(function(){
				    	alert('Batch Upload Complete!');
				    	window.location = "<?php echo APP_URL ?>"+ 'third-parties220?batchUpload=1';
				    });
		        }
	    });
	}
	doAjax(0,0,0,0); // starting from the last entry
}

function redirectWUpload(pageLoc){

	i 			= $("#id_count").val();
	orgId 		= $("#orgId").val();
	srch_id 	= $("#srch_id").val();
	srch_tpi  	= $("#srch_tpi").val();
	var dataToSend = {'pageLoc' : pageLoc, 'js_IDs' : <?php echo $json_IDs; ?>, 'id':i, 'orgId':orgId, 'srch_id':srch_id, 'srch_tpi':srch_tpi };
    $.ajax({  
        'url': "dataHandler.php",
		'type': 'POST',
		'data': dataToSend
    }).done(function(returnData){
	    	returnData = JSON.parse(returnData);
	    	console.log(returnData);
	    	if(returnData == 'mthirdp'){
    			window.location = "<?php echo APP_URL ?>"+ 'third-parties220';
	    	}else{
    			window.location = "<?php echo APP_URL ?>"+ 'due-diligence20';
	    	}
    });
}

</script>

<?php include('footer20.php'); ?>