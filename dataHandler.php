<?php

	// error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	session_start();
	error_reporting(0);
	require_once 'include20/config20.php';
	require_once 'include20/db20.php';
	require_once 'include20/common20.php';
	require_once 'include20/OcFunctions.php';
	@include_once('include20/EthixbaseStrEncryption.php');
	mysql_set_charset('utf8');
	
	require_once 'include20/MysqliDb.php';
	$fdb = new MysqliDb;
	
	if (!$ese) require_once 'include20/EthixbaseStrEncryption.php';
	
	$member_id = (int) $_SESSION['member_id'];
	$company_id = (int) $_SESSION['company_id'];
	
	/*
	if ($member_id == '3831')
	{
		preVar($_SESSION);die;
	}
	*/
	ini_set('memory_limit','2048M');
	set_time_limit(0);
	// ================ when submit from a form ================ //
	
	if (isset($_REQUEST['formsubmit']) || isset($_REQUEST['formsubmit1']))
	{
		$entity_type = $_REQUEST['entity_type'];
		$searchkey = $_REQUEST['entity_type'];
		$is_as_comp = 0;
		
		if ($entity_type == 'company')
		{
			$data = array();
			$tpi = array(
				'name'			=> trim(base64_decode($_REQUEST['search'])),
				'country'		=> countryName(trim($_REQUEST['country'])),
				'countrycode'	=> trim($_REQUEST['country']),
				'searchkey'		=> trim($_REQUEST['searchkey']),
				'searchTemp'	=> trim($_REQUEST['searchTemp']),
			);
			$asso = array();
			foreach($_REQUEST['assofname'] as $k => $v)
			{
				if (!trim($v)) continue;
				$l = $_REQUEST['assolname'][$k];
				if (!trim($l)) continue;
				$asso1 = array(
					'fname'	=> $v,
					'lname'	=> $l,
				);
				$asso[] = $asso1;
			}
			$data[0] = $tpi;
			$data[0]['asso'] = $asso;
			
		}
		
		if ($entity_type == 'individual')
		{
			foreach ($_REQUEST['selections'] as $k => $v)
			{
				$searchkey = $k;
				$search = $v;
			}
			
			if ($_REQUEST['noencode'])
			{
				$_REQUEST['searchTemp'] = $search;
				$search = base64_encode($search);
			}
			
			$data = array();
			$tpi = array(
				'name'			=> trim(base64_decode($search)),
				'country'		=> countryName(trim($_REQUEST['country'])),
				'countrycode'	=> trim($_REQUEST['country']),
				'searchkey'		=> $searchkey,
				'searchTemp'	=> trim($_REQUEST['searchTemp']),
			);
			
			$asso = array();
			
			if ($_REQUEST['is_as_comp']) $is_as_comp = 1;
				
			if ($_REQUEST['icompany'])
			{
				foreach ($_REQUEST['icompany'] as $ikey => $iinfo)
				{
					$tmp1 = explode('|', $iinfo);
					$icode = $tmp1[0];
					$iind = $tmp1[1];
					
					$tpi['searchkey'] = $iind;
					$tpi['skipasso'] = true;
					
					$cdetails = OcGetCompany($ikey,$icode);
					$asso1 = array(
						'name'		=> $cdetails['name'],
						'cdetails'	=> $cdetails,
					);
					$asso[] = $asso1;
				}
			}
			
			$data[0] = $tpi;
			$data[0]['asso'] = $asso;
		}
		
		/*
		preVar($_REQUEST);
		preVar($data);
		die;
		*/
		
		if ($data)
		{
			$ins = array
			(
				'member_id'		=> $member_id,
				'created_by'	=> 'system',
				'date_created'	=> $fdb->now(),
			);
			$shield_id = $fdb->insert('gidd_shieldinfo', $ins);
			
			if ($entity_type == 'company')
			{
				foreach ($data as $data1)
				{
					$name = $ese->encrypt($data1['name']);
					$country = $data1['country'];
					$countrycode = $data1['countrycode'];
					$searchkey = $data1['searchkey'];
					$searchTemp = $data1['searchTemp'];
					
					$ins = array
					(
						'shieldinfo_id'	=> $shield_id,
						'gidd_name'		=> $name,
						'gidd_country'	=> $country,
						'gidd_id'		=> $searchkey,
						'gidd_jcode'	=> $countrycode,
						'date_created'	=> $fdb->now(),
					);
					$orgId = $fdb->insert('gidd_orginfo', $ins);
					
					if ($searchkey)
					{
						$cname = $data1['name'];
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $cname,
							'srch_keyword'	=> $searchTemp,
							'entity_type'	=> 'company',
							'country'		=> $country,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
						
						if (true)
						{
							$cdetails = OcGetCompany($searchkey,$countrycode);
							foreach ($cdetails['officers'] as $o)
							{
								$o1 = $o['officer'];
								$yfullname = $o1['name'];
								$title = $o1['position'];
								$ins = array
								(
									'srch_id'		=> $srch_id,
									'user_id'		=> $member_id,
									'is_oc'			=> 1,
									'srch_type'		=> 'P',
									'asso_name'		=> $yfullname,
									'asso_title'	=> $title,
									'created_date'	=> $fdb->now(),
								);
								$fdb->insert('search20_assos', $ins);
								
								$odetails = OcGetOfficer($o1['id']);
								saveOcOff($srch_id, $odetails);
							}
							saveOcOrg($orgId, $srch_id, $cdetails);
							
							//preVar($cdetails);
						}
					}
					else
					{
						$cname = $data1['name'];
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $cname,
							'srch_keyword'	=> $searchTemp,
							'entity_type'	=> 'company',
							'country'		=> $country,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
					}
					
					$_REQUEST['last_srch_id'] = $srch_id;
					
					foreach ($data1['asso'] as $data2)
					{
						$fname = $ese->encrypt($data2['fname']);
						$lname = $ese->encrypt($data2['lname']);
						
						$ins = array
						(
							'shieldinfo_id'		=> $shield_id,
							'orginfo_id'		=> $orgId,
							'gidd_firstname'	=> $fname,
							'gidd_lastname'		=> $lname,
							'gidd_country'		=> $country,
							'gidd_jcode'		=> $countrycode,
							'date_created'		=> $fdb->now(),
						);
						$fdb->insert('gidd_assoinfo', $ins);
						
						$xfullname = $data2['fname'].' '.$data2['lname'];
						$ins = array
						(
							'srch_id'		=> $srch_id,
							'user_id'		=> $member_id,
							'is_oc'			=> 0,
							'srch_type'		=> 'P',
							'asso_name'		=> $xfullname,
							'created_date'	=> $fdb->now(),
						);
						$fdb->insert('search20_assos', $ins);
					}
				}
			}
			
			if ($entity_type == 'individual')
			{
				foreach ($data as $data1)
				{
					$xfullname = $data1['name'];
					$tmp = explode(' ', $data1['name'], 2);
					$fname = trim($tmp[0]);
					$lname = trim($tmp[1]);
					$fname = str_replace(',','',$fname);
					$lname = str_replace(',','',$lname);
					$fname = $ese->encrypt($fname);
					$lname = $ese->encrypt($lname);
					$country = $data1['country'];
					$searchkey = $data1['searchkey'];
					$searchTemp = $data1['searchTemp'];
					
					$ins = array
					(
						'shieldinfo_id'		=> $shield_id,
						'orginfo_id'		=> 0,
						'gidd_firstname'	=> $fname,
						'gidd_lastname'		=> $lname,
						'gidd_country'		=> $country,
						'gidd_id'			=> $searchkey,
						'gidd_jcode'		=> $countrycode,
						'date_created'		=> $fdb->now(),
					);
					$fdb->insert('gidd_assoinfo', $ins);
					
					if ($searchkey)
					{
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $xfullname,
							'srch_keyword'	=> $searchTemp,
							'entity_type'	=> 'individual',
							'country'		=> $country,
							'is_as_comp'	=> $is_as_comp,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
						
						$odetails = OcGetOfficer($searchkey);
						if ($odetails['company']['name'] && !$data1['skipasso'])
						{
							$zfullname = $odetails['company']['name'];
							$ins = array
							(
								'srch_id'		=> $srch_id,
								'user_id'		=> $member_id,
								'is_oc'			=> 1,
								'srch_type'		=> 'O',
								'asso_name'		=> $zfullname,
								'created_date'	=> $fdb->now(),
							);
							$fdb->insert('search20_assos', $ins);
						}
						saveOcOff($srch_id, $odetails);
					}
					else
					{
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $xfullname,
							'srch_keyword'	=> $xfullname,
							'entity_type'	=> 'individual',
							'country'		=> $country,
							'is_as_comp'	=> $is_as_comp,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
					}
					
					$_REQUEST['last_srch_id'] = $srch_id;
					
					foreach ($data1['asso'] as $data2)
					{
						$xfullname = $data2['name'];
						
						$ins = array
						(
							'shieldinfo_id'	=> $shield_id,
							'gidd_name'		=> $ese->encrypt($xfullname),
							'gidd_country'	=> $country,
							'date_created'	=> $fdb->now(),
						);
						$orgId = $fdb->insert('gidd_orginfo', $ins);
						
						$ins = array
						(
							'srch_id'		=> $srch_id,
							'user_id'		=> $member_id,
							'is_oc'			=> 1,
							'srch_type'		=> 'O',
							'asso_name'		=> $xfullname,
							'created_date'	=> $fdb->now(),
						);
						$fdb->insert('search20_assos', $ins);
						
						if ($data2['cdetails']) saveOcOrg($orgId, $srch_id, $data2['cdetails']);
					}
				}
			}
		}
		
	}

	// ================ when submit from file upload ================ //
	
	if (isset($_REQUEST['json_IDs']))
	{
		error_reporting(E_ERROR);
		// require_once('include20/readExcel.php');
		// $srch_tpi=0;
		// $srch_asso_id=0;
		// $entity_type = 'company';
		// $file = $_FILES['csvfile']['tmp_name'];
		// $filetype = pathinfo($_FILES['csvfile']['name'], PATHINFO_EXTENSION);
		
		// $rows = false;
		// if ($filetype == 'xls' || $filetype == 'xlsx') $rows = readExcel($file);
		// if ($filetype == 'csv') $rows = readCsv($file);
		
		$rows = json_decode( unserialize($_REQUEST['json_IDs']) );
		$counter = $_REQUEST['id'];

		if ($rows)
		{
			// $return_arrays = array('orgId' => 0, 'srch_id' => 0);
			$ins = array
			(
				'member_id'		=> $member_id,
				'created_by'	=> 'system',
				'date_created'	=> $fdb->now(),
			);
			$shield_id = $fdb->insert('gidd_shieldinfo', $ins);
			$return_arrays['orgId'] = $orgId = $_REQUEST['id'];
			$return_arrays['srch_id'] = $srch_id = $_REQUEST['srch_id'];
			$return_arrays['srch_tpi'] = $srch_tpi = $_REQUEST['srch_tpi'];
			
			$total_count = count($rows);
			$row_counter = 1;
			$r = $rows[$counter];
			// foreach ($rows as $r)
			// {
				if ($r[0] == 'O')
				{
					$search = $r[1];
					$name = $ese->encrypt($r[1]);
					$country = $r[4];
					
					$ins = array
					(
						'shieldinfo_id'	=> $shield_id,
						'gidd_name'		=> $name,
						'gidd_country'	=> $country,
						'date_created'	=> $fdb->now(),
					);
					$orgId = $fdb->insert('gidd_orginfo', $ins);
					$ccode = countryCode($country);
					$records = OcSearchCompanies($search, $ccode, true, $pgdetails);
					
					$cname = count($records) ? $records[0]['name'] : $search;
					
					$encname = $ese->encrypt(str_replace("&amp;", "&", htmlspecialchars($cname, ENT_QUOTES)));
					$fdb->where('comp_name_eng', $encname)->where('country', $country)->where('user_id', $member_id);
					$existed = $fdb->getOne('shield_vendor_info');
					
					if ($existed['srch_id'])
					{
						$srch_id = $existed['srch_id'];
						$parent_existed = true;
					}
					else
					{
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $cname,
							'srch_keyword'	=> $search,
							'entity_type'	=> 'company',
							'country'		=> $country,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
						
						$compl_con_nam = trim(join(' ',array($r[2],$r[3])));
						$ins = array
						(
							'srch_id'			=> $srch_id,
							'user_id'			=> $member_id,
							'comp_id'			=> $company_id,
							'tpi_type'			=> 'O',
							'comp_name_eng'		=> $encname,						
							'country'			=> $country,
							'compl_con_nam'		=> $ese->encrypt($compl_con_nam),
						);
						$fdb->insert('shield_vendor_info', $ins);
					
						if (count($records))
						{
							$cdetails = OcGetCompany($records[0]['company_number'],$records[0]['jurisdiction_code']);
							foreach ($cdetails['officers'] as $o)
							{
								$o1 = $o['officer'];
								$yfullname = $o1['name'];
								$title = $o1['position'];
								$ins = array
								(
									'srch_id'		=> $srch_id,
									'user_id'		=> $member_id,
									'is_oc'			=> 1,
									'srch_type'		=> 'P',
									'asso_name'		=> $yfullname,
									'asso_title'	=> $title,
									'created_date'	=> $fdb->now(),
								);
								$fdb->insert('search20_assos', $ins);
								
								$odetails = OcGetOfficer($o1['id']);
								saveOcOff($srch_id, $odetails);
							}
							saveOcOrg($orgId, $srch_id, $cdetails);
						}
						$parent_existed = false;
					}

					$return_arrays['orgId'] = $orgId;
					$return_arrays['srch_id'] = $srch_id;
				}
				
				if ($r[0] == 'A')
				{
					$fname = $ese->encrypt($r[2]);
					$lname = $ese->encrypt($r[3]);
					$country = $r[4];
					
					$ins = array
					(
						'shieldinfo_id'		=> $shield_id,
						'orginfo_id'		=> $orgId,
						'gidd_firstname'	=> $fname,
						'gidd_lastname'		=> $lname,
						'gidd_country'		=> $country,
						'date_created'		=> $fdb->now(),
					);
					$fdb->insert('gidd_assoinfo', $ins);

					$yfullname = $r[2].' '.$r[3];
					$fdb->where('is_oc', 0)->where('asso_name', $yfullname)->where('user_id', $member_id);
					$existed = $fdb->getOne('search20_assos');
					
					if (!$existed || !$parent_existed)
					{
						$ins = array
						(
							'srch_id'		=> $srch_id,
							'user_id'		=> $member_id,
							'is_oc'			=> 0,
							'srch_type'		=> 'P',
							'asso_name'		=> $yfullname,
							'created_date'	=> $fdb->now(),
						);
						$srch_asso_id=$fdb->insert('search20_assos', $ins);
					}
					else
					{$srch_asso_id=$existed['id'];}
						
				}
				
				if ($r[0] == 'P' && false)
				{
					//P type only exists in individual format type
					$entity_type = 'individual';
					$search = $r[2].' '.$r[3];
					$fname = $ese->encrypt($r[2]);
					$lname = $ese->encrypt($r[3]);
					$country = $r[4];
					
					$ins = array
					(
						'shieldinfo_id'		=> $shield_id,
						'orginfo_id'		=> 0,
						'gidd_firstname'	=> $fname,
						'gidd_lastname'		=> $lname,
						'gidd_country'		=> $country,
						'date_created'		=> $fdb->now(),
					);
					$fdb->insert('gidd_assoinfo', $ins);
					
					$records = OcSearchOfficers($search,'',true,$pgdetails);
					$cname = count($records) ? $records[0]['name'] : $search;
					$encname = $ese->encrypt($cname);
					
					$fdb->where('comp_name_eng', $encname)->where('country', $country)->where('user_id', $member_id);
					$existed = $fdb->getOne('shield_vendor_info');
					
					if ($existed['srch_id']) $srch_id = $existed['srch_id'];
					else
					{
						$ins = array
						(
							'user_id'		=> $member_id,
							'srch_txt'		=> $cname,
							'srch_keyword'	=> $search,
							'entity_type'	=> 'individual',
							'country'		=> $country,
							'created_date'	=> $fdb->now(),
						);
						$srch_id = $fdb->insert('search20_tpis', $ins);
						
						$ins = array
						(
							'srch_id'			=> $srch_id,
							'user_id'			=> $member_id,
							'comp_id'			=> $company_id,
							'tpi_type'			=> 'P',
							'comp_name_eng'		=> $encname,
							'country'			=> $country,
						);
						$fdb->insert('shield_vendor_info', $ins);
					
						if (count($records))
						{
							$odetails = OcGetOfficer($records[0]['id']);
							saveOcOff($srch_id, $odetails);
							
							if ($records[0]['company']['name'])
							{
								$ins = array
								(
									'srch_id'		=> $srch_id,
									'user_id'		=> $member_id,
									'is_oc'			=> 1,
									'srch_type'		=> 'O',
									'asso_name'		=> $records[0]['company']['name'],
									'created_date'	=> $fdb->now(),
								);
								$fdb->insert('search20_assos', $ins);
							}
						}
					}
				}
				// Code added by Ramender------Start
				if($srch_id!="" &&  $r[0] != 'P')
				{
						$tpi_rec=NULL;
						$crl = $crl1 = $crl6 = $crl2 = "";
						$user_id=$member_id;
						$comp_id=$company_id;
						$is_alrted = $is_alrted4 = $risk_lvl = 0;
						$sql_top = "SELECT * FROM search20_tpis WHERE id=$srch_id";
						$res_top = mysql_query($sql_top);
						if(mysql_num_rows($res_top)>0)
						{
							$row_top = mysql_fetch_assoc($res_top);
							$orgname = $row_top['srch_txt'];
							$orgnames = str_replace("&amp;", "&", htmlspecialchars($row_top['srch_txt'], ENT_QUOTES));
							$entity_type = $row_top['entity_type'];
							$country = "";
							$countryos = $row_top['country'];
							if($entity_type=='individual')
							{ $tpi_type = "P"; }
							elseif($entity_type=='company')
							{ $tpi_type = "O"; }
							
							if($orgname != '' && $r[0] == 'O')
							{
								$types = 1;
							
								$res_chk_all = srap_call($orgname,$types);
								$sql_ins10 = "INSERT INTO srch_tpi SET user_id=$user_id, srch20_id=$srch_id, comp_id=$comp_id, tpi_id='', tpi_name='".$orgname."'";
								mysql_query($sql_ins10);
								$srch_tpi = mysql_insert_id();
								$cnt_arr = count($res_chk_all);
								if(is_array($res_chk_all))
								{ 
									for($aa=1;$aa<=$cnt_arr;$aa++)
									{
										foreach($res_chk_all[$aa] as $row_chk_all)
										{
											if($row_chk_all['risk_category']!='')
											{
												$rec_id = $row_chk_all['id'];
												$sql_ins = "INSERT INTO srch_tpi_rec SET user_id=$user_id, srch_id=$srch_tpi, comp_id=$comp_id, tpi_id='', src_id=$aa, rec_id=$rec_id";
												//echo $sql_ins."<br />";
												mysql_query($sql_ins);
												$tpi_rec[] = mysql_insert_id();
												$is_alrted = 1;
												$crl1 = 3;
											}
											else
											{
												$crl1 = 0;
											}					
										}
									}
								}
							$srch_asso=$asso_rec=NULL;
							$return_arrays['srch_tpi'] = $srch_tpi;
							}
							if( $r[0] == 'A')
							{
									$sql_req30 = "SELECT * FROM search20_assos WHERE id=$srch_asso_id";// we need to filter if associate already added or not.
									$res_req30 = mysql_query($sql_req30);
									if(mysql_num_rows($res_req30)>0)
									{
										while($row_req30=mysql_fetch_assoc($res_req30))
										{
											$is_alrted4 = 0;
											if($row_req30['asso_name']!='')
											{
												$sql_ins11 = "INSERT INTO srch_tpi_asso SET user_id=$user_id, srch_id=$srch_tpi, comp_id=$comp_id, tpi_id=0, asso_name='".$row_req30['asso_name']."', asso_posi='".$row_req30['asso_title']."';";
												// $return_arrays['qry'] = $sql_ins11;
												mysql_query($sql_ins11);
												$srch_asso[] = $asso_id = mysql_insert_id();
												$res_chk_all_asso = srap_call($row_req30['asso_name'],$types);
												$cnt_arr_asso = count($res_chk_all_asso);
												if(is_array($res_chk_all_asso))
												{ 
													for($aa=1;$aa<=$cnt_arr_asso;$aa++)
													{
														foreach($res_chk_all_asso[$aa] as $row_chk_all_asso)
														{
															if($row_chk_all_asso['risk_category']!='')
															{
																$rec_id1 = $row_chk_all_asso['id'];
																$sql_ins1 = "INSERT INTO srch_asso_rec SET user_id=$user_id, asso_id=$asso_id,srch_id=$srch_tpi, comp_id=$comp_id, tpi_id='', src_id=$aa, rec_id=$rec_id1";
																mysql_query($sql_ins1);
																$asso_rec[] = mysql_insert_id();
																$is_alrted4 = 1;
																$crl2 = 3;
															}
															else
															{
																$crl2 = 0;
															}					
														}
													}
												}
											}
											
											if($is_alrted==1 || $is_alrted4==1 || $risk_lvl==3)
											{ $risk_lvl = 3; }
											else
											{ $risk_lvl = 1; }
										}
									}
							}
							
							if($is_alrted==1 || $is_alrted4==1)
							{ $risk_lvl = 3; }
							else
							{ $risk_lvl = 1; }
							
							$sql_tchk = "SELECT id, risk_level FROM shield_vendor_info WHERE comp_id=$comp_id AND LOWER(comp_name_eng)=Lower('".$ese->encrypt(trim($orgnames))."') AND country='".$countryos."'";
							$res_tchk = mysql_query($sql_tchk);
							if(mysql_num_rows($res_tchk)<=0)
							{
								$sql3 = "INSERT INTO shield_vendor_info SET muser_id='', url_id='', comp_id='{$comp_id}', user_id='{$_SESSION['member_id']}', comp_name_eng='".$ese->encrypt(trim($orgnames))."', address='', country='".$countryos."', industry='', tel_no='', compl_con_nam='', compl_con_tit='', compl_con_email='', lg_email='', tpi_type='{$tpi_type}', is_dd=1, 
									srch_id= {$srch_id}, risk_level={$risk_lvl}, modified_date=now(), `password`=''";
								//echo $sql3;
								mysql_query($sql3);
								$vid = mysql_insert_id();
								
								$srch_tpi1 = "UPDATE srch_tpi SET tpi_id={$vid} WHERE id IN({$srch_tpi})";
								mysql_query($srch_tpi1);
								
								$asso_srch_ids = implode(",",$srch_asso);
								$srch_asso_query = "UPDATE srch_tpi_asso SET tpi_id={$vid} WHERE id IN({$asso_srch_ids})";
								mysql_query($srch_asso_query);
								
								$tpi_rec_ids = implode(",",$tpi_rec);
								$sql_upd_tpi = "UPDATE srch_tpi_rec SET tpi_id={$vid} WHERE id IN({$tpi_rec_ids})";
								mysql_query($sql_upd_tpi);
								
								$asso_ids = implode(",",$asso_rec);
								$sql_upd_asso = "UPDATE srch_asso_rec SET tpi_id={$vid} WHERE id IN({$asso_ids})";
								mysql_query($sql_upd_asso);
							}
							else
							{
								if(mysql_num_rows($res_tchk)>0)
								{
									$row_tchk = mysql_fetch_assoc($res_tchk);
									$tpi_id = $row_tchk['id'];
									if($row_tchk['risk_level']==0)
									{
										$sql3 = "UPDATE shield_vendor_info SET risk_level=$risk_lvl, modified_date=now() WHERE id=$tpi_id AND comp_id=$comp_id";
										mysql_query($sql3);
										//echo $sql3;
									}
									else
									{
										
									}
									
									$sql3 = "UPDATE shield_vendor_info SET modified_date=now() WHERE id=$tpi_id AND comp_id=$comp_id";
									mysql_query($sql3);
									
									$srch_tpi1 = "UPDATE srch_tpi SET tpi_id=$tpi_id WHERE id IN($srch_tpi)";
									//echo $srch_tpi1;
									mysql_query($srch_tpi1);
									
									$asso_srch_ids = implode(",",$srch_asso);
									$srch_asso_query = "UPDATE srch_tpi_asso SET tpi_id=$tpi_id WHERE id IN($asso_srch_ids)";
									mysql_query($srch_asso_query);
								
									$tpi_rec_ids = implode(",",$tpi_rec);
									$sql_upd_tpi = "UPDATE srch_tpi_rec SET tpi_id=$tpi_id WHERE id IN($tpi_rec_ids)";
									mysql_query($sql_upd_tpi);
									
									$asso_ids = implode(",",$asso_rec);
									$sql_upd_asso = "UPDATE srch_asso_rec SET tpi_id=$tpi_id WHERE id IN($asso_ids)";
									mysql_query($sql_upd_asso);
								}
							}
						}
				}
				echo json_encode($return_arrays);
				exit();
			    // Calculate the percentation
			//     $row_percent = intval($row_counter/$total_count * 100)."%";
			//     // Javascript for updating the progress bar and information
			//     echo '<script language="javascript">
			// 		$("#progress").html("<div style=\"width:'.$row_percent.';background-color:#8cc540;height:20px;border-radius:10px;\">&nbsp;</div>");
			// 		$("#information").html("'.$row_counter.' record(s) processed.");
			// 		console.log("'.$row_percent.'");
			//     </script>';
			// 	    // document.getElementById("progress").innerHTML="<div style=\"width:'.$row_percent.';background-color:#8cc540;height:25px;border-radius:10px;\">&nbsp;</div>";
			// 	    // document.getElementById("information").innerHTML="'.$row_counter.' record(s) processed.";

			// // This is for the buffer achieve the minimum size in order to flush data
			//     echo str_repeat(' ',1024*64);
			// 	ob_flush();
			// // Send output to browser immediately
			//     flush();
			// // Sleep one second so we can see the delay
			//     sleep(1);
			//     $row_counter++;

			// }
		}
		else
		{
			echo '<script type="text/javascript">';
			echo 'window.location = "'.APP_URL.'due-diligence20'."?errcode=1".'"';
			echo '</script>';
			exit;
		}
	}

	if (isset($_REQUEST['batchUpload']))
	{
		// Code added by Ramender------End
		// $notice = '';
		// $notice .= 'Thank you for uploading your third parties - we are processing this request now.';
		// $notice .= '\n\n';
		// $notice .= 'Once uploaded all third party results will be visible within Manage Third Parties.';
		// $notice .= '\n';
		// $notice .= 'You will automatically be directed to this page shortly.';
		$_SESSION['bulkupload']=1;

		$members_info = mysql_query("SELECT * FROM ibf_members WHERE member_id='{$_SESSION['member_id']}'");
		$members_info = mysql_fetch_array($members_info);
        //send email
        $headers = 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
        $headers .= "From: support@ethixbase.com\r\nReply-To: support@ethixbase.com"."\r\n";
        
        $message = '<html>
                        <head><meta charset="utf-8">
                            <style>
                            .btn_larg  a:link{
                                text-decoration:none;
                            }
                            </style>
                        </head>
                        <body style="background-color:#F9F9F9;padding-top: 18px;">
                        <div style="border:1px;width: 90%; font-family: \'Helvetica Neue, Helvetica\', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 18px; color: #333; margin: 0 auto; background-color: #fff;border: 1px solid #DDD; border-radius: 4px; padding: 15px;">
                            <p style="margin:0 0 30px 0;"><img style="height:73px;weight:222px;" src="'.APP_URL.'img20/ethixbase-logo.png" alt="Please enable images to load system logo" title="Please enable images"></p>

                            <span style="font-family:Helvetica Neue Light,Helvetica;font-weight:300;font-size:28px;color:#515254">
                                <p><span style="color:#8CC514">ethiXbase 2.0</span> Batch Upload Complete</p>
                            </span>

                            <h3 style="padding-top:20px;">Hi '.$members_info['name'].',</h3>

                            <div style="margin-right: 20%;">

                                <p>Thank you for your recent upload. 
                                Your third party batch file has now been processed and your results are ready <br />
                                for review on ethiXbase 2.0 Third Party Risk Management platform. <br />
                                Please simply visit the link below to review your results within Manage My Third Parties.</p>

                                <table>
                                    <tr>
	                                    <td valign="middle" align="left" style="font-family:HelveticaNeueLight,HelveticaNeue-Light,\'Helvetica Neue Light\',HelveticaNeue,Helvetica,Arial,sans-serif;font-weight:300;font-stretch:normal;text-align:center;color:#fff;font-size:15px;background:#0079c1;border-radius:7px!important;line-height:1.45em;padding:7px 15px 8px;margin:0 auto 16px;font-size:1em;padding-bottom:7px">
	                                        <span>
	                                            <a type="Link" style="color:#ffffff;text-decoration:none;display:block;font-family:Arial,sans-serif;font-weight:bold;font-size:13px;line-height:15px" target="_blank" href="'.APP_URL.'third-parties220">
	                                                <span style="color:#ffffff;text-decoration:none;display:block;font-family:Arial,sans-serif;font-weight:bold;font-size:13px;line-height:15px"> 
	                                                View My Third Parties </span>
	                                            </a>
	                                        </span>
	                                    </td>
                                	</tr>
                                </table>

                                <br />

                            </div>

                        </div>
                        <p>Kind regards, 
                        <br>ethiXbase Support </p>

                        <p style="font-size:13px">
                            This is a system generated message. Please do not reply as your email may not be read.
                            <br>
                            Powered by <a style="color:#4d90fe;" target="_blank" href="http://www.ethixbase.com">ethiXbase</a>
                        </p>
                        </body>
                    </html>';
			
		$tos = array();
		$tos[] = 'jrchu@ethixbase.com';
		$tos[] = 'etalisic@ethixbase.com';
		
		$mail = mail(join(',', $tos),  " Your third party batch upload is complete and ready for review", $message, $headers);

		// $notice = 'Batch Upload Complete!';
		// echo '<script type="text/javascript">';
		// echo 'alert("'.$notice.'");';
		// echo 'window.location = "'.APP_URL.'third-parties220'.'"';
		// echo '</script>';
		// exit;
	}
	
	function saveOcOrg($orgId, $srch_id, $cdetails)
	{
		global $fdb, $ese;
		
		$alternative_names = array();
		foreach ($cdetails['alternative_names'] as $i) $alternative_names[] = $i['company_name'];
		$alternative_names = implode(';;', $alternative_names);
		
		$previous_names = array();
		foreach ($cdetails['previous_names'] as $i) $previous_names[] = $i['company_name'].', '.$i['end_date'];
		$previous_names = implode(';;', $previous_names);
		
		$officers = array();
		foreach ($cdetails['officers'] as $i) $officers[] = $i['officer']['name'].', '.$i['officer']['position'];
		$officers = implode(';;', $officers);
		
		$filings = array();
		foreach ($cdetails['filings'] as $i) $filings[] = $i['filing']['title'].', '.$i['filing']['date'];
		$filings = implode(';;', $filings);
		
		$ins = array
		(
			'orginfo_id'			=> $orgId,
			'srch_id'				=> $srch_id,
			'tpi_id'				=> 0,
			'name'					=> $ese->encrypt($cdetails['name']),
			'company_number'		=> $cdetails['company_number'],
			'jurisdiction_code'		=> $cdetails['jurisdiction_code'],
			'incorporation_date'	=> $cdetails['incorporation_date'],
			'dissolution_date'		=> $cdetails['dissolution_date'],
			'company_type'			=> $cdetails['company_type'],
			'current_status'		=> $cdetails['inactive'] ? 'inactive':'active',
			'created_at'			=> $cdetails['create_at'],
			'updated_at'			=> $cdetails['updated_at'],
			'opencorporates_url'	=> $ese->encrypt($cdetails['opencorporates_url']),
			'registered_address'	=> $ese->encrypt($cdetails['registered_address_in_full']),
			'alternative_names'		=> $alternative_names,
			'previous_names'		=> $ese->encrypt($previous_names),
			'officers'				=> $ese->encrypt($officers),
			'filings'				=> $ese->encrypt($filings),
			'created_by'			=> 'system',
			'created_date'			=> $fdb->now(),
		);
		$fdb->insert('oc_orginfo', $ins);
	}
	
	function saveOcOff($srch_id, $odetails)
	{
		global $fdb, $ese;
		
		$ins = array
		(
			'srch_id'				=> $srch_id,
			'tpi_id'				=> 0,
			'oc_id'					=> $odetails['id'],
			'fullname'				=> $ese->encrypt($odetails['name']),
			'position'				=> $odetails['position'],
			'uid'					=> $odetails['uid'],
			'current_status'		=> $odetails['inactive'] ? 'inactive':'active',
			'start_date'			=> $odetails['start_date'],
			'end_date'				=> $odetails['end_date'],
			'opencorporates_url'	=> $ese->encrypt($odetails['opencorporates_url']),
			'occupation'			=> $odetails['occupation'],
			'address'				=> $ese->encrypt($odetails['address']),
			'nationality'			=> $odetails['nationality'],
			'date_of_birth'			=> $odetails['date_of_birth'],
			'is_dup'				=> 0,
			'created_by'			=> 'system',
			'created_date'			=> $fdb->now(),
		);
		$fdb->insert('oc_offinfo', $ins);
	}
