<?php 
session_start();
error_reporting(0);
  /**** START updated by @ronnieChu 20151106 ***/
header('Cache-Control: max-age=900');
  /**** END updated by @ronnieChu 20151106 ***/
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
@include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Signup | ethiXbase 2.0</title>

  <link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
  <link href="css20/style.css" media="all" rel="stylesheet" />
  <link href="css20/style-2.css" media="all" rel="stylesheet" />
  <script src="js20/jquery-1.11.3.min.js"></script>
  <style>
  .btn_larg  a:link{
      text-decoration:none;
  }

  .btn_larg {
      text-decoration:none;
      color:#ffffff;
      width:250px;
      text-shadow:2px 2px 2px #333333;
      font-weight:bold;
      text-align:center;
      -webkit-border-radius:5px;
      -moz-border-radius:5px;
      border-radius:5px;
      padding:10px;
      font-size:14px;
      float:right;
      }
  .btn_larg a{
      color:#ffffff;
  }

  .orange{
  background: rgb(91,155,1); /* Old browsers */
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzViOWIwMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM4Y2M2M2YiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top, rgb(91,155,1) 0%, rgb(145,206,39) 51%, rgb(140,198,63) 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(91,155,1)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(140,198,63))); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* IE10+ */
      background: linear-gradient(to bottom, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5b9b01', endColorstr='#8cc63f',GradientType=0 ); /* IE6-8 */
  }

  .orange:hover{
  background: rgb(140,198,63); /* Old browsers */
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzhjYzYzZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM1YjliMDEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top, rgb(140,198,63) 0%, rgb(145,206,39) 51%, rgb(91,155,1) 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(140,198,63)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(91,155,1))); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* IE10+ */
      background: linear-gradient(to bottom, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8cc63f', endColorstr='#5b9b01',GradientType=0 ); /* IE6-8 */    }
  </style>
</head>
<body>
<div class="header-lower">
  <nav class="container" role="navigation">
    <h1 class="logo">
      <a href="due-diligence20" class="navbar-brand"><img src="img20/ethixbase-logo.png" /></a>
    </h1>

    <div class="clear"></div>
  </nav>
</div>
    <div style="padding-top:120px;"></div>

    <div class="container">
        <div class="clearfix"></div>
        
            <div>
                <div class="nicontainer" align="left">
                    <div class="cont">
                        <div class="container" style="width:930px;">
                          <h1 style="text-align:center;color:#8cc540">Registration Successful!</h1>
                        </div><!--this -->
                   </div>
                </div>
                <div style="text-align:justify; padding:15px;" class="blul">
                  <?php 
                  // echo '<meta http-equiv="refresh" content="5;URL='.APP_URL.'index20" />';
                   ?>
                    <h3 align="justify" style="line-height: 1.5;margin-left:20px;margin-right:20px;">Thank you for signing up to the ethiXbase 2.0 Third Party Risk Management Platform. Please check your email inbox where you will find a link to access the system and set your password. From there you will be able to access the system. Should you have any questions please do not hesitate to contact us at <a href="mailto:support@ethixbase.com">support@ethixbase.com </a>.  </h3>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>

                    <div class="translation"> 
                      <input onClick="window.location.href='<?php echo APP_TURL."index20"; ?>'" type="button" name="cancel" value="Back to Login Page" class="btn_larg orange" style="float:none; font-size:20px; font-weight: bold; width:300px; border:none;margin-left:22px" />
                    </div>
                </div>
            </div>
            <div class="clear"></div>
    </div>
    <div class="clearfix" style="padding-bottom:10px;margin-bottom:10px;"></div>     
</body>
</html>