<?php
include('header20.php');
include_once('include20/EthixbaseFileEncryption.php');
include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');

$titles = array
(
	'IDD' => 'Instant Due Diligence',
	'RTS' => 'Real Time Search',
	'EDD' => 'Enhanced Due Diligence',
	'KB' => 'Knowledge Base',	'SHIELD' => 'Shield',	'EA' => 'Ethical Alliance',
);

$filter = array();
$filter[] = 'active=1';
$gid = $_SESSION['user_level'];
$filter[] = "faq_id in (select faq_id from faq_groups where g_id = $gid)";

if (isset($_POST['searchbox']))
{
	$phrase = mysql_real_escape_string($_POST['searchbox']);
	$filter[] = "(question like '%$phrase%' or answer like '%$phrase%')";
}
else
{
	$type = isset($_REQUEST['type']) ? mysql_real_escape_string($_REQUEST['type']) : 'IDD';
	$filter[] = "type = '$type'";
}

$filter = join(' and ', $filter);
$query = "select question, answer from faq where $filter";
$res1 = mysql_query($query);
//echo $query;
$rows = array();
while ($r = mysql_fetch_assoc($res1))
{
	if ($phrase)
	{
		$hl = "<span style=\"background:#CCCCCC;\">$phrase</span>";
		$r['question'] = str_replace($phrase, $hl, $r['question']);
		$r['answer'] = str_replace($phrase, $hl, $r['answer']);
	}
	$rows[] = $r;
}

?>
<style>
.faqbox {
	border: 1px solid #cccccc;
	width: 100%;
	background-color: #f7f7f7;
	border-radius: 5px;
}
.faqmenu {
	border: solid 1px;
	width: 78%;
	height: auto;
	margin: 15px;
	padding: 10px 15px;
	background-color: #8cc541;
	border-radius: 5px;
	color: #ffffff;
	font-weight: bold;
	text-shadow: 2px 2px 2px #333333;
	text-align:left;
}
.faqmenu-<?php echo $type; ?> {
	background-color: #5e8130 !important;
}
.menubox {
	border-right: 1px solid #cccccc;
	vertical-align: top;
}
.contentbox {
	vertical-align: top;
	padding: 20px;
}
.faqlist {
	margin: 5px;
}
.faqentry {
	margin: 25px 0px;
	text-align:left;
	list-style:none;
}
.faqtitle {
	color: #8cc540;
	font-family: Arial,Helvetica,sans-serif;
	font-size: 25px;
}
.faq-q {
	font-family: Arial,Helvetica,sans-serif;
	font-size: 14px;
	font-weight: bold;
	cursor: pointer;
}
.faq-a {
	font-family: Arial,Helvetica,sans-serif;
	font-size: 12px;
}
.searchbtn {
	border: medium none;
	border-radius: 5px;
	color: #ffffff;
	font-size: 16px;
	font-weight: bold;
	padding: 5px 15px;
	text-align: center;
	text-shadow: 2px 2px 2px #333333;
	width: auto;
	text-decoration: none;
	background-color: #7a818c;
	cursor: pointer;
}
</style>

<script type="text/javascript">

function faq_search()
{
	d = document.getElementById('searchbox').value;
	if (d) document.getElementById('faq_search_box').submit();
}

	$('.faq-q').click(function()
	{
		el = $(this).next().next();
		if ($(el).is(':visible'))
		{
			$(el).hide();
			$(this).children('img').attr('src','img20/icons/toggle_expand.png');
		}
		else
		{
			$('.faq-a').hide();
			$('.toggle_img').attr('src','img20/icons/toggle_expand.png');
			$(this).children('img').attr('src','img20/icons/toggle_collapse.png');
			$(el).fadeIn('fast');
		}
	});

</script>
<div class="map-bg int-row-1 tp-row-1">
  <div class="container">
    <div class="text-center">
      <h1>FAQ</h1>
      
      <div class="col-xs-12 tp-company-name text-left">
        <h2><!--<img width="100px" height="100px" src="img20/sample-fireball-securities-logo.png" />--><?php echo $row_chk['comp_name_eng']; ?></h2>
      </div>
      
      <div class="col-sm-12">
        <table align="center" class="faqbox">
            <tr>
                <td align="right" colspan="2" style="border-bottom: 1px solid #cccccc;padding-top:10px;padding-bottom:10px;padding-right:20px;">
                <form id="faq_search_box" method="post">
                Search:&nbsp;&nbsp;<input id="searchbox" name="searchbox" type="text" size="40" style="border: 1px solid #cccccc;border-radius:5px;padding:5px;" <?php $z = $_POST['searchbox']; if (isset($z)) echo "value=\"$z\""; ?> />
                &nbsp;<span style="width:10px;" class="searchbtn" onclick="faq_search();">Go</span>
                </form>
                </td>
            </tr>
            
            <tr>
                <td width="25%" class="menubox">
                    <a style="text-decoration:none;" href="faq20.php?type=IDD"><div class="faqmenu faqmenu-IDD">Instant Due Diligence</div></a>							<a style="text-decoration:none;" href="faq20.php?type=RTS"><div class="faqmenu faqmenu-RTS">Real Time Search</div></a>							<a style="text-decoration:none;" href="faq20.php?type=EDD"><div class="faqmenu faqmenu-EDD">Enhanced Due Diligence</div></a>							<a style="text-decoration:none;" href="faq20.php?type=KB"><div class="faqmenu faqmenu-KB">Knowledge Base</div></a>							<a style="text-decoration:none;" href="faq20.php?type=SHIELD"><div class="faqmenu faqmenu-SHIELD">Shield</div></a>							<a style="text-decoration:none;" href="faq20.php?type=EA"><div class="faqmenu faqmenu-EA">Ethical Alliance</div></a>
                </td>

                <td class="contentbox">
                    <div style="overflow:auto;height:500px;overflow-x:hidden;">
                    <p>
                    <h2 class="faqtitle"><?php echo $phrase ? "Search result for '<i>$phrase</i>'" : $titles[$type].' FAQ'; ?></h2>
                    
                    <?php if($rows):?>
                    
                    <ul class="faqlist">
                        
                        <?php foreach ($rows as $r): ?>
                        <li class="faqentry">
                            <span class="faq-q">
                            <img src="img20/icons/toggle_expand.png" border="0" width="15" height="15" class="toggle_img" />&nbsp;
                            <?php echo nl2br($r['question']); ?>
                            </span>
                            <br />
                            <span class="faq-a" style="display:none;">
                            <?php echo nl2br($r['answer']); ?>
                            </span>
                        </li>
                        <?php endforeach; ?>
                        
                    </ul>
                    
                    <?php else: ?>
                    <span style="font-size:0.8em;"><i>No records found.</i></span>
                    <?php endif; ?>
                    
                    </p>
                    </div>
                </td>
            </tr>
        </table>
      </div>
    </div>  
  </div>
</div>

<?php
  include('footer20.php');
?>