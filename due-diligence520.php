<?php
  include('header20.php');
?>


<div class="map-bg int-row-1 edd-row-1">
  <div class="container">
    <div class="text-center">
      <h1>Submit Due Diligence Request</h1>
      <div class="steps-container hidden-xs hidden-sm">
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Search</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Add Individuals</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Run Report</p>
        </div>
        <div class="step-line">
        &nbsp;
        </div>
        <div class="step-dot text-center">
          <img class="step-dot-1" src="img20/icon-steps-1.png" />
          <p>Review Results</p>
        </div>
      </div>
    </div>
    
    <div class="col-xs-12">
      <p class="dd4-label">Names associated with: <b>Marc Armstrong</b></p>
      <div class="form-03">
        <table class="table-01">
          <tr>
            <td>Company/Individual</td>
            <td>Country</td>
            <td>Name</td>
            <td>EthiXbase<br/>Rating</td>
            <td>Include in<br/>Report</td>
            <td>My Remediation</td>
          </tr>
          <tr>
            <td><a href="#"><img src="img20/icon-minus-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc H. Armstrong</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:center !important;"><img src="img20/icon-pass.png" /></td>
            <td><input id="checkbox3" type="checkbox" name="checkbox" value="3">
            <label for="checkbox3">exclude</label></td>
            <td>
              <select>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ></input>
            </td>
          </tr>
          <tr>
            <td colspan="4" class="indent"><a href="#"><img src="img20/icon-minus-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;margin-left:-30px;" /></a><b>Entity Name: </b>Marc Henry Armstrong<br />
            <b>Address: </b>28 Hunt Dr.<br />
            <b>Event: </b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vulputate erat augue,S<br />sed blandit eros ultrices nec. 
Maecenas porttitor vitae neque non egestas<br />
            <b>Alert: </b>Marc Henry Armstrong was a gang member<br /></td>
            <td><input id="checkbox3" type="checkbox" name="checkbox" value="3">
            <label for="checkbox3">exclude</label></td>
            <td>
              <select>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ></input>
            </td>
          </tr>
          <tr>
            <td colspan="4" class="indent"><a href="#"><img src="img20/icon-minus-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;margin-left:-30px;" /></a><b>Entity Name: </b>Marc Harry Armstrong<br />
            <b>Address: </b>22 Baker Street<br />
            <b>Event: </b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vulputate erat augue,<br />sed blandit eros ultrices nec. 
Maecenas porttitor vitae neque non egestas<br />
            <b>Alert: </b><br /></td>
            <td><input id="checkbox3" type="checkbox" name="checkbox" value="3">
            <label for="checkbox3">exclude</label></td>
            <td>
              <select>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ></input>
            </td>
          </tr>
          <tr>
            <td><a href="#"><img src="img20/icon-add-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc D. Armstrong</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:center !important;"><img src="img20/icon-pass.png" /></td>
            <td><input id="checkbox3" type="checkbox" name="checkbox" value="3">
            <label for="checkbox3">exclude</label></td>
            <td>
              <select>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ></input>
            </td>
          </tr>
          <tr>
            <td><a href="#"><img src="img20/icon-add-small.png" style="width:18px;position:relative;bottom:3px;margin-right:10px;" /></a>Marc  Armstrong</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:center !important;"><img src="img20/icon-fail.png" /></td>
            <td><input id="checkbox3" type="checkbox" name="checkbox" value="3">
            <label for="checkbox3">exclude</label></td>
            <td>
              <select>
                <option>Green</option>
                <option>Amber</option>
                <option>Red</option>
                <option>Black</option>
                <option>Grey</option>
              </select>
              <input type="text" value="Free Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ></input>
            </td>
          </tr>
        </table>
        <div class="button-area text-center">
          <button>Save</button><button>Print</button><button>Order EDD</button>
        </div>
      </div>
    </div>
    
  </div>
  
</div>

<?php
  include('footer20.php');
?>