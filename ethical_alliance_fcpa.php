<?php session_start(); 
	include('include_pages/dologin.php');
	include_once('head-data.php');
	@mysql_set_charset('utf8');
	date_default_timezone_set("GMT");
	@header("Content-type:text/html; charset=utf-8");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>FCPA | <?php echo WEBSITE_NAME;?></title>
<meta name="keywords" content="Anti corruption, anti corruption laws, foreign corrupt practices act, anti corruption legislation, global anti corruption, business anti corruption, corruption laws, corruption reports, bribery and corruption, anti money laundering, anti money laundering laws, anti bribery laws" />
<META NAME="Description" CONTENT="The Global Anti-Corruption database providing information on anti corruption laws and anti corruption law firms across the world. Over 130 country profiles that contains Anti-Corruption laws and legislation, anti-money laundering legislation, privacy laws, enforcement agencies, IMF reports, OEDC reports, academic papers, and more.">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<script src="js/jquery-1.11.1.min.js"></script>
<?php include("js/google_add_services_js.php"); ?>
<style>
.side_menu ul
{
	list-style:outside none none;
	line-height:20px;
	margin:0px;
	text-align:left;
}

.side_menu li {
    list-style: outside none none;
    padding: 10px;
	border-bottom: 1px solid #E7E6E6;
	text-align:left;
}

.side_menu li a
{
	color:#000000;
	padding: 10px 10px 10px 0px;
}

.side_menu li a:hover
{
	color: #E9A825 !important;
}

p { margin-top:15px !important; margin-bottom:15px !important; }

h1,h2,h3,h4 { color:#E09626 !important; }
</style>
</head>
<body>	
	<div class="wrapper">
		<?php include("header-new.php"); ?>
		<div class="clearfix"></div>
        <div style="float:left;">
            <?php include('include_pages/banner.php');?>
        </div>
		<div style="clear: both;"></div>
        <div class="" style="margin-top:50px;">
            <div style="text-align:justify;">
                <h1>FCPA Background and Summary</h1>
                <div class="signupfrom content" style="width:68%; float:left;">
                    <div>
    <p style="text-align: justify;">The Foreign Corrupt Practices Act of 1977 (FCPA) (15 U.S.C. § 78dd-1, et seq.) is a United States federal law known primarily for two of its main provisions, one that addresses accounting transparency requirements under the Securities Exchange Act of 1934 and another prohibiting bribery of foreign officials.</p>
    <h3><strong>Provisions and Scope</strong></h3>
    <p style="text-align: justify;">The FCPA applies to any person who has a certain degree of connection to the United States and engages in foreign corrupt practices. The Act also applies to any act by U.S. businesses, foreign corporations trading securities in the United States, American nationals, citizens, and residents acting in furtherance of a foreign corrupt practice whether or not they are physically present in the United States. In the case of foreign natural and legal persons, the Act covers their actions if they are in the United States at the time of the corrupt conduct. Further, the Act governs not only payments to foreign officials, candidates, and parties, but any other recipient if part of the bribe is ultimately attributable to a foreign official, candidate, or party. These payments are not restricted to just monetary forms and may include anything of value.<sup id="cite_ref-2"><a href="http://en.wikipedia.org/wiki/Foreign_Corrupt_Practices_Act#cite_note-2"><br>
    </a></sup></p>
    <h3><strong>Persons subject to the FCPA</strong></h3>
    
    <strong>Issuers</strong>
    
    <p style="text-align: justify;">Includes any U.S. or foreign corporation that has a class of securities registered, or that is required to file reports under the Securities and Exchange Act of 1934</p>
    
    <h3><strong>Domestic concerns</strong></h3>
    
    <p style="text-align: justify;">Refers to any individual who is a citizen, national, or resident of the United States and any corporation and other business entity organized under the laws of the United States or of any individual US State, or having its principal place of business in the United States</p>
    
    <strong>Any person</strong>
    covers both enterprises and individuals
    <h3><strong>Requirements</strong></h3>
    
    <p style="text-align: justify;">The anti-bribery provisions of the FCPA make it unlawful for a U.S. person, and certain foreign issuers of&nbsp;securities, to make a payment to a foreign official for the purpose of obtaining or retaining business for or with, or directing business to, any person. Since 1998, they also apply to foreign firms and persons who take any act in furtherance of such a corrupt payment while in the United States.</p>
    
    <p style="text-align: justify;">The meaning of&nbsp;foreign official&nbsp;is broad. For example, an owner of a bank who is also the minister of finance would count as a foreign official according to the U.S. government. Doctors at government-owned or managed hospitals are also considered to be foreign officials under the FCPA, as is anyone working for a government-owned or managed institution or enterprise. Employees of international organizations such as the United Nations are also considered to be foreign officials under the FCPA.</p>
    
    <p style="text-align: justify;">There is no materiality to this act, making it illegal to offer anything of value as a bribe, including cash or non-cash items. The government focuses on the intent of the bribery rather than on the amount.The FCPA also requires companies whose securities are listed in the United States to meet its accounting provisions. See&nbsp;15 U.S.C.&nbsp;§&nbsp;78m. These accounting provisions, which were designed to operate in tandem with the anti-bribery provisions of the FCPA, require corporations covered by the provisions to make and keep books and records that accurately and fairly reflect the transactions of the corporation and to devise and maintain an adequate system of internal accounting controls. An increasing number of corporations are taking additional steps to protect their reputation and reducing exposure by employing the services of due diligence companies.</p>
    
    <p style="text-align: justify;">Identifying&nbsp;government-owned companies&nbsp;in an effort to identify easily overlooked government officials is rapidly becoming a critical component of more advanced anti-corruption programs.Regarding payments to foreign officials, the act draws a distinction between bribery and facilitation or “grease payments”, which may be permissible under the FCPA but may still violate local laws. The primary distinction is that grease payments are made to an official to expedite his performance of the duties he is already bound to perform. Payments to foreign officials may be legal under the FCPA if the payments are permitted under the written laws of the host country. Certain payments or reimbursements relating to product promotion may also be permitted under the FCPA.</p>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div style="width:26%; float:left; margin-left:15px; margin-top:20px;">
                    <ul class="side_menu">
                    	<li><h3><strong>This Section</strong></h3></li>
                        <li><a href="ethical_alliance_fcpa">FCPA Background and Summary</a></li>
                        <li><a href="ethical_alliance_fcpa_documents">FCPA Documents</a></li>
                        <li><a href="ethical_alliance_fcpa_helpful_resources">Government Resources</a></li>
                        <li><a href="ethical_alliance_fcpa_free_resources">Free Resources</a></li>
                    </ul>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
       <div class="clearfix"></div>
    	<?php include('footer-new.php'); ?>          
	</div>
</body>
</html>