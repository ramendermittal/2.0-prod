<?php 

include('header20.php');
require_once 'include20/MysqliDb.php';
$fdb = new MysqliDb;

$vid = (int) $_REQUEST['vid'];
$srch_id = (int) $_REQUEST['srch_id'];
$request_details = $_REQUEST;
if (!$vid && !$srch_id) die('invalid ID');
if ($vid && $srch_id) die('ID must be either one');

if ($vid)
{
	$fdb->where('comp_id', $_SESSION['company_id'])->where('id', $vid);
	$comp = $fdb->getOne('shield_vendor_info');
	$comp = decryptRow($comp);
	
	$fdb->where('tpi_id', $vid)->where('comp_id', $_SESSION['company_id']);
	$fdb->orderBy('id', 'DESC');
	$glsrch = $fdb->getOne('srch_tpi');
	
	$assos = false;
	if ($glsrch)
	{
		$srch_tpi = $glsrch['id'];
		$fdb->where('srch_id', $srch_tpi)->where('tpi_id', $vid)->where('comp_id', $_SESSION['company_id']);
		$assos = $fdb->get('srch_tpi_asso');
	}
}

if ($srch_id)
{
	$fdb->where('srch_id', $srch_id);
	$comp = $fdb->getOne('shield_vendor_info');
	$comp = decryptRow($comp);

	if( empty($comp) ){
		$fdb->where('id', $srch_id);
		$comp = $fdb->getOne('search20_tpis');
		$comp['comp_name_eng'] = $comp['srch_txt'];
	}
	
	$assos = false;
	$fdb->where('srch_id', $srch_id);
	$assos = $fdb->get('search20_assos');
}

$onSubmit_arrays = array('Credit Card', 'Invoice', 'Vouchers', 'credit_card_return');
$payment_mode = array('Credit Card' => 'creditcard', 'Invoice' => 'invoice', 'Vouchers' => 'voucher');
?>

<link href="js20/bootstrap-modal/css/bootstrap-modal-bs3patch.css" media="all" rel="stylesheet" />
<link href="js20/bootstrap-modal/css/bootstrap-modal.css" media="all" rel="stylesheet" />

<?php

if(isset($_REQUEST['on_submit']) && in_array($_REQUEST['on_submit'], $onSubmit_arrays)) {
	if ($_REQUEST['on_submit'] != 'credit_card_return') {
		$sql_ins = "INSERT INTO edd_orders_company SET 
	                member_id = '{$_SESSION['member_id']}', 
	                srch_id = '" . $request_details['srch_id'] . "',
	                company_name ='" . $request_details['company_name'] . "',
	                country ='" . $request_details['country'] . "',  
	                address ='" . $request_details['address'] . "',
	                payment_mode = '" . $payment_mode[$_REQUEST['on_submit']] . "'
	                ";

		$order_comp_ins = mysql_query($sql_ins);

		if ($order_comp_ins) {
			$order_comp_id = mysql_insert_id();
			$individuals_assoc = array();
			if (!empty($request_details['assoc1'])) {
				$sql_ins = mysql_query("INSERT INTO edd_orders_individuals SET 
		                    edd_order_id = '{$order_comp_id}', 
		                    individual_name = '" . $request_details['assoc1'] . "'
		                    ");
				$individuals_assoc[] = $request_details['assoc1'];
			}
			if (!empty($request_details['assoc2'])) {
				$sql_ins = mysql_query("INSERT INTO edd_orders_individuals SET 
		                    edd_order_id = '{$order_comp_id}', 
		                    individual_name = '" . $request_details['assoc2'] . "'
		                    ");
				$individuals_assoc[] = $request_details['assoc2'];
			}

			$members_info = mysql_query("SELECT * FROM ibf_members WHERE member_id='{$_SESSION['member_id']}'");
			$members_info = mysql_fetch_array($members_info);
			//send email
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: support@ethixbase.com\r\nReply-To: support@ethixbase.com" . "\r\n";

			$message = '<html>
	                        <head><meta charset="utf-8">
	                            <style>
	                            .btn_larg  a:link{
	                                text-decoration:none;
	                            }
	                            </style>
	                        </head>
	                        <body style="background-color:#F9F9F9;padding-top: 18px;">
	                        <div style="border:1px;width: 90%; font-family: \'Helvetica Neue, Helvetica\', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 18px; color: #333; margin: 0 auto; background-color: #fff;border: 1px solid #DDD; border-radius: 4px; padding: 15px;">
	                 

	                            <span style="font-family:Helvetica Neue Light,Helvetica;font-weight:300;font-size:28px;color:#515254">
	                                <p><span style="color:#8CC514">ethiXbase 2.0</span></p>
	                            </span>

	                            <h3 style="padding-top:20px;">Hi,</h3>

	                            <div style="margin-right: 20%;">

	                                <p>Please be informed that ' . $members_info['name'] . ' has ordered an EDD report with the following details:</p>
	                                <br />

	                                <b>' . (isset($_GET['type']) && $_GET['type'] == 1 ? 'Individual' : 'Company') . ': </b> &nbsp; ' . $request_details['company_name'] . '<br />
	                                <b>Country: </b> &nbsp; ' . $request_details['country'] . '<br />
	                                <b>Address: </b> &nbsp; ' . $request_details['address'] . '<br />
	                                <b>Email: </b> &nbsp; ' . $_SESSION['username'] . '<br />
	                                <b>Date: </b> &nbsp; ' . date("F d, Y") . '<br />
	                                ';

			if (!empty($individuals_assoc)) {

				$message .= '<br><b>Associated Individuals:</b>
	        					<ul> ';
				foreach ($individuals_assoc as $assoc) {
					$message .= '<li>' . $assoc . '</li>';
				}
				$message .= '</ul>';
			}

			$message .= '
	                            </div>

	                        </div>
	                        <p>Best regards,<br>ethiXbase</p>

	                        <p style="font-size:13px">
	                            This is a system generated message. Please do not reply as your email may not be read.
	                            <br>
	                            Powered by <a style="color:#4d90fe;" target="_blank" href="http://www.ethixbase.com">ethiXbase</a>
	                        </p>
	                        </body>
	                    </html>';

			$tos = array();
			$tos[] = 'jrchu@ethixbase.com';
			$tos[] = 'etalisic@ethixbase.com';
			$tos[] = 'eesperila@ethixbase.com';

			$mail = mail(join(',', $tos), "EDD Request Order", $message, $headers);

		}
		if ($_REQUEST['on_submit'] == 'Credit Card') {
			$t_cost1 = 900;
			// $url_data = http_build_query($request_details);

			?>
			<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="paypal_form_topup"
				  id="paypal_form_topup">
				<input type="hidden" name="cmd" value="_xclick"/>
				<input type="hidden" name="business" value="rmittal@ethixbase.com" />
				<input type="hidden" name="return"
					   value="<?php echo APP_URL; ?>order_edd_form?on_submit=credit_card_return"/>
				<input type="hidden" name="cancel_return"
					   value="<?php echo APP_URL; ?>online_billing_cancel?action=cancel"/>
				<!-- <input type="hidden" name="notify_url" value="<?php echo APP_URL; ?>payment/topup_paypal?action=ipn" /> -->
				<input type="hidden" name="lc" value="US"/>
				<input type="hidden" name="item_name" value="Standard Enhanced Due Diligence Report"/>
				<input type="hidden" name="item_number" value="1"/>
				<input type="hidden" name="no_note" value="1"/>
				<input type="hidden" name="no_shipping" value="2"/>
				<input type="hidden" name="src" value="0"/>
				<input type="hidden" name="p3" value="1"/>
				<input type="hidden" name="currency_code" value="USD"/>
				<input type="hidden" name="srt" value="0"/>
				<input type="hidden" name="a3" value="<?php echo $t_cost1; ?>"/>
				<input name="amount" type="hidden" id="amount" value="<?php echo $t_cost1; ?>"/>


			</form>
			<script type="text/javascript">
				document.getElementById('paypal_form_topup').submit(); // SUBMIT FORM
			</script>
			<?php

			exit();
		}

	} else {
		echo mysql_error();
	}
	?>

	<style>
		.btn_larg  a:link{
			text-decoration:none;
		}

		.btn_larg {
			text-decoration:none;
			color:#ffffff;
			width:350px;
			text-shadow:2px 2px 2px #333333;
			font-weight:bold;
			text-align:center;
			-webkit-border-radius:5px;
			-moz-border-radius:5px;
			border-radius:5px;
			padding:10px;
			font-size:12px;
			float:right;
		}
		.btn_larg a{
			color:#ffffff;
		}

		.orange{
			background: rgb(91,155,1); /* Old browsers */
			/* IE9 SVG, needs conditional override of 'filter' to 'none' */
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzViOWIwMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM4Y2M2M2YiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top, rgb(91,155,1) 0%, rgb(145,206,39) 51%, rgb(140,198,63) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(91,155,1)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(140,198,63))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* IE10+ */
			background: linear-gradient(to bottom, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5b9b01', endColorstr='#8cc63f',GradientType=0 ); /* IE6-8 */
		}

		.orange:hover{
			background: rgb(140,198,63); /* Old browsers */
			/* IE9 SVG, needs conditional override of 'filter' to 'none' */
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzhjYzYzZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM1YjliMDEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top, rgb(140,198,63) 0%, rgb(145,206,39) 51%, rgb(91,155,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(140,198,63)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(91,155,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8cc63f', endColorstr='#5b9b01',GradientType=0 ); /* IE6-8 */    }
	</style>


	<div style="padding-top:120px;"></div>

	<div class="container">
		<div class="clearfix"></div>

		<div>
			<div class="nicontainer" align="left">
				<div class="cont">
					<div class="container" style="width:930px;">
						<h1 style="text-align:center;color:#8cc540">EDD Order Request Complete!</h1>
					</div><!--this -->
				</div>
			</div>
			<div style="text-align:justify; padding:15px;" class="blul">
				<?php
				// echo '<meta http-equiv="refresh" content="5;URL='.APP_URL.'index20" />';
				?>
				<h3 align="justify" style="line-height: 1.5;margin-left:20px;margin-right:20px;">Thank you for your EDD order request. It is now under processing of our team. Should you have any questions please do not hesitate to contact us at <a href="mailto:support@ethixbase.com">support@ethixbase.com </a>.  </h3>
				<p>&nbsp;</p>
				<p>&nbsp;</p>

				<div class="translation">
					<input onClick="window.location.href='<?php echo APP_URL."third-parties220"; ?>'" type="button" name="cancel" value="Back to My Third Parties Page" class="btn_larg orange" style="float:none; font-size:20px; font-weight: bold; width:350px; border:none;margin-left:22px" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clearfix" style="padding-bottom:10px;margin-bottom:10px;"></div>


<?php
} else {
?>

<link href="css20/style-dashboard-2.css" media="all" rel="stylesheet" />
<div style="padding-top:80px;"></div>
<div style="width:980px; margin-top:34px; margin-left:auto; margin-right:auto;">
	<div class="wrapper">
		<div style="width:260px; text-align:left; margin-right:20px; float:left; padding-top:20px;">
			<div style="padding:10px;">
				<a href="img20/edd/EthixBase_EDD_Flyer_US.PDF" target="_blank">
					<img src="img20/edd/ethixbase-360-Brochure.jpg" width="200" style="border:1px solid #999999;" />
				</a>
			</div>
			<div style="padding:10px;">
				<a href="img20/edd/ethiXbase360-Sample-Premium-Report.pdf" target="_blank">
					<img src="img20/edd/ethiXbase360-Sample-Premium-Report.jpg" width="200" style="border:1px solid #999999;" />
				</a>
			</div>
			<div style="padding:10px;">
				<a href="img20/edd/ethiXbase360-Sample-Standard-Report.pdf" target="_blank">
					<img src="img20/edd/ethiXbase360-Sample-Standard-Report.jpg" width="200" style="border:1px solid #999999;" />
				</a>
			</div>
		</div>
		
		<div id="div1" style=" float:left; text-align:left; width:700px;">
						
			<p align="center" style="width:100%;background-color:#e0e0e0;">
			<img src="img20/ethixbase-logo.png" width="333" height="100">
			<h2>ethiXbase Enhanced Due Diligence Order Form</h2>
			</p>
			<hr>
			
			<?php if ($_REQUEST['formsubmit']): ?>
			
				<?php
				
				//preVar($_REQUEST);
				
				//send email
				$headers = 'MIME-Version: 1.0'."\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
				$headers .= "From: support@ethixbase.com\r\nReply-To: support@ethixbase.com"."\r\n";
				
				$assolist = '&nbsp;';
				if ($assos)
				{
					$assolist = '<ul>';
					foreach($assos as $a)
					{
						$assolist .= '<li>'.ucwords($a['asso_name']).'</li>';
					}
					$assolist .= '</ul>';
				}
				
				$message = '
					<html>
					<h2>ethiXbase Enhanced Due Diligence Order</h2>
					<table border="0">
						<tr>
							<td><strong>Company Name: </strong></td>
							<td>'.strtoupper($comp['comp_name_eng']).'</td>
						</tr>
						<tr>
							<td><strong>Country: </strong></td>
							<td>'.$comp['country'].'</td>
						</tr>
						<tr>
							<td><strong>Address: </strong></td>
							<td>'.$comp['address'].'</td>
						</tr>
						<tr>
							<td><strong>Associated Individuals: </strong></td>
							<td>'.$assolist.'</td>
						</tr>
					</table>
					</html>
				';
				
				$tos = array();
				$tos[] = 'mfsaad@ethixbase.com';
				/*
				*/
			$tos[] = 'rmittal@ethixbase.com';
			$tos[] = 'lbachatene@ethixbase.com';
			$tos[] = 'cching@ethixbase.com';
				
				$mail = mail(join(',', $tos), "ethiXbase Enhanced Due Diligence Order", $message, $headers);
				//preVar($mail);
				
				?>
				<h5>Thank you, your order has been successfully submitted.</h5>
				
			<?php else: ?>
				<form id="order_form" onSubmit="return validateFormOnSubmit(this)" class="form-horizontal" action="order_edd.php?srch_id=<?php echo $srch_id.(isset($_GET['type']) ? '&type='.$_GET['type'] : ''); ?>" method="post">
				<input type="hidden" name="vid" value="<?php echo $vid; ?>">
				<input type="hidden" name="srch_id" value="<?php echo $srch_id; ?>">
                    <div class="form-body">
                        <div class="form-group">
							<label class="control-label col-md-3"><?php echo isset($_GET['type']) && $_GET['type'] == 1 ? 'Individual' : 'Company'; ?><span class="required">*</span></label>
							<div class="col-md-7">
								<input type="text" id="maxlength_defaultconfig" disabled name="companName" class="form-control" value="<?php echo strtoupper($comp['comp_name_eng']); ?>">
								<input type="hidden" id="maxlength_defaultconfig" name="company_name" class="form-control" value="<?php echo strtoupper($comp['comp_name_eng']); ?>">
								<div class="help-block small">
									Input <?php echo (isset($_GET['type']) && $_GET['type'] == 1 ? 'Individual' : 'Company'); ?> Name
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Country<span class="required">*</span></label>
							<div class="col-md-7">
								<!-- <input type="text" id="maxlength_defaultconfig" name="country" maxlength="25" class="form-control" value="<?php //echo $comp['country']; ?>"> -->
								<!-- <div class="input-group"> -->
									<select name="country" id="country" title="Country or Territory of registration" class="form-control">
										<option value="">-- Select Country --</option>
										<?php
										$fdb->orderBy('country', 'asc');
										$oc_countries = $fdb->get('oc_countries', null, 'code,country');

										foreach ($oc_countries as $c)
										{
											//filters out weird countries
											if (strlen($c['country']) > 30) continue;

											$countrylbl = strlen($c['country']) > 30 ? substr($c['country'],0,30).'..' : $c['country'];
											$selected_country = '';
											if( strtolower($countrylbl) == strtolower($comp['country']) ){
												$selected_country = 'selected = "selected"';
											}
											echo '<option value="'.$countrylbl.'" '.$selected_country.'>'.$countrylbl.'</option>';
										}
										?>
									</select>
								<!-- </div> -->
								<div class="help-block small">
									Select Country
								</div>
							</div>
						</div>
                        <!-- <div class="form-group">
                            <label class="control-label col-md-3">Address<span class="required">*</span></label>
                            <div class="col-md-7">
								<input type="text" id="maxlength_defaultconfig" name="address" class="form-control" value="<?php //echo $comp['address']; ?>">
								<div class="help-block small">
									Input Company Address
								</div>
                            </div>
                        </div> -->
						<?php if(isset($_GET['sac']) && $_GET['sac'] == 1) { ?>
						<hr>
						<h4>Stored Associated <?php echo isset($_GET['type']) && $_GET['type'] == 1 ? 'Company':'Individuals' ?></h4>
						<p>

						<?php if ($assos){
						$thead_width="85%";
						if(count($assos) > 5){
						$thead_width="81%";
						}

						if(!isset($_GET['type']) || $_GET['type'] != 1) {
						?>
						<table style="width:82%;padding-top:10px;padding-bottom:0px;margin-bottom:0px;" class="table  ">
						    <thead>
						    <tr>
								<th class="td-word-wraps" style="width:50%;vertical-align:middle;text-align:center;">Name</th>
						        <th class="td-word-wraps" style="width:35%;vertical-align:middle;text-align:center;">Position</th>
						        <th class="td-word-wraps" style="width:15%;text-align:center;vertical-align:middle">Select Associate</th>
						    </tr>
						    </thead>
					    </table>
      					<div class="form-group" style="max-height:250px;width:85%; overflow:auto;padding-left:15px;">
							<table class="table table-bordered" >
								<?php //foreach($assos as $a): 
									foreach ($assos as $akey => $a) :
										if ( !array_key_exists('asso_posi', $a) ){
											$a['asso_posi'] = $a['asso_title']; 
										}
								?>
								<tr>
									<td class="td-word-wraps" style="width:50%;vertical-align:middle;"><?php echo ucwords($a["asso_name"]); ?></td>
									<td class="td-word-wraps" style="width:35%;vertical-align:middle"><?php echo ucwords($a["asso_posi"]); ?></td>
									<td class="td-word-wraps" style="width:15%;text-align:center;vertical-align:middle">
										<div class="checkbox">
										  <label>
										  	<input type="checkbox" name="assoc_chk[]" value="<?php echo $akey; ?>" <?php //if($akey < 2) { echo "checked"; } ?> class="assos_check" assoc-name="<?php echo ucwords($a["asso_name"]); ?>">
										  </label>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>
							</table>
						</div>
						<?php } else {
							foreach ($assos as $akey => $a) {
								if (!array_key_exists('asso_posi', $a)) {
									$a['asso_posi'] = $a['asso_title'];
								}
								if (isset($_GET['type']) && $_GET['type'] == 1) {
									$individual_name = ucwords($a["asso_name"]);
								}
							}
						} ?>
						<?php } else { ?>
						<h5><i>No Associated Individuals found</i></h5>
						<?php } ?>
						</p>
						<br>
						<?php
							$assoc1 = '';
							$assoc2 = '';
							if ($assos){
								$assoc1 = ucwords($assos[0]['asso_name']);
								$assoc2 = ucwords($assos[1]['asso_name']);
							} 
						?>
						<input type="hidden" id="assoc_count" name="assoc_count" class="form-control" value="1">
                        <div class="form-group">
                            <label class="control-label col-md-3">Individual<?php echo isset($_GET['type']) && $_GET['type'] == 1 ? '':' 1' ?>:</label>
                            <div class="col-md-7">
								<input type="text" id="assoc1" name="assoc1" class="form-control" value="<?php echo isset($_GET['type']) && $_GET['type'] == 1 ? $individual_name : '*'; ?>"<?php echo isset($_GET['type']) && $_GET['type'] == 1 ? ' readonly' : ''; ?>>
								<div class="help-block small">
									Input Associated Name
								</div>
                            </div>
                        </div>
						<?php if(!isset($_GET['type']) || $_GET['type'] != 1) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Individual 2:</label>
                            <div class="col-md-7">
								<input type="text" id="assoc2" name="assoc2" class="form-control" value="<?php //echo $assoc2; ?>">
								<div class="help-block small">
									Input Associated Name
								</div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                	<div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-offset-3 col-md-9">
                                	<a href="javascript:history.back()" onclick="" class="btn btn-default ">Cancel</a>
                                    <input type="button" id="place_order" name="place_order" class="btn btn-success" value="Place Order" data-toggle="modal" data-target="#modal-order">
                                </div>
                            </div>
                        </div>
                    </div>

					<div id="modal-order" class="modal fade modal-order" aria-hidden="true" >
						<div class="" >
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" style="text-align:center">Payment Options</h4>
								<span class="text-muted padding-3 small"></span>
							</div>
							<div class="modal-body padding-bottom-0">
								<!--form action="" method="post" name="payForm" id="payForm" class="login_form2"-->
									<!--input type="hidden" id="maxlength_defaultconfig" name="company_name" class="form-control" value="<?php echo strtoupper($request_details['company_name']); ?>">
									<input type="hidden" id="maxlength_defaultconfig" name="country" class="form-control" value="<?php echo strtoupper($request_details['country']); ?>">
									<input type="hidden" id="maxlength_defaultconfig" name="address" class="form-control" value="<?php echo strtoupper($request_details['address']); ?>">
									<input type="hidden" id="maxlength_defaultconfig" name="assoc1" class="form-control" value="<?php echo strtoupper($request_details['assoc1']); ?>">
									<input type="hidden" id="maxlength_defaultconfig" name="assoc2" class="form-control" value="<?php echo strtoupper($request_details['assoc2']); ?>">
									<input type="hidden" id="maxlength_defaultconfig" name="srch_id" class="form-control" value="<?php echo strtoupper($request_details['srch_id']); ?>"-->
									Would you like to pay by credit card, be invoiced or use your ethiXbase credits?
									<br><br>
<!--									<a style="text-decoration:none;" href="http://client.dev.ethixbase.com/ethixbase2/iddpay_online?">-->
										<!-- <a href=""> -->
										<input type="submit" class="btn btn-default " value="Credit Card" id="on_submit" name="on_submit" style="margin-left:35px;width:110px;">
										<!-- <button class="btn btn-default " type="button" style="margin-left:50px;margin-right:15px;width:110px;">Credit Card</button> -->
<!--									</a>-->
<!--									<a style="text-decoration:none;" href="#">-->
										<!-- <a href=""> -->
										<input type="submit" class="btn btn-default " value="Invoice" id="on_submit" name="on_submit" style="margin-left:30px;width:110px;">
										<!-- <button class="btn btn-default " type="button" style="margin-left:50px;margin-right:15px;width:110px;">Credit Card</button> -->
<!--									</a>-->
<!--									<a style="text-decoration:none;" href="#">-->
										<!-- <a href=""> -->
										<input type="submit" class="btn btn-default " value="Vouchers" id="on_submit" name="on_submit" style="margin-left:30px;margin-right:15px;width:110px;">
										<!-- <button class="btn btn-default " type="button" style="margin-left:50px;margin-right:15px;width:110px;">Credit Card</button> -->
<!--									</a>-->
								<!--/form-->
							</div>
						</div>
					</div>
                </form>

			<!-- 	<p>
				<h3><?php //echo strtoupper($comp['comp_name_eng']); ?></h3>
				<table>
					<tr>
						<td><strong>Country:&nbsp;</strong></td>
						<td><input type="text" name="company_edd" value="<?php //echo $comp['country']; ?>" class="form-control placeholder-no-fix"></td>
					</tr>
					<tr>
						<td><strong>Address:&nbsp;</strong></td>
						<td><?php //echo $comp['address']; ?></td>
					</tr>
				</table>
				</p>
				<br>
				
				<p>
				<?php //if ($assos): ?>
				<h4>Stored Associated Individuals</h4>
				<ul>
					<?php //foreach($assos as $a): ?>
					<li><?php //echo ucwords($a['asso_name']); ?></li>
					<?php// endforeach; ?>
				</ul>
				<?php //else: ?>
				<h5><i>No Associated Individuals found</i></h5>
				<?php //endif; ?>
				</p> -->
				
				<!-- <form method="post" action="" onSubmit="return fsubmit(this);">
				<p style="width:100%; text-align:right;">
				<input type="hidden" name="vid" value="<?php //echo $vid; ?>">
				<input type="hidden" name="srch_id" value="<?php //echo $srch_id; ?>">
				<input type="submit" name="formsubmit" value="Place Order">
				</p>
				</form> -->
			<?php endif; ?>
			
		</div>
		
		<div class="clearfix"></div>
		<div class="clear"></div>
		<style>
		.section_footer {
			text-align:left !important;
		}
		#div1 {
			border: 1px solid #999999;
			padding: 20px;
			border-radius: 2px;
			box-shadow: -1px -1px 3px rgba(0, 0, 0, 0.10);
			-moz-box-shadow: -1px -1px 3px rgba(0, 0, 0, 0.10);
			-webkit-box-shadow: -1px -1px 3px rgba(0, 0, 0, 0.10);
		}
		</style>
	</div>
</div>

<script type="text/javascript">

	var arr = {};

    $(".assos_check").each(function () {
        $(this).prop("checked", false);
    });
    $('#assoc_count').val(1);

	<?php if(!isset($_GET['type']) || $_GET['type'] != 1) { ?>
    //$('#assoc1').val('');
    //$('#assoc2').val('');
	<?php } ?>

	$( ".assos_check" ).click(function() {
		if($(this).is(':checked')){
			if($('input.assos_check:checked').length > 2){
				$(this).attr('checked', false);
        		$(this).attr( 'value', 0);
				alert("Maximum of two associated individuals can be selected.");
			}else{
				/*var chk_lngth = $('#assoc_count').val();
				$(this).attr( 'value', chk_lngth );
				$('#assoc'+chk_lngth).val($(this).attr("assoc-name"));
				if($('input.assos_check:checked').length == 1){
					$('#assoc_count').val(2);
				}*/

				if($.trim($('#assoc1').val()) == ''){
					$('#assoc1').val($.trim($(this).attr('assoc-name')));
					arr[$(this).val()] = 1;
				}
				else if($.trim($('#assoc2').val()) == ''){
					$('#assoc2').val($.trim($(this).attr('assoc-name')));
					arr[$(this).val()] = 2;
				}
			}
		}
        else{
			/*if($(this).attr('value') == 1){
				$('#assoc_count').val(1);
			}else{
				$('#assoc_count').val(2);
			}
        	$('#assoc'+$(this).attr('value')).val('');
        	$(this).attr( 'value', 0 );*/
			$('#assoc' + arr[$(this).val()]).val('');
        }
	});

	/*$( "#place_order" ).click(function() {
		$('.modal-order').modal();
	});*/

	function toTitleCase(str)
	{
	    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	function validateEmpty(fld) {
		var error = "";
	  
		if (fld.value.length == 0) {
			fld.style.background = 'Yellow'; 
			error = toTitleCase(fld.name);
		} else {
			fld.style.background = 'White';
		}
		return error;   
	}
	
	function trim(s)
	{
	  return s.replace(/^\s+|\s+$/, '');
	} 
	
	function validateFormOnSubmit(theForm) {
		var reason = "";
		reason += validateEmpty(theForm.country);
		  
		if (reason != "") {
			alert("Please select " + reason + ".");
			return false;
		}
		return true;
	}

</script>
<?php
}
?>
<?php include('footer20.php'); ?>
