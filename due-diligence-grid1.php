<?php

	date_default_timezone_set("GMT");
	
	@mysql_set_charset('utf8');
	@header("Content-type:text/html; charset=utf-8");
	
	if(!isset($_SESSION['member_id'])){
		@header("Location: ".APP_URL);
	}
	
	define('__DEBUG__', false);
	
	//file upload
	if($_FILES['csvfile']){
		$now = date("y") . date("m") . date("d") . date("His");
		$target_dir = "giddUpload/user/";
		$target_file = $target_dir . "{$_SESSION['member_id']}-{$now}-" . basename($_FILES["csvfile"]["name"]);
		$uploadOk = 1;
		$readOk = 0;
		$fileType = pathinfo($_FILES["csvfile"]["name"],PATHINFO_EXTENSION);
		// Check if file already exists
		if(file_exists($target_file)){
			echo "Sorry, {$target_file} already exists.";
		}
		// Check file size
		if($_FILES["csvfile"]["size"] > 500000){
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		// Allow certain file formats
		if($fileType != "csv"){
			echo "Sorry, only csv files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if($uploadOk == 0){
				echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else{
			if (!file_exists($target_dir)) {
				mkdir($target_dir, 0777, true);
			}
			if(move_uploaded_file($_FILES["csvfile"]["tmp_name"], $target_file) || copy($_FILES["csvfile"]["tmp_name"], $target_file)){
				//file uploaded successfully
				$readOk = 1;
			}else{
				echo 'file upload fail';
			}
		}
		
		if($readOk == 1){
			$cntRow = 0;
			$cntCol = 0;
			$arr_content = array();
			$arr_title = array();
			if (($handle = fopen($target_file, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					if($cntRow == 0){
						//process csv header.
						$cntCol = count($data);
						$arr_title = $data;
					}else{
						if(count($data) == $cntCol){
							$tmp_arr = array();
							foreach($data as $key => $value){
								$tmp_name = $arr_title[$key];
								$tmp_arr[$tmp_name] = $value;
							}
							$arr_content[] = $tmp_arr;
						}else{
							//something went wrong.
						}
					}
					$cntRow++;
				}
				fclose($handle);
			}else{
				//unable to open file.
			}
		}
		
		//if data available 
		if(count($arr_content)>0)
		{
			$ese = new EthixbaseStrEncryption();
			
			$arr_dataSets = array();
			$cnt_set = 0;
			$set_mainKey = '';
			$shield_id = '';
			$orginfo_id = '';
			$assoinfo_value = '';
			$sql_shieldIns = "	INSERT INTO gidd_shieldinfo SET
													member_id = '{$_SESSION['member_id']}'
													, gidd_filepath = '{$target_file}'
													, created_by = 'system'
													, date_created = now()
													";
			mysql_query($sql_shieldIns) or die(mysql_error());
			$shield_id = mysql_insert_id();
			$shieldId = $shield_id;
			if(__DEBUG__){
				echo "{$sql_shieldIns}<br />";
			}

			if ($_REQUEST['csvtype'] == 'both')
			{
				foreach($arr_content as $data){
					$subjectName = trim($data['subject_name']);
					$firstName = trim($data['first_name']);
					$lastName = trim($data['last_name']);
					$address = trim($data['address']);
					$country = trim($data['country']);
					$state = trim($data['state']);
					$industry = trim($data['industry']);
					$jobTitle = trim($data['job_title']);
					$contactNo = trim($data['contact_no']);
					$email = trim($data['email']);
					$fullName = "{$firstName} {$lastName}";
					
					$xSubjectName = $ese->encrypt($subjectName);
					$xFirstName = $ese->encrypt($firstName);
					$xLastName = $ese->encrypt($lastName);
					$xFullName = $ese->encrypt($fullName);
					$xJobTitle = $jobTitle; //$ese->encrypt($jobTitle);
					$xContactNo = $ese->encrypt($contactNo);
					$xAddress = $ese->encrypt($address);
					$xEmail = $ese->encrypt($email);
					if($set_mainKey == ''){
						$set_mainKey = $cnt_set == 0 ? $arr_content[$cnt_set]['type'] : $set_mainKey;
						//store user prvided data for organization
						$sql_orginfoIns = "	INSERT INTO gidd_orginfo SET
																shieldinfo_id = '{$shield_id}'
																, gidd_name = '{$xSubjectName}'
																, gidd_country = '{$country}'
																, gidd_state = '{$state}'
																, gidd_address = '{$xAddress}'
																, gidd_industry = '{$industry}'
																, gidd_fullname = '{$xLastName}'
																, gidd_jobtitle = '{$xJobTitle}'
																, gidd_contactno = '{$xContactNo}'
																, gidd_email = '{$xEmail}'
																, created_by = 'system'
																, date_created = now()
																";
						mysql_query($sql_orginfoIns) or die(mysql_error());
						$orginfo_id =  mysql_insert_id();
						//echo $set_mainKey;
						if(__DEBUG__){
							echo "{$sql_orginfoIns}<br />";
						}
					}elseif($set_mainKey == $data['type']){
						if($assoinfo_value != ''){
							//store user proided data from associates
							$sql_assoinfoIns = "	INSERT INTO gidd_assoinfo
																		(shieldinfo_id, orginfo_id, gidd_firstname, gidd_lastname, gidd_jobtitle, gidd_address, gidd_country, gidd_state, gidd_contactno, gidd_email, created_by, date_created)
																		VALUES
																		{$assoinfo_value}
																		";
							mysql_query($sql_assoinfoIns) or die(mysql_error());
							if(__DEBUG__){
								echo "{$sql_assoinfoIns}<br /><br />";
							}
						}
						
						$assoinfo_value = '';
						$orginfo_id = '';
						$cnt_set++;
						//store user prvided data for organization
						$sql_orginfoIns = "	INSERT INTO gidd_orginfo SET
																shieldinfo_id = '{$shield_id}'
																, gidd_name = '{$xSubjectName}'
																, gidd_country = '{$country}'
																, gidd_state = '{$state}'
																, gidd_address = '{$xAddress}'
																, gidd_industry = '{$industry}'
																, gidd_fullname = '{$xLastName}'
																, gidd_jobtitle = '{$xJobTitle}'
																, gidd_contactno = '{$xContactNo}'
																, gidd_email = '{$xEmail}'
																, created_by = 'system'
																, date_created = now()
																";
						mysql_query($sql_orginfoIns) or die(mysql_error());
						$orginfo_id =  mysql_insert_id();
						if(__DEBUG__){
							echo "{$sql_orginfoIns}<br />";
							//echo "<br />{$data['type']}";
						}
					}else{
						//insert into same row
						$assoinfo_value .= ($assoinfo_value == '' ? " " : ", ") . "('{$shield_id}', '{$orginfo_id}', '{$xFirstName}', '{$xLastName}', '{$xJobTitle}', '{$xAddress}', '{$country}', '{$state}', '{$xContactNo}', '{$xEmail}', 'system', now())";
						//echo ", {$data['type']}"; 
					}
				}
				//uninserted leftover
				if($assoinfo_value != ''){
					//store user prvided data for organization
					$sql_assoinfoIns = "	INSERT INTO gidd_assoinfo
																(shieldinfo_id, orginfo_id, gidd_firstname, gidd_lastname, gidd_jobtitle, gidd_address, gidd_country, gidd_state, gidd_contactno, gidd_email, created_by, date_created)
																VALUES
																{$assoinfo_value}
																";
					mysql_query($sql_assoinfoIns) or die(mysql_error());
					if(__DEBUG__){
						echo "{$sql_assoinfoIns}<br /><br />";
					}
				}
				$assoinfo_value = '';
				$orginfo_id = '';
			}
			
			if ($_REQUEST['csvtype'] == 'individuals')
			{
				foreach($arr_content as $data)
				{
					$subjectName = trim($data['subject_name']);
					$firstName = trim($data['first_name']);
					$lastName = trim($data['last_name']);
					$address = trim($data['address']);
					$country = trim($data['country']);
					$state = trim($data['state']);
					$industry = trim($data['industry']);
					$jobTitle = trim($data['job_title']);
					$contactNo = trim($data['contact_no']);
					$email = trim($data['email']);
					$fullName = "{$firstName} {$lastName}";
					
					$xSubjectName = $ese->encrypt($subjectName);
					$xFirstName = $ese->encrypt($firstName);
					$xLastName = $ese->encrypt($lastName);
					$xFullName = $ese->encrypt($fullName);
					$xJobTitle = $jobTitle; //$ese->encrypt($jobTitle);
					$xContactNo = $ese->encrypt($contactNo);
					$xAddress = $ese->encrypt($address);
					$xEmail = $ese->encrypt($email);
					
					$asso1 = "('{$shield_id}', '0', '{$xFirstName}', '{$xLastName}', '{$xJobTitle}', '{$xAddress}', '{$country}', '{$state}', '{$xContactNo}', '{$xEmail}', 'system', now())";
					$sql_assoinfoIns = "	INSERT INTO gidd_assoinfo
																(shieldinfo_id, orginfo_id, gidd_firstname, gidd_lastname, gidd_jobtitle, gidd_address, gidd_country, gidd_state, gidd_contactno, gidd_email, created_by, date_created)
																VALUES
																{$asso1}
																";
					mysql_query($sql_assoinfoIns) or die(mysql_error());
				}
			}
		}
	}
	
?>
