<?php
	//header('Content-Type: text/html; charset=UTF-8');
	//header( "refresh:5;url=contact-us.php" );
	//include_once 'dbcon.php';
	
	$isPostBack = false;
	$referer = "";
	$thisPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	$isFillIn = false;
	$isError = false;
	$isSuccess = false;
	$errMsg_NullValue = "Please fill in a valid value for all required fields.";
	$msg_SubmitSuccess = "Form submit success! You'll be contacted soon. Thank you.";
	$cssErr = "err";
	$val_fname = "";
	$val_lname = "";
	$val_jtitle = "";
	$val_company = "";
	$val_phoneNo = "";
	$val_email = "";
	$val_country = "";
	$content = "";

	if (isset($_SERVER['HTTP_REFERER'])){
		$referer = $_SERVER['HTTP_REFERER'];
	}

	if ($referer == $thisPage){
		$isPostBack = true;
	}
	
	if ($isPostBack && $_SERVER['REQUEST_METHOD'] == "POST"){
		$fname = (ISSET($_POST['fname']) ? $_POST['fname']:'');
		$lname = (ISSET($_POST['lname']) ? $_POST['lname']:'');
		$jtitle = (ISSET($_POST['jtitle']) ? $_POST['jtitle']:'');
		$company = (ISSET($_POST['company']) ? $_POST['company']:'');
		$phoneNo = (ISSET($_POST['phoneNo']) ? $_POST['phoneNo']:'');
		$email = (ISSET($_POST['email']) ? $_POST['email']:'');
		$country = (ISSET($_POST['country']) ? $_POST['country']:'');
		$industry = (ISSET($_POST['industry']) ? $_POST['industry']:'');
		$content = (ISSET($_POST['content']) ? $_POST['content']:'');

		if (!(strlen($fname)>0)){
			$isError = true;
		} else {
			$val_fname = showValue($fname);
		}
		if (!(strlen($lname)>0)){
			$isError = true;
		} else {
			$val_lname = showValue($lname);
		}
		if (!(strlen($jtitle)>0)){
			$isError = true;
		} else {
			$val_jtitle = showValue($jtitle);
		}
		if (!(strlen($company)>0)){
			$isError = true;
		} else {
			$val_company = showValue($company);
		}
		if (!(strlen($phoneNo)>0)){
			$isError = true;
		} else {
			$val_phoneNo = showValue($phoneNo);
		}
		if (!(strlen($email)>0)){
			$isError = true;
		} else {
			$val_email = showValue($email);
		}
		if (!(strlen($country)>0)){
			$isError = true;
		} else {
			$val_country = showValue($country);
		}
		if (!$isError){
			$array = array("fname" => $fname, "lname" => $lname, "jtitle" => $jtitle, "company" => $company, "phoneNo" => $phoneNo, "email" => $email, "country" => $country, "industry" => $industry, "content" => $content);
			initialize_contactus($array);
		}
		
	}
	
	function showValue($val){
		return 'value="'.$val.'"';
	}
	
	function showError($errMsg){
		
		$err = '<div class="row"><div class="error">'.$errMsg.'</div></div>';
		
		return $err;
	}
	
	function initialize_contactus($array){
		sendMail($array);
		insert_contactinfo($array);
		$isSuccess = true;
	}
		
	function sendMail($array){
		
		$to = 'zclim@ethixbase.com';
		$subject = 'Enquiry';
		
		// Always set content-type when sending HTML email
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";

		// More headers
		$headers .= 'From: <zclim@ethixbase.com>' . "\r\n";
		//$headers .= 'Cc: [name]@example.com' . "\r\n";

		$message = '<html><head><title>Contact Us Email</title></head>';
		$message .= '<body>'.
		'<p>Enquiry Details:</p>'.
		'<table>'.
		'<tr>'.
		'<td>First Name:</td>'.
		'<td>'.$array['fname'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Last Name:</td>'.
		'<td>'.$array['lname'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Job Title:</td>'.
		'<td>'.$array['jtitle'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Company Name:</td>'.
		'<td>'.$array['company'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Phone Number:</td>'.
		'<td>'.$array['phoneNo'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Email Address:</td>'.
		'<td>'.$array['email'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Country of Residence:</td>'.
		'<td>'.$array['country'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td>Industry:</td>'.
		'<td>'.$array['industry'].'</td>'.
		'</tr>'.
		'<tr>'.
		'<td colspan="2">Content:</td>'.
		'</tr>'.
		'<tr>'.
		'<td colspan="2">'.$array['content'].'</td>'.
		'</tr>'.
		'</table>'.
		'</body></html>';

		mail($to,$subject,$message,$headers);
		
		//echo ' DONE!';
		//echo '<br/>This page will be redirected back after 5sec.<br />If not, click <a href="../contact-us.php">here</a>.';
		
	}
	
	
	
?>