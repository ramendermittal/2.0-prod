<?php
	
	//country code configuration
	function getCountryName($countryCode){
		if(strlen($countryCode)>0){
			switch($countryCode){
				case 'ae': return 'UAE'; break;
				case 'ae_az': return 'Abu Dhabi (UAE)'; break;
				case 'ae_du': return 'Dubai (UAE)'; break;
				case 'al': return 'Albania'; break;
				case 'aw': return 'Aruba'; break;
				case 'au': return 'Australia'; break;
				case 'bb': return 'Barbados'; break;
				case 'bd': return 'Bangladesh'; break;
				case 'be': return 'Belgium'; break;
				case 'bm': return 'Bermuda'; break;
				case 'bh': return 'Bahrain'; break;
				case 'bs': return 'Bahamas'; break;
				case 'ca': return 'Canada'; break;
				case 'ca_nb': return 'New Brunswick (Canada)'; break;
				case 'ca_nl': return 'Newfoundland and Labrador (Canada)'; break;
				case 'ca_ns': return 'Nova Scotia (Canada)'; break;
				case 'ca_pe': return 'Prince Edward Island (Canada)'; break;
				case 'ca_qc': return 'Quebec (Canada)'; break;
				case 'hr': return 'Croatia'; break;
				case 'cy': return 'Cyprus'; break;
				case 'dk': return 'Denmark'; break;
				case 'do': return 'Dominican Republic'; break;
				case 'fi': return 'Finland'; break;
				case 'gi': return 'Gibraltar'; break;
				case 'gl': return 'Greenland'; break;
				case 'hk': return 'Hong Kong'; break;
				case 'ie': return 'Ireland'; break;
				case 'im': return 'Isle of Man'; break;
				case 'in': return 'India'; break;
				case 'is': return 'Iceland'; break;
				case 'je': return 'Jersey'; break;
				case 'jm': return 'Jamaica'; break;
				case 'li': return 'Liechtenstein'; break;
				case 'lu': return 'Luxembourg'; break;
				case 'lv': return 'Latvia'; break;
				case 'me': return 'Montenegro'; break;
				case 'mm': return 'Myanmar'; break;
				case 'mt': return 'Malta'; break;
				case 'mu': return 'Mauritius'; break;
				case 'mx': return 'Mexico'; break;
				case 'nl': return 'Netherlands'; break;
				case 'no': return 'Norway'; break;
				case 'nz': return 'New Zealand'; break;
				case 'pa': return 'Panama'; break;
				case 'pk': return 'Pakistan'; break;
				case 'pl': return 'Poland'; break;
				case 'pr': return 'Puerto Rico'; break;
				case 'ro': return 'Romania'; break;
				case 'rw': return 'Rwanda'; break;
				case 'se': return 'Sweden'; break;
				case 'sg': return 'Singapore'; break;
				case 'si': return 'Slovenia'; break;
				case 'za': return 'South Africa'; break;
				case 'es': return 'Spain'; break;
				case 'tj': return 'Tajikistan'; break;
				case 'th': return 'Thailand'; break;
				case 'tn': return 'Tunisia'; break;
				case 'to': return 'Tonga'; break;
				case 'tz': return 'Tanzania'; break;
				case 'ug': return 'Uganda'; break;
				case 'gb': return 'United Kingdom'; break;
				case 'us': return 'United States'; break;
				case 'us_al': return 'Alabama (US)'; break;
				case 'us_ak': return 'Alaska (US)'; break;
				case 'us_az': return 'Arizona (US)'; break;
				case 'us_ar': return 'Arkansas (US)'; break;
				case 'us_ca': return 'California (US)'; break;
				case 'us_cr': return 'Colorado (US)'; break; //temp
				case 'us_ct': return 'Connecticut (US)'; break;
				case 'us_dc': return 'District of Columbia (US)'; break;
				case 'us_de': return 'Delaware (US)'; break;
				case 'us_fl': return 'Florida (US)'; break;
				case 'us_ga': return 'Georgia (US)'; break;
				case 'us_hi': return 'Hawaii (US)'; break;
				case 'us_in': return 'Illinois (US)'; break; //temp
				case 'us_ii': return 'Indiana (US)'; break; //temp
				case 'us_ia': return 'Iowa (US)'; break;
				case 'us_id': return 'Idaho (US)'; break;
				case 'us_ks': return 'Kansas (US)'; break;
				case 'us_ky': return 'Kentucky (US)'; break; //temp
				case 'us_la': return 'Louisiana (US)'; break;
				case 'us_ma': return 'Massachusetts (US)'; break;
				case 'us_md': return 'Maryland (US)'; break;
				case 'us_me': return 'Maine (US)'; break;
				case 'us_mi': return 'Michigan (US)'; break;
				case 'us_mn': return 'Minnesota (US)'; break; //temp
				case 'us_mo': return 'Missouri (US)'; break;
				case 'us_ms': return 'Mississippi (US)'; break;
				case 'us_mt': return 'Montana (US)'; break; //temp
				case 'us_nb': return 'Nebraska (US)'; break; //temp
				case 'us_nv': return 'Nevada (US)'; break; //temp
				case 'us_nh': return 'New Hampshire (US)'; break;
				case 'us_nj': return 'New Jersey (US)'; break;
				case 'us_nm': return 'New Mexico (US)'; break;
				case 'us_ny': return 'New York (US)'; break;
				case 'us_nc': return 'North Carolina (US)'; break;
				case 'us_nd': return 'North Dakota (US)'; break; //temp
				case 'us_oh': return 'Ohio (US)'; break;
				case 'us_ok': return 'Oklahoma (US)'; break;
				case 'us_or': return 'Oregon (US)'; break;
				case 'us_pa': return 'Pennsylvania (US)'; break;
				case 'us_ri': return 'Rhode Island (US)'; break;
				case 'us_sc': return 'South Carolina (US)'; break;
				case 'us_sd': return 'South Dakota (US)'; break;
				case 'us_tn': return 'Tennessee (US)'; break;
				case 'us_tx': return 'Texas (US)'; break; //temp
				case 'us_ut': return 'Utah (US)'; break;
				case 'us_va': return 'Virginia (US)'; break;
				case 'us_vt': return 'Vermont (US)'; break;
				case 'us_wa': return 'Washington (US)'; break;
				case 'us_wv': return 'West Virginia (US)'; break;
				case 'us_wc': return 'Wisconsin (US)'; break; //temp
				case 'us_wy': return 'Wyoming (US)'; break;
				case 'vn': return 'Viet Nam'; break;
				case 'vu': return 'Vanuatu'; break;
				default: return str_replace("_", " ", $countryCode);
			}
		}else{
			return $countryCode;
		}
	}
	
?>