<?php
	function parse_data($url){

		$raw_data = get_cdata($url);
		if(isset($raw_data["curlError"])){
			$arr_data = $raw_data;
		}else{
			$arr_data = new SimpleXMLElement($raw_data);
		}
		return $arr_data;
	}

	function searchResult_officers($officers){
		$i = 0;
		echo '<table cellspacing="0" cellpadding="0" id="tz large" class="tablesorter" width="100%" align="center">';
		echo '<thead>';
		echo '<tr>';
			echo '<th style="text-align:center; line-height:33px;"><strong>Name</strong></th>';
			echo '<th style="text-align:center; line-height:33px;"><strong>Position</strong></th>';
			echo '<th style="text-align:center; line-height:33px;"><strong>Country</strong></th>';
			echo '<th style="text-align:center; line-height:33px;"><strong>Company</strong></th>';
		echo '</tr>';
		echo '</thead>';
		foreach($officers as $officer){
			$id = $officer->id;
			$name = $officer->name;
			$position = $officer->position;
			$jcode = $officer->{'jurisdiction-code'};
			$arr_companies = array();
			foreach($officer->company as $company){
				$comp_id = $company->{'company-number'};
				$comp_name = $company->name;
				$comp_jcode = $company->{'jurisdiction-code'};
				$arr_company = array(array("id" => $comp_id, "name" => $comp_name, "jcode" => $comp_jcode ));
				$arr_companies = array_merge($arr_companies, $arr_company);
			}
			
			echo '<tr ';
				if($i%2 == 0){ 
					echo 'id="tc1"'; 
				} else { 
					echo 'id="tc2"'; 
				}
				echo "onmouseover=\"this.id='tc3'\"";
				
				if($i%2 == 0){ 
					echo "onmouseout=\"this.id='tc1'\""; 
				} else { 
					echo "onmouseout=\"this.id='tc2'\""; 
				}
			echo '>';
				echo '<td id="lia"><a href="edd_detailed.php?id='.$id.'&jcode=&type=officer_details&rdc_names='.$name.'&rdc_type=P&rdc_pagi=1&rdc_id=OCRDC0001" target="_blank">'.$name.'</label></a></td>';
				echo '<td id="lia" style="text-align:center;">'.$position.'</td>';
				echo '<td id="lia">'.getCountryName($jcode).'</td>';
				echo '<td id="lia">';
				foreach($arr_companies as $company){
					echo '<a href="edd_detailed.php?id='.$company['id'].'&jcode='.$company['jcode'].'&type=company_details&rdc_names='.$company['name'].'&rdc_type=O&rdc_pagi=1&rdc_id=OCRDC0001" target="_blank">'.$company['name'].'</label></a>';
					echo '<label id="jcode">,'.getCountryName($company['jcode']).'</label>';
					echo '<br/>';
				}
				echo '</td>';
			echo '</tr>';
			
			$i++;
		}
		echo '</table>';

	}
	
	function off_searchResultArray($officers){
		$arr_offSrchResults = array();
	
		foreach($officers as $officer){
			array_push($arr_offSrchResults, array('Officer_ID' => $officer->id , 'Officer_Name' => $officer->name , 'Position' => $officer->position, 'Jurisdiction_Code' => $officer->{'jurisdiction-code'}, 'Company_ID' => $officer->company->{'company-number'}, 'Company_Name' => $officer->company->name, 'Company_Jurisdiction_Code' => $officer->company->{'jurisdiction-code'}));
		}
		return $arr_offSrchResults;
		
	}
	
	function searchResult_companies($companies){
		//param = $xml->results->companies->company;
		$i = 0;
		echo '<table cellspacing="0" cellpadding="0" id="tz large" class="tablesorter" width="100%" align="center">';
		echo '<thead>';
		echo '<tr>';
			echo '<th style="text-align:center; line-height:33px;"><strong>Company Name</strong></th>';
			echo '<th style="text-align:center; line-height:33px;">State/Country</strong></th>';
			echo '<th style="text-align:center; line-height:33px;">Generate PDF Report</strong></th>';
		echo '</tr>';
		echo '</thead>';
		foreach($companies as $company){
			$company = $company->company;
			$comp_id = $company->{'company-number'};
			$comp_name = strip_tags(trim($company->name, '"'));
			//$comp_name = preg_replace('/\"\'+/', '', $comp_name);
			$comp_name = preg_replace('/[^A-Za-z0-9\_ &]/', '', $comp_name);
			$comp_name = urlencode($comp_name);
			$comp_jcode = $company->{'jurisdiction-code'};
			$comp_incorp_date = $company->{'incorporation-date'};
			
			echo '<tr ';
				if($i%2 == 0){ 
					echo 'id="tc1"'; 
				} else { 
					echo 'id="tc2"'; 
				}
				echo "onmouseover=\"this.id='tc3'\"";
				if($i%2 == 0){ 
					echo "onmouseout=\"this.id='tc1'\""; 
				} else { 
					echo "onmouseout=\"this.id='tc2'\""; 
				}
			echo '>';
			echo '<td id="lia"><a href="edd_detailed.php?id='.$comp_id.'&jcode='.$comp_jcode.'&type=company_details&rdc_names='.$comp_name.'&rdc_type=O&rdc_pagi=1&rdc_id=OCRDC0001" target="_blank">'.$company->name.'</a></td>';
			echo '<td id="lia" style="text-align:center;">'.getCountryName($comp_jcode).'</td>';
			
			//change from corporate date to Generate PDF 20140903 - zchai
			echo '<td id="lia" style="text-align:center;width:150px;"><a href="#">Generate Report</a></td>';
			/*
			if (strlen($comp_incorp_date) > 0){
				echo '<td id="lia" style="text-align:center;">'.$comp_incorp_date.'</td>';
			}else{
				echo '<td id="lia" style="text-align:center;">Unknown</td>';
			}
			*/
			
			
			echo '</tr>';
			$i++;
		} 
		echo '</table>';
	}
	
	function comp_searchResultArray($companies){
		$arr_compSrchResults = array();
		
		foreach($companies as $company){			
			array_push($arr_compSrchResults, array('ID' => $company->company->{'company-number'} , 'Subject_Name' => $company->company->name , 'Jurisdiction_Code' => $company->company->{'jurisdiction-code'}, 'Incorporate_Date' => $company->company->{'incorporation-date'}));
		}
		return $arr_compSrchResults;
	}
	
	function officer_main_detail($officer){
		//param = $xml->results->officer
		$company = $officer->company;
		$source = $officer->source;
		
		
		echo '<tr id="tc1"><td id="lia">Company Name</td><td id="lia">'.$company->name.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Name</td><td id="lia">'.$officer->name.'</td></tr>';
		echo '<tr id="tc1"><td id="lia">Position</td><td id="lia">'.$officer->position.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">External Source</td><td id="lia"><a href="'.$officer->source->url.'" target="_blank">'.$source->publisher.'</a></td></tr>';
		
		
		$company_info = array( 'id' => $company->{'company-number'}, 'jcode' => $company->{'jurisdiction-code'} );
		
		return $company_info;
	}
	
	function off_mainDetailArray($officer){
		$arr_offMainArray = array();
		
		$arr_offMainArray["Company_Name"] = $officer->company->name;
		$arr_offMainArray["Officer_Name"] = $officer->name;
		$arr_offMainArray["Position"] = $officer->position;
		$arr_offMainArray["External_Source"] = $officer->source->publisher;
		$arr_offMainArray["External_Source_URL"] = $officer->source->url;
		$arr_offMainArray["Company_Number"] = $officer->company->{'company-number'};
		$arr_offMainArray["Jurisdiction_Code"] = $officer->company->{'jurisdiction-code'};
		
		return $arr_offMainArray;
	}
	
	function off_mainDetailArrayFix($officer){
		//for static custom officer
		$arr_offMainArray = array();
		
		$arr_offMainArray["Company_Name"] = $officer->company->name;
		$arr_offMainArray["Officer_Name"] = $officer->name;
		$arr_offMainArray["Position"] = "Unknown";
		$arr_offMainArray["External_Source"] = "-";
		$arr_offMainArray["External_Source_URL"] = "-";
		$arr_offMainArray["Company_Number"] = "-";
		$arr_offMainArray["Jurisdiction_Code"] = "Unknown";
		
		return $arr_offMainArray;
	}

	function company_main_detail($company){
		//param = $xml->results->company
		$list_relOfficers = "";
		
		echo '<tr id="tc1"><td id="lia">Company Name:</td><td id="lia">'.$company->name.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Company Number:</td><td id="lia">'.$company->{'company-number'}.'</td></tr>';
		echo '<tr id="tc1"><td id="lia">Company Status:</td><td id="lia">'.$company->{'current-status'}.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Incorporate Date:</td><td id="lia">'.$company->{'incorporation-date'}.'</td></tr>';
		echo '<tr id="tc1"><td id="lia">Dissolution Date:</td><td id="lia">'.$company->{'dissolution-date'}.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Company Type:</td><td id="lia">'.$company->{'company-type'}.'</td></tr>';
		echo '<tr id="tc1"><td id="lia">Jurisdiction Code:</td><td id="lia">'.getCountryName($company->{'jurisdiction-code'}).'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Registered Address:</td><td id="lia">'.$company->{'registered-address-in-full'}.'</td></tr>';
		echo '<tr id="tc1"><td id="lia">SIC Codes:</td><td id="lia">'.$company->{'industry-codes'}->{'industry-code'}->uid.' - '.$company->{'industry-codes'}->{'industry-code'}->title.'</td></tr>';
		echo '<tr id="tc2"><td id="lia">Previous Names:</td>';
		echo '<td id="lia">';
		
		$prev_names = xml_structure($company, 'prev_names_comp');
		prev_names_comp($prev_names);
		
		echo '</td>';
		echo '</tr>';
		echo '<tr id="tc1"><td>Directors/ Officers:</td>';
			echo '<td>';
				$officers = xml_structure($company, 'rel_officers_comp');
				$list_relOfficers = related_officers($officers);
			echo '</td>';
		echo '</tr>';

		echo '<tr id="tc3">';
		echo '<td colspan="2"><h4>Recent filings</h4></td>';
		
		$filings = xml_structure($company, 'filings_comp');
		filings_comp($filings);
		
		echo '</tr>';
		
		return $list_relOfficers;
	}
	
	function comp_mainDetailArray($company, $flag_relOfficer = 2){
		//param = $xml->results->company
		$arr_compMainDetails = array();
		
		$arr_compMainDetails["Company_Name"] = $company->name;
		$arr_compMainDetails["Company_Number"] = $company->{'company-number'};
		$arr_compMainDetails["Company_Status"] = $company->{'current-status'};
		$arr_compMainDetails["Incorporate_Date"] = $company->{'incorporation-date'};
		$arr_compMainDetails["Dissolution_Date"] = $company->{'dissolution-date'};
		$arr_compMainDetails["Company_Type"] = $company->{'company-type'};
		$arr_compMainDetails["Jurisdiction_Code"] = $company->{'jurisdiction-code'};
		$arr_compMainDetails["Registered_Address"] = $company->{'registered-address-in-full'};
		$arr_compMainDetails["SIC_Codes"] = $company->{'industry-codes'}->{'industry-code'}->uid.' - '.$company->{'industry-codes'}->{'industry-code'}->title;
		$arr_compMainDetails["Publisher"] = $company->source->publisher;
		$arr_compMainDetails["Source_URL"] = $company->source->url;
		
		//check and retrieve prev company name in an array
		$prev_names = xml_structure($company, 'prev_names_comp');
		$arr_compMainDetails["Company_Previous_Names"] = comp_prevNamesArray($prev_names);
		
		//check and retrieve prev company name in an array
		$officers = xml_structure($company, 'rel_officers_comp');
		$arr_compMainDetails["Company_Related_Officers"] = rel_officersArray($officers, $flag_relOfficer);

		//check and retrieve prev company name in an array
		$filings = xml_structure($company, 'filings_comp');
		$arr_compMainDetails["Company_Filings"] = comp_filingsArray($filings);

		return $arr_compMainDetails;
	}
	
	function filing_main_detail($filing){
		//param = $XML->results->filing;
		$company = $filing->company;
		$source = $filing->source;
		
		echo '<div class="title">'.$filing->title.'</div>';
		echo '<table class="result filing">';
		echo '<tr id="tc1"><td id="lia">Company:</td>';
		echo '<td id="lia">'.$company->name.'</td>'; 
		echo '</tr>';
		echo '<tr id="tc2"><td id="lia">Filing Date:</td>';
		echo '<td id="lia">'.$filing->date.'</td>'; 
		echo '</tr>';
		echo '<tr id="tc1"><td id="lia">Filing Type:</td>';
		echo '<td id="lia">'.$filing->{'filing-type'}.'</td>'; 
		echo '</tr>';
		echo '<tr id="tc2"><td id="lia">Filing Code:</td>';
		echo '<td id="lia">'.$filing->{'filing-code'}.'</td>'; 
		echo '</tr>';
		echo '<tr id="tc1"><td id="lia">Description:</td>';
		echo '<td id="lia">'.$filing->description.'</td>'; 
		echo '</tr>';
		echo '</table>';
		
	}
	
	function related_officers($officers){
		//param = $XML->results->company->officers->officer
		$list_relOfficers = "";
		foreach($officers as $officer){
			$id = $officer->id;
			$name = $officer->name;
			$position = $officer->position;
			
			if(strlen($list_relOfficers) == 0){
				$list_relOfficers = $id;
			}elseif(strlen($list_relOfficers) > 0){
				$list_relOfficers .= "," . $id;
			}
			
			echo '<a href="edd_detailed.php?id='.$id.'&jcode=&type=officer_details&rdc_names='.$name.'&rdc_type=P&rdc_pagi=1&rdc_id=OCRDC0001" target="_blank">' . $name . '</a>'; 
			echo '<label id="position"> - ' . $position . '</label>';
			echo '<br />';
			
		}
		return $list_relOfficers;
	}
	
	function rel_officersArray($officers, $flag_relOfficer, $filter_duplicates = true){
		//param = $XML->results->company->officers->officer
		//0 = inactive
		//1 = active
		//2 = all
		$tmp_arr = array();
		$tmp_arr2 = array();
		foreach($officers as $officer){
			if($flag_relOfficer > 1){
				if($filter_duplicates){
					$add2arr = true;
					if(count($tmp_arr2) > 0){
						/*
						var_dump($tmp_arr2);
						echo "<br />";
						*/
						$i = 0;
						$cnt_needle = 0;
						$tmp_arr_holder = array();
						foreach($tmp_arr2 as $tmp_officer){
							$name_haystack = (string)$tmp_officer["Officer_Name"];
							$name_needle = (string)$officer->name;
							//echo $name_haystack . " == " . $name_needle . "<br />";
							if($name_haystack == $name_needle){
								$officer->position .= "/" . $tmp_officer["Position"];
								//echo "<br />$i Position: " . $tmp_officer["Position"] . "/ " . $tmp_officer["Officer_Name"] . "<br />";
								//echo "<br />$i Object: " . $officer->position . "<br />";
								$tmp_arr_holder = array('ID' => $tmp_officer["ID"], 'Officer_Name' => $tmp_officer["Officer_Name"], 'Position' => $officer->position);
								//echo "<br />$i Position: " . $tmp_officer["Position"] . "<br />";
								$add2arr = false;
								$cnt_needle = $i;
								break;
							}
							$i++;
						}
						//echo "<hr />";
					}
					if($add2arr){
						array_push($tmp_arr2, array('ID' => $officer->id, 'Officer_Name' => $officer->name , 'Position' => $officer->position));
					}else{
						$tmp_arr2[$cnt_needle] = $tmp_arr_holder;
					}
				}
				array_push($tmp_arr, array('ID' => $officer->id, 'Officer_Name' => $officer->name , 'Position' => $officer->position));
			}else{
				if($filter_duplicates){
					$add2arr = true;
					if(count($tmp_arr2) > 0){
						/*
						var_dump($tmp_arr2);
						echo "<br />";
						*/
						$i = 0;
						$cnt_needle = 0;
						$tmp_arr_holder = array();
						foreach($tmp_arr2 as $tmp_officer){
							$name_haystack = (string)$tmp_officer["Officer_Name"];
							$name_needle = (string)$officer->name;
							//echo $name_haystack . " == " . $name_needle . "<br />";
							if($name_haystack == $name_needle){
								$officer->position .= "/" . $tmp_officer["Position"];
								//echo "<br />$i Position: " . $tmp_officer["Position"] . "/ " . $tmp_officer["Officer_Name"] . "<br />";
								//echo "<br />$i Object: " . $officer->position . "<br />";
								$tmp_arr_holder = array('ID' => $tmp_officer["ID"], 'Officer_Name' => $tmp_officer["Officer_Name"], 'Position' => $officer->position);
								//echo "<br />$i Position: " . $tmp_officer["Position"] . "<br />";
								$add2arr = false;
								$cnt_needle = $i;
								break;
							}
							$i++;
						}
						//echo "<hr />";
					}
					if($add2arr){
						array_push($tmp_arr2, array('ID' => $officer->id, 'Officer_Name' => $officer->name , 'Position' => $officer->position));
					}else{
						$tmp_arr2[$cnt_needle] = $tmp_arr_holder;
					}
				}
				if($officer->{'end-date'} == (!(boolean)$flag_relOfficer)){
					array_push($tmp_arr, array('ID' => $officer->id, 'Officer_Name' => $officer->name , 'Position' => $officer->position));
				}
			}
		}
		if($filter_duplicates){
			/*
			echo "$thisis<hr />";
			var_dump( $haha );
			echo "<hr />";
			var_dump( $tmp_arr2 );
			*/
			return $tmp_arr2;
		}
		return $tmp_arr;
		
	}
	
	function prev_names_comp($prev_names){
		//param = $XML->results->company->{'previous-names'}->{'previous-name'}
		echo '<table class="result pre_comp_names">';
		foreach($prev_names as $prev_name){
			$name = $prev_name->{'company-name'};
			$date = $prev_name->{'con-date'};
			echo '<tr><td>';
			echo '<span id="position">' . $name . ' ('. $date .')</span>';
			echo '</td></tr>';
		}
		echo '</table>';
	}
	
	function comp_prevNamesArray($prev_names){
		//param = $XML->results->company->{'previous-names'}->{'previous-name'}
		$tmp_arr = array();
		foreach($prev_names as $prev_name){
			array_push($tmp_arr, array('Company_Name' => $prev_name->{'company-name'}, 'Con_Date' => $prev_name->{'con-date'}));
		}
		return $tmp_arr;
	}
	
	function filings_comp($filings){
		//param = $XML->results->company->filings->filing
		$i = 0;
		foreach($filings as $filing){
			$id = $filing->id;
			$title = $filing->title;
			$date = $filing->date;
			echo '<tr ';
				if($i%2 == 0){ echo 'id="tc1"'; } else { echo 'id="tc2"'; }
			echo '>';
				echo '<td id="lia">'.$date.'</td>';
				echo '<td id="lia"><a href="edd_detailed.php?id='.$id.'&jcode=&type=filing_details" target="_blank">' . $title . '</a></td>';
			echo '</tr>';
			$i++;
		}
	}
	
	function comp_filingsArray($filings){
		//param = $XML->results->company->filings->filing
		$tmp_arr = array();
		foreach($filings as $filing){
			array_push($tmp_arr, array('ID' => $filing->id , 'Title' => $filing->title , 'Date' => $filing->{'date'}));
		}
		return $tmp_arr;
	}
	
	###### TEST RDC ######
	function rdc_getSearchResult($officers){
		echo 'rdc test <br />';
		foreach($officers as $officer){
			$id = $officer->id;
			$name = $officer->name;
			$position = $officer->position;
			$jcode = $officer->{'jurisdiction-code'};
			$arr_companies = array();
			foreach($officer->company as $company){
				$comp_id = $company->{'company-number'};
				$comp_name = strip_tags(trim($company->name, '"'));
				$comp_jcode = $company->{'jurisdiction-code'};
				$arr_company = array(array("id" => $comp_id, "name" => $comp_name, "jcode" => $comp_jcode ));
				$arr_companies = array_merge($arr_companies, $arr_company);
			}
			
			#srch_type: P, O.
			echo '<div class="row">';
			echo '<div class="result_officer">';
			echo '<a href="rdc_result.php?srch_names='.$name.'&srch_type=P&pagi=1&lst_id=OCRDC0001" target="_blank">'.$name.'</label></a>';
			echo '<label id="position">'.$position.'</label>';
			echo '<label id="jcode">'.getCountryName($jcode).'&nbsp;</label>';
			echo '</div>'; //result_officer
			echo '<div class="result_company">';
			foreach($arr_companies as $company){
				echo '(<a href="rdc_result.php?srch_names='.$company['name'].'&srch_type=O&pagi=1&lst_id=OCRDC0001" target="_blank">'.$company['name'].'</label></a>';
				echo '<label id="jcode">'.getCountryName($company['jcode']).'</label>';
			}
			echo ')</div>'; //result_company
			echo '</div>'; //row
			echo '<br/>';
		}
	}
?>