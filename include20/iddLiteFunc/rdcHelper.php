<?php
	
	function rdc_search($srch_names, $srch_type, $pagi, $lst_id, $add_filter = '', $srch_dates = ''){
	
		#RDC API call function SEARCH.
		#$lst_id = last id that is being saved in the search table from the database.
		$xmlurl = "https://service.rdc.com/api/rdc_search/SearchRequest";
			
		$xml_post = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
		<SearchRequest xmlns:ns2="http://service.rdc.com/api/rdc_search/rdc_search.xsd">
			<header>
				<version>1.3</version>
				<correlationId>'.$lst_id.'</correlationId>
				<credentials>
					<serviceId>EH01005P</serviceId>
					<password>6jPpQ3An</password>
				</credentials>
			</header>
			<pageNumber>'.$pagi.'</pageNumber>
			<pageSize>20</pageSize>
			'.$add_filter.'
			'.$srch_dates.'
			<inquiry>
				<name>'.$srch_names.'</name>
				<type>'.$srch_type.'</type>
			</inquiry>
		</SearchRequest>';

		//echo $xmlurl . '<br />';
		//echo $xml_post . '<br />';

		$dataos = get_cdata_field($xmlurl, $xml_post);
		
		$xmls =  simplexml_load_string($dataos);		
		//print_r($xmls);
		$jsons = json_encode($xmls);
		//echo "".$jsons;
		$rdc_arrays = json_decode($jsons,TRUE);
		//print_r($rdc_arrays);
		
		return $rdc_arrays;
	}
	
	function rdc_getDetails($cor_id, $sys_id){
	
		#RDC API call function Entity Details..
		#$cor_id = last id that is being saved in the search table from the database, which is passed from calling rdc_search function.
		#$sys_id = systemId param from rdc_search function.

		$xmlurl = "https://service.rdc.com/api/rdc_search/EntityDetailRequest";
			
		$xml_post = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
		<EntityDetailRequest xmlns:ns2="http://service.rdc.com/api/rdc_search/rdc_search.xsd">
			<header>
				<version>1.3</version>
				<correlationId>'.$cor_id.'</correlationId>
				<credentials>
					<serviceId>EH01005P</serviceId>
					<password>6jPpQ3An</password>
				</credentials>
			</header>
			<systemId>'.$sys_id.'</systemId>
		</EntityDetailRequest>';

		//echo $xml_post;

		$data = get_cdata_field($xmlurl, $xml_post);
		
		$xmls = simplexml_load_string($data);
		$jsons = json_encode($xmls);
		$rdc_arrays = json_decode($jsons,TRUE);
		/*echo "<pre>";
		print_r($rdc_arrays);
		echo "</pre>";*/

		return $rdc_arrays;
	}
	
	
?>