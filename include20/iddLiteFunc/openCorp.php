<?php

	//config
	CONST formatXML = true;
	CONST apiToken = true;
	
	function get_URLString($id, $jcode, $selection, $pageno = 1, $pagesize = 30){
	
		switch($selection){
			case 'officer_search':
				return searchOfficers_url($id, $jcode);
			break;
			case 'company_search':
				return searchCompanies_url($id, $jcode, $pageno, $pagesize);
			break;
			case 'officer_details':
				return detailedOfficer_url($id);
			break;
			case 'company_details':
				return detailedCompany_url($id, $jcode);
			break;
			case 'filing_details':
				return detailedFiling_url($id);
			break;
			default:
				return null;
		}
	}
	
	function chkJCode($jcode){
		$jcode = urlencode($jcode);
		if (strlen($jcode) > 0 && $jcode != ""){
			$jcode = "&jurisdiction_code=" . $jcode;
		}
		return $jcode;
	}
	
	//return list of officers
	function searchOfficers_url($officer, $jcode){
		//https://api.opencorporates.com/v0.3/officers/search?q=john+smith
		$name = urlencode($officer);
		$call_str = return_xml('https://api.opencorporates.com/v0.3/officers/search?q='.$name . chkJCode($jcode). "&order=score", formatXML, apiToken);
		return $call_str;
	}
	
	//return list of company
	function searchCompanies_url($company, $jcode, $page_number = 1, $page_size = 30){
		//https://api.opencorporates.com/v0.3/companies/search?q=barclays+bank

		$name = urlencode($company);
		$misc = "&order=score&per_page=$page_size&page=$page_number";
		$call_str = return_xml('https://api.opencorporates.com/v0.3/companies/search?q='.$name . chkJCode($jcode) . $misc, formatXML, apiToken);
		return $call_str;
	}
	
	function detailedOfficer_url($id){
		//https://api.opencorporates.com/officers/95309350?format=xml
		$call_str = return_xml('https://api.opencorporates.com/v0.3/officers/'.$id, formatXML, apiToken);
		return $call_str;
	}
	
	function detailedCompany_url($id,$jcode){
		//https://api.opencorporates.com/companies/gb/00102498?format=xml
		$call_str = return_xml('https://api.opencorporates.com/v0.3/companies/'.$jcode.'/'.$id, formatXML, apiToken);
		return $call_str;
	}
	
	function detailedFiling_url($id){
		//https://api.opencorporates.com/v0.3/filings/160728597?format=xml
		$call_str = return_xml('https://api.opencorporates.com/v0.3/filings/'.$id, formatXML, apiToken);
		return $call_str;
	}
	
	########### XML ###########
	function return_xml($string, $xmlFormat, $apiToken){
		if($xmlFormat){
			if (strpos($string, '?')){
				$string = $string.'&format=xml';
			} else {
				$string =  $string.'?format=xml';
			}
		}
		if($apiToken){
			if (strpos($string, '?')){
				$string =  $string.'&api_token=ItDiy0fVGlGyQ3E7R29s';
			} else {
				$string =  $string.'?api_token=ItDiy0fVGlGyQ3E7R29s';
			}
		}
		return $string;
	}
	
	//xml structure
	function xml_structure($data, $type){
		//OpenCorporates v0.3
		
		switch($type){
			case 'officer_details':
				return $data->results->officer;
			break;
			case 'company_details':
				return $data->results->company;
			break;
			case 'officer_search':
				return $data->results->hash->officers->officer;
			break;
			case 'company_search':
				return $data->results->companies->company;
			break;
			case 'filing_details':
				return $data->results->filing;
			break;
			case 'rel_officers_comp':
				return $data->officers->officer;
			break;
			case 'rel_officer_off':
				return $data->results->company->officers->officer;
			break;
			case 'prev_names_comp':
				return $data->{'previous-names'}->{'previous-name'};
			break;
			case 'filings_comp';
				return $data->filings->filing;
			break;
		}
	}
	########### XML END ###########
	
	########### JSON ###########
	
	########### JSON END ###########
?>