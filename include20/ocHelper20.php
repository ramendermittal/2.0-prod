<?php
	class ocHelper {
		const OC_VERSION = 'v0.4';
		private $formatXML = true;
		private $apiToken = true;
		private $mode	= 'loose';
		private $isActiveOnly = true;
		private $category = ''; //company / individual
		private $subject = '';
		private $jcode = '';
		private $ccode = '';
		private $srchStr = '';
		private $url_ocDetailed = '';
		private $url_ocSearched = '';
		
		//public function __construct () {
		//}
		
		public function __get($property){
			if(property_exists($this, $property)){
				return $this->$property;
			}
		}

		public function __set($property, $value){
			if(property_exists($this, $property)){
				$this->$property = $value;
			}
			return $this;
		}
		
		public function srchSubject($pagesize = 30, $pageno = 1, $simplify = false){
			$strURL = '';
			$subjectName = urlencode($this->subject);
			$subjectName = $this->mode == 'strict' ? "{$subjectName}*" : $subjectName;
			$str1 = $this->getURLJCode() . $this->getURLPageNo($pageno) . $this->getURLPageSize($pagesize) . $this->getURLSorting() . $this->getActive() . $this->getURLFormat() . $this->getURLAPIKey();
			switch($this->category){
				case 'organization':
					//https://api.opencorporates.com/v0.3/companies/search?q=barclays+bank
					$strURL = "https://api.opencorporates.com/{$this->OC_VERSION}/companies/search?q={$subjectName}";
				break;
				case 'individual':
				//https://api.opencorporates.com/v0.4/officers/search?q=john+smith
					$strURL = "https://api.opencorporates.com/{$this->OC_VERSION}/officers/search?q={$subjectName}";
					
				break;
				default:
					return null;
			}
			
			$strURL .= strpos($strURL, '?') ? $str1 : substr_replace($str1, '?' , 0, 1);
			$this->url_ocSearched = $strURL;
			$result = $this->retrieveOC($strURL);
			
			if ($simplify)
			{
				if ($this->category == 'organization')
				{
					$companies = array();
					foreach ($result['results']['companies']['company'] as $c)
					{
						$tmp = array();
						$tmp['name'] = $c['name'];
						$tmp['company-number'] = $c['company-number'];
						$tmp['jurisdiction-code'] = $c['jurisdiction-code'];
						$tmp['incorporation-date'] = $c['incorporation-date'];
						$tmp['opencorporates-url'] = $c['opencorporates-url'];
						$tmp['registered-address-in-full'] = $c['registered-address-in-full'];
						$companies[] = $tmp;
					}
					return $companies;
				}
				if ($this->category == 'individual')
				{
					$individuals = array();
					foreach ($result['results']['hash']['officers']['officer'] as $o)
					{
						$tmp = array();
						$tmp['id'] = $o['id'];
						$tmp['name'] = $o['name'];
						$tmp['jurisdiction-code'] = $o['jurisdiction-code'];
						$tmp['position'] = $o['position'];
						$tmp['opencorporates-url'] = $o['opencorporates-url'];
						$tmp['occupation'] = is_array($o['occupation']) ? '':$o['occupation'];
						$tmp['address'] = is_array($o['address']) ? '':preg_replace("/\r|\n/", ' ', $o['address']);
						$tmp['nationality'] = is_array($o['nationality']) ? '':$o['nationality'];
						$tmp['date-of-birth'] = is_array($o['date-of-birth']) ? '':$o['date-of-birth'];
						$tmp['company-name'] = $o['company']['name'];
						$tmp['company-jurisdiction-code'] = $o['company']['jurisdiction-code'];
						$tmp['company-number'] = $o['company']['company-number'];
						$tmp['company-opencorporates-url'] = $o['company']['opencorporates-url'];
						$individuals[] = $tmp;
					}
					return $individuals;
				}
			}
			
			return $result;
		}
		
		public function getSubjectDetails($id, $jcode){
			$strURL = '';
			$str1 = $this->getURLFormat() . $this->getURLAPIKey();
			switch($this->category){
				case 'organization':
					//https://api.opencorporates.com/companies/gb/00102498?format=xml
					$strURL = "https://api.opencorporates.com/v0.4/companies/{$jcode}/{$id}";
				break;
				case 'individual':
					//https://api.opencorporates.com/v0.4/officers/108682693
					$strURL = "https://api.opencorporates.com/v0.4/officers/{$id}";
				break;
				default:
					return null;
			}

			$strURL .= strpos($strURL, '?') ? $str1 : substr_replace($str1, '?' , 0, 1);
			$this->url_ocDetailed = $strURL;
			$result = $this->retrieveOC($strURL);
			
			return $result;
		}
		
		public function getJurisdictions(){
			$strURL = '';
			$str1 = $this->getURLFormat() . $this->getURLAPIKey();
			$strURL = 'https://api.opencorporates.com/v0.4/jurisdictions';
			$strURL .= strpos($strURL, '?') ? $str1 : substr_replace($str1, '?' , 0, 1);
			$this->url_ocDetailed = $strURL;
			$result = $this->retrieveOC($strURL);
			return $result;
		}
		
		private function getURLPageNo($param){
			return $param == '' ? '' : "&page={$param}";
		}
		
		private function getURLPageSize($param){
			return $param == '' ? '' : "&per_page={$param}";
		}
		
		private function getURLSorting(){
			return '&order=score';
		}
		
		private function getActive(){
			return $this->isActiveOnly ? '&inactive=false' : '&active=true';
		}

		private function getURLJCode(){
			return $this->jcode == '' ? "&country_code={$this->ccode}" : "&jurisdiction_code={$this->jcode}";
		}
		
		private function getURLFormat(){
			return $this->formatXML ? '&format=xml' : '';
		}
		
		private function getURLAPIKey(){
			return $this->apiToken ? '&api_token=ItDiy0fVGlGyQ3E7R29s' : '';
		}
		
		
		private function retrieveOC($url){			
			$raw_data = $this->getOCDataCURL($url);
			if(isset($raw_data["curlError"])){
				$arr_data = $raw_data;
			}else{
				/*
				*/
				$arr_data = new SimpleXMLElement($raw_data);
				
				/*
				$xml = simplexml_load_string($raw_data, "SimpleXMLElement", LIBXML_NOCDATA);
				$json = json_encode($xml);
				$arr_data = json_decode($json,TRUE);
				
				$arr_data = json_decode($raw_data,TRUE);
				*/
			}
			return $arr_data;
		}
		
		private function getOCDataCURL($path){
			$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$path);
			curl_setopt($ch, CURLOPT_USERAGENT, $ua);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_FAILONERROR,true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			//curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			$retValue = curl_exec($ch);	

			if(curl_errno($ch)){
				$retValue = array("curlError" => curl_error($ch));
			}
			
			curl_close($ch);

			return $retValue;
		}
	}
	
?>
