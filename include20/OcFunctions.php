<?php

/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/

if (isset($_REQUEST['pageChange']))
{
	require_once 'config20.php';
	require_once 'db20.php';
	require_once 'common20.php';
	require_once 'FilterFunctions.php';
	//preVar($_REQUEST);
	echo getRows();
	die();
}

if (isset($_REQUEST['pageNumChange']))
{
	echo pgHtml();
	die();
}

require_once 'FilterFunctions.php';

function OcRequest($type,$params=array())
{
	$urls = array
	(
		'jurisdictions'			=>	'jurisdictions?api_token=ItDiy0fVGlGyQ3E7R29s',
		'searchcompanies'		=>	'companies/search?q=%s&jurisdiction_code=%s&api_token=ItDiy0fVGlGyQ3E7R29s&page=%s&per_page=10',
		'searchcompaniesall'	=>	'companies/search?q=%s&api_token=ItDiy0fVGlGyQ3E7R29s&page=%s&per_page=10',
		'searchcompaniesone'	=>	'companies/search?q=%s&jurisdiction_code=%s&api_token=ItDiy0fVGlGyQ3E7R29s&order=score&page=1&per_page=1',
		'searchcompaniestest'	=>	'companies/search?q=%s&api_token=ItDiy0fVGlGyQ3E7R29s&per_page=100&page=%s',
		'searchofficers'		=>	'officers/search?q=%s&jurisdiction_code=%s&api_token=ItDiy0fVGlGyQ3E7R29s&page=%s&per_page=10',
		'searchofficersall'		=>	'officers/search?q=%s&api_token=ItDiy0fVGlGyQ3E7R29s&page=%s&per_page=10',
		'searchofficersone'		=>	'officers/search?q=%s&api_token=ItDiy0fVGlGyQ3E7R29s&order=score&page=1&per_page=1',
		'getcompany'			=>	'companies/%s/%s?api_token=ItDiy0fVGlGyQ3E7R29s',
		'getofficer'			=>	'officers/%s?sparse=true&api_token=ItDiy0fVGlGyQ3E7R29s',
	);
	
	$url = 'https://api.opencorporates.com/v0.4/'.$urls[$type];
	$params = array_map('urlencode',$params);
	$url = vsprintf($url,$params);
	//echo "$url<br><br>";
	return json_decode(file_get_contents($url), true);
}
function OcGetJurisdictions()
{
	$res = OcRequest('jurisdictions');
	$data = array();
	foreach($res['results']['jurisdictions'] as $i)
	{
		$data[$i['jurisdiction']['code']] = $i['jurisdiction']['name'];
	}
	ksort($data);
	return $data;
}
function OcSearchCompanies($search, $jurisdiction = '', $single = false, &$pgdetails, $page = 1)
{
	if (!$search) die('empty string search is not allowed');
	$search = filter_keywords($search);
	if ($single) $res = OcRequest('searchcompaniesone',array($search,$jurisdiction));
	elseif ($jurisdiction) $res = OcRequest('searchcompanies',array($search,$jurisdiction,$page));
	else $res = OcRequest('searchcompaniesall',array($search,$page));
	$data = array();
	foreach($res['results']['companies'] as $i)
	{
		$data[] = $i['company'];
	}
	$pgdetails['page'] = $res['results']['page'];
	$pgdetails['per_page'] = $res['results']['per_page'];
	$pgdetails['total_count'] = $res['results']['total_count'];
	$pgdetails['total_pages'] = $res['results']['total_pages'];
	return $data;
}
function OcSearchCompaniesTest($search, &$pgdetails, $page = 1)
{
	if (!$search) die('empty string search is not allowed');
	$search = filter_keywords($search);
	$res = OcRequest('searchcompaniestest',array($search,$page));
	$data = array();
	foreach($res['results']['companies'] as $i)
	{
		$data[] = $i['company'];
	}
	$pgdetails['page'] = $res['results']['page'];
	$pgdetails['per_page'] = $res['results']['per_page'];
	$pgdetails['total_count'] = $res['results']['total_count'];
	$pgdetails['total_pages'] = $res['results']['total_pages'];
	return $data;
}
function OcSearchOfficers($search, $jurisdiction = '', $single = false, &$pgdetails, $page = 1)
{
	if (!$search) die('empty string search is not allowed');
	$search = filter_keywords($search);
	if ($jurisdiction) $res = OcRequest('searchofficers',array($search,$jurisdiction,$page));
	elseif ($single) $res = OcRequest('searchofficersone',array($search));
	else $res = OcRequest('searchofficersall',array($search,$page));
	$data = array();
	foreach($res['results']['officers'] as $i)
	{
		$data[] = $i['officer'];
	}
	$pgdetails['page'] = $res['results']['page'];
	$pgdetails['per_page'] = $res['results']['per_page'];
	$pgdetails['total_count'] = $res['results']['total_count'];
	$pgdetails['total_pages'] = $res['results']['total_pages'];
	return $data;
}
function OcGetCompany($id,$jurisdiction,$specific = false)
{
	$res = OcRequest('getcompany',array($jurisdiction,$id));
	if ($specific)
	{
		switch ($specific)
		{
			case 'officers':
				$off = array();
				foreach ($res['results']['company']['officers'] as $o)
				{
					$off[] = $o['officer'];
				}
				return $off;
				break;
		}
	}
	return $res['results']['company'];
}
function OcGetOfficer($id)
{
	$res = OcRequest('getofficer',array($id));
	return $res['results']['officer'];
}

if (isset($_REQUEST['ocdebug']))
{
	/*
	$x = OcSearchOfficers('romero rauls','us_al');
	$x = OcGetJurisdictions();
	$x = OcGetOfficer('140098322');
	*/
	$x = OcSearchCompanies('uberstuff','',false,$pgdetails);
	echo '<pre>';
	print_r($x);
	echo '<hr>';
	$x = OcGetCompany('1949751','nz');
	print_r($x);
	print_r($pgdetails);
	echo '</pre>';
}

function countryName($code, $withState = false)
{
	require_once 'MysqliDb.php';
	$fdb = new MysqliDb;
	
	if (!$code) return '';
	$fullcode = $code;
	$codes = explode('_',$code);
	$code = $codes[0];
	$scode = $codes[1];
	if (!$scode) $withState = false;
	
	$fdb->where('code', $code);
	$row = $fdb->getOne('oc_countries', 'country');
	$cname = $row['country'];
	
	if ($withState)
	{
		$fdb->where('code', $fullcode);
		$row = $fdb->getOne('oc_states', 'state');
		$sname = $row['state'];
	}
	
	return $withState ? "$sname ($cname)" : $cname;
}

function countryCode($country)
{
	if (!$country) return '';
	require_once 'MysqliDb.php';
	$fdb = new MysqliDb;
	
	$fdb->where('country', $country);
	$row = $fdb->getOne('oc_countries', 'code');
	if ($row) return $row['code'];
	
	//in case it is a state
	$fdb->where('state', $country);
	$row = $fdb->getOne('oc_states', 'code');
	if ($row) return $row['code'];
	
	return '';
}

function getPgNums($pg, $tt)
{
	$pg = (int) $pg;
	$tt = (int) $tt;
	if (!$pg || !$tt) return array(1);
	
	$ptd = array();
	$bx = 14;
	if ($tt <= $bx) $bx = $tt-1;

	$i = 1;
	while (true)
	{
		$p = $pg-$i;
		if ($p > 0)
		{
			$ptd[] = $p;
			$bx--;
		}
		
		$q = $pg+$i;
		if ($q <= $tt)
		{
			$ptd[] = $q;
			$bx--;
		}
		
		if (!$bx) break;
		$i++;
	}

	$ptd[] = $pg;
	sort($ptd);
	return $ptd;
}

function pgHtml($page=0,$total_pages=0,$alt=0)
{
	if (!$page) $page = $_REQUEST['alt'] ? $_REQUEST['pageno_alt'] : $_REQUEST['pageno'];
	if (!$total_pages) $total_pages = $_REQUEST['alt'] ? $_REQUEST['tpagealt'] : $_REQUEST['tpage'];
	if (!$alt) $alt = $_REQUEST['alt'] ? true : false;
	
	//we are only allowed 100 pages max from OC
	if ($total_pages > 100) $total_pages = 100;
	
	$pgname = $alt ? 'pageboxalt':'pagebox';
	$pgnamec = $alt ? 'pageboxaltc':'pageboxc';
	$inpname = $alt ? 'pageno_alt':'pageno';
	
	$ptd = getPgNums($page,$total_pages);
	$str = 'Page&nbsp;:&nbsp;';
	if ($ptd[0] != 1) $str .= '<span pg="1" class="'.$pgname.'">&laquo;</span>&nbsp;';
	
	foreach($ptd as $p)
	{
		$cl = ($p == $page) ? $pgnamec:$pgname;
		$str .= '<span pg="'.$p.'" class="'.$cl.'">'.$p.'</span>&nbsp;';
	}
	
	if (end($ptd) != $total_pages) $str .= '<span pg="'.$total_pages.'" class="'.$pgname.'">&raquo;</span>&nbsp;';
	$str .= '<input type="hidden" name="'.$inpname.'" id="'.$inpname.'" value="'.$page.'">';
	return $str;
}

function getRows()
{
	$search = trim($_REQUEST['search']);
	$jurisdiction = $_REQUEST['alt'] ? '' : trim($_REQUEST['country']);
	$page = $_REQUEST['alt'] ? $_REQUEST['pageno_alt'] : $_REQUEST['pageno'];
	$cbclass = $_REQUEST['alt'] ? 'cbsel2' : 'cbsel1';
	$str = '';
	
	if (isset($_REQUEST['sch']))
	{
		if ($_REQUEST['entity_type'] == 'company') $records = OcSearchCompanies($search, $jurisdiction, false, $pgdetails, $page);
		if ($_REQUEST['entity_type'] == 'individual') $records = OcSearchOfficers($search, $jurisdiction, false, $pgdetails, $page);
		
		foreach($records as $r)
		{
			if ($_REQUEST['alt'])
			{
				if ($r['jurisdiction_code'] == $jurisdiction) continue;
			}

			$str .= '<tr>';
			$str .= '<td style="width:800px;border:1px solid #9a9c9e;">'.$r['name'].'</td>';

			if ($_REQUEST['entity_type'] == 'individual')
			{
				$selectionkey = 'id';
				$str .= '<td style="border:1px solid #9a9c9e;">'.$r['company']['name'].'</td>';
			}

			$str .= '<td style="text-align:center;border:1px solid #9a9c9e;">'.strtoupper(countryName($r['jurisdiction_code'],true)).'</td>';

			if ($_REQUEST['entity_type'] == 'company')
			{
				$selectionkey = 'company_number';
				$str .= '<td style="text-align:center;border:1px solid #9a9c9e;">'.strtoupper($r['current_status']).'</td>';
			}

			$str .= '<td style="text-align:center;border:1px solid #9a9c9e;">';
			$str .= '<input type="checkbox" class="cbsel '.$cbclass.'" ctr="'.$r['jurisdiction_code'].'" name="selections['.$r[$selectionkey].']" value="'.base64_encode($r['name']).'">';
			$str .= '</td>';
			$str .= '<td></td>';
			$str .= '</tr>';
		}
	}
	
	if (isset($_REQUEST['cmp']))
	{
		$records = OcSearchOfficers($search, '', false, $pgdetails, $page);
		
		foreach($records as $r)
		{
			if (!$r['company']['name'] || !$r['company']['company_number']) continue;
			$str .= '<tr>';
			$str .= '<td style="border:1px solid #9a9c9e;">'.$r['company']['name'].'</td>';
			$str .= '<td style="border:1px solid #9a9c9e;">'.strtoupper(countryName($r['jurisdiction_code'], true)).'</td>';
			$str .= '<td style="border:1px solid #9a9c9e;">'.$r['position'].'</td>';
			$str .= '<td style="border:1px solid #9a9c9e;" align="center"><input type="checkbox" class="cbsel" name="icompany['.$r['company']['company_number'].']" value="'.$r['company']['jurisdiction_code'].'|'.$r['id'].'"></td>';
			$str .= '</tr>';
		}
	}
	
	return $str;
}
