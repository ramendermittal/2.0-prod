<?php

class EthixbaseStrEncryption
{
	const CIPHER = MCRYPT_RIJNDAEL_128;
	const MODE = MCRYPT_MODE_CBC;

	private $key;
	public function __construct()
	{
		$this->key = '70d66bd93235688dc3c02d833bc6f6a0';
	}

	public function encrypt($plaintext)
	{
		if (empty($plaintext)) return '';
		$plaintext = base64_encode($plaintext);
		$iv = 'b0a6693171d8d2f9';
		$ciphertext = mcrypt_encrypt(self::CIPHER, $this->key, $plaintext, self::MODE, $iv);
		return base64_encode($iv.$ciphertext);
	}

	public function decrypt($ciphertext)
	{
		if (empty($ciphertext)) return '';
		$ciphertext = base64_decode($ciphertext);
		$ivSize = 16;
		if (strlen($ciphertext) < $ivSize) {
			//throw new Exception('Missing initialization vector');
			return 'DECRYPT_ERR_IV';
		}

		$iv = substr($ciphertext, 0, $ivSize);
		$ciphertext = substr($ciphertext, $ivSize);
		$plaintext = mcrypt_decrypt(self::CIPHER, $this->key, $ciphertext, self::MODE, $iv);
		if (!$plaintext) return 'DECRYPT_ERR_DEC';
		return base64_decode(rtrim($plaintext, "\0"));
	}
}

$ese = new EthixbaseStrEncryption();

function decryptRow($row)
{
	if (!$row) return $row;
	
	$enc = new EthixbaseStrEncryption();
	
	$cols = array(
	
		//svi
		'comp_name_eng',
		'comp_name_chi',
		'address',
		'tel_no',
		'website',
		'address1',
		'address2',
		'city',
		'state',
		'postal_code',
		'comp_desc',
		'vendor_info',
		'dd_quest',
		'employee_ci',
		'compl_con_nam',
		'compl_con_tit',
		'compl_con_email',
		'compl_con_nam2',
		'compl_con_tit2',
		'compl_con_email2',
		'compl_con_nam3',
		'compl_con_tit3',
		'compl_con_email3',
		'director_1',
		'director_2',
		'director_3',
		'director_4',
		'shareholder_1',
		'shareholder_2',
		'shareholder_3',
		'shareholder_4',
		'lg_email',
		
		//ve
		'company_name',
		'contact_name',
		'contact_email',
		'corp_address',
		'company_logo',
		'company_contacts',
		'company_dd',
		'company_ethics',
	);
	
	foreach ($row as $key => $value)
	{
		if (in_array($key, $cols))
		{
			$row[$key] = $enc->decrypt($value);
		}
	}
	
	return $row;
}
