<?php

	function readExcel($f)
	{
		require_once 'include20/PHPExcel/PHPExcel/IOFactory.php';
		$time = time();
		$fname = 'giddUpload/phpexcel_convert_'.$time.'.csv';
		$objPHPExcel = PHPExcel_IOFactory::load($f);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(',')->setEnclosure('"')->setLineEnding("\r\n")->setSheetIndex(0)->save($fname);

		$out = array();
		$err = array();
		$csv = array_map('str_getcsv', file($fname));
		foreach($csv as $k => $c)
		{
			$ct = array_map('trim',$c);
			
			//give em name for easier reference
			
			$xType =	$ct[0];
			if (!in_array($xType,array('O','P','A'))) continue;
			
			$xCname =		$ct[1];
			$xOCountry =	$ct[2];
			
			$xFname =		$ct[3];
			$xLname =		$ct[4];
			$xPCountry =	$ct[5];
			
			if (empty($xCname) && empty($xFname) && empty($xLname)) continue;
			
			$e = array();
			if ($xType == 'O' && empty($xCname)) $e[] = "empty company name";
			if ($xType == 'O' && empty($xOCountry)) $e[] = "empty company country";
			if ($xType == 'A' && (empty($xFname)||empty($xLname))) $e[] = "incomplete associate name";
			if ($xType == 'P' && (empty($xFname)||empty($xLname))) $e[] = "incomplete individual name";
			if ($e)
			{
				$err[] = join(', ',$e)." (row:$k)";
				continue;
			}
			
			$out[] = $ct;
		}
		
		if ($err)
		{
			//preVar($err);die;
			return false;
		}
		
		if (!$out) return false;

		$formatted = array();
		$formatted[] = array('type','subject_name','first_name','last_name','job_title','industry','country','state','address','contact_no','email');

		foreach ($out as $o)
		{
			$ztype =			$o[0];
			$zsubject_name =	$o[1];
			$zfirst_name =		$o[3];
			$zlast_name =		$o[4];
			//$zjob_title =		$o[9];
			//$zindustry =		'';
			$zcountry =			$o[2];
			//$zstate =			$o[20];
			//$zaddress =			$o[17];
			//$zcontact_no =		'';
			//$zemail =			$o[15];
			$formatted[] = array($ztype,$zsubject_name,$zfirst_name,$zlast_name,$zcountry);
		}
		
		//return array instead for now
		return $formatted;

		$outfile = 'giddUpload/generated_'.$time.'.csv';
		$fp = fopen($outfile, 'w+');
		foreach ($formatted as $f) fputcsv($fp, $f);
		fclose($fp);
		
		/*
		preVar($out);
		preVar($err);
		preVar($formatted);
		*/
		
		return $outfile;
	}
	
	function readCsv($f)
	{
		$errCode = 0;
		$csv = array_map('str_getcsv', file($f));
		foreach ($csv as $c)
		{
			if (count($c) != 6)
			{
				$errCode = 1;//Invalid file content (mismatch columns count)
				break;
			}
		}

		$data = array();
		if (!$errCode)
		{
			foreach ($csv as $c)
			{
				$ct = array_map('trim',$c);
				if
				(
					empty($ct[0])
					||
					(empty($ct[1]))
					||
					empty($ct[3])
				)
				{
					$errCode = 2;//Invalid file content (missing required information)
					break;
				}
				else
				{
					$data[] = $ct;
				}
			}
		}
		return $errCode ? false : $data;
	}
	