<?php

	require_once 'include20/ocHelper20.php';
	
	if($shield_id != '')
	{
		//die('going into '.__FILE__);
		
		define('__DEBUG__', false);
		$ese = new EthixbaseStrEncryption();
		
		//get vendor edu info for TPI
		$sql_tpiInfo = "	SELECT ve.id, ve.url_id, ve.user_id, ve.access_user_id
											FROM vendor_edu ve
											WHERE (FIND_IN_SET({$_SESSION['member_id']}, ve.access_user_id) IS NOT NULL AND FIND_IN_SET({$_SESSION['member_id']}, ve.access_user_id) > 0)
											";
		$res_tpiInfo = mysql_query($sql_tpiInfo) or die(mysql_error());
		$row_tpiInfo = mysql_fetch_assoc($res_tpiInfo);
		$glb_compId = $row_tpiInfo['id'];
		$glb_urlId = $row_tpiInfo['url_id'];
		
		if ($entity_type == 'company')
		{
			//get batch uploaded information of organization (from user)
			$set_orgInfo = array();
			$sql_orginfo = "	SELECT oi.id
												, oi.gidd_name
												, oi.gidd_address
												, oi.gidd_country
												, oi.gidd_state
												, oi.gidd_fullname
												, oi.gidd_email
												, oi.gidd_jobtitle
												, oi.gidd_contactno
												, oi.gidd_desc
												, cl.oc_ccode
												, sl.oc_scode
												FROM gidd_orginfo oi
												LEFT JOIN country_list cl ON cl.country_name = oi.gidd_country
												LEFT JOIN state_list sl ON sl.oc_ccode = cl.oc_ccode AND sl.state_name = oi.gidd_state
												WHERE shieldinfo_id = '{$shield_id}'
												ORDER BY id
												";
			$res_orgInfo = mysql_query($sql_orginfo) or die(mysql_error());
			
			//if(__DEBUG__) echo "$sql_orginfo<br><br>";
			
			if(__DEBUG__) echo "shieldId:{$shield_id}<br />";
			
			##MAIN PROCESS BEGIN
			while($row_orgInfo = mysql_fetch_assoc($res_orgInfo)){
				$main_id = $row_orgInfo['id'];
				$main_xSubjectName = $row_orgInfo['gidd_name'];
				$main_country = $row_orgInfo['gidd_country'];
				$main_state = $row_orgInfo['gidd_state'];
				$main_xAddress = $row_orgInfo['gidd_address'];
				$main_xFullName = $row_orgInfo['gidd_fullname'];
				$main_xEmail = $row_orgInfo['gidd_email'];
				$main_xJobTitle = $row_orgInfo['gidd_jobtitle'];
				$main_xContactNo = $row_orgInfo['gidd_contactno'];
				$main_ocCountryCode = $row_orgInfo['oc_ccode'];
				$main_ocStateCode = $row_orgInfo['oc_scode'];
				$jcode = $row_orgInfo['gidd_desc'];
				
				$main_subjectName = $ese->decrypt($main_xSubjectName);
				$main_address = $ese->decrypt($main_xAddress);
				
				$arr_assoInfoUser = array();
				$arr_assoID = array();
				$arr_assoName = array();
				$arr_assoGRID = array();
				$ins_offGRID = '';
				
				//check here
				$sql5 = "select id from shield_vendor_info where comp_id = '$glb_compId' and comp_name_eng = '$main_xSubjectName' and country = '$main_country' limit 1 ";
				$res5 = mysql_query($sql5) or die(mysql_error());
				if (mysql_num_rows($res5))
				{
					$row5 = mysql_fetch_assoc($res5);
					$glb_tpiId = (int) $row5['id'];
				}
				else
				{
					//register uploaded information one by one into shield_vendor_info table
					$ins_orgTPI = " INSERT INTO shield_vendor_info SET
													url_id = '{$glb_urlId}'
													, comp_id = '{$glb_compId}'
													, tpi_type = 'O'
													, comp_name_eng = '{$main_xSubjectName}'
													, address = '{$main_xAddress}'
													, country = '{$main_country}'
													, state = '{$main_state}'
													, tel_no = '{$main_xContactNo}'
													, compl_con_nam = '{$main_xFullName}'
													, compl_con_tit = '{$main_xJobTitle}'
													, compl_con_email = '{$main_xEmail}'
													, lg_email = '{$main_xEmail}'
													";
					//yet another changes
					/*
					$res_orgTPI = mysql_query($ins_orgTPI) or die(mysql_error());
					$glb_tpiId = mysql_insert_id();
					*/
					$glb_tpiId = 0;
				}
				
				//update gidd_orginfo with TPI_id
				$upd_orgInfo = "	UPDATE gidd_orginfo
													SET
													tpi_id = '{$glb_tpiId}'
													WHERE
													id = '{$main_id}'
													";
				mysql_query($upd_orgInfo) or die(mysql_error());
				
				if(__DEBUG__){
					echo "<hr />Search Name: {$main_xSubjectName}<br />";
					echo "Search Name (decrypted): {$main_subjectName}<br />";
					echo "Search Country: {$main_country}<br />";
					echo "Search State: {$main_state}<br />";
					echo "Search ID: {$main_id}<br />";
					echo "***********************<br />";
					//echo "<br />Organization(TPI) sql insert:<br />{$ins_orgTPI}<br />";
					//echo "<br />Organization(TPI) sql insert:<br />{$upd_orgInfo}<br />";
					echo "***********************<br />";
				}
				
				//get list of associates that are link to the organization (from user)
				$sql_assoinfo = "	SELECT id
													, tpi_id
													, gidd_firstname
													, gidd_lastname
													, gidd_jobtitle
													, gidd_address
													, gidd_country
													FROM gidd_assoinfo
													WHERE shieldinfo_id = '{$shield_id}'
													AND orginfo_id = '{$main_id}'
													";
				$res_assoInfo = mysql_query($sql_assoinfo) or die(mysql_error());
				
				//parse associates (user)
				##ASSOCIATES SECTION BEGIN
				while($row_assoInfo = mysql_fetch_assoc($res_assoInfo)) {
					$arr_assoInfoUser[] = $row_assoInfo;
					$arr_assoID[] = $row_assoInfo['id'];
					$asso_xFirstName = $row_assoInfo['gidd_firstname'];
					$asso_xLastName = $row_assoInfo['gidd_lastname'];
					$asso_xAddress = $row_assoInfo['gidd_address'];
					$asso_xJobTitle = //$row_assoInfo['gidd_jobtitle'];
					$asso_countryName = $row_assoInfo['gidd_country'];
					
					$asso_firstName = $ese->decrypt($asso_xFirstName);
					$asso_lastName = $ese->decrypt($asso_xLastName);
					$asso_address = $ese->decrypt($asso_xAddress);
					
					$asso_fullName = "{$asso_firstName} {$asso_lastName}";
					
					$asso_xFullName = $ese->encrypt($asso_fullName);
					$asso_xAddress = strlen($asso_address) > 60 ? '' : $ese->encrypt($asso_address);
					
					//collecting name to compare associates (from user) with officers (from oc) for duplicates
					$arr_assoName[] = strtolower($asso_fullName);
					//collecting bactch batch insert query for rdc_grd_asso.
					$arr_assoGRID[] = "('{$glb_compId}', '{$glb_tpiId}', '{$row_assoInfo['id']}', 'asso', 'P', '{$asso_xFullName}', '{$asso_xJobTitle}', '{$asso_xAddress}', '{$asso_countryName}','1', '0', now())";
				}
				
				//populate associate from user input into rdc_grd_asso table
				if(count($arr_assoGRID) > 0){
					$str_assoGRIDValues = implode(', ', $arr_assoGRID);
					$ins_offGRID = " 	INSERT INTO rdc_grd_asso
														(comp_id, tpi_id, gidd_id, gidd_type, gr_type, rec_name, rec_title, rec_address, rec_country, is_idd, is_reviewed, inserted_date) 
														VALUES 
														{$str_assoGRIDValues} 
														ON DUPLICATE KEY UPDATE
														rec_note = VALUES(comp_id)
														";
					//yet another changes
					/*
					$res_offGRID = mysql_query($ins_offGRID) or die(mysql_error());
					*/
				}
				##ASSOCIATES SECTION END
				
				if(__DEBUG__){
					echo "Associates:<br />";
					if(count($arr_assoInfoUser) > 0){
						$i = 1;
						foreach($arr_assoInfoUser as $associate){
							echo "-> {$i}) Name (user): {$asso_xFullName}<br />";
							echo "-> {$i}) Name (user) (decrypted): {$asso_fullName}<br />";
							echo "-> {$i}) ID (user): {$associate['id']}<br />";
						}
						echo "-----------------------<br />";
					}
				}
				
				##OC SECTION BE
				if($main_ocCountryCode != '' || $main_ocStateCode != '')
				{
					//call oc class
					$och = new ocHelper();
					//set oc search type organization or individual
					$och->category = 'organization';
					//mode of searches strict or loose (wildcard searches)
					$och->mode = 'strict';
					//set search subject
					$och->subject = urlencode($main_subjectName);
					$och->subject = $main_subjectName;
					
					//OC filter condition based on user's provided state
					/*
					if($main_state == '' || empty($main_state) || ){
						$och->ccode = $main_ocCountryCode;
					}else{
						$och->jcode = "{$main_ocCountryCode}_{$main_ocStateCode}";
					}
					*/
					
					$och->jcode = $main_ocStateCode ? "{$main_ocCountryCode}_{$main_ocStateCode}" : "{$main_ocCountryCode}";
					$och->jcode = $jcode;
					
					if(__DEBUG__){
						echo "Search Area: " . $main_state == '' || empty($main_state) ? "{$main_country}<br />" : "{$main_state}, {$main_country}<br />";
					}
					
					
					//get organization result(oc)
					//preVar($och);
					$srchResult = $och->srchSubject(1, 1);
					
					if(__DEBUG__){
						echo "=======================<br />";
						echo "OC hits data:<br />";
						echo "No of company found: {$srchResult->results->{'total-count'}}<br />";
					}
					
					//check if there is any hit
					if($srchResult->results->{'total-count'} > 0){
						foreach($srchResult->results->companies->company as $company){
							$oc_strOffInfo = '';
							$oc_arrOffGRID = array();
							$oc_arrOfficer = array();
							$oc_arrFile = array();
							$oc_arrPrevName = array();
							$oc_arrAltName = array();
							$ins_offOC = '';
							
							//get organization detailed (oc)
							$rsltDetail = $och->getSubjectDetails($company->{'company-number'}, $company->{'jurisdiction-code'});
							$oc_companyName = $rsltDetail->results->company->name;
							$oc_xCompanyName = $ese->encrypt($oc_companyName);

							if(__DEBUG__){
								echo "{$och->url_ocDetailed}<br />";
								echo "Company-Name: {$oc_companyName}<br />";
								echo "Company-Name (encrypted): {$oc_xCompanyName}<br />";
								echo "Company-Number: {$rsltDetail->results->company->{'company-number'}}<br />";
								echo "officer's info:<br />";
							}
							
							//parse alternative names
							if(count($rsltDetail->results->company->{'alternative-names'}->{'alternative-name'}) > 0){
								foreach($rsltDetail->results->company->{'alternative-names'}->{'alternative-name'} as $altName){
									$oc_altNameCompName = $altName->{'company-name'};
									$oc_arrAltName[] = "{$oc_altNameCompName}";
								}
							}
							
							//parse previous names
							if(count($rsltDetail->results->company->{'previous-names'}->{'previous-name'}) > 0){
								foreach($rsltDetail->results->company->{'previous-names'}->{'previous-name'} as $prevName){
									$oc_prevNameCompName = $prevName->{'company-name'};
									$oc_prevNameEndDate = $prevName->{'end-date'};
									$oc_arrPrevName[] = "{$oc_prevNameCompName}, {$oc_prevNameEndDate}";
								}
							}
							
							//parse fillings
							if(count($rsltDetail->results->company->filings->filing) > 0){
								foreach($rsltDetail->results->company->filings->filing as $filing){
									$oc_fileTitle = $filing->title;
									$oc_fileDate = $filing->{'date'};
									$oc_arrFile[] = "{$oc_fileTitle}, {$oc_fileDate}";
								}
							}
							
							//search20_tpis
							$xuid = $_SESSION['member_id'];
							$sql3 = "insert into search20_tpis (user_id,srch_txt,entity_type,country,created_date) values('$xuid','$oc_companyName','company','$main_country',now())";
							$res3 = mysql_query($sql3) or die(mysql_error());
							$srch_id = mysql_insert_id();
							$_REQUEST['last_srch_id'] = $srch_id;
							
							//parse associates (oc)
							if(count($rsltDetail->results->company->officers->officer) > 0){
								if(__DEBUG__){
									echo "OC Officers:<br />";
								}
								foreach($rsltDetail->results->company->officers->officer as $officer){
									$oc_offName = $officer->name;
									$oc_offPosition = $officer->position;
									$oc_offAddress = $officer->address;
									$oc_offOCURL = $officer->{'opencorporates-url'};
									
									$oc_xOffName = $ese->encrypt($oc_offName);
									$oc_xOffPosition = $oc_offPosition; //$ese->encrypt($oc_offPosition);
									$oc_xOffAddress = strlen($oc_offAddress) > 60 ? '' : $ese->encrypt($oc_offAddress);
									$oc_xOffOCURL = $ese->encrypt($oc_offOCURL);
									
									//compare name with Associates (user provided)
									if(in_array(strtolower($officer->name), $arr_assoName)){
										$is_duplicate = '1';
									}else{
										$is_duplicate = '0';
									}
									
									/* oc_id, fullname, position, uid, start_date, end_date, opencorporates_url, occupation, address, nationality, date_of_birth, created_by, created_date */
									$oc_strOffInfo = "('{$officer->id}', '{$glb_tpiId}', '$srch_id' ,'{$oc_xOffName}', '{$oc_xOffPosition}', '{$officer->uid}', '{$officer->{'start-date'}}', '{$officer->{'end-date'}}', '{$oc_xOffOCURL}', '{$officer->{'occupation'}}', '{$oc_xOffAddress}', '{$officer->{'nationality'}}', '{$officer->{'date-of-birth'}}', '{$is_duplicate}', 'system', now())";
									$oc_arrOfficer[] = "{$oc_offName}, {$oc_offPosition}";

									//insert into oc officer info table
									$ins_offOC = " 	INSERT INTO oc_offinfo
																	(oc_id, tpi_id, srch_id, fullname, position, uid, start_date, end_date, opencorporates_url, occupation, address, nationality, date_of_birth, is_dup, created_by, created_date) 
																	VALUES 
																	{$oc_strOffInfo}
																	";
									$res_offOC = mysql_query($ins_offOC) or die(mysql_error());
									$oc_offId = mysql_insert_id();
									
									//add to GRID if not duplicate
									if($is_duplicate == '0'){
										$oc_arrOffGRID[] = "('{$glb_compId}', '{$glb_tpiId}', '{$oc_offId}', 'offi', 'P', '{$oc_xOffName}', '{$oc_xOffPosition}', '{$oc_xOffAddress}', '', '1', '0', now())";
									}
									
									//search20_assos
									$xuid = $_SESSION['member_id'];
									$sql4 = "insert into search20_assos (srch_id,user_id,asso_name,created_date) values ('$srch_id','$xuid','$oc_offName',now()) ";
									$res4 = mysql_query($sql4) or die(mysql_error());
									$xxx = mysql_insert_id();
									
									if(__DEBUG__){
										echo "-- Name (oc): {$oc_offName}<br />";
										echo "-- Name (oc) (encrypted): {$oc_xOffName}<br />";
										echo "-- ID (oc): {$officer->id}<br />";
										echo "-----------------------<br />";
									}
								}

							}else{
								if(__DEBUG__){
									echo "-- No officers provided by company (oc hits).<br />";
									echo "-----------------------<br />";
								}
							}
							
							$str_associates = implode(', ', $arr_assoID);
							$str_officers = implode(';;', $oc_arrOfficer);
							$str_altnames = implode(';;', $oc_arrAltName);
							$str_prevnames = implode(';;', $oc_arrPrevName);
							$str_filing = implode(';;', $oc_arrFile);
							
							$str_xAltnames = $ese->encrypt($str_altnames);
							$str_xPrevnames = $ese->encrypt($str_prevnames);
							$str_xOfficers = $ese->encrypt($str_officers);
							$str_xFiling = $ese->encrypt($str_filing);
							
							if($rsltDetail->results->company->{'registered-address-in-full'}){
								$oc_address = $rsltDetail->results->company->{'registered-address-in-full'};
							}else{
								$oc_address = "address:{$rsltDetail->results->company->{'registered-address'}}, locality:{$rsltDetail->results->company->locality}, region:{$rsltDetail->results->company->region}, postal-code:{$rsltDetail->results->company->{'postal-code'}}, country:{$rsltDetail->results->company->country}";
							}
							$oc_xAddress = $ese->encrypt($oc_address);
							//populate oc company hits into oc_orginfo table
							
							$ins_orgOC = "	INSERT INTO oc_orginfo SET 
															name = '{$oc_xCompanyName}'
															, orginfo_id = '{$main_id}'
															, tpi_id = '{$glb_tpiId}'
															, srch_id = '{$srch_id}'
															, company_number = '{$rsltDetail->results->company->{'company-number'}}'
															, jurisdiction_code = '{$rsltDetail->results->company->{'jurisdiction-code'}}'
															, incorporation_date = '{$rsltDetail->results->company->{'incorporation-date'}}'
															, dissolution_date = '{$rsltDetail->results->company->{'dissolution-date'}}'
															, company_type = '{$rsltDetail->results->company->{'company-type'}}'
															, created_at = '{$rsltDetail->results->company->{'create-at'}}'
															, updated_at = '{$rsltDetail->results->company->{'updated-at'}}'
															, opencorporates_url = '{$rsltDetail->results->company->{'opencorporates-url'}}'
															, alternative_names = '{$str_xAltnames}'
															, previous_names = '{$str_xPrevnames}'
															, registered_address = '{$oc_xAddress}'
															, officers = '{$str_xOfficers}'
															, associates = '{$str_associates}'
															, filings = '{$str_xFiling}'
															, created_by = 'system'
															, created_date = now()
															";
							$res_orgOC = mysql_query($ins_orgOC) or die(mysql_error());

							//populate officer found in company hits into rdc_grd_asso table
							if(count($oc_arrOffGRID) > 0){
								$str_offGRIDValues = implode(', ', $oc_arrOffGRID);
								$ins_offGRID = " 	INSERT INTO rdc_grd_asso
																	(comp_id, tpi_id, gidd_id, gidd_type, gr_type, rec_name, rec_title, rec_address, rec_country, is_idd, is_reviewed, inserted_date) 
																	VALUES 
																	{$str_offGRIDValues} 
																	ON DUPLICATE KEY UPDATE
																	rec_note = VALUES(comp_id)
																	";
								//yet another changes
								/*
								$res_offGRID = mysql_query($ins_offGRID) or die(mysql_error());
								*/
							}
							
							if(__DEBUG__){
								echo "+++++++++++++++++++++++<br />";
								echo "<br />Organization sql insert:<br />{$ins_orgOC}<br />";
								echo "<br />Officer sql insert:<br />{$ins_offOC}<br />";
								echo "+++++++++++++++++++++++<br />";
							}
						}
					}
					else
					{
						preVar($och);
						die('cant find oc hit');
					}
				}
				else
				{
					die('cant find country');
				}
				##OC SECTION END
				
				//populate oc company hits into rdc_grd_tpis table
				$main_xAddress = strlen($main_address) > 60 ? '' : $ese->encrypt($main_address);
				$ins_orgGRID = "	INSERT INTO rdc_grd_tpis SET 
													comp_id = '{$glb_compId}'
													, tpi_id = '{$glb_tpiId}'
													, comp_country = '{$main_country}'
													, comp_add = '{$main_xAddress}'
													, comp_name = '{$main_xSubjectName}'
													, gr_type = 'O'
													, is_idd = '1'
													, is_reviewed = '0'
													, inserted_date = now()
													";
				//yet another changes
				/*
				$res_orgGRID = mysql_query($ins_orgGRID) or die(mysql_error());
				*/
				
				if(false){
					echo "***********************<br />";
					echo "<br />Organization(GRID) sql insert:<br />{$ins_orgGRID}<br />";
					echo "<br />Officer(GRID) sql insert:<br />{$ins_offGRID}<br />";
					echo "***********************<br />";
					echo "=======================<br />";
					echo "<hr />";
				}
			}
			##MAIN PROCESS END
		}
		
		if ($entity_type == 'individual')
		{
			$sql1 = "select oi.*, cl.oc_ccode, sl.oc_scode from gidd_assoinfo oi LEFT JOIN country_list cl ON cl.country_name = oi.gidd_country LEFT JOIN state_list sl ON sl.oc_ccode = cl.oc_ccode AND sl.state_name = oi.gidd_state where shieldinfo_id = '$shield_id'";
			
			$res1 = mysql_query($sql1) or die(mysql_error());
			
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$fullname = $main_xSubjectName = $ese->decrypt($row1['gidd_firstname']).' '.$ese->decrypt($row1['gidd_lastname']);
				$main_xSubjectName = $ese->encrypt($main_xSubjectName);
				$main_country = $row1['gidd_country'];
				$main_state = $row1['gidd_state'];
				
				$och1 = new ocHelper();
				$och1->mode = 'strict';
				$och1->subject = $fullname;
				$och1->ccode = $row1['oc_ccode'];
				$och1->category = 'individual';
				$srchResult = $och1->srchSubject(1, 1);
				
				if($srchResult->results->{'total-count'} > 0 || $srchResult->results->hash->{'total-count'} > 0)
				{
					$addedlist = array();
					
					foreach($srchResult->results->hash->officers->officer as $officer)
					{
						//register uploaded information one by one into shield_vendor_info table
						$sviname = $ese->encrypt($officer->{'name'});
						
						$duplicate = in_array($sviname, $addedlist);
						$addedlist[] = $sviname;
						
						//check here
						$sql6 = "select id from shield_vendor_info where comp_id = '$glb_compId' and comp_name_eng = '$sviname' and country = '$main_country' limit 1 ";
						$res6 = mysql_query($sql6) or die(mysql_error());
						if (mysql_num_rows($res6))
						{
							$row6 = mysql_fetch_assoc($res6);
							$glb_tpiId = (int) $row6['id'];
						}
						else
						{
							$ins_orgTPI = " INSERT INTO shield_vendor_info SET
															url_id = '{$glb_urlId}'
															, tpi_type = 'P'
															, comp_id = '{$glb_compId}'
															, comp_name_eng = '$sviname'
															, country = '{$main_country}'
															, state = '{$main_state}'
															";
							if(__DEBUG__) echo "$ins_orgTPI<br><br>";
							//yet another changes
							/*
							$res_orgTPI = mysql_query($ins_orgTPI) or die(mysql_error());
							$glb_tpiId = mysql_insert_id();
							*/
							$glb_tpiId = 0;
						}
						
						//get asso details from oc
						$och = new ocHelper();
						$och->category = 'individual';
						$och->mode = 'strict';
						$offid = $officer->{'id'};
						$offcompany = $ese->encrypt($officer->{'company'}->{'name'});
						$details = $och->getSubjectDetails($offid, '');
						
						$xname = $details->results->officer->name;
						$name = $ese->encrypt($details->results->officer->name);
						$uid = addslashes($details->results->officer->uid);
						$position = $ese->encrypt($details->results->officer->position);
						$address = $ese->encrypt($details->results->officer->address);
						$start_date = addslashes($details->results->officer->{'start-date'});
						$end_date = addslashes($details->results->officer->{'end-date'});
						$oc_url = $ese->encrypt($details->results->officer->{'opencorporates-url'});
						$occupation = addslashes($details->results->officer->occupation);
						$nationality = addslashes($details->results->officer->nationality);
						$dob = addslashes($details->results->officer->{'date-of-birth'});
						
						/*
						echo '<pre style="font-size:10px;line-height:1;">';
						print_r($details);
						echo '</pre>';
						*/
						
						//search20_tpis
						$xuid = $_SESSION['member_id'];
						$sql3 = "insert into search20_tpis (user_id,srch_txt,entity_type,country,created_date) values('$xuid','$xname','individual','$main_country',now())";
						$res3 = mysql_query($sql3) or die(mysql_error());
						$srch_id = mysql_insert_id();
						$_REQUEST['last_srch_id'] = $srch_id;
						
						//insert into oc officer info table
						$oc_strOffInfo = " ('$offid', '$glb_tpiId', '$srch_id', '$name', '$position', '$uid', '$start_date', '$end_date', '$oc_url', '$occupation', '$address', '$nationality', '$dob', 0, 'system', now()) ";
						$ins_offOC = " 	INSERT INTO oc_offinfo
										(oc_id, tpi_id, srch_id, fullname, position, uid, start_date, end_date, opencorporates_url, occupation, address, nationality, date_of_birth, is_dup, created_by, created_date) 
										VALUES 
										{$oc_strOffInfo}
										";
						if(__DEBUG__) echo "$ins_offOC<br><br>";
						$res_offOC = mysql_query($ins_offOC) or die(mysql_error());
						$oc_offId = mysql_insert_id();
						
						if (!$duplicate) {
							$ins_orgGRID = "	INSERT INTO rdc_grd_tpis SET 
																comp_id = '{$glb_compId}'
																, tpi_id = '{$glb_tpiId}'
																, comp_country = '{$main_country}'
																, comp_name = '{$name}'
																, gr_type = 'P'
																, is_idd = '1'
																, is_reviewed = '0'
																, inserted_date = now()
																";
							//if(__DEBUG__) echo "$ins_orgGRID<br><br>";
							//yet another changes
							/*
							$res_orgGRID = mysql_query($ins_orgGRID) or die(mysql_error());
							*/
						}
						
						//insert rdc_grd_asso
						$str_offGRIDValues = " ('$glb_compId', '$glb_tpiId', '$oc_offId', 'asso', 'O', '$offcompany', 1, 0, now()) ";
						$ins_offGRID = " 	INSERT INTO rdc_grd_asso
															(comp_id, tpi_id, gidd_id, gidd_type, gr_type, rec_name, is_idd, is_reviewed, inserted_date) 
															VALUES 
															{$str_offGRIDValues} 
															ON DUPLICATE KEY UPDATE
															rec_note = VALUES(comp_id)
															";
						//if(__DEBUG__) echo "$ins_offGRID<br><br>";
						//yet another changes
						/*
						$res_offGRID = mysql_query($ins_offGRID) or die(mysql_error());
						*/
					}
				}
				
			}
		}
		
		##update gidd_shieldinfo table flag
		$sql_shieldinfoUpd = "	UPDATE gidd_shieldinfo
														SET
														gidd_flag = '1'
														WHERE
														id = '{$shield_id}'
														";
		mysql_query($sql_shieldinfoUpd) or die(mysql_error());
	}
	
	if (isset($_REQUEST['csvsubmit']))
	{
	  echo '<script type="text/javascript">';
	  echo 'window.location = "'.APP_URL.'third-parties220'.'"';
	  echo '</script>';
	  exit;
	}
