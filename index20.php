<?php
session_start();
error_reporting(0);
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
include('include20/dologin.php');

if(isset($_SESSION['member_id'])){
	@header("Location:".APP_URL."due-diligence20");
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Login<?php echo WEBSITE_NAME; ?></title>
<link type="text/css" rel="stylesheet"  href="https://client.ethixbase.com/style.css" >
<!--<link href="styles/styles_login.css" rel="stylesheet" type="text/css" />
<link href="login_main_css.css" rel="stylesheet" type="text/css" />-->
<style>
	#lostpasdiv1{
	margin-top:-2px;
	width:100%;
	-moz-border-radius: 5px;
	-webkit-border-radius:5px;
	border-radius:5px;
}
ul.navig{ margin-top:30px; font-family:Arial, Helvetica, sans-serif; 
	}
ul.navig li a{ text-decoration:none; color:#666666;}
ul.navig li{ 
	padding:4px 8px; 
	float:left;  
	list-style:none; 
	font-weight:bold; 
	font-size:14px;
	margin-right:8px;
	border-radius:8px;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	text-align:center;
	}

ul.navig li:hover a{ color:#ffffff;}
ul.navig li:hover{
	background-color:#000;
	font-weight:bold;
	}	
</style>
<script type="text/javascript">
	function showLostpass1(th){
		switch(th.className){
		case 'showme':
			th.className = 'hideme';
			document.getElementById('lostpasdiv1').style.display = 'block';
			//document.getElementById('inoffbtn1').src = 'images/icons/fugue/chevron-off.png';
		break;
		case 'hideme':
			th.className = 'showme';
			document.getElementById('lostpasdiv1').style.display = 'none';
			//document.getElementById('inoffbtn1').src = 'images/icons/fugue/chevron-expand-off.png';
		break;			
		}
	}
</script>
<!--<link href="https://client.ethixbase.com/blucss-foot.css" rel="stylesheet" type="text/css" />-->
<link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
<!--<link href="https://client.ethixbase.com/stylesheet.css" rel="stylesheet" type="text/css" />-->
</head>
<body>
	<div id="ajaxLoader" align="center" style=" display:none;" >
    <div><strong>Loading...</strong></div>
    <img style="margin-left:-3px;" src="images/ajax-loader.gif"  />
</div>
	<span id="pgagInfo" ></span>
    <div>
        <div style="width:980px; margin:auto;">
            <div class="blu_10">
                <div class="navigation">
                    <div class="blu_10">
                        <div class="blu_4">
                            <a href="<?php echo APP_URL; ?>">
                                <img src="img20/ethixbase-logo.png" alt="logo" width="256px" />
                            </a>
                        </div>
                        <div class="blu_6">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <div class="wrapper">
        <div class="clearfix"></div>
        
        	<div>
                <div class="nicontainer" align="left">
					<span id="pgagInfo" ></span>
                     <div class="nicontent">
    				   	<div class="signupfrom content" style="width:750px; margin-top:0;">
                       		<div style="float:left; width:470px; padding-bottom:10px;">
                                <div style="padding-left:40px; padding-top:10px;">
                                    <h2>Secure login to ethiXbase&nbsp;&nbsp;<img src="img20/locks.png" width="25px" /></h2>
                                </div>
                                <div class="signupfrom">
                                    <?php if(isset($loginEr)){ ?>
                                        <div style="width:100%; margin-top:10px;" >
                                            <span style="margin-bottom:0px;" class="message message-error">
                                                <img onclick="closeMessage(this)" style="float:right;margin-top:3px;cursor:pointer;" src="images/cross-circle.png" alt="x"  />
                                                <?php echo $loginEr;?>
                                            </span>
                                        </div>
                                    <?php } ?>
                                    <?php if(isset($messageSuc)){ ?>
                                        <div style="width:100%; margin-top:10px;" >
                                            <span style="margin-bottom:0px;" class="message message-success">
                                                <img onclick="closeMessage(this)" style="float:right;margin-top:3px;cursor:pointer;" src="images/cross-circle.png" alt="x"  />
                                                <?php echo $messageSuc;?>
                                            </span>
                                        </div>
                                    <?php } ?>
                                </div>
                                <form onsubmit="return validateLogin(dlogin);" name="dlogin" action="" method="post" class="login_form1" style="margin-top:30px;" enctype="multipart/form-data">
                                <input type="hidden" name="refURL" value="<?php if($_REQUEST['refURL']) echo $_REQUEST['refURL']; ?>" />
                                <table cellpadding="1px" cellspacing="1px" border="0" style="margin-left:30px;">
                                	<tr>
                                    	<td align="left">
                                        		<label style="width:100px; padding-left:10px;"><strong>Email:</strong></label>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="left">
                                        		<input type="text" name="username" style="width:250px;" autocomplete="off" />
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="left">
                                        		<label style="width:100px; padding-left:10px;"><strong>Password:</strong></label>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="left">
                                        		<input type="password" name="password" style="width:250px;" autocomplete="off" />
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="center" style="padding-top:20px;">
                                        	<input type="submit" name="login" value="Login" class="btn_small gray" style="width:110px;">
                                        </td>
                                    </tr>
                                </table>
                                <div style="clear:both;"></div>
                                </form>
                            </div>
                            <div class="right_box2 box" style="background:none; background-color:#F7F7F7; float:right; border:none;">
                                <div align="left" style="padding-top:20px;">
                                	<div class="reg" style="color:#000;"> Did you forget your password?<br />
                                        <a class="showme" onclick="showLostpass1(this);" href="javascript:void(0)" style="color:#00F;">Recover it here.</a>
                                        <div class="clear"></div>
                                    </div>              
									<form onsubmit="return validateLostPass(loastPass);" class="form label-inline" method="post" name="loastPass" action="" style="margin-top:0px; padding:0px;">
                                        <div id="lostpasdiv1" class="field" style=" display:none;" >
                                            <div align="left" style="padding-bottom:10px; padding-top:10px; width:200px;">
                                                <strong>Enter your e-mail address</strong><br>
                                                <input style="float:none;" class="medium" type="text" size="40" name="lost_email" id="lost_email" value="" />
                                                <span style="float:left;">
                                                <input name="lost_pass" type="submit" class="blue1" id="lost_pass" value="Send" >
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="clear"></div>
                                        </div> 
                                    </form>
                                    <br />
                                    <div class="reg" style="color:#000;"> Frequently Asked Questions (FAQ)<br />
                                        <a class="showme" href="/faq-guest20" style="color:#00F;">Click here</a>
                                        <div class="clear"></div>
                                    </div>  
                                    <br />
                                    <div class="reg" style="color:#000;"> Don't have an account yet?<br />
                                        <a class="showme" href="<?php echo APP_URL."signup"?>" style="color:#00F;">Register here</a>
                                        <div class="clear"></div>
                                    </div>              
                                </div>
                        	</div>
                        	<div class="clear"></div>
  					  </div>	
   					<div class="clear"></div>
                    </div>
                	<div class="clear"></div>
                 </div>
        	</div>
    </div>                 	
</body>
</html>