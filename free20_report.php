<?php
session_start();
error_reporting(0);
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
@include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');

$bar = 7; 
$offset = 6;
$srch_id = $_REQUEST['srch_id'];
$comp_id = $_REQUEST['comp_id'];

$crl = $crl1 = $crl6 = $crl2 = "";
$is_alrted = $is_alrted4 = $risk_lvl = 0;
$sql_top = "SELECT * FROM search20_tpis WHERE id=$srch_id";
$res_top = mysql_query($sql_top, $con1);
if(mysql_num_rows($res_top)>0)
{
	$row_top = mysql_fetch_assoc($res_top);
	$orgname = $row_top['srch_txt'];
	$entity_type = $row_top['entity_type'];
	$country = $row_top['country'];
	
	if($entity_type=='individual')
	{ $tpi_type = "P"; }
	elseif($entity_type=='company')
	{ $tpi_type = "O"; }
	
	if($orgname != '')
	{
	 	$sql_chk1 = "SELECT * FROM tbl_Enfor_Actions_FinCEN WHERE legal_name LIKE '%".$orgname."%'";
		$res_chk1 = mysql_query($sql_chk1,$con2);
		$chk1 = mysql_num_rows($res_chk1);
		if(mysql_num_rows($res_chk1)>0)
		{ 
			$is_alrted = 1;
			$crl1 = 3;
		}
		else
		{
			$res_chk1 = "";
			$crl1 = 0;
		}
		
		if($entity_type=='company')
		{
			$sql_chk2 = "SELECT * FROM tbl_Reg_Sanc_EBRD WHERE entity LIKE '%".$orgname."%' AND country like '%".$country."%'";
			$res_chk2 = mysql_query($sql_chk2,$con2);
			$chk2 = mysql_num_rows($res_chk2);
			if(mysql_num_rows($res_chk2)>0)
			{ 
				$is_alrted = 1;
				$crl1 = 3; 
			}
			else
			{
				if($crl1==0)
				{
					$res_chk2 = "";
					$crl1 = 0;
				}
			}
		}
		elseif($entity_type=='individual')
		{
			$sql_chk2 = "SELECT * FROM tbl_Reg_Sanc_EBRD WHERE individual LIKE '%".$orgname."%' AND country like '%".$country."%'";
			$res_chk2 = mysql_query($sql_chk2,$con2);
			$chk2 = mysql_num_rows($res_chk2);
			if(mysql_num_rows($res_chk2)>0)
			{ 
				$is_alrted = 1;
				$crl1 = 3; 
			}
			else
			{
				if($crl1==0)
				{
					$res_chk2 = "";
					$crl1 = 0;
				}
			}
		}
	}
	
	$alrt_cnts = $chk1 + $chk2;
	
	if($crl1==3)
	{
		$crl6 = "#FF0000";
	}
	else
	{
		$crl6 = "#00b050";
	}
	
	$sql_req30 = "SELECT * FROM search20_assos WHERE srch_id=$srch_id";
	$res_req30 = mysql_query($sql_req30,$con1);
	if(mysql_num_rows($res_req30)>0)
	{
		while($row_req30=mysql_fetch_assoc($res_req30))
		{
			$is_alrted4 = 0;
			if($row_req30['asso_name']!='')
			{
				$sql_chk1 = "SELECT * FROM tbl_Enfor_Actions_FinCEN WHERE legal_name LIKE '%".$row_req30['asso_name']."%'";
				$res_chk1 = mysql_query($sql_chk1,$con2);
				
				if(mysql_num_rows($res_chk1)>0)
				{ 
					$is_alrted4 = 1;
					$crl2 = 3;
				}
				else
				{
					$res_chk1 = "";
					$crl2 = 0;
				}
				$sql_chk2 = "SELECT * FROM tbl_Reg_Sanc_EBRD WHERE individual LIKE '%".$row_req30['asso_name']."%'";
				$res_chk2 = mysql_query($sql_chk2,$con2);
				if(mysql_num_rows($res_chk2)>0)
				{ 
					$is_alrted4 = 1; 
					$crl2 = 3;
				}
				else
				{
					if($is_alrted4==0)
					{
						$res_chk2 = "";
						$crl2 = 0;
					}
				}
			}
			
			if($crl2==3)
			{
				$crl = "#FF0000";
			}
			elseif($crl1==3)
			{
				$crl = "#FF0000";
			}
			else
			{
				if($crl!="#FF0000")
				{ $crl="#00b050"; }
				else
				{ $crl = "#FF0000"; }
			}
		}
	}
	else
	{
		if($crl1==3 && $crl2=="")
		{
			$crl = "#FF0000";
		}
		else
		{
			$crl = "#00b050";
		}
	}
	
	if($is_alrted==1 || $is_alrted4==1)
	{ $risk_lvl = 3; }
	else
	{ $risk_lvl = 1; }
	
	define('__DEBUG__', false);
	
	include_once('include20/iddLiteFunc/countryName.php');
	include_once('include20/iddLiteFunc/notes.php');
	include_once('include20/iddLiteFunc/num2words.php');
	
	mysql_set_charset('utf8');
	date_default_timezone_set("Asia/Singapore");
	
	$tpiId = trim($srch_id);
	$glb_cpi_year = '2014';
	$glb_oc_archiveCountryCnt = '53';
	
	$bar = 7; 
	$offset = 6;
	
	$is_foundOC = false;
	$is_foundGRID = false;
	
	//get OC organization info
	$sql_getOCInfo = "	SELECT `name`
						, company_number
						, jurisdiction_code
						, incorporation_date
						, dissolution_date
						, company_type
						, created_at
						, updated_at
						, alternative_names
						, previous_names
						, registered_address
						, registry_url
						, source_publisher
						, source_url
						, source_date
						, agent_name
						, agent_address
						, officers
						, filings
						FROM oc_orginfo
						WHERE srch_id = '{$srch_id}'
						";
	$res_getOCInfo = mysql_query($sql_getOCInfo,$con1);
	$row_getOCInfo = mysql_fetch_assoc($res_getOCInfo);
	
	if(__DEBUG__){
		echo "Get main data<br />";
		echo "info query:<br />{$sql_getOCInfo}<br />";
		print_r($row_getOCInfo);
		echo "<hr />";
	}
	$glb_cpi_year = '2014';
	$sql_getMainInfo = "SELECT itc.country_rank
						, itc.cpi_score
						, itc.country
						, itc.region
						,SUM(ioc.total_record) AS 'oc_totalrecord'
						FROM  idd_tranparency_cpi itc where itc.country = '{$country}' AND itc.year = '{$glb_cpi_year}'
						LEFT JOIN idd_oc_collections ioc ON ioc.code1 = itc.oc_country_code
						";
	$res_getMainInfo = mysql_query($sql_getMainInfo);
	$row_getMainInfo = mysql_fetch_assoc($res_getMainInfo);
	$glb_srchCountry = $row_getMainInfo['country'];
	$glb_cpi_countryRank = $row_getMainInfo['country_rank'];
	$glb_oc_archiveRecord_country = $row_getMainInfo['oc_totalrecord'];
	$glb_cpi_score = $row_getMainInfo['cpi_score'];
	$glb_cpi_countryName = $row_getMainInfo['country'];
	$glb_cpi_region = $row_getMainInfo['region'];
	
	$glb_srchName = $ese->decrypt($glb_xSrchName);
	
	$tmp_cnt = strlen($glb_oc_archiveRecord_country);
	$glb_oc_archiveRecord_countryRound = round($glb_oc_archiveRecord_country, (-1 * abs($tmp_cnt - 3)));
	?>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
            <title>ethiXbase Shield ethiXbase 2.0</title>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
            <link href="https://client.ethixbase.com/style/idd.report.css" rel="stylesheet" type="text/css" />
            <link type="text/css" rel="stylesheet"  href="https://client.ethixbase.com/style.css" />
            <style type="text/css">
            .page-report{
                width:980px;
                margin:auto;
                margin-top:20px;
                min-height:1300px;
                font-family:Arial, Helvetica, sans-serif;
                border:1px solid #000000;
                position:relative;
            }
            .logo-report{ 
                margin:30px auto;
                text-align:left;
                width:430px;
                }
            .footer-report{
                text-align:right;
                position:relative;
                margin-bottom:0px;
                top: 170px;
            }
            .corner{ width:150px; float:left; left:0px;}
            
            .finalrpbg{ 
                width:100%; 
                height:880px; 
                background-repeat:no-repeat; background-image:url("https://client.ethixbase.com/images/final-report-bg.jpg");
                background-size:100%;
            }
            
            .content{
                border:3px solid #4a7ebb;
                font-size:20px; 
                line-height:30px;
            }
            
            .imgdis {
                width:45%; 
                text-align:left;
                margin-left:80px;
                margin-top:40px;
                color:#ffffff;
            }
            
            .imgdis_tit {
                width:31%; 
                text-align:Right;
                margin-left:170px;
                margin-top:10px;
                color:#ffffff;
                font-size:60px;
                word-wrap:auto;
                line-height:55px;
                text-shadow: black 0.1em 0.1em 0.2em;
                font-weight:bolder;
                height:130px;
            }
            
            .imgdis_img {
                width:45%; 
                text-align:left;
                margin-left:80px;
                margin-top:10px;
                height: 130px;
                text-shadow: black 0.1em 0.1em 0.2em;
            }
            
            .compname {
                width:45%;
                text-align:left;
                margin-left:32px;
                margin-top:10px;
                font-size:210%;
                color:#ffffff;
                word-wrap:auto;
                line-height:50px;
                text-shadow: black 0.1em 0.1em 0.2em;
                height:150px;
            }
            
            .asso_name {
                width:45%;
                text-align:left;
                margin-left:10px;
                margin-top:5px;
                font-size:20px;
                color:#ffffff;
                word-wrap:auto;
                line-height:25px;
                text-shadow: black 0.1em 0.1em 0.2em;
                height:260px;
            }
            
            .builddate {
                width:45%;
                text-align:left;
                margin-left:270px;
                margin-top:10px;
                font-size:16px;
                color:#ffffff;
                word-wrap:auto;
                line-height:30px;
                text-shadow: black 0.1em 0.1em 0.2em;
            }
            
            .headboard {
                border-bottom:2px solid #4a7ebb; 
                border-top:2px solid #4a7ebb;
            }
            
            .headboard1 {
                border-bottom:2px solid #000000; 
                border-top:2px solid #000000;
                height:200px;
                background-color:#F2F2F2;
            }
            
            .headtxt {
                margin-top:5px;
                margin-bottom:5px;
                background-color:#f2f2f2;
                color:#000000;
                font-size:45px;
                line-height:55px;
                text-align:center;
            }
            
            .monyr {
                text-align:right;
                margin-right:30px;
                font-size:18px;
            }
            
            h2 {
                color: #414141;
                font-size: 26px;
                line-height: 1;
                margin-bottom: 2px;
                margin-top: 5px;
            }
            
            h1 {
                color: #414141;
                font-size: 30px;
                margin-bottom: 2px;
                margin-top: 5px;
            }	
            
            .eventsbg{ 
                width:100%; 
                height:994px; 
                background-repeat:no-repeat; background-image:url("https://client.ethixbase.com/images/baseline-events-bg.jpg");
                background-size:100%;
            }
            
            .content-report{ text-align:justify; margin:auto; line-height:25px; height:auto; padding:20px; border-top:3px solid #6CF; border-bottom:3px solid #6CF; margin-top:150px; padding-bottom:25px; }	
            .content-report .diss{ font-size:15px; text-decoration:underline; }
            .content-report .distext{ font-size:13px; }
            .content{
                border:2px solid #6b6b6b;
                font-size:20px; 
                line-height:30px;
            }
            
            .content-report1{ text-align:justify; margin:auto; line-height:auto; height:auto; padding:20px; }	
            .content-report1 .diss{ font-size:15px; text-decoration:underline; }
            .content-report1 .distext{ font-size:13px; }
            .content1{
                border:2px solid #000000;
                font-size:20px; 
                line-height:auto;
            }
            
            .content2{
                font-size:20px; 
            }
            
            .colcode {
                text-align:right;
                margin-right:10px;
                font-size:20px;
                font-weight:bold;
                color:#000000;
            }
            .color_code
            {
                background-color:<?php echo $crl; ?>;
                width:400px;
                line-height:23px;
                float:right;
                margin-right:10px;
            }
            
            ul.alt_cnt li
            {
                font-size:14px !important;
            }
            
            .page-report-title {
                margin:15px;
            }
            
            .content-report-cpi {
                text-align: justify;
                margin: 50px auto auto;
                line-height: 25px;
                height: auto;
                padding: 20px;
                border-top: 3px solid #6CF;
                border-bottom: 3px solid #6CF;
            }
            
            .content-paragraph-cpi {
                
            }
            
            .content-report-cpiinn {
                text-align: justify;
                margin: 30px auto auto;
                line-height: 25px;
                height: auto;
                /*padding: 20px;*/
                border: none;
            }
            
            .page-report-content {
                padding-top: 10px;
                width: 100%;
                margin-left: 0px;
            }
            
            #watermark {
              color: #000000;
              font-size: 79px;
              -webkit-transform: rotate(60deg);
              -moz-transform: rotate(60deg);
              -o-transform: rotate(60deg);
              position:absolute;
              width: 100%;
              height: 100%;
              margin: 0;
              z-index: 100;
              left: -520px;
              top: 280px;
              filter: opacity(0.6);
              opacity:0.6;
            }
            </style>
        </head>
		<body>
            <div class="page-report">
                <div class="finalrpbg">
                    <div id="watermark">          
                    </div>
                    <div style="text-align:right; margin-right:10px;">
                        &nbsp;
                    </div>
                    <div class="imgdis">
                        &nbsp;
                    </div>
                    <div class="imgdis_img">
                        <img src="https://client.ethixbase.com/images/ethixbase_logo_white.png" width="400px" />
                    </div>
                    <div class="imgdis_tit">
                        Screening Report
                    </div>
                    <div class="compname"><strong><?php echo ucwords($orgname); ?></strong></div>
                    <div class="asso_name">
                    <?php
                    $sql_req3 = "SELECT * FROM search20_assos WHERE srch_id=$srch_id";
                    $res_req3 = mysql_query($sql_req3,$con1);
                    if(mysql_num_rows($res_req3)>0)		
                    {
                    ?>
                    <strong>Including Associated Person:</strong><br />
                    <div style="font-size:16px;">
                        <?php
                        while($row_req3 = mysql_fetch_assoc($res_req3))
                        {
                            echo ucwords($row_req3['asso_name']);
                            
                            echo "<br />";
                        }
                        ?>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                    <div class="builddate"><strong><?php echo date("d M Y"); ?></strong></div>
                </div>
                <div class="colcode">Color Code: <div class="color_code">&nbsp;</div></div>
                <div class="content-report">
                    <div class="diss">Disclaimer:</div>
                    <div class="distext">
                        This report sets out information obtained by ethiXbase from third-party sources as well as ethiXbase&acute;s objective analysis of the same, and should in no way be construed as a recommendation as to what course of action the client should take, or a personal opinion of ethiXbase with respect to any of the corporate entities or individuals named in this report. ethiXbase takes all due care to ensure the accuracy of the information provided, but cannot guarantee with absolute finality the validity or accuracy of all sources of information. All actions taken by the client subsequent to receiving our report shall be understood to be the result of its decision alone, and the client shall hold ethiXbase free and harmless from any and all liability to itself or to any other party as a result of said decision.
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
        
            <div class="page-report">
                <div id="watermark">
                    ethiXbase 2.0 Free Account
                </div>
                <div style="text-align:center; margin-top:2px; margin-bottom:2px;">
                    <img src="https://client.ethixbase.com/images/baseline-events-bg.jpg" width="930px" />
                </div>
            </div>
            <div style="clear:both"></div>
            
            <div class="page-report" style="page-break-after:always;">
                <div id="watermark">
                    ethiXbase 2.0 Free Account
                </div>
                <div style="margin-top:20px;">
                    <div class="headboard">
                        <div class="headtxt"><strong>EXECUTIVE SUMMARY</strong></div>
                    </div>
                </div>
                <div style="padding-top:10px; width:80%; text-align:center; margin:auto;" >
                    <div class="content-report1">
                        <div class="content2">
                            <table cellpadding="5" cellspacing="0" border="1" width="100%">
                                <tr style="background-color:#F2F2F2;">
                                    <th align="left" width="4%"><strong>I.D</strong></td>
                                    <th colspan="2" width="76%" align="left"><strong>Entities</strong></td>
                                    <th align="left" width="20%"><strong>Color Code</strong></td>
                                </tr>
                                
                                    <tr>
                                        <td align="left" width="4%"><strong><?php echo $srch_id; ?></strong></td>
                                        <td width="22%" align="left" style="background-color:#F2F2F2;"><strong>Principal</strong></td>
                                        <td width="54%" align="left"><strong><?php echo ucwords($orgname); ?></strong></td>
                                        <td align="left" width="20%" style="background-color:<?php echo $crl6; ?>;"></td>
                                    </tr>
                                    <?php
                                    $sql_req4 = "SELECT * FROM search20_assos WHERE srch_id=$srch_id";
                                    $res_req4 = mysql_query($sql_req4,$con1);
                                    if(mysql_num_rows($res_req4)>0)		
                                    {
                                        while($row_req4 = mysql_fetch_assoc($res_req4))
                                        {
                                            $sql_chk1 = "SELECT * FROM tbl_Enfor_Actions_FinCEN WHERE legal_name LIKE '%".$row_req4['asso_name']."%'";
                                            $res_chk1 = mysql_query($sql_chk1,$con2);
                                            
                                            if(mysql_num_rows($res_chk1)>0)
                                            { 
                                                $is_alrted = 1;
                                                $asso_per_col="#FF0000";
                                            }
                                            else
                                            {
                                                $res_chk1 = "";
                                                $asso_per_col="#00b050";
                                            }
                                            $sql_chk2 = "SELECT * FROM tbl_Reg_Sanc_EBRD WHERE individual LIKE '%".$row_req4['asso_name']."%'";
                                            $res_chk2 = mysql_query($sql_chk2,$con2);
                                            if(mysql_num_rows($res_chk2)>0)
                                            { $is_alrted = 1; $asso_per_col="#FF0000"; }
                                            else
                                            {
                                                if($is_alrted==0)
                                                {
                                                    $res_chk2 = "";
                                                    $asso_per_col="#00b050";
                                                }
                                            }
                                        ?>
                                            <tr>
                                                <td align="left" width="4%"><strong><?php echo $row_req4['id']; ?></strong></td>
                                                <td width="22%" align="left" style="background-color:#F2F2F2;"><strong>Associated Person</strong></td>
                                                <td width="54%" align="left"><strong><?php echo ucwords($row_req4['asso_name']); ?></strong></td>
                                                <td align="left" width="20%" style="background-color:<?php echo $asso_per_col; ?>;"></td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                            </table>
                            <div>
                              <table border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                  <td colspan="2"><strong><u>LEGEND</u></strong></td>
                                </tr>
                                <tr>
                                  <td width="15%" style="background-color:#2ee600;">&nbsp;</td>
                                  <td>No results found from the ethiXbase database</td>
                                </tr>
                                <tr>
                                  <td style="background-color:#fafd0b;">&nbsp;</td>
                                  <td>Any match from the database check, which requires review
                                    <ul>
                                      <li><em>Fraud or IP infringement etc.</em></li>
                                      <li><em>Any other matches found not involving anti-bribery and corruption critical hits such as those found below listed under the &lsquo;red&rsquo; color code.</em></li>
                                    </ul></td>
                                </tr>
                                <tr>
                                  <td style="background-color:#ff0000;">&nbsp;</td>
                                  <td>Match Found
                                    <ul>
                                      <li><em>Person/organization with an exact name match found in the database.</em></li>
                                      <li><em>Politically Exposed Person (PEP) risk, Bribery and Corruption risk, US/EU MNC &lsquo;alerted&rsquo;, Sanctions Alert, FCPA investigation/conviction, Shell Companies</em></li>
                                    </ul></td>
                                </tr>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            
            <div class="page-report page-break">
                <div id="watermark">                 
                </div>
                <div class="page-report-title"><h2>Transparency International's <?php echo $glb_cpi_year; ?> Corruption Perceptions Index (CPI)</h2></div>
                <div class="clear"></div>
                <div class="page-report-content" <?php echo ($glb_cpi_score == '' ? " hidden" : ""); ?>>
                    <div class="content-report-cpi">
                        <div class="content-paragraph">
                            <?php 
                            
                                echo "The Transparency International's {$glb_cpi_year} Corruption Perceptions Index (CPI) (http://www.transparency.org/{$glb_cpi_year}/results) ranked {$glb_cpi_countryName} <strong>{$glb_cpi_countryRank}</strong> out of 177 countries and territories surveyed, with a CPI score of <strong>{$glb_cpi_score}</strong>. The CPI ranks these countries by their perceived levels of public sector corruption, as determined by expert assessments and opinion surveys. The ranking is on a scale of 0 to 100 with 0 being highly corrupt.";
                            
                            ?>
                        </div>
        
                        <div class="content-paragraph">
                            <div class="graph-container" style="width:840px;height:150px;margin:0 auto;position:relative;">
                                <div class="graph-title" style="padding-left:60px;"><h3>Score</h3></div>
                                <div class="graph-content-holder" style="width:810px;height:150px;position:relative;">
                                    <div class="graph-content-left-marker" style="width:60px;height:50px;float:left;">Highly Corrupt</div>
                                    <div class="graph-content-center-bar" style="width:700px;height:100px;float:left;position:relative;background-image:url('https://client.ethixbase.com/images/idd-graph-bar3.png');background-repeat:no-repeat;"></div>
                                    <div class="graph-content-right-marker" style="width:40px;height:50px;float:left;margin-left:10px;">Very Clean</div>
                                    <div class="graph-indicator" style="width:100px;height:115px;padding:1px 5px;position:absolute;background-color:transparent;<?php echo "left:" . (($glb_cpi_score * $bar) + $offset) . "px;"; ?>">
                                        <div class="graph-arrow" style="width:1px;height:80px;margin:0 auto;border:1px solid gray;background-color:gray;"></div>
                                        <div class="graph-indicator-text" style="position:absolute;bottom:0px;line-height:13px;text-align:center;"><?php echo strlen($jcode) > 0 ? getCountryName(substr($jcode, 0, 2)) : ($srch_jcode == substr($srch_territory, 0, 12) ? $display_jcode : getCountryName(substr($srch_jcode, 0, 2))); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="content-paragraph">
                            <div class="cpi-maps" style="padding-top:30px;">
                                <img src="https://client.ethixbase.com/images/idd-cpi-map2.jpg" width="880px" />
                            </div>
                        </div>
                        
                        <div class="content-paragraph">
                            <div class="cpi-copyrights">
                                <img src="https://client.ethixbase.com/images/idd-transparency-disclaimer.png" width="100%" />
                            </div>
                        </div>
        
                    </div>
                </div>
                <div class="page-report-content" <?php echo ($glb_cpi_score == '' ? "" : " hidden"); ?>>
                    <div class="content-report">
                        <div class="content-paragraph">
                            Sorry, No Corruption Perceptions Index(CPI) data available for Country (<?php echo $country;?>)
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            
            <div class="page-report page-break">
            <div id="watermark">
                ethiXbase 2.0 Free Account
            </div>
            <div class="page-report-title"><h1>Company Registry Information</h1></div>
            <div class="clear"></div>
            <div class="page-report-content" >
                <div class="content-report-cpi">
                    <h2 style="margin-top:0px;">Company Information</h2>
                    <div class="content-paragraph">
                        <br />ethixbase has over <?php echo number_format($glb_oc_archiveRecord_countryRound); ?> company records for <?php echo $glb_srchCountry; ?> on file.<br /><br /><?php //We consider this to be  echo "moderate"; [poor/moderate/extensive] coverage of the total number of companies registered in that country. ?>
                    </div>
                    
                    <div class="content-paragraph">
                        <?php
                            //print_r($row_getOCInfo);                     
                            if(is_array($row_getOCInfo) || is_object($row_getOCInfo)){
                                $i = 1;
                                echo '<h2>Company Name</h2><br />';
                                echo '<table class="tablesorter">';
                                //print_r($row_getOCInfo);
                                foreach($row_getOCInfo as $key => $value){
                                    if($value != ''){
                                            if($key == 'name' || $key == 'registered_address' || $key == 'officers' || $key == 'alternative_names' || $key == 'previous_names' || $key == 'filings'){
                                                $value = $ese->decrypt($value);
                                            }
                                            if($key == "jurisdiction_code"){
                                                $value = getCountryName($value);
                                            }
                                            if($key == 'previous_names' || $key == 'alternative_names' || $key == 'officers' || $key == 'filings'){
                                                $tmp_arr = explode(';;', $value);
                                                echo '<tr id="tc3" width="33%"><td colspan="2"><h4>' . ucwords(str_replace("_", " ", $key)) . '</h4></td>';
                                                $j = 1;
                                                foreach($tmp_arr as $row){
                                                    echo '<tr id="tc' . (($j % 2 == 1) ? 2 : 1) . '"><td id="lia" colspan="2" style="word-wrap:break-word; word-break:break-all;">'. $j . ") " . $row . '</td></tr>';
                                                    $j++;
                                                }
                                            }else{
                                                echo '<tr id="tc' . (($i % 2 == 1) ? 1 : 2) . '" width="33%"><td id="lia">' . ucwords(str_replace("_", " ", $key)) . ':</td><td id="lia" style="word-wrap:break-word; word-break:break-all;">'. $value .'</td></tr>';
                                                $i++;
                                            }
                                    }
                                }
                                echo "</table>";
                            }else{
                                echo '<div class="content-report-cpiinn">';
                                    echo '<div class="content-paragraph">';
                                        echo '<h2>Company Name</h2>';
                                        echo "<br />ethiXbase Instant Due Diligence has searched online company registry information from over 85 million companies and was unable to locate the corporate filings for <strong>{$glb_srchName}</strong>. They may still be a registered company, however <u>not listed</u> electronically. You may run an ethiXbase Standard Enhanced Due Diligence report which will take approximately 12 days to confirm their registration.";
                                    echo '</div>';
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
            <div style="clear:both;"></div>
            
            <div class="page-report" style="page-break-after:always;">
                <div id="watermark">
                    ethiXbase 2.0 Free Account
                </div>
                <div style="margin-top:30px;">
                    <div class="headboard">
                        <div class="headtxt"><strong>REPORT OVERVIEW</strong></div>
                    </div>
                </div>
                <div style="padding-top:10px; width:80%; text-align:center; margin:auto;" >
                    <div class="content-report1">
                        <div class="content1">
                            <table cellpadding="5" cellspacing="0" border="1" width="100%">
                                <tr>
                                    <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Principal</strong></th>
                                    <th align="left" width="65%" colspan="2"><?php echo ucwords($orgname); ?></th>
                                    <th align="left" width="5%" style="background-color:#F2F2F2;"><strong>I.D.</strong></th>
                                    <th align="left" width="10%"><?php echo $srch_id; ?></th>
                                </tr>
                                <tr>
                                    <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Match found</strong></th>
                                    <th align="left" width="80%" colspan="4">
                                    <?php
                                    if($alrt_cnts>0)
                                    { echo "Yes"; }else{ echo "No"; }
                                    ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th align="left" colspan="2" style="background-color:#F2F2F2;"><strong>Screening Results</strong></th>
                                    <th align="left" colspan="3" style="background-color:#F2F2F2;"><strong>Details</strong></th>
                                </tr>
                                <tr>
                                    <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Alerted</strong></th>
                                    <th align="left" width="8%"><?php echo $alrt_cnts; ?></th>
                                    <th align="left" width="72%" colspan="3">
                                        N/A
                                        <!-- style="padding-left:30px;"<ul class="alt_cnt" style="list-style:circle; line-height:normal; margin-left:-5px;">
                                        <?php //echo $alrt_events; ?>
                                        </ul>-->
                                    </th>
                                </tr>
                                <tr>
                                    <th align="left" style="background-color:#F2F2F2;"><strong>Not Alerted</strong></th>
                                    <th align="left">0</th>
                                    <th align="left" colspan="3">N/A</th>
                                </tr>
                                <tr>
                                    <th align="right" colspan="3" style="background-color:#F2F2F2;"><strong>Color Code</strong></th>
                                    <th align="left" colspan="2" style="background-color:<?php echo $crl6; ?>"></th>
                                </tr>
                                <tr>
                                    <td align="left" colspan="5">
                                        <ul>
                                            <li>* No Match Found – no hits found in the system.</li>
                                            <li>* Alerted – there was a hit that analysts were not able to rule out.</li>
                                            <li>* Not Alerted – system found potential matches, but analyst found they were not true matches/irrelevant based on standard analyst review guidelines</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <?php
            $sql_req2 = "SELECT * FROM search20_assos WHERE srch_id=$srch_id";
            //echo $sql_req2."<br />";
            $res_req2 = mysql_query($sql_req2,$con1);
            if(mysql_num_rows($res_req2)>0)
            {
            ?>
                <div class="page-report" style="page-break-after:always;">
                    <div id="watermark">
                        ethiXbase 2.0 Free Account
                    </div>
                <?php
                    while($row_ass_per = mysql_fetch_assoc($res_req2))
                    {
                        $is_alrted = 0;
                        $asso_per_nam = $row_ass_per['asso_name'];
                        $asso_per_id = $row_ass_per['id'];
                        
                        $sql_chk1 = "SELECT * FROM tbl_Enfor_Actions_FinCEN WHERE legal_name LIKE '%".$asso_per_nam."%'";
                        $res_chk1 = mysql_query($sql_chk1,$con2);
                        $acnt_cnk1 = mysql_num_rows($res_chk1);
                        if(mysql_num_rows($res_chk1)>0)
                        { 
                            $is_alrted = 1;
                            $asso_per_col="#FF0000";
                        }
                        else
                        {
                            $res_chk1 = "";
                            $asso_per_col="#00b050";
                        }
                        $sql_chk2 = "SELECT * FROM tbl_Reg_Sanc_EBRD WHERE individual LIKE '%".$asso_per_nam."%'";
                        $res_chk2 = mysql_query($sql_chk2,$con2);
                        $acnt_cnk2 = mysql_num_rows($res_chk2);
                        if(mysql_num_rows($res_chk2)>0)
                        { $is_alrted = 1; $asso_per_col="#FF0000"; }
                        else
                        {
                            if($is_alrted ==0)
                            {
                                $res_chk2 = "";
                                $asso_per_col="#00b050";
                            }
                        }
                        if($is_alrted==1)
                        { $match_found = "Yes"; }
                        else
                        { $match_found = "No"; }
                        $acnt_cnk = $acnt_cnk1 + $acnt_cnk2;
                        ?>   
                        <div style="padding-top:10px; width:80%; text-align:center; margin:auto;" >
                            <div class="content-report1">
                                <div class="content1">
                                    <table cellpadding="5" cellspacing="0" border="1" width="100%">
                                        <tr>
                                            <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Associated Person</strong></th>
                                            <th align="left" width="40%" colspan="2"><?php echo ucwords($asso_per_nam); ?></th>
                                            <th align="left" width="10%" style="background-color:#F2F2F2;"><strong>Title</strong></th>
                                            <th align="left" width="15%"></th>
                                            <th align="left" width="5%" style="background-color:#F2F2F2;"><strong>I.D.</strong></th>
                                            <th align="left" width="10%"><?php echo $asso_per_id; ?></th>
                                        </tr>
                                        <tr>
                                            <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Match found</strong></th>
                                            <th align="left" width="80%" colspan="6">
                                            <?php echo $match_found; ?>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th align="left" colspan="2" style="background-color:#F2F2F2;"><strong>Screening Results</strong></th>
                                            <th align="left" colspan="5" style="background-color:#F2F2F2;"><strong>Details</strong></th>
                                        </tr>
                                        <tr>
                                            <th align="left" width="20%" style="background-color:#F2F2F2;"><strong>Alerted</strong></th>
                                            <th align="left" width="8%"><?php echo $acnt_cnk; ?></th>
                                            <th align="left" width="72%" colspan="5">N/A
                                                 <!--style="padding-left:30px;"<ul class="alt_cnt" style="list-style:circle; line-height:normal; margin-left:-5px;">
                                                </ul>-->
                                            </th>
                                        </tr>
                                        <tr>
                                            <th align="left" style="background-color:#F2F2F2;"><strong>Not Alerted</strong></th>
                                            <th align="left">0</th>
                                            <th align="left" colspan="5">N/A</th>
                                        </tr>
                                        <tr>
                                            <th align="right" colspan="5" style="background-color:#F2F2F2;"><strong>Color Code</strong></th>
                                            <th align="left" colspan="2" style="background-color:<?php echo $asso_per_col; ?>"></th>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="7">
                                                <ul>
                                                    <li>* No Match Found – no hits found in the system.</li>
                                                    <li>* Alerted – there was a hit that analysts were not able to rule out.</li>
                                                    <li>* Not Alerted – system found potential matches, but analyst found they were not true matches/irrelevant based on standard analyst review guidelines</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                ?>
                </div>
                <div style="clear:both"></div>
            <?php
            }
			?>
		</body>
	</html>
<?php
}
?>