<?php
session_start();
error_reporting(0);
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';

$filter = array();
$filter[] = 'active=1';
//$filter[] = "type='GENERAL'"; //guest FAQ is not categorized
$gid = $_SESSION['user_level'] ? $_SESSION['user_level'] : 2;// 2 is guest
$filter[] = "faq_id in (select faq_id from faq_groups where g_id = $gid)";
$filter = join(' and ', $filter);

$query = "select question, answer from faq where $filter";
$res1 = mysql_query($query);
//echo $query;
$rows = array();
while ($r = mysql_fetch_assoc($res1))
{
	$rows[] = $r;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>FAQ<?php echo WEBSITE_NAME; ?></title>
<link type="text/css" rel="stylesheet"  href="https://client.ethixbase.com/style.css" >
<!--<link href="styles/styles_login.css" rel="stylesheet" type="text/css" />
<link href="login_main_css.css" rel="stylesheet" type="text/css" />-->
<script type="text/javascript" src="https://client.ethixbase.com//tips/jquery-1.4.2.min.js"></script>
<style>
	#lostpasdiv1{
	margin-top:-2px;
	width:100%;
	-moz-border-radius: 5px;
	-webkit-border-radius:5px;
	border-radius:5px;
}
ul.navig{ margin-top:30px; font-family:Arial, Helvetica, sans-serif; 
	}
ul.navig li a{ text-decoration:none; color:#666666;}
ul.navig li{ 
	padding:4px 8px; 
	float:left;  
	list-style:none; 
	font-weight:bold; 
	font-size:14px;
	margin-right:8px;
	border-radius:8px;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	text-align:center;
	}

ul.navig li:hover a{ color:#ffffff;}
ul.navig li:hover{
	background-color:#000;
	font-weight:bold;
	}	
.faq-q {
	font-family: Arial,Helvetica,sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #000000;
	cursor: pointer;
}
.faq-a {
	font-family: Arial,Helvetica,sans-serif;
	font-size: 12px;
	color: #111111;
}
.homebtn {
	border: medium none;
	border-radius: 5px;
	color: #ffffff;
	font-size: 16px;
	font-weight: bold;
	padding: 5px 15px;
	text-align: center;
	text-shadow: 2px 2px 2px #333333;
	width: auto;
	text-decoration: none;
	background-color: #7a818c;
	cursor: pointer;
}
</style>
<link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
<!--<link href="https://client.ethixbase.com/blucss-foot.css" rel="stylesheet" type="text/css" />
<link href="stylesheet.css" rel="stylesheet" type="text/css" />-->
</head>
<body>
	<span id="pgagInfo" ></span>
    <div>
        <div style="width:980px; margin:auto;">
            <div class="blu_10">
                <div class="navigation">
                    <div class="blu_10">
                        <div class="blu_4">
                            <a href="<?php echo APP_URL; ?>">
                                <img src="img20/ethixbase-logo.png" alt="logo" width="256px" />
                            </a>
                        </div>
                        <div class="blu_6">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <div class="wrapper">
        <div class="clearfix"></div>
        
        	<div>
                <div class="nicontainer" align="left">
					<span id="pgagInfo" ></span>
                     <div class="nicontent">
    				   	<div class="signupfrom content" style="width:750px; margin-top:0;">
                       		<div style="float:left; padding-bottom:10px;">
                                <div style="padding-left:40px;padding-top:10px;overflow:auto;height:500px;overflow-x:hidden;">
                                    <h2>Frequently Asked Questions (FAQ)</h2><br />
									<ul>
										<?php foreach ($rows as $r): ?>
										<li>
											<span class="faq-q">
												<img src="img20/icons/toggle_expand.png" border="0" width="15" height="15" class="toggle_img" />&nbsp;
												<?php echo nl2br($r['question']); ?>
											</span><br />
											<span class="faq-a" style="display:none;"><?php echo nl2br($r['answer']); ?><br /></span>
											<br />
										</li>
										<?php endforeach; ?>
									</ul>
                                </div>
                            </div>
                        	<div class="clear"></div>
  					  </div>		
   					<div class="clear"></div>
                    </div>
                	<div class="clear"></div>
                 </div>
        	</div>
		<div style="width:750px;" align="center">
			<a href="/index20"><span class="homebtn">Home</span></a>
		</div>

    </div>                 	
</body>

<script type="text/javascript">

	$('.faq-q').click(function()
	{
		el = $(this).next().next();
		if ($(el).is(':visible'))
		{
			$(el).hide();
			$(this).children('img').attr('src','img20/icons/toggle_expand.png');
		}
		else
		{
			$('.faq-a').hide();
			$('.toggle_img').attr('src','img20/icons/toggle_expand.png');
			$(this).children('img').attr('src','img20/icons/toggle_collapse.png');
			$(el).fadeIn('fast');
		}
	});

</script>

</html>