<?php
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
include_once 'include20/EthixbaseStrEncryption.php';
require_once 'include20/MysqliDb.php';
require_once 'include20/PHPExcel/PHPExcel.php';
require_once 'include20/PHPExcel/PHPExcel/Writer/Excel2007.php';
require_once 'include20/PHPMailer/class.phpmailer.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$fdb = new MysqliDb;

$login_query = "SELECT tmp.*, COUNT(*) AS count FROM (
                    SELECT DISTINCT a.user_id, a.comp_id, b.name, DATE_FORMAT(FROM_UNIXTIME(b.joined), '%m/%d/%Y %h:%i %p') AS created,
                    (SELECT company_name FROM vendor_edu WHERE id=a.comp_id LIMIT 1) AS company_name,
	                a.inserted_date AS last_login,
	                (SELECT COUNT(*) FROM shield_vendor_info WHERE comp_id=a.comp_id) AS num_of_suppliers,
	                (SELECT count(*) AS count FROM shield_vendor_info WHERE is_idd = 1 AND comp_id=a.comp_id) AS idd,
	               (SELECT COUNT(*) FROM idd_plus_orders t1 INNER JOIN srch_tpi t2 on t1.srch_id=t2.id WHERE t2.comp_id=a.comp_id) AS idd_plus,
	                (SELECT COUNT(*) FROM edd_orders_company WHERE srch_id=a.comp_id) AS edd,
	                (SELECT total_credits FROM vendor_edu WHERE id=a.comp_id) AS total_credits,
	                (SELECT used_credits FROM vendor_edu WHERE id=a.comp_id) AS used_credits,
                    (SELECT COUNT(*) FROM shield_vendor_info WHERE comp_id=a.comp_id AND risk_level=3 AND is_idd=0) AS idd_risk_alerts,
                    (SELECT COUNT(*) FROM shield_vendor_info WHERE comp_id=a.comp_id AND idd_color=3 AND is_idd=1) AS idd_plus_risk_alerts,
                    (SELECT COUNT(*) FROM idd_plus_orders t1 INNER JOIN srch_tpi t2 on t1.srch_id=t2.id WHERE t2.comp_id=a.comp_id AND t1.payment_mode='invoice') AS invoice_hits ,
                    (SELECT COUNT(*) FROM idd_plus_orders t1 INNER JOIN srch_tpi t2 on t1.srch_id=t2.id WHERE t2.comp_id=a.comp_id AND t1.payment_mode='creditcard') AS credit_card_hits 
                    FROM login_stats AS a 
                    INNER JOIN ibf_members AS b ON a.user_id = b.member_id
                    -- WHERE b.email NOT LIKE '%@ethixbase.com'
                    ORDER BY comp_id
                ) AS tmp
                -- WHERE tmp.comp_id <> 0 
                GROUP BY tmp.comp_id";

if(($from != '' && $to != '') || $asof != '') {
    $idd_query .= "WHERE ";
    if ($from != '' && $to != '') $login_query .= "inserted_date BETWEEN '" . $from . " 00:00:00' AND '" . $to . " 23:59:59'";
    if ($asof != '') $login_query .= "inserted_date BETWEEN '" . $asof . " 00:00:00' AND '" . $asof . " 23:59:59'";
}

$login_data = $fdb->query($login_query);

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);

$row = 5;
$column = 1;

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Company');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Number of Users');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Account Created');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Last Login');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Number of Suppliers');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;

$row = 5;
$column = 7;

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'IDD Report');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Number of Risk Alerts');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'IDD Plus');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Number of Risk Alerts');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'EDD');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;

$row = 5;
$column = 13;

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Credit/Tokens');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Credit/Tokens used');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Number of Invoice hits');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Credit Card');
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
$column++;

$row+=2;
$count = 0;

$column = 1;
foreach ($login_data as $login) {
    $login = decryptRow($login);
    $column = 1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['company_name']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['count']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, trim($login['created']) != '' ? date('m/d/Y h:i A') : '');
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, trim($login['last_login']) != '' ? date('m/d/Y h:i A', strtotime($login['last_login'])) : '');
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['num_of_suppliers']);
    $column+=2;

    //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['idd']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['idd_risk_alerts']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['idd_plus']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['idd_plus_risk_alerts']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['edd']);
    $column+=2;

    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['total_credits']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['used_credits']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['invoice_hits']);
    $column++;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $login['credit_card_hits']);
    $column++;

    $row++; $count++;
}
//----------------------------------------------------

if(!is_dir('auto-email')) mkdir('auto-email');
$file = 'auto-email/LOGS_IDD_EDD_' . date('mdYHis') . '.xlsx';
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save($file);

$email              = new PHPMailer();
$email->From        = 'no-reply@ethixbase.com';
$email->FromName    = 'EthixBase2.0';
$email->Subject     = 'EthixBase Weekly Report';
$email->Body        = <<<BODY
    Hi,
    
    This is an automated report sent from EthixBase 2.0. Please do not reply.
    
    Thanks!
BODY;
$email->AddAddress('eesperila@ethixbase.com');
$email->AddAddress('etalisic@ethixbase.com');
$email->AddAddress('rmittal@ethixbase.com');
$file_info = pathinfo($file);
$email->AddAttachment($file, $file_info['basename']);

$email->Send();
