<?php

	session_start();
	
	require_once('../EthixbaseStrEncryption.php');
	$ese = new EthixbaseStrEncryption();

  if ($_FILES['csvfile'])
  {
	  $csv = array_map('str_getcsv', file($_FILES['csvfile']['tmp_name']));
	  array_shift($csv);
	  
	  $type = 'individual';
	  $_REQUEST['csvtype'] = 'individuals';
	  foreach ($csv as $c)
	  {
		  if ($c[0] == 'O')
		  {
			$type = 'organization';
			$_REQUEST['csvtype'] = 'both';
			break;
		  }
	  }
	  
  if (true) {
	  $asso = array();
	  
	  foreach ($csv as $c)
	  {
		  $fullname = trim($c[2]).' '.trim($c[3]);
		  $search = ($type == 'organization') ? trim($c[1]) : $fullname;
		  
		  if ($type == 'organization' && $c[0] == 'A')
		  {
			  $asso[] = $fullname;
			  continue;
		  }
		  else
		  {
			if ($last_tpi) $assolist[$last_tpi] = $asso;
			$asso = array();
			
			$uid = $_SESSION['member_id'];
			$sch = addslashes($search);
			$ent = ($type == 'organization') ? 'company':'individual';
			$cnt = addslashes(trim($c[6]));

			$sql3 = "insert into search20_tpis (user_id,srch_txt,entity_type,country,created_date) values('$uid','$sch','$ent','$cnt',now())";
			//echo "$sql3<br><br>";
			$res3 = mysql_query($sql3) or die(mysql_error());
			$last_tpi = mysql_insert_id();
			$_SESSION['srch_id'][$search] = $last_tpi;
			$_REQUEST['last_srch_id'] = $last_tpi;
			
			//shield_vendor_info
			$comp_id = $_SESSION['company_id'];
			$uid = $_SESSION['member_id'];
			$svi_comp_name_eng = $ese->encrypt($search);
			$svi_address = $ese->encrypt(trim($c[8]));
			$svi_country = addslashes(trim($c[6]));
			$svi_state = addslashes(trim($c[7]));
			$svi_tel_no = $ese->encrypt(trim($c[9]));
			$svi_compl_con_email = $ese->encrypt(trim($c[10]));
			$svi_lg_email = $ese->encrypt(trim($c[10]));
			$tpi_type = ($type == 'organization') ? 'O':'P';
			
			$sql5 = "select id from shield_vendor_info where comp_id = '$comp_id' and comp_name_eng = '$svi_comp_name_eng' and country = '$svi_country' limit 1 ";
			$res5 = mysql_query($sql5) or die(mysql_error());
			
			if (mysql_num_rows($res5))
			{
				//echo "duplicated tpi - $search<br><br>";
			}
			else
			{
				$ins_orgTPI = " INSERT INTO shield_vendor_info SET
												comp_id = '$comp_id'
												, user_id = '$uid'
												, tpi_type = '$tpi_type'
												, is_dd = '1'
												, srch_id = '$last_tpi'
												, comp_name_eng = '$svi_comp_name_eng'
												, address = '$svi_address'
												, country = '$svi_country'
												, state = '$svi_state'
												, tel_no = '$svi_tel_no'
												, compl_con_email = '$svi_compl_con_email'
												, lg_email = '$svi_lg_email'
												";
				//echo "$ins_orgTPI<br><br>";
				mysql_query($ins_orgTPI) or die(mysql_error());
			}
			
		  }
	  }
	  
	  $assolist[$last_tpi] = $asso;
	  //array_shift($assolist);
	  if ($type == 'individual') $assolist = false;
	  
	if ($assolist)
	{
	  foreach($assolist as $key => $data)
	  {
		foreach($data as $d)
		{
		  $uid = $_SESSION['member_id'];
		  $asso_name = addslashes(trim($d));
		  if (!$asso_name) continue;
		  $sql4 = "insert into search20_assos (srch_id,user_id,asso_name,created_date) values ('$key','$uid','$asso_name',now()) ";
		  //echo "$sql4<br><br>";
		  $res4 = mysql_query($sql4) or die(mysql_error());
		}
	  }
    }
  }
	  
	  //if ($_SESSION['v2_account'] == 'paid')
	  if (true)
	  {
		//processing for rdc grid
		require_once('due-diligence-grid1.php');
		require_once('due-diligence-grid2.php');
	  }
	  
	if (!$_REQUEST['alt']) {
	  echo '<script type="text/javascript">';
	  echo 'window.location = "'.APP_URL.'third-parties220'.'"';
	  echo '</script>';
	  exit;
	}
  }
  
  function get_jcode($j1,$j2)
  {
	$j1 = trim($j1);
	$sql1 = "SELECT id, country_name, oc_ccode FROM country_list WHERE country_name = '$j1' limit 1";
	$res1 = mysql_query($sql1) or die(mysql_error());
	$row1 = mysql_fetch_assoc($res1);
	
	if ($row1)
	{
		$j2 = trim($j2);
		$sql2 = "SELECT state_name, oc_ccode, oc_scode FROM state_list WHERE state_name = '$j2' limit 1";
		$res2 = mysql_query($sql2) or die(mysql_error());
		$row2 = mysql_fetch_assoc($res2);
		if ($row2) return $row1['oc_ccode'].'_'.$row2['oc_scode'];
		return $row1['oc_ccode'];
	}
	else return '';
  }
  