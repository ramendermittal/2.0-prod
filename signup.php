<?php
session_start();
error_reporting(0);
  /**** START updated by @ronnieChu 20151106 ***/
header('Cache-Control: max-age=900');
  /**** END updated by @ronnieChu 20151106 ***/
require_once 'include20/config20.php';
require_once 'include20/db20.php';
require_once 'include20/common20.php';
@include_once('include20/EthixbaseStrEncryption.php');
mysql_set_charset('utf8');

if(isset($_REQUEST['on_submit']) && $_REQUEST['on_submit']=='Submit')
{

    function redirect($url) {
        ob_start();
        header('Location: '.$url);
        ob_end_flush();
        die();
    }

    $email_ad       =   isset($_REQUEST['email_ad'])        ?   $_REQUEST['email_ad']       :   "";

    $banned_domains_qry = mysql_query("SELECT * FROM banned_email_domain WHERE deleted=0");
    $banned_domains  =   array();
    while ($row = mysql_fetch_array($banned_domains_qry)) {
       $banned_domains[] = $row['domain_name'];
    }
    list($email_user, $email_domain) = explode('@', $email_ad);

    if ( in_array($email_domain, $banned_domains) ) {
        redirect(APP_URL."signup_invalid");
    }

    $email_exists = mysql_query("SELECT * FROM ibf_members WHERE email='{$email_ad}'");
    $email_exists_err = '';
    if(mysql_num_rows($email_exists) > 0){
        $email_exists_err = 'It seems you are already a registered User. Please do reset your account password if you have forgotten it.';
    }else{
        require_once 'include20/phpass.php';
        require_once 'include20/string_helper.php';

        $salt = random_string( 'alnum', 8);

        $phpass = new phpass();
        $sign_password_hash = $phpass->hash( $salt );

        $fr_name        =   isset($_REQUEST['fr_name'])         ?   $_REQUEST['fr_name']        :   "";
        $lt_name        =   isset($_REQUEST['lt_name'])         ?   $_REQUEST['lt_name']        :   "";
        $jb_title       =   isset($_REQUEST['jb_title'])        ?   $_REQUEST['jb_title']       :   "";
        $com_name       =   isset($_REQUEST['com_name'])        ?   $_REQUEST['com_name']       :   "";
        $phone_no       =   isset($_REQUEST['phone_no'])        ?   $_REQUEST['phone_no']       :   "";
        $industry_id        =   isset($_REQUEST['industry_id'])         ?   $_REQUEST['industry_id']        :   "";
        $country_id   =   isset($_REQUEST['country_id'])    ?   $_REQUEST['country_id']   :   "";
        
        $sql_ins = "INSERT INTO ibf_members SET 
                    name ='".$fr_name." ".$lt_name."', 
                    firstname ='".$fr_name."', 
                    lastname ='".$lt_name."', 
                    members_display_name = '".$fr_name." ".$lt_name."', 
                    job_title ='".$jb_title."', 
                    company='".$com_name."', 
                    email='".$email_ad."', 
                    phone='".$phone_no."', 
                    industry_id='".$industry_id."', 
                    country_id='".$country_id."',
                    sign_password_hash = '{$sign_password_hash}'";
        $member_ins = mysql_query($sql_ins);

        if ($member_ins)
        {
            $member_id = mysql_insert_id();
            //send email
            $headers = 'MIME-Version: 1.0'."\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
            $headers .= "From: support@ethixbase.com\r\nReply-To: support@ethixbase.com"."\r\n";
            
            $message = '<html>
                            <head><meta charset="utf-8">
                                <style>
                                .btn_larg  a:link{
                                    text-decoration:none;
                                }

                                .btn_larg {
                                    text-decoration:none;
                                    color:#407199;
                                    width:250px;
                                    text-shadow:2px 2px 2px #333333;
                                    font-weight:bold;
                                    text-align:center;
                                    -webkit-border-radius:5px;
                                    -moz-border-radius:5px;
                                    border-radius:5px;
                                    padding:10px;
                                    font-size:16px;
                                    float:right;
                                    cursor: pointer; 
                                    cursor: hand;
                                    }
                                .btn_larg a{
                                    color:#407199;
                                }

                                </style>
                            </head>
                            <body style="background-color:#F9F9F9;padding-top: 18px;">
                            <div style="border:1px;width: 90%; font-family: \'Helvetica Neue, Helvetica\', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 18px; color: #333; margin: 0 auto; background-color: #fff;border: 1px solid #DDD; border-radius: 4px; padding: 15px;">
                                <p style="margin:0 0 30px 0;"><img style="height:73px;weight:222px;" src="'.APP_URL.'img20/ethixbase-logo.png" alt="Please enable images to load system logo" title="Please enable images"></p>

                                <span style="font-family:Helvetica Neue Light,Helvetica;font-weight:300;font-size:28px;color:#515254">
                                    <p>Welcome to <span style="color:#8CC514">ethiXbase 2.0</span></p>
                                </span>

                                <h3 style="padding-top:20px;">Hello '.$fr_name.' '.$lt_name.',</h3>

                                <div style="margin-right: 20%;">

                                    <p>Congratulations! Your ethiXbase account has been successfully created. To complete your setup and create account password, simply click the button below.</p>

                                    <table><tr>
                                    <td valign="middle" align="left" style="font-family:HelveticaNeueLight,HelveticaNeue-Light,\'Helvetica Neue Light\',HelveticaNeue,Helvetica,Arial,sans-serif;font-weight:300;font-stretch:normal;text-align:center;color:#fff;font-size:15px;background:#0079c1;border-radius:7px!important;line-height:1.45em;padding:7px 15px 8px;margin:0 auto 16px;font-size:1em;padding-bottom:7px">
                                        <span>
                                            <a type="Link" style="color:#ffffff;text-decoration:none;display:block;font-family:Arial,sans-serif;font-weight:bold;font-size:13px;line-height:15px" target="_blank" href="'.APP_URL.'create_password?id='.$member_id.'&salt='.$sign_password_hash.'">
                                                <span style="color:#ffffff;text-decoration:none;display:block;font-family:Arial,sans-serif;font-weight:bold;font-size:13px;line-height:15px"> 
                                                Create Password </span>
                                            </a>
                                        </span>
                                    </td>
                                    </tr></table>

                                    <br />

                                </div>

                            </div>
                            <p>Best regards,<br>ethiXbase</p>

                            <p style="font-size:13px">
                                This is a system generated message. Please do not reply as your email may not be read.
                                <br>
                                Powered by <a style="color:#4d90fe;" target="_blank" href="http://www.ethixbase.com">ethiXbase</a>
                            </p>
                            </body>
                        </html>';
            
            $mail = mail($email_ad, "Complete your ethiXbase account setup", $message, $headers);
            redirect(APP_URL."signup_done");
        }else{
            echo mysql_error();
        }
    }

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Signup | ethiXbase 2.0</title>

<link href="css20/bootstrap.min.css" media="all" rel="stylesheet" />
<link href="css20/style.css" media="all" rel="stylesheet" />
<link href="css20/style-2.css" media="all" rel="stylesheet" />
<script src="js20/jquery-1.11.3.min.js"></script>

<style>
.login_form2 ul{
    margin-left:30px;
    margin-top:10px;
}
.login_form2 li{
    padding-left:10px;
    list-style-type:disc;
}
.login_form2 input, textarea, select{
    margin:3px 5px;
    width:242px;
}
#range{
    text-align:left; 
    padding-left:20px; 
    display:none; 
    float:left; 
    width:300px;
}   
.ui-widget {
    font-family: Arial,sans-serif;
    font-size: 12px;
}

.frm_label {
    float:left;
    text-align:left;
    margin:10px;
    margin-left:0px;
    margin-bottom:0px;
    width:140px;
    padding:10px;
    padding-bottom:0px;
}

.field_styl {
    border:1px solid #8cc540;
    border-radius:10px;
    padding:10px;
}

h1 {
    font-family:Verdana !important;
}

.frm_field {
    float:left;
    text-align:left;
    margin:10px;
    width:260px;
    margin-right:20px;
}

.frm_rows {
    margin-top:15px;
    margin-bottom:0px;
    padding-bottom:0px;
}

.frm_rows_sec {
    margin-top:35px;
}

.submi {
    text-align:center;
}

.req_sg {
    color:#F05151;
    font-size:18px;
}

.ord_conf {
    border:4px solid #8CC540;
    border-radius:15px;
    text-align:left;
    width:318px;
    padding:25px;
}

.error_strings{ font-family:Verdana; font-size:14px; color:#660000; background-color:#ff0;}

.btn_larg  a:link{
    text-decoration:none;
}

.btn_larg {
    text-decoration:none;
    color:#ffffff;
    width:250px;
    text-shadow:2px 2px 2px #333333;
    font-weight:bold;
    text-align:center;
    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    border-radius:5px;
    padding:10px;
    font-size:16px;
    float:right;
    }
.btn_larg a{
    color:#ffffff;
}

.orange{
background: rgb(91,155,1); /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzViOWIwMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM4Y2M2M2YiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
    background: -moz-linear-gradient(top, rgb(91,155,1) 0%, rgb(145,206,39) 51%, rgb(140,198,63) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(91,155,1)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(140,198,63))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* IE10+ */
    background: linear-gradient(to bottom, rgb(91,155,1) 0%,rgb(145,206,39) 51%,rgb(140,198,63) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5b9b01', endColorstr='#8cc63f',GradientType=0 ); /* IE6-8 */
}

.orange:hover{
background: rgb(140,198,63); /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzhjYzYzZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzkxY2UyNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM1YjliMDEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
    background: -moz-linear-gradient(top, rgb(140,198,63) 0%, rgb(145,206,39) 51%, rgb(91,155,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(140,198,63)), color-stop(51%,rgb(145,206,39)), color-stop(100%,rgb(91,155,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom, rgb(140,198,63) 0%,rgb(145,206,39) 51%,rgb(91,155,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8cc63f', endColorstr='#5b9b01',GradientType=0 ); /* IE6-8 */    }

.div_shadow {
    -moz-box-shadow: 1px 1px 5px #9A9C9E;
 -webkit-box-shadow: 1px 1px 5px #9A9C9E;
 box-shadow: 1px 1px 10px #9A9C9E;
}
    /* -------------------------------------------------------------
    11. MESSAGE
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    .message
    {
    background: #CCC url(images/silk/icon_notice.gif) no-repeat 10px 11px;
    font-size: 13px;
    color: #74655f;
    display:block;  
    padding:10px 10px 10px 40px;
    margin-bottom: 1em;
    border: 1px solid #c2beb1;
    position:relative;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    border-radius:6px;
    }
    .message span  
    {   
    display: none !important;
    position: absolute;
    right:7px;
    top:7px;
    font-weight:bold;
    font-size:10px;
    padding:5px;
    cursor:pointer;
    }
    .message-closable span { display: block !important; }
    .message-error 
    {
    background-color: #f6b9b9;
    background-image: url(images/msg/exclamation.png);
    color: #c32727;
    border-color: #e18484;
    }

</style>
<script type="text/javascript">

    function closeMessage(){
        $('.message_disp').hide();
    }

    function validateFormOnSubmit(theForm) {
        var reason = "";
        var vfld = theForm.email_ad.value;
        var fld = theForm.email_ad;
        var tfld = trim(vfld);                        // value of field with whitespace trimmed off
        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
        var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
        
        if (vfld.length == 0) {
            fld.style.background = 'Yellow';
            reason += "You didn't enter an email address.\n";
        } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
            fld.style.background = 'Yellow';
            reason += "Please enter a valid email address.\n";
        } else if (vfld.match(illegalChars)) {
            fld.style.background = 'Yellow';
            reason += "The email address contains illegal characters.\n";
        } else {
            fld.style.background = 'White';
        }
        reason += validateEmpty(theForm.fr_name, "First Name");
        reason += validateEmpty(theForm.lt_name, "Last Name");
        reason += validateEmpty(theForm.com_name, "Company");
        reason += validatePhone(theForm.phone_no);
        reason += validateEmpty(theForm.country_id, "Country");
          
        if (reason != "") {
            alert("The following are required:\n\n" + reason);
            return false;
        }
        
        return true;
    }
    
    function validateEmpty(fld, fld_name) {
        var error = "";
      
        if (fld.value.length == 0) {
            fld.style.background = 'Yellow'; 
            error = fld_name+" is a required field.\n"
        } else {
            fld.style.background = 'White';
        }
        return error;   
    }
    
    function trim(s)
    {
      return s.replace(/^\s+|\s+$/, '');
    } 
    
    function validateEmail(vfld,fld) {
        var error="";
        var tfld = trim(vfld);                        // value of field with whitespace trimmed off
        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
        var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
        
        if (vfld.length == 0) {
            fld.style.background = 'Yellow';
            error = "You didn't enter an email address.\n";
        } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
            fld.style.background = 'Yellow';
            error = "Please enter a valid email address.\n";
        } else if (vfld.match(illegalChars)) {
            fld.style.background = 'Yellow';
            error = "The email address contains illegal characters.\n";
        } else {
            fld.style.background = 'White';
        }
        return error;
    }
    
    function validatePhone(fld) {
        var error = "";
        var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');     
    
       if (fld.value == "") {
            error = "You didn't enter a phone number.\n";
            fld.style.background = 'Yellow';
        } else if (isNaN(stripped)) {
            error = "The phone number contains illegal characters.\n";
            fld.style.background = 'Yellow';
        }
        return error;
    }
</script>

</head>
<body>
<div class="header-lower">
  <nav class="container" role="navigation">
    <h1 class="logo">
      <a href="due-diligence20" class="navbar-brand"><img src="img20/ethixbase-logo.png" /></a>
    </h1>

    <div class="clear"></div>
  </nav>
</div>
    <div style="padding-top:120px;"></div>
    <div class="container">
        <div class="clearfix"></div>
        <div style="float:left;">
        </div>
        <div style="clear: both;"></div>
        <div class="div_shadow" style="padding-top:15px;padding-bottom:50px">
            <h2 style="text-align:left; margin-left:20px;text-align:center;color:#8cc540;"><strong>create your ethiXbase account</strong></h2>
            <div style="width:980px; margin-left:30px; margin-top:20px;">
                    <div style="padding-left:40px;">
                        <div style="margin-top:30px; margin-bottom:20px;">
                            Please complete the form below in order to register an account in ethiXbase. A confirmation message will be sent to your email address after submitting the form.<br />
                        </div>
                        <?php 
                            if($email_exists_err!=''){
                        ?>
                            <div class="message_disp">                            
                                <br />
                                <span class="message message-error" >
                                <img onclick="closeMessage(this)" style="float:right;margin-top:3px;cursor:pointer;" src="<?php echo APP_URL; ?>img20/cross-circle.png" alt="x"  />
                                <?php echo $email_exists_err; ?>
                                </span>
                            </div>  
                        <?php } ?>
                        <div>
                            <form onSubmit="return validateFormOnSubmit(this)" action="" method="post" name="payForm" id="payForm" class="login_form2">
                                <div id="payForm_errorloc" class="error_strings">
                                </div>
                                <div class="frm_rows" id="name_row">
                                    <div class="frm_label" id="lb_fr_name">First Name <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_fr_name"><input class="field_styl" type="text" name="fr_name" id="fr_name" /></div>
                                    <div class="frm_label" id="lb_lt_name">Last Name <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_lt_name"><input class="field_styl" type="text" name="lt_name" id="lt_name" /></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="frm_rows" id="email_row">
                                    <div class="frm_label" id="lb_email_ad">Email Address <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_email_ad"><input class="field_styl" type="text" name="email_ad" id="email_ad" /></div>
                                    <div class="frm_label" id="lb_phone_no">Phone Number <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_phone_no"><input class="field_styl" type="text" name="phone_no" id="phone_no" /></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="frm_rows" id="job_row">
                                    <div class="frm_label" id="lb_jb_title">Job Title  :</div>
                                    <div class="frm_field" id="fld_jb_title"><input class="field_styl" type="text" name="jb_title" id="jb_title" /></div>
                                    <div class="frm_label" id="lb_com_name">Company <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_com_name"><input class="field_styl" type="text" name="com_name" id="com_name" /></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="frm_rows" id="bil_row">
                                    <div class="frm_label" id="lb_count_name">Country <span class="req_sg">*</span> :</div>
                                    <div class="frm_field" id="fld_count_name">
                                        <select class="field_styl" name="country_id" id="country_id" style="width:250px; text-align:left;">
                                            <option value="" selected="selected">---- Select ----</option>
                                        <?php
                                            $sql_dd = "SELECT id, country_name FROM iso_countries ORDER BY country_name ASC";
                                            $res_dd = mysql_query($sql_dd);
                                            while($row_dd=mysql_fetch_assoc($res_dd))
                                            {
                                            ?>
                                                <option value="<?php echo $row_dd['id']; ?>"><?php echo $row_dd['country_name']; ?></option>
                                            <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="frm_label" id="lb_count_name">Industry  :</div>
                                    <div class="frm_field" id="fld_count_name">
                                        <select class="field_styl" name="industry_id" id="industry_id" style="width:250px; text-align:left;">
                                            <option value="" selected="selected">---- Select ----</option>
                                        <?php
                                            $sql_dd = "SELECT id, indus_name FROM industry_dropdown ORDER BY indus_name ASC";
                                            $res_dd = mysql_query($sql_dd);
                                            while($row_dd=mysql_fetch_assoc($res_dd))
                                            {
                                            ?>
                                                <option value="<?php echo $row_dd['id']; ?>"><?php echo $row_dd['indus_name']; ?></option>
                                            <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="frm_rows_sec submi" id="submit">
                                    <input type="hidden" name="pkids" value="<?php echo $pkid; ?>" />
                                    <input  style="float:none; font-size:20px; font-weight: bold; width: 300px; border:none;margin-left:55px;" type="submit" name="on_submit" id="on_submit" value="Submit" class="btn_larg orange" />
                                    <input onClick="window.location.href='<?php echo APP_TURL."index20"; ?>'" type="button" name="cancel" value="Cancel" class="btn_larg orange" style="float:none; font-size:20px; font-weight: bold; width:300px; border:none;margin-left:55px;" />
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                // }
                ?>
                <div class="clear"></div>
            </div>
        </div>                  
    </div>    
      
</body>
</html>