<?php include('header20.php'); ?>

<?php

	require_once 'include20/MysqliDb.php';
	$fdb = new MysqliDb;

?>

<div class="int-row-1 edd-row-1">

	<div class="text-center">
	<p>&nbsp;</p>
	<div class="steps-container hidden-xs hidden-sm">
	<div class="step-dot text-center">
	<?php if ($_REQUEST['formsubmit1']): ?>
	<a href="javascript:history.go(-2)"><img class="step-dot-0" src="img20/icsteps1g.png" /></a>
	<?php else: ?>
	<a href="javascript:history.back()"><img class="step-dot-0" src="img20/icsteps1g.png" /></a>
	<?php endif; ?>
	<p>Search</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<?php if ($_REQUEST['formsubmit']): ?>
	<img class="step-dot-1" src="img20/icsteps3r.png" />
	<?php else: ?>
		<?php if ($_REQUEST['formsubmit1']): ?>
		<a href="javascript:history.back()"><img class="step-dot-0" src="img20/icsteps3g.png" /></a>
		<?php else: ?>
		<img class="step-dot-0" src="img20/icsteps3g.png" />
		<?php endif; ?>
	<?php endif; ?>
	<p>Review Individual Results</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<?php if ($_REQUEST['formsubmit1']): ?>
	<img class="step-dot-1" src="img20/icsteps2r.png" />
	<?php else: ?>
	<img class="step-dot-0" src="img20/icsteps2g.png" />
	<?php endif; ?>
	<p>Review Associated Companies</p>
	</div>
	<div class="step-line">
	&nbsp;
	</div>
	<div class="step-dot text-center">
	<img class="step-dot-0" src="img20/icsteps4g.png" />
	<p>Review Results &amp; Download Report</p>
	</div>
	</div>
	</div>
	
	<?php if ($_REQUEST['formsubmit']): ?>
		<div class="form-section">
			<form class="form-area-1 form-01-1" action="" method="post" name="form3">
				
				<input type="hidden" name="entity_type" value="<?php echo $_REQUEST['entity_type']; ?>">
				<input type="hidden" id="thecountry" name="country" value="<?php echo $_REQUEST['country']; ?>">
				<input type="hidden" id="search" name="search" value="<?php echo $_REQUEST['search']; ?>">
				<input type="hidden" id="searchTemp" name="searchTemp" value="<?php echo $_REQUEST['search']; ?>">
                <input type="hidden" name="is_idd" id="is_idd" value="<?php echo $_REQUEST['is_idd']; ?>">
				<input type="hidden" name="formsubmit1" value="formsubmit1">
				
				<?php
				
				$search = trim($_REQUEST['search']);
				
				$types = 1;
				$res_chk_all = srap_call($search,$types);
				$cnt_arr = count($res_chk_all);
				if(is_array($res_chk_all))
				{
					for ($aa=1; $aa<=$cnt_arr; $aa++)
					{
						foreach($res_chk_all[$aa] as $row_chk_all)
						{
							$is_alrted = 1;
							$final_risk = $risk_cate = "";
							$risk_cate = $row_chk_all['risk_category'];
							$country_arr[] = trim($row_chk_all['country_name']);
							$final_risk = explode(",",$risk_cate);
							
							if(in_array("Sanctions",$final_risk))
							{
								$cnt_sanc++; 
							}
							
							if(in_array("Enforcements",$final_risk))
							{
								$cnt_enf++; 
							}					
						}
					}
				}
				
				$country_arr = array_unique($country_arr);
				$tcountries = count($country_arr);
				
				?>
				
				<div class="container">
					<div class="row">
						<div class="col-xs-12">

							<strong>Your search: <?php echo $search; ?></strong>
							<br><br>
							
							<table>
								<tr>
									<td style="padding-right:25px;">
										
										<p>The below individual has been searched against <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a> with the following result.</p>
										
										<table class="table table-responsive" style="border-bottom:none;border-collapse:collapse;">
											
											<thead>
												<tr class="t-head">
													<td style="border-bottom:none;">Individual Name</td>
													<td style="border-bottom:none;">Results Found</td>
												</tr>
											</thead>

											<tbody>
												<tr>
													<td style="border:1px solid #9a9c9e;">
														
														<?php if ($is_alrted): ?>
														<img id="tginfo" class="img-plus" src="img20/icon-add-small.png">&nbsp;
														<?php endif; ?>
														
														<?php echo $search; ?>
													</td>
													<td style="border:1px solid #9a9c9e;" align="center">
														<img src="img20/SE.png" width="30" style="margin-right:5px;" />
														
														<?php if ($cnt_enf>0 || $cnt_sanc>0): ?>
														<span style='color:#FF0000;'>Alerted</span>
														<?php else: ?>
														Not Alerted
														<?php endif; ?>
														
													</td>
												</tr>
												
												<!-- //////////////////////////////////////////// FROM OSAMA'S CODE //////////////////////////////////////////// -->
												<?php

												if($is_alrted==1)
												{
													for($aa=1;$aa<=$cnt_arr;$aa++)
													{
														foreach($res_chk_all[$aa] as $row_chk_all)
														{
															?>
															
															<tr class="detailrow" style="display:none;">
																<td colspan="2" style="border:1px solid #9a9c9e;">
																
																	<?php if(trim($row_chk_all['source_name'])): ?>
																	<b>Source Name: </b><span><?php echo $row_chk_all['source_name']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['url_site'])): ?>
																	<b>Source URL: </b><span><a href="<?php echo $row_chk_all['url_site']; ?>" target="_blank"><?php echo $row_chk_all['url_site']; ?></a></span><br />
																	<?php endif; ?>

																	<b>Entity Name: </b><span><?php echo $row_chk_all['Entity_Name']; ?></span><br />

																	<?php if(trim($row_chk_all['Entity_Type'])): ?>
																	<b>Entity Type: </b><span><?php echo $row_chk_all['Entity_Type']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['Alt_Name'])): ?>
																	<b>Alt Name: </b><span><?php echo $row_chk_all['Alt_Name']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['Alert'])): ?>
																	<b>Alert: </b><span><?php echo $row_chk_all['Alert']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['Event'])): ?>
																	<b>Event: </b><span><?php echo $row_chk_all['Event']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['Description'])): ?>
																	<b>Description: </b><span><?php echo $row_chk_all['Description']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['Address'])): ?>
																	<b>Address: </b><span><?php echo $row_chk_all['Address']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['city_state_zip'])): ?>
																	<b>City/State/Zip: </b><span><?php echo $row_chk_all['city_state_zip']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['country_name'])): ?>
																	<b>Country: </b><span><?php echo $row_chk_all['country_name']; ?></span><br />
																	<?php endif; ?>

																	<?php if(trim($row_chk_all['risk_category'])): ?>
																	<b>Risk Category: </b><span><?php echo $row_chk_all['risk_category']; ?></span><br />
																	<?php endif; ?>
																
																</td>
															</tr>
															<?php
														}
													}
												}
												?>
												<!-- //////////////////////////////////////////// FROM OSAMA'S CODE //////////////////////////////////////////// -->
												
											</tbody>
										</table>
										
									</td>
									
									<td valign="top">
										<div class="btn-next">
											<button class="btn-submit" style="white-space:nowrap;">Next <span class="glyphicon glyphicon-menu-right"></span></button>
										</div>
									</td>
								</tr>
							</table>
							
							<p>Please click <strong>Next</strong> to review potential Associated Companies.</p>
							
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php endif; ?>
	
	<?php if ($_REQUEST['formsubmit1']): 
		$is_idd = $_REQUEST['is_idd'];
	?>
		<div class="form-section">
				<form class="form-area-1 form-01-1" action="<?php if($is_idd==1){ echo "due-diligenceidd.php"; }else{ echo "due-diligence420.php".(isset($_GET['type']) || isset($_GET['sac']) ? '?':'').(isset($_GET['type']) ? 'type=' . $_GET['type'] : '').(isset($_GET['type']) ? '&':'').(isset($_GET['sac']) ? 'sac=' . $_GET['sac'] : ''); } ?>" method="post" name="form3" onSubmit="return fsubmit();">
				
				<?php
				
				require_once('include20/OcFunctions.php');
				
				$search = trim($_REQUEST['search']);
				$page = $_REQUEST['pageno'] ? (int) $_REQUEST['pageno'] : 1;
				$res1 = OcSearchOfficers($search, '', false, $pgdetails, $page);
				
				$page = (int) $pgdetails['page'];
				$per_page = (int) $pgdetails['per_page'];
				$total_count = (int) $pgdetails['total_count'];
				$total_pages = (int) $pgdetails['total_pages'];
				
				?>
				
				<input type="hidden" name="entity_type" value="<?php echo $_REQUEST['entity_type']; ?>">
				<input type="hidden" id="thecountry" name="country" value="<?php echo $_REQUEST['country']; ?>">
				<input type="hidden" id="search" name="search" value="<?php echo $_REQUEST['search']; ?>">
				<input type="hidden" id="searchTemp" name="searchTemp" value="<?php echo $_REQUEST['search']; ?>">
				<input type="hidden" name="selections[]" value="<?php echo base64_encode($_REQUEST['search']); ?>">
                <input type="hidden" name="is_idd" id="is_idd" value="<?php echo $is_idd; ?>">
				<input type="hidden" name="is_as_comp" value="1">
				<input type="hidden" name="cmp" value="1">
				<input type="hidden" name="formsubmit1" value="formsubmit1">
				
				<div class="container">
					<div class="row">
						<div class="col-xs-12">

							<strong>Associated Companies:</strong>
							<br><br>
							
							<table>
								<tr>
									<td style="padding-right:25px;">
										
										<p>The companies listed below have an associated individual named "<strong><?php echo $search; ?></strong>" listed within that companies online registry records.</p>

										<p>Should you wish to run due diligence on one of these companies in conjunction with "<?php echo $search; ?>" please simply select the company from the list below. If you do not believe any of these companies to be associated with "<?php echo $search; ?>" simply click <strong>Next</strong> to run an Instant Due Diligence report (including <a href="sanctions_enforcement" target="_blank">ethiXbase Sanctions &amp; Enforcements</a>) on the individual entered only.</p>
										
										<?php
										
										//preVar($res1);
										//preVar($_REQUEST);
										
										?>
										
										<table class="table table-responsive" id="tb1" style="border-bottom:none;border-collapse:collapse;">
											
											<thead>
												<tr class="t-head">
													<td style="border-bottom:none;">Company</td>
													<td style="border-bottom:none;">Registered In</td>
													<td style="border-bottom:none;">Role</td>
													<td style="border-bottom:none;width:50px;">Select</td>
												</tr>
											</thead>

											<tbody id="tbody1">
											  <?php if (count($res1)==0): ?>
											  <tr>
								
													<td colspan="3" style="text-align:center !important;"><?php echo "No Records Found"; ?></td></tr>
											  <?php endif; ?>
											  
											  
												<?php foreach($res1 as $r): ?>
												<?php if (!$r['company']['name'] || !$r['company']['company_number']) continue; ?>
												<tr>
													<td style="border:1px solid #9a9c9e;"><?php echo $r['company']['name']; ?></td>
													<td style="border:1px solid #9a9c9e;"><?php echo strtoupper(countryName($r['jurisdiction_code'], true)); ?></td>
													<td style="border:1px solid #9a9c9e;"><?php echo $r['position']; ?></td>
													<td style="border:1px solid #9a9c9e;" align="center"><input type="checkbox" class="cbsel" name="icompany[<?php echo $r['company']['company_number']; ?>]" value="<?php echo $r['company']['jurisdiction_code'].'|'.$r['id']; ?>"></td>
												</tr>
												<?php endforeach; ?>
											
											</tbody>
										</table>
										
										<?php if ($total_pages > 1): ?>
											<div id="plist1" style="margin-top:5px;">
											<?php echo pgHtml($page,$total_pages,false); ?>
											</div>
											<input type="hidden" name="tpage" value="<?php echo $total_pages; ?>">
										<?php endif; ?>
										
									</td>
									
									<td valign="top">
										<div class="btn-next">
											<button class="btn-submit" style="white-space:nowrap;">Next <span class="glyphicon glyphicon-menu-right"></span></button>
										</div>
									</td>
								</tr>
							</table>
							
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php endif; ?>
	
</div>

<script type="text/javascript">

var tgshow = false;

function cbClick()
{
	$(".cbsel").click(function(e)
	{
		chk = $(this).prop('checked');
		$(".cbsel").prop('checked', false);
		$(this).prop('checked', chk);
	});
}

function pgClick()
{
	$('.pagebox').click(function()
	{
		var pgn = $(this).attr('pg');
		$("#pageno").val(pgn);
		pageChange($('#tbody1'),$('#plist1'));
	});
}

$(document).ready(function()
{
	
	/*
	$('#tb1').DataTable
	({
		"paging":		false,
		"searching":	false,
		"ordering":		true,
		"info":			false,
		"autoWidth":	false,
		"oLanguage":	{"sEmptyTable":"No record(s) found."},
		"columns": [{"orderData": [0]},{"orderable": false},{"orderable": false},{"orderable": false}]
	});
	*/
	
	$("#tginfo").click(function()
	{
		tgshow = !tgshow;
		if (tgshow)
		{
			$(".detailrow").show();
			$("#tginfo").attr("src", "img20/icon-minus-small.png");
		}
		else
		{
			$(".detailrow").hide();
			$("#tginfo").attr("src", "img20/icon-add-small.png");
		}
	});
	
	pgClick();
	cbClick();
});

function pageChange(e,p)
{
	/*
	alert($(document.forms['form3']).serialize());
	return false;
	*/
	
	$(".cbsel").unbind("click");
	$('.pagebox').unbind("click");
	$(e).fadeTo(0,0.1);
	$(p).fadeTo(0,0.1);
	
	$.ajax({
		method: "POST",
		url: "include20/OcFunctions.php",
		data: $(document.forms['form3']).serialize()+'&pageChange=1'
	})
	.done(function(msg) {
		//alert(msg);
		$(e).html(msg);
		$(e).fadeTo(0,1);
		cbClick();
	});
	
	$.ajax({
		method: "POST",
		url: "include20/OcFunctions.php",
		data: $(document.forms['form3']).serialize()+'&pageNumChange=1'
	})
	.done(function(msg) {
		//alert(msg);
		$(p).html(msg);
		$(p).fadeTo(0,1);
		pgClick();
	});
	
	return false;
	
	document.forms['form3'].action = '';
	document.forms['form3'].submit();
}

function fsubmit()
{
	if ($(".cbsel").length > 0)
	{
		if ($(".cbsel:checked").length != 1)
		{
			if (!confirm('Are you sure want to proceed without selecting any company?')) return false;
		}
	}
	return true;
}

</script>

<?php include('footer20.php'); ?>
