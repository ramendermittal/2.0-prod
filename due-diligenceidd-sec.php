<?php
  include('header20.php');
  require_once 'due-diligence320.php';
  @include_once('include20/iddLiteFunc/countryName.php');
  @include_once('include20/iddLiteFunc/notes.php');
  @include_once('include20/iddLiteFunc/num2words.php');
  $nsrch_id = $_REQUEST['srch_id'];
  $is_moni = $_REQUEST['is_moni'];
  if($is_moni==1)
  { $tps = 5; }
  else
  { $tps = 3; $is_moni=0; }
  //echo $srch_id;
  $cnt_sanc = $cnt_enf = $cnt_sanc_asso = $cnt_enf_asso = $is_alrted1 = $is_alrted = $is_idd = 0;
  $idd_color = 1;
  $country_arr = array();
  $sql_sel = "SELECT * FROM srch_tpi st, search20_tpis sts WHERE sts.id=st.srch20_id AND st.id=$nsrch_id";
  //echo $sql_sel;
  $res_sel = mysql_query($sql_sel,$con1);
  $row_sel = mysql_fetch_assoc($res_sel);
  $comp_id = $row_sel['comp_id'];
  $tpi_id = $row_sel['tpi_id'];
  $srch_id = $row_sel['srch20_id'];
  $entity_type = $row_sel['entity_type'];
  $countryos = $row_sel['country'];
  $country = "";
  //echo $row_sel['srch_txt'];
  $srch_txts = htmlspecialchars(trim($row_sel['srch_txt']), ENT_QUOTES);
  $srch_txt = trim($row_sel['srch_txt']);
  $srch_keyword = trim($row_sel['srch_keyword']);
  if($entity_type=='company')
  {
	  $e_type = "O";
 	  $sql_getOCInfo = "SELECT `name`
					, company_number
					, current_status
					, jurisdiction_code
					, incorporation_date
					, dissolution_date
					, company_type
					, created_at
					, updated_at
					, alternative_names
					, previous_names
					, registered_address
					, registry_url
					, source_publisher
					, source_url
					, source_date
					, agent_name
					, agent_address
					, officers
					, filings
					FROM oc_orginfo
					WHERE srch_id = '{$srch_id}'";
  }
  elseif($entity_type=='individual')
  {
	  $e_type = "P";
	  $sql_getOCInfo = "SELECT fullname, `position`, current_status, occupation, address, nationality 
	  FROM oc_offinfo WHERE srch_id='{$srch_id}'";
  }
	$res_getOCInfo = mysql_query($sql_getOCInfo,$con1);
	$row_getOCInfo = mysql_fetch_assoc($res_getOCInfo);
	
	$is_alrted = 0;
  	//echo $srch_txt;
	if($srch_txt != '')
	{
		$sql_gcor = "SELECT * FROM srch_tpi_indivi WHERE srch_id=$nsrch_id AND comp_id=$comp_id AND tpi_id=$tpi_id ORDER BY id DESC LIMIT 1";
		//echo $sql_gcor;
		$res_gcor = mysql_query($sql_gcor);
		$row_gcor = mysql_fetch_assoc($res_gcor);
		$cor_id = $row_gcor['cor_id'];	
		$grid_id = $row_gcor['grid_id'];	
		
		if($is_moni==1)
		{
			$sql_galrts = "SELECT id
						FROM srch_tpi_rdc_sup 
						WHERE comp_id=$comp_id AND tpi_id=$tpi_id AND grid_id=$grid_id";
		}
		else
		{
			$sql_galrts = "SELECT id
						FROM srch_tpi_rdc_indivi_alrt 
						WHERE comp_id=$comp_id AND tpi_id=$tpi_id AND srch_id=$nsrch_id AND cor_id=$cor_id";
		}
		//echo $sql_galrts;
		$res_galrts = mysql_query($sql_galrts);
		if(mysql_num_rows($res_galrts)>0)
		{
			$is_alrted = 1;
		}
		else
		{
			$is_alrted = 0;
		}
	}
	$idd_color10 = 1;
	$jk = $ij = 0;
	$sql_sel1 = "SELECT * FROM srch_tpi_asso WHERE srch_id=$nsrch_id AND tpi_id=$tpi_id AND comp_id=$comp_id";
	$res_sel1 = mysql_query($sql_sel1);
	
	if(mysql_num_rows($res_sel1))
	{
		while($row_sel1 = mysql_fetch_assoc($res_sel1))
		{
			$tcntos10 = 0;
			$asso_id = $row_sel1['id'];
			$srch_txt_asso = trim($row_sel1['asso_name']);
			$name_org10 = str_replace(" & "," &#38; ",$srch_txt_asso);
			$name_org10 = str_replace("&","&#38;",$name_org10);
			$e_type10 = "P";
			
		}
	}
?>
<style>
	.table-inner {
		border-collapse: collapse;
		border-spacing: 0px;
		width: 100%;
		height: 100%;
		margin: 0px;
		padding: 0px;
	}
	.table-inner tr:first-child td {
		background:#FFF !important;
		color:#000 !important;
		font-size: 14px !important;
		font-weight: normal !important;
		vertical-align: middle !important;
		text-align:left !important;
	}
	.table-inner tr:first-child:hover td {
		background:#FFF !important;
		color:#000 !important;
		font-size: 14px !important;
		font-weight: normal !important;
		vertical-align: middle !important;
		text-align:left !important;
	}
	.td-word-wraps {
		  overflow-wrap: break-word;
		  word-wrap: break-word;
		  -ms-word-break: break-all;
		  word-break: break-all;
		  -ms-hyphens: auto;
		  -moz-hyphens: auto;
		  -webkit-hyphens: auto;
		  hyphens: auto;
		}
		
</style>
<div class="int-row-1 edd-row-1">
  <div class="container">
    <div class="col-xs-12">
      <p class="dd4-label td-word-wraps"><strong>Review Results & Download Report</strong></p>
      <p class="dd4-label td-word-wraps" style="width:1000px;">Name Searched: <b><?php echo $srch_keyword; ?></b>
      <?php
	  if(($row_getOCInfo['fullname']=="" && $row_getOCInfo['name']!="") || $countryos!='')
	  {
	  ?>
          <br />
          Jurisdiction: <b><?php if($row_getOCInfo['jurisdiction_code']!=""){ echo getCountryName($row_getOCInfo['jurisdiction_code']); }elseif($countryos!=''){ echo $countryos; } ?></b>
      <?php
	  }
	  ?>
      </p>
      
      <div class="form-03">
        <table class="table-02">
          <thead>
            <tr>
            <?php
			if($entity_type=='company')
			{
			?>
            	<td style="width:250px;">Subject Name</td>
            	<td style="width:150px;">Country</td>
            	<td style="width:150px;">
                	<a href="sanctions_enforcement" target="_blank" style="text-decoration:none; color:#FFF;">IDD<sup>+</sup> Report Results</a>
                </td>
                <td style="width:150px;">Download & Save Instant Due Diligence (IDD) Report</td>
                <!--<td style="width:60px;">Order<br />IDD+</td>-->
                <td style="width:60px;">Order<br />EDD</td>
			<?php
			}
			elseif($entity_type=='individual')
			{
			?>
            	<td style="width:350px;">Subject Name</td>
            	<td style="width:200px;"><a href="sanctions_enforcement" target="_blank" style="text-decoration:none; color:#FFF;">IDD<sup>+</sup> Report Results</a></td>
                <td style="width:150px;">Download & Save Instant Due Diligence (IDD) Report</td>
                <!--<td style="width:60px;">Order<br />IDD+</td>-->
                <td style="width:60px;">Order<br />EDD</td>
            <?php	
			}
			?>
            </tr>
          	</thead>
          	<tr>
            <td class="td-word-wraps">
            	<?php if($is_alrted==1 || $row_getOCInfo['name']!="" || $row_getOCInfo['fullname']!=""){ ?><img src="img20/icon-add-small.png" class="img-plus" id="plus1" onClick="get_alerts('include1','plus1');" /><?php } echo $srch_txt; ?>
            </td>
            <?php
			if($entity_type=='company')
			{
			?>
            	<td style="text-align:center !important;"><?php if($tcountries>1){ echo "Multiple"; }elseif($row_getOCInfo['jurisdiction_code']!=""){  echo getCountryName($row_getOCInfo['jurisdiction_code']); }elseif($country_arr[0]!=''){ echo $country_arr[0]; }elseif($countryos!=''){ echo $countryos; } ?></td>
            <?php
			}
			?>
            <td style="text-align:center !important;">
            	<?php if($is_alrted==1){ ?>
            	<img src="img20/TS_0004_Ellipse-1.png" width="30px" height="30px" />
            	<?php }else{ ?>
                <img src="img20/TS_0003_Ellipse-1-copy.png" width="30px" height="30px" />
            	<?php } ?>
            </td>
            <td style="text-align:center !important;">
            	<div style="float:left; margin-left:20px;">
            		<a data-toggle="tooltip" title="Download and Save Third Party" href="#" onClick='printRpt(<?php echo $nsrch_id; ?>,<?php echo $_SESSION['company_id']; ?>,<?php echo $cor_id; ?>,<?php echo $is_moni; ?>,<?php echo $tps; ?>);' data-placement="bottom"><img src="img20/Download_icon.png" width="30" /></a>
            	</div>
            </td>
            <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Order EDD Report (click to learn more)" style="margin-left:20px; text-align:center !important;">
            	<a href="<?php echo APP_URL.'order_edd_form?srch_id='.$srch_id; ?>" target="_blank"><img src="img20/EDD_icon.png" width="30" /></a>
            </td>
          </tr>
          <?php
		  	if($entity_type=='company')
			{
		  		if($res_getOCInfo!='' && $row_getOCInfo['name']!="")
				{
					?>    
                    <tr id="include1" style="display:none;">
                        <td colspan="5" class="indent">
                            <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['name']); ?></span><br />
                            <?php if($row_getOCInfo['company_number']!=""){ ?><b>Company Number: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['company_number']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['current_status']!=""){ ?><b>Status: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['current_status']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['company_type']!=""){ ?><b>Company Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['company_type']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['registered_address']!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['registered_address']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['jurisdiction_code']!=""){ ?><b>Jurisdiction: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo getCountryName($row_getOCInfo['jurisdiction_code']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['incorporation_date']!=""){ ?><b>Incorporation Date: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['incorporation_date']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['dissolution_date']!=""){ ?><b>Dissolution Date: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['dissolution_date']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['registry_url']!=""){ ?><b>Registry URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_getOCInfo['registry_url']; ?>" target="_blank"><?php echo $row_getOCInfo['registry_url']; ?></a></span><br /><?php } ?>
                            <?php if($row_getOCInfo['previous_names']!=""){ ?><b>Previous Names: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['previous_names']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['filings']!=""){ ?><b>Filings: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['filings']); ?></span><br /><?php } ?>
                        </td>
                    </tr>
					<?php
				}
			}
			elseif($entity_type=='individual')
			{
				if($res_getOCInfo!='' && $row_getOCInfo['fullname']!="")
				{
					?>    
                    <tr id="include1" style="display:none;">
                        <td colspan="4" class="indent">
                            <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['fullname']); ?></span><br />
                            <?php if($row_getOCInfo['position']!=""){ ?><b>Position: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo ucwords($row_getOCInfo['position']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['current_status']!=""){ ?><b>Status: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['current_status']; ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['occupation']!=""){ ?><b>Occupation: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo ucwords($row_getOCInfo['occupation']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['address']!=""){ ?><b>Address: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $ese->decrypt($row_getOCInfo['address']); ?></span><br /><?php } ?>
                            <?php if($row_getOCInfo['nationality']!=""){ ?><b>Nationality: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_getOCInfo['nationality']; ?></span><br /><?php } ?>
                        </td>
                    </tr>
					<?php
				}
			}
			if($is_alrted==1)
			{
				if($is_moni==1)
				{
					$sql_galrts = "SELECT id, entity_name as 'ent_name', rdc_url as 'ent_src_url', alert_alias as 'ent_alias', alert_events as 'ent_evnt', alert_add as 'ent_address', alert_note, alert_att as 'ent_attri'
								FROM srch_tpi_rdc_sup 
								WHERE comp_id=$comp_id AND tpi_id=$tpi_id AND grid_id=$grid_id";
				}
				else
				{
					$sql_galrts = "SELECT id, ent_name, ent_src_name, ent_src_url, ent_tp, ent_alias, ent_posi, ent_evnt, ent_address, ent_sources, ent_attri
								FROM srch_tpi_rdc_indivi_alrt 
								WHERE comp_id=$comp_id AND tpi_id=$tpi_id AND srch_id=$nsrch_id AND cor_id=$cor_id";
				}
				$res_galrts = mysql_query($sql_galrts);
				if(mysql_num_rows($res_galrts)>0)
				{
					while($row_galrts = mysql_fetch_assoc($res_galrts))
					{
					?>
                    	<tr id="include1" style="display:none;">
							<?php
                            if($entity_type=='company')
                            {
                            ?>
                                <td colspan="5" class="indent">
                            <?php
                            }
                            elseif($entity_type=='individual')
                            {
                            ?>
                                <td colspan="4" class="indent">
                            <?php	
                            }
                            if(!isset($_REQUEST['is_moni']))
							{
								if(trim($row_galrts['ent_src_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrts['ent_src_name']; ?></span><br /><?php } 
							}
								if(trim($row_galrts['ent_src_url'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_galrts['ent_src_url']; ?>" target="_blank"><?php echo $row_galrts['ent_src_url']; ?></a></span><br /><?php } ?>
                                <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrts['ent_name']; ?></span><br />
                                <?php 
							if(!isset($_REQUEST['is_moni']))
							{	
								if(trim($row_galrts['ent_tp'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrts['ent_tp']; ?></span><br /><?php } 
							}
                                if(trim($row_galrts['ent_alias'])!="")
                                { 
                                ?>
                                    <b style="font-size:16px;">Aliases: </b><br />
                                <?php
                                    $aliases = explode(",",$row_galrts['ent_alias']);
                                    $sr_cnt = 1;
                                    foreach($aliases as $alias)
                                    {
                                        echo "<b>Alias $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                    ?>
                                        <span style="max-width:200px;word-wrap: break-word;"><?php echo $alias; ?></span><br />
                                    <?php 
                                        echo "<br /></div>";
                                        $sr_cnt++;
                                    }
                                }
								
							if(!isset($_REQUEST['is_moni']))
							{
                                if(trim($row_galrts['ent_posi'])!="")
                                { 
                                ?>
                                    <b style="font-size:16px;">Positions: </b><br />
                                <?php
                                    $positions = explode(",",$row_galrts['ent_posi']);
                                    $sr_cnt = 1;
                                    foreach($positions as $position)
                                    {
                                        echo "<b>Position $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                    ?>
                                        <span style="max-width:200px;word-wrap: break-word;"><?php echo $position; ?></span><br />
                                    <?php
                                        echo "<br /></div>";
                                        $sr_cnt++; 
                                    }
                                }
							}
							
                                $events = json_decode($row_galrts['ent_evnt'],true);
                                if($events[0]['category']!='')
                                {
                                    $sr_cnt = 1;
                                    if(trim($events[0]['category'])!=""){ ?><?php } ?>
                                    <?php 
                                    foreach($events as $event)
                                    {
                                        echo "<b>Risk Alert Information $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                        if(trim($event['category'])!=""){ ?><b>Category: </b><span style="max-width:200px;word-wrap: break-word;">
										<?php 
											$rdc_event = $event['category']; 
											$sql_shrt = "SELECT * FROM rdc_events WHERE short_event='$rdc_event'";
											//echo $sql_shrt."<br />";
											$res_shrt = mysql_query($sql_shrt);
											$row_shrt = mysql_fetch_assoc($res_shrt);
											if($row_shrt['long_event']!='')
											echo $row_shrt['long_event'];
											else
											echo $event['category'];
											//echo $event['category']; 
										?>
                                        </span><br /><?php }
                                        if(trim($event['subCategory'])!=""){ ?><b>Sub Category: </b><span style="max-width:200px;word-wrap: break-word;">
										<?php
											$rdc_event = $event['subCategory']; 
											$sql_shrt = "SELECT * FROM rdc_subevents WHERE shrt_sub_evnt='$rdc_event'";
											//echo $sql_shrt."<br />";
											$res_shrt = mysql_query($sql_shrt);
											$row_shrt = mysql_fetch_assoc($res_shrt);
											if($row_shrt['lng_sub_evnt']!='')
											echo $row_shrt['lng_sub_evnt'];
											else
											echo $event['subCategory']; 
											//echo $event['subCategory']; 
										?>
                                        </span><br /><?php }
                                        if(trim($event['description'])!=""){ ?><b>Description: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['description']; ?></span><br /><?php }
                                        if(trim($event['date'])!=""){ ?><b>Date: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['date']; ?></span><br /><?php }
                                        if(trim($event['source name'])!=""){ ?><b>Source Name: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source name']; ?></span><br /><?php }
                                        if(trim($event['source url'])!=""){ ?><b>Source URL: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source url']; ?></span><br /><?php }
                                        if(trim($event['source date'])!=""){ ?><b>Source Date: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source date']; ?></span><br /><?php }
                                        echo "<br /></div>";
                                        $sr_cnt++;
                                    }
                                }
								
                                if(trim($row_galrts['ent_address'])!="")
                                { 
                                ?>
                                    <b style="font-size:16px;">Addresses: </b><br />
                                <?php
                                    $addresses = explode(",",$row_galrts['ent_address']);
                                    $sr_cnt = 1;
                                    foreach($addresses as $address)
                                    {
                                        echo "<b>Address $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                    ?>
                                        <span style="max-width:200px;word-wrap: break-word;"><?php echo $address; ?></span><br />
                                    <?php 
                                        echo "</div>";
                                        $sr_cnt++;
                                    }
                                }
							
							if(!isset($_REQUEST['is_moni']))
							{	
                                $sources = json_decode($row_galrts['ent_sources'],true);
                                if($sources[0]['name']!='')
                                {
                                    $sr_cnt = 1;
                                    if(trim($sources[0]['name'])!=""){ ?><b style="font-size:16px;">Sources Information: </b><br /><?php } ?>
                                    <?php 
                                    foreach($sources as $source)
                                    {
                                        echo "<b>Source $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                        if(trim($source['name'])!=""){ ?><b>Name: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $source['name']; ?></span><br /><?php }
                                        if(trim($source['url'])!=""){ ?><b>URL: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $source['url']; ?></span><br /><?php }
                                        echo "<br /></div>";
                                        $sr_cnt++;
                                    }
                                }
							}
								
                                $attributes = json_decode($row_galrts['ent_attri'],true);
                                if($attributes[0]['code']!='' && $attributes[0]['code']!='RID')
                                {
                                    $sr_cnt = 1;
                                    if(trim($attributes[0]['code'])!=""){ ?><b>Additional Information: </b><br /><?php } ?>
                                    <div style="padding-left:20px;">
                                        <?php 
                                        foreach($attributes as $attribute)
                                        {
                                            if($attribute['code']!="RID")
                                            {
                                                if(trim($attribute['code'])!=""){ ?><span style="max-width:200px;word-wrap: break-word;"><?php echo "<b>".$attribute['code'].":</b> ".$attribute['value']; ?></span><br /><?php }
                                                echo "<br />";
                                                $sr_cnt++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
								
							if(isset($_REQUEST['is_moni']))
							{
								if(trim($row_galrts['alert_note'])!=""){ ?><b>Note: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrts['alert_note']; ?></span><br />
                                <?php
								}
							}
                                ?>
                            </td>
                        </tr>
                    <?php
					}
				}
			}
			$jk = $ij = 0;
			$sql_sel10 = "SELECT * FROM srch_tpi_asso WHERE srch_id=$nsrch_id AND tpi_id=$tpi_id AND comp_id=$comp_id";
			$res_sel10 = mysql_query($sql_sel10);
		  	while($row_sel10 = mysql_fetch_assoc($res_sel10))
		  	{
				$asso_id = $row_sel10['id'];
				$fullassoname = trim($row_sel10['asso_name']);
				$sql_galrt10 = "SELECT * FROM srch_asso_rdc_indivi_alrt 
								WHERE comp_id=$comp_id AND tpi_id=$tpi_id AND srch_id=$nsrch_id AND asso_id=$asso_id GROUP BY sys_id";
				$res_galrt10 = mysql_query($sql_galrt10);
				if(mysql_num_rows($res_galrt10)>0)
				{
					$is_alrted1 = 1;
				}
				else
				{
					$is_alrted1 = 0;
				}
			?>
            	<tr>
                    <td class="td-word-wraps">
                    	<?php if($is_alrted1==1 || $row_sel10['asso_posi']!=''){ ?><img src="img20/icon-add-small.png" class="img-plus" id="plusa<?php echo $ij.$jk; ?>" onClick="get_alerts_asso('includea<?php echo $ij; ?>','plusa<?php echo $ij.$jk; ?>');"/><?php } echo $fullassoname; ?>
                    </td>
                    <?php
					if($entity_type=='company')
					{
					?>
						<td style="text-align:center !important;"></td>
					<?php
					}
					elseif($entity_type=='individual')
					{
					?>
					<?php	
					}
					?>
                    <td style="text-align:center !important;">
                    <?php if($is_alrted1==1){ ?>
                        <img src="img20/TS_0004_Ellipse-1.png" width="30px" height="30px" />
                        <?php }else{ ?>
                        <img src="img20/TS_0003_Ellipse-1-copy.png" width="30px" height="30px" />
                        <?php } ?>
                    </td>
                    <td style="text-align:center !important;"></td>
                    <td data-original-title="Premium Service" data-container="body" data-toggle="tooltip" data-placement="bottom" title="Premium Service" style="margin-left:20px; text-align:center !important;"></td>
				</tr>
                <?php if($row_sel10['asso_posi']!=''){ ?>
                    <tr id="includea<?php echo $ij; ?>" style="display:none;">
                        <td colspan="5" class="indent">
                            Subject Type: <?php echo ucwords($row_sel10['asso_posi']); ?>
                        </td>
                    </tr>
				<?php } ?>
            <?php
				if(mysql_num_rows($res_galrt10)>0)
				{
					while($row_galrt10 = mysql_fetch_assoc($res_galrt10))
					{
					?>
                    	<tr id="includea<?php echo $ij; ?>" style="display:none;">
                        	<?php
                            if($entity_type=='company')
                            {
                            ?>
                                <td colspan="5" class="indent">
                            <?php
                            }
                            elseif($entity_type=='individual')
                            {
                            ?>
                                <td colspan="4" class="indent">
                            <?php	
                            }
								
								if(!isset($_REQUEST['is_moni']))
								{
									if(trim($row_galrt10['ent_src_name'])!=""){ ?><b>Source Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrt10['ent_src_name']; ?></span><br /><?php } 
								}
								
								if(trim($row_galrt10['ent_src_url'])!=""){ ?><b>Source URL: </b><span style="max-width:220px;word-wrap: break-word;"><a href="<?php echo $row_galrt10['ent_src_url']; ?>" target="_blank"><?php echo $row_galrt10['ent_src_url']; ?></a></span><br /><?php } ?>
                                <b>Entity Name: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrt10['ent_name']; ?></span><br />
                                <?php if(trim($row_galrt10['ent_tp'])!=""){ ?><b>Entity Type: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrt10['ent_tp']; ?></span><br /><?php } ?>
                                <?php 
								
                                if(trim($row_galrt10['ent_alias'])!="")
                                { 
                                ?>
                                    <b style="font-size:16px;">Aliases: </b><br />
                                	<?php
                                    $aliases = explode(",",$row_galrt10['ent_alias']);
                                    $sr_cnt = 1;
                                    foreach($aliases as $alias)
                                    {
                                        echo "<b>Alias $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                    ?>
                                        <span style="max-width:200px;word-wrap: break-word;"><?php echo $alias; ?></span><br />
                                    <?php 
                                        echo "<br /></div>";
                                        $sr_cnt++;
                                    }
                                }
								
								if(!isset($_REQUEST['is_moni']))
								{
									if(trim($row_galrt10['ent_posi'])!="")
									{ 
									?>
										<b style="font-size:16px;">Positions: </b><br />
									<?php
										$positions = explode(",",$row_galrt10['ent_posi']);
										$sr_cnt = 1;
										foreach($positions as $position)
										{
											echo "<b>Position $sr_cnt: </b><br /><div style='padding-left:20px;'>";
										?>
											<span style="max-width:200px;word-wrap: break-word;"><?php echo $position; ?></span><br />
										<?php
											echo "<br /></div>";
											$sr_cnt++; 
										}
									}
								}
								
                                $events = json_decode($row_galrt10['ent_evnt'],true);
                                if($events[0]['category']!='')
                                {
                                    $sr_cnt = 1;
                                    if(trim($events[0]['category'])!=""){ ?><?php } ?>
                                    <?php 
                                    foreach($events as $event)
                                    {
                                        echo "<b>Risk Alert Information $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                        if(trim($event['category'])!=""){ ?><b>Category: </b><span style="max-width:200px;word-wrap: break-word;">
										<?php 
											$rdc_event = $event['category']; 
											$sql_shrt = "SELECT * FROM rdc_events WHERE short_event='$rdc_event'";
											//echo $sql_shrt."<br />";
											$res_shrt = mysql_query($sql_shrt);
											$row_shrt = mysql_fetch_assoc($res_shrt);
											if($row_shrt['long_event']!='')
											echo $row_shrt['long_event'];
											else
											echo $event['category'];
											//echo $event['category']; 
										?>
                                        </span><br /><?php }
                                        if(trim($event['subCategory'])!=""){ ?><b>Sub Category: </b><span style="max-width:200px;word-wrap: break-word;">
										<?php
											$rdc_event = $event['subCategory']; 
											$sql_shrt = "SELECT * FROM rdc_subevents WHERE shrt_sub_evnt='$rdc_event'";
											//echo $sql_shrt."<br />";
											$res_shrt = mysql_query($sql_shrt);
											$row_shrt = mysql_fetch_assoc($res_shrt);
											if($row_shrt['lng_sub_evnt']!='')
											echo $row_shrt['lng_sub_evnt'];
											else
											echo $event['subCategory']; 
											//echo $event['subCategory']; 
										?>
                                        </span><br /><?php }
                                        if(trim($event['description'])!=""){ ?><b>Description: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['description']; ?></span><br /><?php }
                                        if(trim($event['date'])!=""){ ?><b>Date: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['date']; ?></span><br /><?php }
                                        if(trim($event['source name'])!=""){ ?><b>Source Name: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source name']; ?></span><br /><?php }
                                        if(trim($event['source url'])!=""){ ?><b>Source URL: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source url']; ?></span><br /><?php }
                                        if(trim($event['source date'])!=""){ ?><b>Source Date: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $event['source date']; ?></span><br /><?php }
                                        echo "<br /></div>";
                                        $sr_cnt++;
                                    }
                                }
								
                                if(trim($row_galrt10['ent_address'])!="")
                                { 
                                ?>
                                    <b style="font-size:16px;">Addresses: </b><br />
                                <?php
                                    $addresses = explode(",",$row_galrt10['ent_address']);
                                    $sr_cnt = 1;
                                    foreach($addresses as $address)
                                    {
                                        echo "<b>Address $sr_cnt: </b><br /><div style='padding-left:20px;'>";
                                    ?>
                                        <span style="max-width:200px;word-wrap: break-word;"><?php echo $address; ?></span><br />
                                    <?php 
                                        echo "</div>";
                                        $sr_cnt++;
                                    }
                                }
								
								if(!isset($_REQUEST['is_moni']))
								{
									$sources = json_decode($row_galrt10['ent_sources'],true);
									if($sources[0]['name']!='')
									{
										$sr_cnt = 1;
										if(trim($sources[0]['name'])!=""){ ?><b style="font-size:16px;">Sources Information: </b><br /><?php } ?>
										<?php 
										foreach($sources as $source)
										{
											echo "<b>Source $sr_cnt: </b><br /><div style='padding-left:20px;'>";
											if(trim($source['name'])!=""){ ?><b>Name: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $source['name']; ?></span><br /><?php }
											if(trim($source['url'])!=""){ ?><b>URL: </b><span style="max-width:200px;word-wrap: break-word;"><?php echo $source['url']; ?></span><br /><?php }
											echo "<br /></div>";
											$sr_cnt++;
										}
									}
								}
								
								if(isset($_REQUEST['is_moni']))
								{
									if(trim($row_galrts['alert_note'])!=""){ ?><b>Note: </b><span style="max-width:220px;word-wrap: break-word;"><?php echo $row_galrts['alert_note']; ?></span><br />
									<?php
									}
								}
								
                                $attributes = json_decode($row_galrt10['ent_attri'],true);
                                if($attributes[0]['code']!='' && $attributes[0]['code']!='RID')
                                {
                                    $sr_cnt = 1;
                                    if(trim($attributes[0]['code'])!=""){ ?><b>Additional Information: </b><br /><?php } ?>
                                    <div style="padding-left:20px;">
                                        <?php 
                                        foreach($attributes as $attribute)
                                        {
                                            if($attribute['code']!="RID")
                                            {
                                                if(trim($attribute['code'])!=""){ ?><span style="max-width:200px;word-wrap: break-word;"><?php echo "<b>".$attribute['code'].":</b> ".$attribute['value']; ?></span><br /><?php }
                                                echo "<br />";
                                                $sr_cnt++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
					}
				}
				$jk++;
				$ij++;
			}
		  ?>
        </table>
        <ul class="bottom-menu">
		  <li><a data-toggle="tooltip" title="MIS" href="mis_graphs" data-placement="bottom" class="animated pulse"><img src="img20/Pie_maron.png" style="width:30px; height:30px !important; margin-left:5px;" /></a></li>
          <li><a data-toggle="tooltip" title="Save and Download" href="#" onClick='printRpt(<?php echo $nsrch_id; ?>,<?php echo $_SESSION['company_id']; ?>,<?php echo $cor_id; ?>,<?php echo $is_moni; ?>,<?php echo $tps; ?>);' data-placement="bottom" class="animated pulse"><img src="img20/icon-header2-analytics.png" /></a></li>
          <li><a data-toggle="tooltip" title="Manage Third Parties" href="#" data-placement="bottom" class="animated pulse save"><img src="img20/icon-header2-bars.png" /></a></li>
          <li><a data-toggle="tooltip" title="Order EDD Report" href="<?php echo APP_URL.'order_edd_form?srch_id='.$srch_id; ?>" data-placement="bottom"><img src="img20/icon-header2-edd.png" /></a></li>
       </ul>
      </div>
    </div>
  </div>
</div>
<div class="overlay"></div>
<div class="modal-confirm">
  <p class="message">Would you like to save your third party before you leave?</p>
  <div class="action">
     <a href="#" class="btn btn-default btn-yes" role="button">Yes</a>
     <a href="#" class="btn btn-default btn-no" role="button">No</a>
  </div>
</div>

<script language="javascript">
function get_alerts(divname,imageid)
{ 
	if(document.getElementById(divname).style.display=='none')
	{
		divs=document.getElementsByTagName('tr'); 
		for (var i=0;i<divs.length;i++){ 
			if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
				divs[i].style.display=""; 
			} 
		} 
		document.getElementById(imageid).src="img20/icon-minus-small.png";
		document.getElementById(divname).style.display='';
	}
	else
	{
		divs=document.getElementsByTagName('tr'); 
		for (var i=0;i<divs.length;i++){ 
			if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
				divs[i].style.display="none"; 
			} 
		} 
		document.getElementById(imageid).src="img20/icon-add-small.png";
		document.getElementById(divname).style.display='none';
	}
}

function get_alerts_asso(divname,imageid)
{ 
	if(document.getElementById(divname).style.display=='none')
	{
		divs=document.getElementsByTagName('tr'); 
		for (var i=0;i<divs.length;i++){ 
			if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
				divs[i].style.display=""; 
			} 
		} 
		document.getElementById(imageid).src="img20/icon-minus-small.png";
		document.getElementById(divname).style.display='';
	}
	else
	{
		divs=document.getElementsByTagName('tr'); 
		for (var i=0;i<divs.length;i++){ 
			if (divs[i].id.indexOf(divname)==0){//only if 'image' is at the beginning of the id 
				divs[i].style.display="none"; 
			} 
		} 
		document.getElementById(imageid).src="img20/icon-add-small.png";
		document.getElementById(divname).style.display='none';
	}
}

function save_alerts(srch_id)
{ 
	var comp_id = <?php echo $_SESSION['company_id']; ?>;
	if(window.XMLHttpRequest){
		xmlHttp=new XMLHttpRequest();
	}
	else{
		xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url="save_tpis";
	url=url+"?srch_id="+srch_id+"&comp_id="+comp_id;
	xmlHttp.onreadystatechange=function()
	{ 
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
		{
			alert('Saved!');
			window.location = "<?php echo APP_URL."third-parties220"; ?>";
			/*var comp_id = '<?php //echo $_SESSION['company_id']; ?>';
			var names = '<?php //echo $srch_txt; ?>';
			printRpt(srch_id,names,comp_id);
			document.getElementById(divname).innerHTML=xmlHttp.responseText
			document.getElementById(divname).style.display='';*/
		}
	}
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}

function printRpt(srch_id,comp_id,cor_id,is_moni,tps)
{
    var URL = "free20_rpt_gen?srch_id="+srch_id+"&comp_id="+comp_id+"&cor_id="+cor_id+"&is_moni="+is_moni+"&tp="+tps;
    var W = window.open(URL);
}

function printRpt1(comp_id)
{
	var srch_id = '<?php echo $srch_id; ?>';
	var names = '<?php echo $srch_txts; ?>';
	printRpt(srch_id,comp_id);
}
</script>
<?php
  include('footer20.php');
?>